<?php
require 'cio.php';

# Facebook Pixel URL
$fbURL = "https://www.facebook.com/tr?id=" . $config[TrackingTools][FacebookPixel] . "&ev=PageView&noscript=1";

# Google Maps URL
$googleURL = "https://maps.googleapis.com/maps/api/js?key=" . $config[TrackingTools][GoogleAPIKey] . "&libraries=places";

#Google Analytics Key
$googleAnalyticsURL = "https://www.googletagmanager.com/gtag/js?id=" . $config[TrackingTools][GoogleAnalytics];
?>

<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>treaction - Galaktische Preise Gewinnen!</title>

        <link href="css/creative.css" rel="stylesheet" type="text/css">  
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,600,700" rel="stylesheet">

        <link rel="stylesheet" href="vendor/telefonvalidator-client/build/css/intlTelInput.css"/>
    <script type="text/javascript" src="vendor/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="vendor/telefonvalidator-client/build/js/utils.js"></script>
    <script type="text/javascript" src="vendor/telefonvalidator-client/build/js/intlTelInput.js"></script>
    <script type="text/javascript" src="vendor/bxslider/src/js/jquery.bxslider.js "></script>
    <script type="text/javascript" src="vendor/google/maps.js"></script>
    <script type="text/javascript" src="vendor/jquery-cookie-master/src/jquery.cookie.js"></script>
    <script type="text/javascript">
        var countdown_status = "<?= $counter_status; ?>";
        var countdown_expire = "<?= $config[GeneralSetting][CountdownExpire]; ?>";
		var countdown_expire_period = "<?= $config[GeneralSetting][MaximumCountdownperiod]; ?>";
		var countdown_end_date = "<?= $current_end_date ?>";
		var single_submit = "<?= $config[GeneralSetting][SingleSubmit]; ?>";
		var webgains_cookie = "<?= $webgains_cookie ?>";
        var current_stocks = "<?= $current_count_value; ?>";
        var autofill_postal_code = "<?= $config[Postal_Code][Autofill]; ?>";
        var localization_postal_code = "<?= $config[Postal_Code][Localization]; ?>"
        var countdown_expire_message = "<?= $config[GeneralSetting][CountdownExpireMessage] ?>";
        var validate_telefon = "<?= $config[Telefon][Status]; ?>";
        var single_submit_text = "<?= $single_submit_text ?>"
        var cookie = "<?= $cookie; ?>";
        var fb_events = "<?= $config[TrackingTools][FacebookEvent] ?>";
        var fb_event = (fb_events.split(",")); // Split the events to array
        var fb_curreny = "<?= $config[TrackingTools][FacebookCurrency] ?>";
        var fb_value = "<?= $config[TrackingTools][FacebookValue] ?>";
    </script>

        <script language="JavaScript" type="text/javascript" src="cio.js"></script>          

        <!-- Google Analytics -->
        <?php include_once 'clients/tracking/google/google.php'; ?>        

        <!-- Facebook Pixel -->
        <?php include_once('clients/tracking/facebook/facebook.php'); ?>      

    </head>

    <!-- Outbrain Tracking -->
    <?php $config[TrackingTools][EnableOutbrain] ? include_once('clients/tracking/outbrain/outbrain_index.php') : " "; ?>

    <body>
        <!-- Web Gains Tracking -->
        <?php $config[TrackingTools][EnableWebGains] ? include_once 'clients/tracking/webgains/webgains_index.php' : " "; ?>

        <!-- Remarketing Target360 Tracking -->
        <?php $config[TrackingTools][EnableTarget360] ? include_once 'clients/tracking/target360/target_index.php' : ' ' ;?>                


        <!-- COUNTDOWN -->
        <section>
            <div class="nav-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <span style="letter-spacing: 3px; font-family: 'Titillium Web', sans-serif; color:#fff;" class="countdown-nav">Gewinnspiel endet in</span>
                            <span style="letter-spacing: 3px; font-family: 'Titillium Web', sans-serif; color:#fff;" class="countdown-nav" id="demo"></span>
                        </div> 
                    </div>
                </div>   
            </div>  
        </section>   
        <!-- ./COUNTDOWN -->      

        <!-- CONTENT -->

    <center><div class="astronaut" style="z-index: 10; margin-bottom:-6px;"><img class="fadeInUp" style="width:500px;" src="img/astronaut.png" /></div></center>

    <div class="container">
        <div class="row">

            <div class="col-sm-12 tre-content">

                <div class="col-md-12">
                    <center>
                        <img style="padding-top:30px;width:250px;" alt="EMS-Training-zuhause" src="https://treaction.net/wp-content/uploads/2018/11/treaction-logo_website_weiss.svg">
                        <p>&nbsp;</p>
                        <h1 class="tre-header">GEWINNE GALAKTISCHES ONLINE-MARKETING<br> IM WERT VON INSGESAMT 15.000€</h1>
                        <p>&nbsp;</p>
                    </center>
                </div>

                <div class="container">
                    <div class="row">

                        <div class="col-sm-4 tre-preise"> 
                            <center>
                                <img style="width:200px;" src="img/tre-preis-1.png" />
                                <h3 style="font-size:24px; padding:10px; text-transform: uppercase; color:#fff;">Online-Marketing<br> Enterprise Paket*</h3>
                                <li style="list-style-type: none;">
                                    <p style="color:#fff; font-weight:500;">Online Marketing Analyse</p>
                                    <p style="color:#fff; font-weight:500;">Mail-In-One Enterprise</p>
                                    <p style="color:#fff; font-weight:500;">Popup-In-One Professional</p>
                                    <p style="color:#fff; font-weight:500;">Online Kampagne</p>
                                    <p style="color:#fff; font-weight:500;">Landing Page</p>
                                </li>
                            </center>
                        </div>

                        <div class="col-sm-4 tre-preise"> 
                            <center>
                                <img style="width:200px;" src="img/tre-preis-2.png" />
                                <h3 style="font-size:24px; padding:10px; text-transform: uppercase; color:#fff;">Online-Marketing<br> Professional Paket*</h3>
                                <li style="list-style-type: none;">
                                    <p style="color:#fff; font-weight:500;">Online Marketing Analyse</p>
                                    <p style="color:#fff; font-weight:500;">Mail-In-One Enterprise</p>
                                    <p style="color:#fff; font-weight:500;">Popup-In-One Professional</p>
                                    <p style="color:#fff; font-weight:500;">Popup Design</p>
                                </li>
                            </center>
                        </div> 

                        <div class="col-sm-4 tre-preise"> 
                            <center>
                                <img style="width:200px;" src="img/tre-preis-3.png" />
                                <h3 style="font-size:24px;padding:10px; text-transform: uppercase; color:#fff;">Online-Marketing<br> Essential Paket*</h3>
                                <li style="list-style-type: none;">
                                    <p style="color:#fff; font-weight:500;">Online Marketing Analyse</p>
                                    <p style="color:#fff; font-weight:500;">Mail-In-One Enterprise</p>
                                </li>
                            </center>
                        </div>

                    </div>
                </div>


                <div class="container">
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10 ">
                            <p>&nbsp;</p>
                            <hr>
                            <center>
                                <h2 class="tre-sub-head">Desweiteren verlosen wir 30x unsere <br>Online Marketing Analyse</h2>
                            </center>
                            <hr>
                            <p>&nbsp;</p>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>                


                <div class="container">
                    <div class="row">


                        <div class="col-lg-3"></div>

                        <div class="col-lg-6 check-submit-form" id="form" >     
                            <form id="api-data-form" action="<?= $_SERVER['PHP_SELF'] ?>" method="post" accept-charset="utf-8" class="form-horizontal" role="form">
                                <input type="hidden" name="contactid" value="<?= $contactid ?>">
                                <input type="hidden" name="checksum" value="<?= $checksum ?>">
                                <input type="hidden" name="mailingid" value="<?= $mailingid ?>">

                                <!-- INPUT MESSE -->
                                <div class="form-group">
                                    <div class="col-sm-12">

                                        <center><p style="color:#fff;">An welcher Messe nimmst Du teil?</p></center>

                                        <div style="width:100%;"class="btn-group-toggle" data-toggle="buttons">
                                            <label style="width:100%;" class="btn-tre btn btn-outline-light active">
                                                <input class="btn-tre" value="net&work" type="radio" name="<?php echo $config[MailInOne][Mapping][Messe]; ?>" id="option1" autocomplete="off" checked>Net&Work Frankfurt
                                            </label>
                                            <br><br>
                                            <label style="width:100%;" class="btn-tre btn btn-outline-light">
                                                <input class="btn-tre" value="ecommerce" type="radio"name="<?php echo $config[MailInOne][Mapping][Messe]; ?>" id="option2" autocomplete="off">eCommerce Berlin
                                            </label>
                                            <br><br>
                                            <label style="width:100%;" class="btn btn-outline-light">
                                                <input class="btn-tre" value="ohk" type="radio"name="<?php echo $config[MailInOne][Mapping][Messe]; ?>" id="option2" autocomplete="off">OHK Kassel
                                            </label>
                                        </div>   
                                    </div>       
                                </div>

                                <p>&nbsp;</p>
                                <center><p style="color:#fff;">Bitte trage deine Daten ein:</p></center>

                                <!-- INPUT ANREDE FRAU & HERR -->
                                <div class="form-group">
                                    <div class="col-sm-12">

                                        <div style="width:100%;"class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label style="width:100%;" class="btn btn-outline-light active">
                                                <input style="color:#fff; ;background:transparent;" value="Frau" type="radio" name="<?php echo $config[MailInOne][Mapping][SALUTATION]; ?>" id="option1" autocomplete="off" checked> Frau
                                            </label>
                                            <label style="width:100%;" class="btn btn-outline-light">
                                                <input style="color:#fff; ;background:transparent;" value="Herr" type="radio"name="<?php echo $config[MailInOne][Mapping][SALUTATION]; ?>" id="option2" autocomplete="off"> Herr
                                            </label>

                                        </div>
                                    </div>       
                                </div>


                                <!-- INPUT VORNAME -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <input style="color:#fff;background:transparent;" value="<?php echo ((!empty($standard_001) ? $standard_001 : $_SESSION['data'][$config[MailInOne][Mapping][FIRSTNAME]])); ?>" class="input-fields form-control" id="" name="<?php echo $config[MailInOne][Mapping][FIRSTNAME]; ?>" placeholder="*Dein Vorname" type="text" required 
                                                   <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>      
                                        </div>
                                    </div>       
                                </div>


                                <!-- INPUT NACHNAME -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">

                                            <input style="color:#fff; ;background:transparent;" value="<?php echo ((!empty($standard_002) ? $standard_002 : $_SESSION['data'][$config[MailInOne][Mapping][LASTNAME]])); ?>" class="input-fields form-control" id="nachname" name="<?php echo $config[MailInOne][Mapping][LASTNAME]; ?>" placeholder="*Dein Nachname" type="text" required
                                                   <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                        </div>
                                    </div>       
                                </div>

                                <!-- INPUT WEBSEITE -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <input style="color:#fff; ;background:transparent;" value="<?php echo ((!empty($standard_002) ? $standard_002 : $_SESSION['data'][$config[MailInOne][Mapping][Webseite]])); ?>" class="input-fields form-control" id="website" name="<?php echo $config[MailInOne][Mapping][Webseite]; ?>" placeholder="*Deine Webseite" type="text" required
                                                   <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                        </div>
                                    </div>       
                                </div>

                                <!-- INPUT TELEFON -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <input style="color:#fff;background:transparent;" value="<?php echo ((!empty($custom_003) ? $custom_003 : $_SESSION['data'][$config[MailInOne][Mapping][Telefon]])); ?>" class="input-fields form-control" id="telefon-mobile"  placeholder="*Telefonnummer" type="text" required <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?> >
                                            <input value="<?php echo $_SESSION['data'][$config[MailInOne][Mapping][Telefon]] ?>" class="input-fields form-control telefon" id="tel2" name="<?php echo $config[MailInOne][Mapping][Telefon]; ?>" placeholder="*Telefonnummer" type="text" hidden >   
                                        </div>
                                        <span class="p-light errortelefon" style="color:<?php echo $config[ErrorHandling][ErrorMsg_Color]; ?> ;float:left; padding-bottom:15px;"><?php echo $config[ErrorHandling][ErrorMsgTelefon]; ?></span>
                                    </div>       
                                </div>

                                <!-- INPUT EMAIL -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <input style="color:#fff; ;background:transparent;" value="<?php echo ((!empty($email_002) ? $email_002 : $_SESSION['data'][$config[MailInOne][Mapping][EMAIL]])); ?>" class="input-fields form-control email" id="email" name="<?php echo $config[MailInOne][Mapping][EMAIL]; ?>" placeholder="*Deine E-Mail-Adresse" type="email" required 
                                                   <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                        </div>
                                        <?php if (($_SESSION['error'] === "email") && !empty($_SESSION['error_msg'])) { ?>   
                                            <span class="p-light erroremail"  style="float: left; padding-bottom:15px; color:<?php echo $config[ErrorHandling][ErrorMsg_Color]; ?>"><?php echo $_SESSION['error_msg']; ?></span>
                                        <?php } ?>  										
                                    </div>       
                                </div>



                                <label class="form-group">
                                    <div class="col-sm-12">
                                        <input style="color:#fff; border:transparent;background:#545b62;" style="width: 20px; height: 20px;" type="checkbox" required>
                                        <span style="color:#fff!important;"><small>&nbsp;&nbsp;Ich, erkläre mich mit den AGB und Datenschutz der treaction AG, Gerda-Krüger-Nieland-Str. 1, 76149 Karlsruhe, einverstanden. Ich bin einverstanden, dass die treaction AG, meine eingetragenen Daten nutzt und mir E-Mails zuschickt bzw. mich anruft, um mir Angebote/Beratung zu den Bereichen Lead-Generierung, Marketing Automation oder E-Mail-Marketing zukommen zu lassen. Diese Einwilligung kann ich jederzeit widerrufen, etwa durch einen Brief an die oben genannte Adresse oder durch eine E-Mail an <a href="mailto:widerruf@treaction.de ">widerruf@treaction.de</a>. Anschließend wird jede werbliche Nutzung unterbleiben.</small></span>
                                    </div>    
                                </label>    

                                <!--Hidden Fields-->   
                                <div class="form-group hidden">
                                    <label class="col-sm-2 control-label" for="">URL</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][url]; ?>" value="<?php echo $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>">
                                    </div>
                                </div>                                        

                                <div class="form-group hidden">
                                    <label class="col-sm-2 control-label" for="">UTM Source</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_source]; ?>" value="<?php echo $_GET['utm_source'] ?>" >
                                    </div>
                                </div>  

                                <div class="form-group hidden">
                                    <label class="col-sm-2 control-label" for="">UTM Name</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_name]; ?>" value="<?php echo $_GET['utm_name'] ?>">
                                    </div>
                                </div>      

                                <div class="form-group hidden">
                                    <label class="col-sm-2 control-label" for="">UTM Term</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_term]; ?>" value="<?php echo $_GET['utm_term'] ?>">
                                    </div>
                                </div>                             

                                <div class="form-group hidden">
                                    <label class="col-sm-2 control-label" for="">UTM Content</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_content]; ?>" value="<?php echo $_GET['utm_content'] ?>">
                                    </div>
                                </div>                              

                                <div class="form-group hidden">
                                    <label class="col-sm-2 control-label" for="">UTM Medium</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_medium]; ?>" value="<?php echo $_GET['utm_medium'] ?>">
                                    </div>
                                </div>    

                                <div class="form-group hidden">
                                    <label class="col-sm-2 control-label" for="">UTM Campaign</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_campaign]; ?>" value="<?php echo $_GET['utm_campaign'] ?>">
                                    </div>
                                </div>                                               

                                <div class="form-group hidden">
                                    <label class="col-sm-2 control-label" for="">TRAFFICSOURCE</label>
                                    <div class="col-sm-10">
                                        <input class="form-control"  placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][trafficsource]; ?>" id="ts" value="<?php echo $_GET['trafficsource'] ?>">
                                    </div>
                                </div>

                                <div class="form-group hidden">
                                    <label class="col-sm-2 control-label" for="">Quelle</label>
                                    <div class="col-sm-10">
                                        <input class="form-control"  placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][Quelle]; ?>" id="quelle" value="<?php echo $config[MailInOne][Constants][Quelle]; ?>">
                                    </div>
                                </div>

                                <div class="form-group hidden">
                                    <label class="col-sm-2 control-label" for="">Typ</label>
                                    <div class="col-sm-10">
                                        <input class="form-control"  placeholder="" type="text" ame="<?php echo $config[MailInOne][Mapping][Typ]; ?>" id="" value="<?php echo $config[MailInOne][Constants][Typ]; ?>">
                                    </div>
                                </div>

                                <div class="form-group hidden">
                                    <label class="col-sm-2 control-label" for="">Segment</label>
                                    <div class="col-sm-10">
                                        <input class="form-control"  placeholder="" type="text" ame="<?php echo $config[MailInOne][Mapping][Segment]; ?>" id="" value="<?php echo $config[MailInOne][Constants][Segment]; ?>">
                                    </div>
                                </div>

                                <div class="form-group hidden">
                                    <label class="col-sm-2 control-label" for="">Test Mode</label>
                                    <div class="col-sm-10">
                                        <input class="form-control"  placeholder="" type="text" name="test_mode" id="" value="<?php echo $_GET['testmode']; ?>">
                                    </div>
                                </div>

                                <div class="form-group"> 
                                    <div class="col-sm-12">
                                        <input style="margin-top: 25px; cursor: pointer; background-color: #64a676; border-color: #64a676;" type="submit" class="col-sm-12 btn btn-danger btn-xl js-scroll-trigger" value="JETZT TEILNEHMEN" name="submit" style="width: 100%;">
                                    </div>        
                                </div>

                                <center><p style="color:#fff;">Gewinnspieldauer: 01.01.2019 - 01.04.2019</p></center>

                                    <?php if (isset($response) && $response->isSuccess()) { ?>
                                    <div class="alert alert-success fade in">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>Subscription successful</strong>
                                    </div>
<?php } elseif (isset($warning)) { ?>
                                    <div class="alert alert-warning fade in">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong style="color: red; z-index: 1000;">Subscription failed</strong>
    <?= $warning['message'] ?>
                                    </div>
<?php } elseif (isset($response)) { ?>
                                    <div class="alert alert-danger fade in">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>Subscription failed</strong>
                                    </div>
<?php } ?>
                            </form>          


                        </div>


                        <div class="col-lg-3"></div>
                    </div>
                </div>
            </div>


        </div>
    </div>


    <!-- /CONTENT 

    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <center><img style="width:250px;" alt="MIO" src="https://treaction.net/wp-content/uploads/2018/10/mio-logo-laptop-bg-1.png"></center>
            </div>
            <div class="col-sm-8">
            
            </div>
        </div>
    </div>-->


    <footer class="footer" stlye="background:transparent;">
        <div class="container">
            <div class="row">   
                <div class="col-lg-8 h-100 text-center text-lg-left my-auto" style="text-transform:uppercase; height:auto!important">
                    <ul class="list-inline mb-2">
                        <li class="list-inline-item">
                            <a href="https://treaction.net/rechtliches/#impressum" target="_blank">Impressum</a>
                        </li>
                        <li class="list-inline-item">&sdot;</li>
                        <li class="list-inline-item">
                            <a href="https://treaction.net/rechtliches/#agb" target="_blank">AGB</a>
                        </li>
                        <li class="list-inline-item">&sdot;</li>
                        <li class="list-inline-item">
                            <a href="https://treaction.net/rechtliches/#datenschutz" target="_blank">Datenschutz</a>
                        </li>
                        <li class="list-inline-item">&sdot;</li>
                        <li class="list-inline-item">
                            <a href="#" data-toggle="modal" data-target="#exampleModalLong">Teilnahmebedingungen</a>
                        </li>
                    </ul>

                    <p style="padding-top:15px;" class="text-muted small mb-4 mb-lg-0">*Online-Marketing Enterprise Paket beinhaltet: 1x Mail-In-One Enterprise 12 Monate mit 5.000 E-Mails, 1x Online Kampagne 399 Euro, 1x Landingpage, 1x Online Marketing Analyse, Popup-In-One Professional, Popup Design.<br><br>

                        *Online-Marketing Professional Paket beinhaltet: 1x Mail-In-One Enterprise 12 Monate mit 5.000 E-Mails, 1x Online Marketing Analyse, Popup-In-One Professional, Popup Design.<br><br>

                        *Online-Marketing Essential Paket beinhaltet: 1x Mail-In-One Enterprise 12 Monate mit 5.000 E-Mails, 1x Online Marketing Analyse.<br></p>

                </div>
                <div class="col-lg-4 h-100 text-center text-lg-right my-auto" style="height:auto!important">
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item mr-3">
                            <a href="https://www.facebook.com/treactionag" target="_blank">
                                <i class="fa fa-facebook fa-2x fa-fw"></i>
                            </a>
                        </li>

                    </ul>
                </div>

            </div>   
        </div>
    </footer>

    <!-- Modal -->
    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 style=" text-transform: uppercase;">Teilnahmebedingungen</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">


                    <p style="color:"><strong>1.</strong> Veranstalter des Gewinnspiels ist die treaction AG, HRB 722397 – Amtsgericht Mannheim, Gerda-Krüger-Nieland-Str. 1, 76149 Karlsruhe.</p> 

                    <p style="color: "><strong>2.</strong> An dem Gewinnspiel nimmt ein Nutzer teil, indem er sich bis zum angegebenen Teilnahmeschluss über die Gewinnspielseite registriert und dabei seine Teilnahmedaten (Name, E-Mail-Adresse, Telefonnummer) angibt und eine Einwilligung zur E-Mail-/Telefonwerbung und zur werblichen Nutzer seiner Daten (im Folgen-den: Werbeeinwilligung) erklärt. Alternativ ist auch eine Teilnahme ohne eine Werbeeinwilli-gung möglich, wenn ein Nutzer innerhalb der Teilnahmefrist eine E-Mail an die E-Mail-Adresse <a href="mailto:widerruf@treaction.de ">widerruf@treaction.de </a> unter Angabe seines Namens und seiner Telefonnummer und des Hauptpreises des Gewinnspiels sendet. Die Form der Teilnahme (also Online-Registrierung mit Werbeeinwilligung oder E-Mail-Registrierung ohne Werbeeinwilligung) hat auf die Gewinn-chancen keinen Einfluss.</p> 

                    <p style="color:"><strong>3.</strong> Jeder Teilnehmer kann nur einmal an einem Gewinnspiel teilnehmen. Mitarbeitern von treaction  ag und ihren unmittelbaren Angehörigen (Eltern, Kinder, Ehepartner, Geschwister) ist eine Teilnahme untersagt. Zudem ist eine Teilnahme unter Angabe einer falschen E-Mail-Adresse oder Telefonnummer unzulässig. </p>

                    <p style="color:"><strong>4.</strong> Der Gewinner wird per Verlosung unter allen zugelassenen Teilnehmern (vgl. Ziff. 3) ermittelt. Die Gewinnermittlung findet innerhalb von zwei Wochen nach Teilnahmeschluss statt. </p>

                    <p style="color: "><strong>5.</strong> Der oder die Gewinner werden per E-Mail oder per Telefon über den Gewinn informiert. Wird der Nutzer bei einem Telefonanruf nicht erreicht, wird er – sofern eine Mailbox aktiviert ist – zu einem Rückruf innerhalb von zwei Wochen unter einer konkret benannten Telefonnummer aufge-fordert. Hat er keine aktivierte Mailbox, wird es noch drei weitere Anrufversuche geben. Er-hält die Veranstalterin bei keinem der Anrufe eine nähere Information und wird auch auf eine Rückrufbitte per Mailbox nicht innerhalb der Zwei-Wochen-Frist reagiert, ist die telefonische Gewinnmitteilung gescheitert. Bei einer Gewinnmitteilung per E-Mail wird der Gewinner auf-gefordert, innerhalb von zwei Wochen seine Postadresse zu benennen. Reagiert der Gewin-ner darauf nicht, gilt auch die Gewinnmitteilung per E-Mail als gescheitert. Wenn sowohl die Gewinnmitteilung per Telefon als auch die Gewinnmitteilung per E-Mail gescheitert ist, ver-fällt der Gewinnanspruch. In diesem Fall kommt es zu einer Wiederholung der Gewinnermitt-lung.</p>

                    <p style=""><strong>6.</strong> Stellt sich nach der Gewinnermittlung heraus, dass ein Nutzer unter Verstoß gegen Ziff. 3 teilgenommen hat, kann die Gewinnermittlung wiederholt haben. </p>

                    <p style=""><strong>7.</strong> Sofern in den Gewinnspielinformationen nichts Anderes mitgeteilt wird, wird ein Gewinn auf Kosten der treaction ag an den Gewinnspielteilnehmer verschickt. </p>

                    <p style=""><strong>8.</strong> Es gilt deutsches Recht.  </p>  

                </div>
                <div class="modal-footer">
                    <button type="button" style="background-color: #64a676; border-color: #64a676;" class="btn btn-danger" data-dismiss="modal">Zurück</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('#myModal').on('shown.bs.modal', function () {
            $('#myInput').focus()
        })
    </script>


</body>
</html>        


<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Frauen haben es gut: Ihre Östrogene sind ein natürliches Haarwuchsmittel. Die Haare verweilen im Vergleich zu einem normalen Männer-Haarzyklus länger in der Wachstumsphase und fallen später aus. So um die 50, mit der Menopause, ändert sich das Bild. Wenn der Organismus die Östrogen-Produktion nach und nach reduziert, tauchen oftmals Haarprobleme auf: Aus vollem Haar wird weniger und oft auch dünneres Haar, die Haardichte geht verloren. Haben Frauen auch noch eine erbliche Veranlagung zur androgenetischen Alopezie fällt der Haarausfall in den Wechseljahren umso stärker aus.">
    <meta name="author" content="Thiocyn">
      
    <title>Haarausfall in den Wechseljahren - Jetzt informieren &amp; anmelden</title>
      
    <link href="css/creative.css" rel="stylesheet" type="text/css">
    <link href="css/animate.css" rel="stylesheet" type="text/css"> 
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins:500|Robotoo:900" rel="stylesheet">   
	<link rel="stylesheet" href="vendor/telefonvalidator-client/build/css/intlTelInput.css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
      
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127480495-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-127480495-1');
    </script>
      
              <!-- Facebook Pixel Code -->
        <script>
          !function(f,b,e,v,n,t,s)
          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
          n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t,s)}(window, document,'script',
          'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '1940027029413570');
          fbq('track', 'PageView');
            
             fbq('track', 'CompleteRegistration');
        </script>
      
     
      
        <noscript><img height="1" width="1" style="display:none"
          src="https://www.facebook.com/tr?id=1940027029413570&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->


      
  </head>

    
  <body>
      
    <nav class="nav-top">
       <div class="container">
           <div class="row">
               <span class="nav-top-text"><strong>&#10004;&nbsp;&nbsp;LABORFORSCHUNG MADE IN GERMANY</strong></span>
               <span class="nav-top-text"><strong>&#10004;&nbsp;&nbsp;KEINE VERSANDKOSTEN</strong></span>
               <span class="nav-top-text"><strong>&#10004;&nbsp;&nbsp;EXKLUSIVE GRATIS-VORTEILE</strong></span>
               <span class="nav-top-text"><strong>&#10004;&nbsp;&nbsp;HÖCHSTE QUALITÄT</strong></span>
           </div>
        </div>   
    </nav>
      
    <nav class="navbar navbar-light bg-light static-top" style="background-color: #ffffff!important; margin-top: px!important;">
      <div class="container">
          <a class="navbar-brand" style="text-transform: uppercase;" href="https://www.thiocyn-haarserum.de/" target="_blank"><img style="width:100px;" alt="Haarausfall in den Wechseljahren - Jetzt informieren &amp; anmelden" src="img/thiocyn-logo.jpg"></a>
          <span style="color:#000; text-transform:uppercase;">Starten Sie jetzt mit Ihrem <strong>30 Tage Test!</strong></span>
      </div>
    </nav>
      
    <section class="form-container" id="about">
    <div class="container">
    <div class="row">
        <div class="col-sm-6 gewinn-form">
           <img class="partner-img animated fadeInLeft" style="margin-left: -50px;" src="img/produkt-bilder-30-tage.png" alt="Preise"/>
            <center><small>*Unser Versprechen: Zufrieden oder Geld zurück! Das Kennenlern-Angebot für eine Flasche ist für Sie in DE, AT versandkostenfrei. Aktion gültig nur für Neukunden.</small></center>
        </div>
        
        <div style="padding-top: 40px; padding-bottom: 40px;" class="col-sm-6 gewinn-form">       
           <h4 style="color: #000; text-transform: uppercase; padding-left:20px;">THIOCYN HAARSERUM - UNSER KENNENLERN-ANGEBOT FÜR NEUKUNDEN</h4>
           <p style="padding-left:20px;">Starten Sie jetzt mit Ihrem 30 Tage Test für nur <strong>25,00 € *</strong></p>
            <ul>
            <li>Neuer Wirkstoff gegen Haarausfall</li>
            <li>Forschung Made in Germany</li>
            <li>Testen Sie 30 Tage Anwendung & Verträglichkeit</li>
            <li>Zufrieden oder Geld zurück!</li>
            <li>Wenn Sie jetzt mitmachen, erhalten Sie noch einen <strong>5€-Willkommens-Gutschein</strong></li>
            </ul>
            
            <form  role="form" data-toggle="validator" ACTION="https://web.inxmail.com/thiocyn/subscription/servlet" METHOD="post">
                <input type="hidden" name="INXMAIL_SUBSCRIPTION" value="THS Newsletter-Empfänger"/> <!-- Name der Liste -->
                <input type="hidden" name="INXMAIL_HTTP_REDIRECT" value="https://thiocyn-haarserum.de/newsletter-anmeldung-step1"/> <!-- redirect wenn erfolgreich -->
                <input type="hidden" name="INXMAIL_HTTP_REDIRECT_ERROR" value="https://www.thiocyn-haarserum.de/ratgeber/newsletter-anmeldung-nicht-erfolgreich"/> <!-- redirect wenn nicht erfolgreich -->

                    <!-- INPUT EMAIL -->
					<div class="form-group">
						<div class="col-sm-12">
							<div class="input-group">
                                <input type="hidden" name="quelle" value="extern1"/>
                                <input type="hidden" name="INXMAIL_CHARSET" value="utf-8"/>		
								<input type="email" class="form-control" id="inputEmail" placeholder="*Bitte tragen Sie Ihre E-Mail-Adresse ein" name="email" data-error="Ungültige E-Mail-Adresse" required>
                                
							</div>					
						</div>       
					</div>
                   <label class="form-group">
                        <div class="col-sm-12">
                            <input style="width: 20px; height: 20px; margin-top:15px;" type="checkbox" required>
                            <span class="p-dark">&nbsp;&nbsp;Ich bin einverstanden, dass die Thiocyn GmbH, Stiftstraße 30, 60313 Frankfurt, Deutschland, meine eingetragenen Daten nutzt und mir E-Mails zuschickt bzw. mich anruft, um mir Angebote aus dem Bereich Haarausfall zukommen zu lassen. Diese Einwilligung kann ich jederzeit widerrufen, etwa durch einen Brief an die oben genannte Adresse oder durch eine E-Mail an <a href="mailto:widerruf@thiocyn-haarserum.de">widerruf@thiocyn-haarserum.de</a>. Anschließend wird jede werbliche Nutzung unterbleiben.</span><br><br>
                        </div>    
                    </label>
                
                <div class="submit-btn" style="margin: -15px!important;">
				<div class="form-group"> 
                <div class="col-sm-12">
				<input type="submit" class="col-sm-12 btn btn-danger btn-xl js-scroll-trigger" value="JETZT SICHERN" name="submit" style="width: 100%;">
                </div>        
				</div>
                </div>
              
			</form> 
            
        </div>  
        
        </div>
        </div>  
    </section> 
     
    <section class="form-container-mobile bg-dark">  
    <div class="container">
    <div class="row">
    <div style="padding-top: 25px;" class="col-sm-12 text-center">
    <center><img class="img-fluid" src="img/produkt-bilder-30-tage.png" alt="Preise"/></center>
    <div class="mobile-bullets">
    <center>
           <p style="padding-left:20px;">Starten Sie jetzt mit Ihrem 30 Tage Test für nur <strong>25,00 € *</strong> und erhalten Sie noch einen <strong>5€-Willkommens-Gutschein</strong></p></center>
        
        
    <center><p style="color: #000; text-transform: uppercase;">JETZT SICHERN: </center>
    </div>
    </div> 
    </div>
    </div>  
    </section>   
      
    <section class="form-container-mobile bg-dark">  
    <div class="container">
    <div class="row">
        
        
				<form  role="form" data-toggle="validator" ACTION="https://web.inxmail.com/thiocyn/subscription/servlet" METHOD="post">
                <input type="hidden" name="INXMAIL_SUBSCRIPTION" value="THS Newsletter-Empfänger"/> <!-- Name der Liste -->
                <input type="hidden" name="INXMAIL_HTTP_REDIRECT" value="https://thiocyn-haarserum.de/newsletter-anmeldung-step1"/> <!-- redirect wenn erfolgreich -->
                <input type="hidden" name="INXMAIL_HTTP_REDIRECT_ERROR" value="https://www.thiocyn-haarserum.de/ratgeber/newsletter-anmeldung-nicht-erfolgreich"/> <!-- redirect wenn nicht erfolgreich -->

                    <!-- INPUT EMAIL -->
					<div class="form-group">
						<div class="col-sm-12">
							<div class="input-group">
                                <input type="hidden" name="quelle" value="extern1"/>
                                <input type="hidden" name="INXMAIL_CHARSET" value="utf-8"/>		
								<input type="email" class="form-control" id="inputEmail" placeholder="*Ihre E-Mail-Adresse ein" name="email" data-error="Ungültige E-Mail-Adresse" required>
                                
							</div>					
						</div>       
					</div>
                   <label class="form-group">
                        <div class="col-sm-12">
                            <input style="width: 20px; height: 20px; margin-top:15px;" type="checkbox" required>
                            <span class="p-dark">&nbsp;&nbsp;Ich bin einverstanden, dass die Thiocyn GmbH, Stiftstraße 30, 60313 Frankfurt, Deutschland, meine eingetragenen Daten nutzt und mir E-Mails zuschickt bzw. mich anruft, um mir Angebote aus dem Bereich Haarausfall zukommen zu lassen. Diese Einwilligung kann ich jederzeit widerrufen, etwa durch einen Brief an die oben genannte Adresse oder durch eine E-Mail an <a href="mailto:widerruf@thiocyn-haarserum.de">widerruf@thiocyn-haarserum.de</a>. Anschließend wird jede werbliche Nutzung unterbleiben.</span><br><br>
                        </div>    
                    </label>
                
               <div class="form-group"> 
                    <div class="col-sm-12">
					<input style="margin-top: 25px; cursor: pointer;" type="submit" class="col-sm-12 btn btn-danger btn-xl" value="JETZT SICHERN" name="submit" style="width: 100%;">
                </div>        
				</div>
              <center><small>*Unser Versprechen: Zufrieden oder Geld zurück! Das Kennenlern-Angebot für eine Flasche ist für Sie in DE, AT versandkostenfrei. Aktion gültig nur für Neukunden.</small></center>
                    <p>&nbsp;</p>
			</form>  
  
    </div>
    </div>
    </section>
      
    
    <p>&nbsp;</p>
	  
    <section class="testimonials text-center bg-light" style="background-color: #fff!important;">
      <div class="container">
          <h2 style="text-transform: uppercase; color: #000;">THIOCYN HAARSERUM – HORMONFREIE SPEZIALPFLEGE BEI HAARAUSFALL</h2> 
              <hr>
              <p>Mit dem Thiocyn Haarserum haben wir eine Spezialpflege bei nicht krankheitsbedingtem Haarausfall entwickelt, die wissenschaftlich fundiert ist und eine echte weitere Option darstellt. Hoch wirksam bei diffusem Haarausfall sowie schütterem, dünner werdendem Haar in und nach den Wechseljahren. Die wichtigsten Vorteile:</p>
              <p>&nbsp;</p>
      </div>
        
       <div class="container">
        <div class="row">
          <div class="col-lg-3">
            <div class="mx-auto mb-5 mb-lg-0">
              <i class="fas fa-check-circle fa-2x"></i>
              <hr>
              <p class="font-weight-light mb-0">KEINE BEKANNTEN NEBENWIRKUNGEN</p>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="mx-auto mb-5 mb-lg-0">
              <i class="fas fa-check-circle fa-2x"></i>
              <hr>
              <p class="font-weight-light mb-0">DURCH STUDIEN BELEGTE WIRKSAMKEIT BEI MÄNNER UND FRAUEN</p>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="mx-auto mb-5 mb-lg-0">
              <i class="fas fa-check-circle fa-2x"></i>
              <hr>
              <p class="font-weight-light mb-0">SEHR GUTE, DERMATOLOGISCH GEPRÜFTE VERTRÄGLICHKEIT</p>
            </div>
          </div>
             <div class="col-lg-3">
            <div class="mx-auto mb-5 mb-lg-0">
               <i class="fas fa-check-circle fa-2x"></i>
              <hr>
              <p class="font-weight-light mb-0">KÖRPEREIGENER WIRKSTOFF THIOCYANAT</p>
            </div>
          </div>

            
        </div>
           <!--
             <div class="col-md-12" style="padding-top: 50px;">
                <a href="#about" class="btn btn-text btn-danger">JETZT TESTPAKET SICHERN!</a>
          </div>-->
      </div>
    </section> 
      
    <section class="partner-container" style="background-color: #fff!important;">
    <div class="container">
    <div class="row">
        
        <div class="col-sm-6 model-feature">       
            <h2 style="text-transform: uppercase; color:#000;">HAARAUSFALL IN DEN WECHSELJAHREN: WAS PASSIERT?</h2>
              <hr>
          <p style="color:#000;">Frauen haben es gut: Ihre Östrogene sind ein natürliches Haarwuchsmittel. So um die 50, mit Beginn der Wechseljahre, ändert sich das Bild im Leben einer Frau. Wenn der Organismus die Östrogen-Produktion nach und nach reduziert, tauchen oftmals Haarprobleme auf: Aus vollem Haar wird weniger und oft auch dünneres Haar, die Haardichte geht verloren. Haben Frauen auch noch eine erbliche Veranlagung zu androgenetischer Alopezie macht sich der Haarausfall während der Wechseljahre rund um die Menopause umso stärker bemerkbar.</p>
        <img class="img-fluid" src="img/wechseljahre-was-hilft-gegen-haarausfall-1.jpg" />
        </div>  
        
        <div class="col-sm-6 model-feature">       
            <h2 style="text-transform: uppercase; color:#000;">WAS GESCHIEHT IN DEN WECHSELJAHREN MIT DEM KÖRPER?
</h2>
              <hr>
          <p style="color:#000;">Im Grunde sind die Wechseljahre eine Umkehrung der Pubertät. Während sich der weibliche Körper in jungen Jahren darauf einstellt, befruchtungsfähige Eizellen zu produzieren, wird diese Fähigkeit ab Mitte 40 bis Mitte 50 systematisch zurückgefahren. Wenn die Follikelreifung in den Eierstöcken nicht mehr stattfindet und der Eisprung früher oder später ganz ausfällt, ist für die weitere Entwicklung der Eizelle kein Östrogen, das „weibliche“ Sexualhormon, mehr notwendig. Keine Fortpflanzung, kein Östrogen – diese Reaktion erscheint logisch.<br><br>

<strong>Was die Evolution offenbar nicht „bedacht“ hat, ist, dass dem weiblichen Organismus schlagartig Östrogen für andere Körperfunktionen fehlt!</strong><br><br>

Wechseljahresbeschwerden wie Hitzewallungen, Schweißausbrüche, Schlafstörungen sind die Folge. Nicht zu unterschätzen ist die psychische Belastung, wenn Frauen plötzlich und unerwartet die Haare ausgehen.</p>
            
            <div class="col-md-12" style="padding-top: 10px; width: 100%;">
                <a href="#about" class="btn btn-text btn-danger">JETZT SICHERN!</a>
          </div>
        </div>  
            </div>
        </div>  
    </section>   
      
    <section class="partner-container form-container form-desktop">
    <div class="container">
    <div class="row">
        
        <div class="col-sm-6 model-feature" style="padding-top: 150px;">         
            <h2 style="text-transform: uppercase; color:#000;">DAS RESULTAT UNSER FORSCHUNG: VIELVERSPRECHEND, VERTRÄGLICH, FREI VON NEBENWIRKUNGEN</h2>
              <hr>
          <p style="color:#000;">Mit dem Thiocyn Haarserum haben wir eine Spezialpflege bei erblich bedingtem Haarausfall entwickelt, die wissenschaftlich fundiert ist und eine echte weitere Option darstellt. Auch unser Haarserum kann nicht allen helfen - aber vielen. Zudem ist das Haarserum sehr gut verträglich und relevante Neben- oder Wechselwirkungen sind nicht bekannt.</p>
        
        </div>  
        
        <div class="col-sm-6 model-feature">       
         
        <img class="img-fluid partner-img animated fadeInRight" src="img/Statistiken.png" />    
            
        </div>  
            </div>
        </div>  
    </section>   
      
     <section class="partner-container" style="background-color: #fff!important;">
    <div class="container">
    <div class="row">
        
        <div class="col-sm-6 model-feature" style="">         
            <h2 style="text-transform: uppercase; color:#000;">MADE IN GERMANY</h2>
              <hr>
          <p style="color:#000;">Das Thiocyn Haarserum wurde und wird in Deutschland entwickelt, produziert und kontrolliert. </p>
        
        </div>  
        
        <div class="col-sm-6 model-feature">       
         
        <center><img class="img-fluid" src="img/LaborforschungGER.png" /></center>      
            
        </div>  
            </div>
        </div>  
    </section>     
      
    <section class="partner-container bg-dark" style="background-position: left!important; background-image: url('img/bg3.jpg');">
    <div class="container">
    <div class="row">
        
        <div class="col-sm-6 text-center"></div>
        
        <div class="col-sm-6 model-feature">    
            
                        <h2 style="text-transform: uppercase;">GUTER GRIFF IN DEN WECHSELJAHREN: THIOCYANAT</h2>
            <hr>
            <p>Für alle Frauen in den Wechseljahren, die keine Hormone brauchen oder sich bewusst gegen Hormone entscheiden, ist das natürliche Molekül Thiocyanat eine sehr attraktive Alternative, um dem Haarausfall entgegenzuwirken. Der Thiocyanat-Wirkkomplex in Thiocyn Haarserum kommt ganz ohne Hormone aus und greift nicht in den komplexe Hormonkreisläufe ein. Neben- und Wechselwirkungen sind deshalb nicht bekannt. Thiocyanat stärkt die Haarwurzeln, schützt sie vor wachstumsstörenden Einflüssen. Ein stabiler Zellstoffwechsel und ein normalisierter Haarzyklus sind wichtige Voraussetzungen für gesundes Haarwachstum.</p><br>
            
            
            
        </div>  
            </div>
        </div>  
    </section>    
      
    <section class="testimonials text-center" style="background-color: #fff!important;">
      <div class="container">
          <h3 style="color: #000;">KUNDEN-ERFAHRUNGEN</h3>
         
              <hr>
          <p>&nbsp;</p>   
        <div class="row">
            <div class="col-lg-3">
            <div class="">
              <img class="img-fluid  mb-3" src="img/pp-christiane.png" alt="erfahrungsbericht">   
               
            </div>
          </div>
            <div class="col-lg-3">
            <div class="testimonial-item mx-auto mb-5 mb-lg-0">
              <p class="font-weight-light mb-0"><i>„Ich fühle mich jetzt wieder richtig wohl, wenn ich in den Spiegel schaue. Ich kann mich nicht erinnern, wann ich das letzte Mal so schöne dichte Haare hatte. Und es ist jetzt gerade mal 9 Monate her, seitdem ich anfing, Thiocyn zu nehmen.“</i></p>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="">
              <img class="img-fluid mb-3" src="img/pp-simone.png" alt="erfahrungsbericht">
            </div>
          </div>
          <div class="col-lg-3">
            <div class="testimonial-item mx-auto mb-5 mb-lg-0">
              <p class="font-weight-light mb-0"><i>„Mein Beispiel zeigt, dass hier scheinbar das Unmögliche möglich wird. Ich bin heute immer noch total begeistert und froh darüber, dass ich vor über einem Jahr für mich Thiocyn entdeckt habe.“</i></p>
            </div>
          </div>
        </div>
          <p>&nbsp;</p>
              <hr>
      </div>
    </section>   
      
    <section class="testimonials text-center"  style="background-color: #fff!important;">
      <div class="container">
          
          <center><h4>BEKANNT AUS:</h4></center>
          <p>&nbsp;</p>

        <div class="row">
            <div class="col-lg-2">
            <div class=" mx-auto mb-5 mb-lg-0">  
              
       
            </div>
          </div>
           
          <div class="col-lg-2">
            <div class=" mx-auto mb-5 mb-lg-0">  
              <center><img style="padding-top:20px;" class="img-fluid  mb-3" src="img/thiocyn-apotheke.jpg" alt="thiocyn-apotheke"></center>
            </div>
          </div>
            <div class="col-lg-2">
            <div class="mx-auto mb-5 mb-lg-0">
             <center> <img style="padding-top:-10px;" class="img-fluid  mb-3" src="img/thiocyn-bild.jpg" alt="thiocyn-bild"></center>
            </div>
          </div>
          <div class="col-lg-2">
            <div class=" mx-auto mb-5 mb-lg-0">
              <center><img style="padding-top:20px;" class="img-fluid  mb-3" src="img/thiocyn-focus.jpg" alt="thiocyn-focus"></center>
            </div>
          </div>
          <div class="col-lg-2">
            <div class="mx-auto mb-5 mb-lg-0">
             <center><img style="width: px;" class="img-fluid  mb-3" src="img/thiocyn-ndr.jpg" alt="thiocyn-ndr"></center>
            </div>
          </div>
            
                  <div class="col-lg-2">
            <div class=" mx-auto mb-5 mb-lg-0">  
              
       
            </div>
          </div>
        </div>
      </div>
    </section>  
      
   

    <section class="partner-container bg-dark"  style="background-image: url('img/bg4.jpg'); background-position: right!important;">
    <div class="container">
    <div class="row">
         <div class="col-sm-6 model-feature">       
            <h2 style="text-transform: uppercase; color:#000;">BEGINNEN SIE JETZT MIT DER REGELMÄSSIGEN ANWENDUNG</h2>
              <hr>
          <p style="color:#000;">Sowohl die Anwendungsstudie als auch eine klinisch dermatologische Studie belegen, dass eine dauerhafte Anwendung für einen konstanten Effekt notwendig ist. Mit unserem Haar-Abo werden Sie regelmäßig beliefert und sichern somit die regelmäßige Anwendung. Sie sparen zudem dauerhaft 20% und die Versandkosten.</p><br>
             
                <a href="#about" class="btn btn-text btn-danger">JETZT SICHERN!</a>
        
        </div>     
    </div>
    </div>  
    </section>  

    <footer class="footer bg-light">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 h-100 text-center text-lg-left my-auto" style="height:auto!important">
            <ul class="list-inline mb-2">
              <li class="list-inline-item">
                <a style="color:#000;" href="https://www.thiocyn-haarserum.de/" target="_blank">Shop</a>
              </li>
                <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a style="color:#000;" href="https://www.thiocyn-haarserum.de/studien" target="_blank">Studien</a>
              </li>
                <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a style="color:#000;" href="https://www.thiocyn-haarserum.de/impressum/" target="_blank">Impressum</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a style="color:#000;" href="https://www.thiocyn-haarserum.de/agb" target="_blank">AGB</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a style="color:#000;" href="https://www.thiocyn-haarserum.de/datenschutz" target="_blank">Datenschutz</a>
              </li>
            </ul>
            <p class="text-muted small mb-4 mb-lg-0">#THIOCYN</p>
          </div>
            
            
          <div class="col-lg-6 h-100 text-center text-lg-right my-auto" style="height:auto!important">
            <ul class="list-inline mb-0">
                
              <li class="list-inline-item mr-3">
                <a href="https://www.facebook.com/thiocynhaarserum" target="_blank">
                  <i style="color:#000;" class="fab fa-facebook fa-3x"></i>
                </a>
              </li>
                
              <li class="list-inline-item mr-3">
                <a href="https://twitter.com/thiocyn" target="_blank">
                  <i style="color:#000;" class="fab fa-twitter-square fa-3x"></i>
                </a>
              </li>
                
            <li class="list-inline-item mr-3">
                <a href="https://www.instagram.com/thiocyn.haarserum/" target="_blank">
                  <i style="color:#000;" class="fab fa-instagram fa-3x"></i>
                </a>
              </li>
                
            <li class="list-inline-item mr-3">
                <a href="https://plus.google.com/+ThiocynHaarserum-Mittel-gegen-Haarausfall" target="_blank">
                  <i style="color:#000;" class="fab fa-google-plus-square fa-3x"></i>
                </a>
              </li>    
                
            </ul>
          </div>
        </div> 
            
        </div>
    </footer>
      
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script type="text/javascript" src="vendor/telefonvalidator-client/build/js/utils.js"></script>		
	<script type="text/javascript" src="vendor/telefonvalidator-client/build/js/intlTelInput.js"></script>	
    <script src="vendor/google/maps.js"></script>  
    <script type="text/javascript ">

        
        $(document ).ready(function() {
            var autofill_postal_code = "<?= $config[Postal_Code][Autofill];?>";
            console.log("autofill_postal_code", autofill_postal_code);    
			var validate_telefon = "<?= $config[Telefon][Status];?>";
			
			// telefon validation 
			var settings = {
				utilsScript: "vendor/telefonvalidator-php-client/build/js/utils.js",
				preferredCountries: ['de'],			
				onlyCountries:['de','ch','at'],
			};		
			$('#telefon').intlTelInput(settings);
			$('#telefon-mobile').intlTelInput(settings);				

			// Error messages
            $('.errorplz').hide();
			$('.errortelefon').hide();
            
            //Parse config.ini file 
                        
            
            /* JQuery Blur event for the postal code
             * Parameters : 
             * Returns : 
             *    1. fill the city Name on success.
             */
            
            $('.plz').blur(function(){
                var zip = $(this).val();
                var city;
               
                
                if($('.plz').hasClass('wrongplz')){
                    $('.plz').removeClass('wrongplz'); 
                    $('.errorplz').hide();
                }
                // Check for autofill postal code value
                if(autofill_postal_code){      
                    city = checkZipCode(zip);
                    
                    if(city){
                        $('.ort').val(city);
                    }else{
                        $('.ort').val("");      
                        $('.plz').addClass("wrongplz");
                    }
                }    
            });                


            /* Function Name : checkZipCode
             * Parameters : zipcode
             * Returns : 
             *    1. countryName on success .
             *    2. False on faiure.
             */
            
            function checkZipCode(zip){
                var status = true;
                var city ;
                if(zip.length == 5) {
                    var gurl = 'https://maps.googleapis.com/maps/api/geocode/json?address=Germany'+zip+'&key=AIzaSyDA10Y_CEIbkz2OY-Zp7PsBBjKB5YNh77I';
                    $.getJSON({
                        url : gurl,
                        async: false,
                        success:function(response, textStatus){
                            // check the status of the request
                            if(response.status !== "OK") {
                                status = false;         // Postal code not found or wrong postal code.
                            } else{    
                                // Postal code is found
                                status = true
                                var address_components = response.results[0].address_components;
                                $.each(address_components, function(index, component){
                                    var types = component.types;
                                    // Find the city for the postal code
                                    $.each(types, function(index, type){
                                        if(type == 'locality'){
                                            city = component.long_name;
                                            status = true;
                                        }
                                    });
                                });                                 
                            }
                         }
                    });
                } else{        
                    status = false;
                }
                if(status){
                    return city;
                }else {
                    return false;
                }
            }

			/* Desktop Version
			*  Validates the phone number format for specified country. 
			*
			*  Parameters:  Telephone field in the form.
			*
			*  Returns: 
			*		false : shows the error message 
			*       true  : hides the error message
			*/  	
			
			$('#telefon').blur(function() {
				/* Regex for all the special characters and alphabits */
				alpha = /^[a-zA-Z!@#$§?=´:;<>|{}\[\]*~_%&`.,"]*$/;
				tel = $('#telefon'); 
				var error = false;
				telnum = tel.val();
				
				// If telefon validation true 
				if ($.trim(tel.val()) && validate_telefon) {
					console.log(telnum);
				// Check for alphabits and special characters from regex expresssion.
					for(var i = 0;  i < telnum.length; i++){
						var c = telnum.charAt(i);
						if(alpha.test(c)){
							error = true;
						}
					}
					console.log(error);
				// Check for valid number  
					if (!tel.intlTelInput("isValidNumber")) {
						
						$('.errortelefon').show();
					}else {
						if(error){
							$('.errortelefon').show();
						}else{
							$('.errortelefon').hide();
						}	
					}					
				}
			});	
			
			/* Mobile version
			*  Validates the phone number format for specified country. 
			*
			*  Parameters:  Telephone field in the form.
			*
			*  Returns: 
			*		false : shows the error message 
			*       true  : hides the error message
			*/  			
			
			$('#telefon-mobile').blur(function() {
				/* Regex for all the special characters and alphabits */
				alpha = /^[a-zA-Z!@#$§?=´:;<>|{}\[\]*~_%&`.,"]*$/;
				tel = $('#telefon-mobile'); 
				telnum = tel.val();				
				var error = false;
				// If telefon validation true 
				if ($.trim(tel.val()) && validate_telefon) {
				// Check for alphabits and special characters from regex expresssion
					for(var i = 0;  i < telnum.length; i++){
						var c = telnum.charAt(i);
						if(alpha.test(c)){
							error = true;
						}
					}			
				// Check for valid number  
					if (!tel.intlTelInput("isValidNumber")) {
						$('.errortelefon').show();
					}else {
						if(error){
							$('.errortelefon').show();
						}else{
							$('.errortelefon').hide();
						}	
					}					
				}
			});						
			
            /* JQuery submit event for the postal code
             * Parameters : 
             * Returns : 
             *    1. check the postal code and accordingly display the error message for postal code.
             */
            form = $('.check-submit-form');
            form.submit(function(e)  {
               //e.preventDefault();
                var form = $(this);
                var readyforsubmit = true;     
                var plz = $('.plz');
                var ort = $('.ort');
                var email = $('.email');
                
                if(autofill_postal_code) {
                    if(plz.hasClass("wrongplz")){    
                        $('.errorplz').show();
                        return false;
                    }
                }
				if(validate_telefon) {
					if($('.errortelefon').css('display') !== 'none'){
						return false;
					}
				}				
				form.find("input[type='submit']").prop('disabled', true);
            }); 
        });     
       </script>	
  </body>
</html>

<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Frauen haben es gut: Ihre Östrogene sind ein natürliches Haarwuchsmittel. Die Haare verweilen im Vergleich zu einem normalen Männer-Haarzyklus länger in der Wachstumsphase und fallen später aus. So um die 50, mit der Menopause, ändert sich das Bild. Wenn der Organismus die Östrogen-Produktion nach und nach reduziert, tauchen oftmals Haarprobleme auf: Aus vollem Haar wird weniger und oft auch dünneres Haar, die Haardichte geht verloren. Haben Frauen auch noch eine erbliche Veranlagung zur androgenetischen Alopezie fällt der Haarausfall in den Wechseljahren umso stärker aus.">
    <meta name="author" content="Thiocyn">
    
    <meta http-equiv="refresh" content="10;url=https://www.thiocyn-haarserum.de/" />  
	
    <title>Haarausfall in den Wechseljahren - Jetzt informieren &amp; anmelden</title>
      
    <link href="css/creative.css" rel="stylesheet" type="text/css">
    <link href="css/animate.css" rel="stylesheet" type="text/css"> 
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins:500|Robotoo:900" rel="stylesheet">   
	<link rel="stylesheet" href="vendor/telefonvalidator-client/build/css/intlTelInput.css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
  </head>

    
  <body>
      
    <nav class="nav-top">
       <div class="container">
           <div class="row">
               <span class="nav-top-text"><strong>&#10004;&nbsp;&nbsp;LABORFORSCHUNG MADE IN GERMANY</strong></span>
               <span class="nav-top-text"><strong>&#10004;&nbsp;&nbsp;KEINE VERSANDKOSTEN</strong></span>
               <span class="nav-top-text"><strong>&#10004;&nbsp;&nbsp;EXKLUSIVE GRATIS-VORTEILE</strong></span>
               <span class="nav-top-text"><strong>&#10004;&nbsp;&nbsp;HÖCHSTE QUALITÄT</strong></span>
           </div>
        </div>   
    </nav>
      
    <nav class="navbar navbar-light bg-light static-top" style="background-color: #ffffff!important; margin-top: px!important;">
      <div class="container">
          <a class="navbar-brand" style="text-transform: uppercase;" href="https://www.thiocyn-haarserum.de/" target="_blank"><img style="width:100px;" alt="Haarausfall in den Wechseljahren - Jetzt informieren &amp; anmelden" src="img/thiocyn-logo.jpg"></a>
          <span style="color: #000;">THIOCYN TESTPAKET MIT<strong> 10% RABATT + GRATIS VERSAND!</strong></span>
      </div>
    </nav>
      
 
        <section class="form-container" style="padding-top: 50px; padding-bottom: 250px; height:100%;" id="about">
            <div class="container">  
            <div class="row">
            <div class="col-sm-12">
                <center>
                    <h1 style="color:#000; text-transform: uppercase;">Vielen Dank für deine Anmeldung.</h1>
                    <hr>
                    <p style="color:#000; margin-top: 30px; padding: 15px;">BITTE AKTIVIEREN SIE IHRE ANMELDUNG ZUM NEWSLETTER NUN NOCH ÜBER DEN LINK IN DER BESTÄTIGUNGS-E-MAIL, DIE WIR IHNEN SOEBEN ZUGESCHICKT HABEN. </p> 
                </center>
            </div>
            </div>
            </div>  

        </section>  
    
    <footer class="footer bg-light">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 h-100 text-center text-lg-left my-auto" style="height:auto!important">
            <ul class="list-inline mb-2">
              <li class="list-inline-item">
                <a style="color:#000;" href="https://www.thiocyn-haarserum.de/" target="_blank">Shop</a>
              </li>
                <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a style="color:#000;" href="https://www.thiocyn-haarserum.de/studien" target="_blank">Studien</a>
              </li>
                <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a style="color:#000;" href="https://www.thiocyn-haarserum.de/impressum/" target="_blank">Impressum</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a style="color:#000;" href="https://www.thiocyn-haarserum.de/agb" target="_blank">AGB</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a style="color:#000;" href="https://www.thiocyn-haarserum.de/datenschutz" target="_blank">Datenschutz</a>
              </li>
            </ul>
            <p class="text-muted small mb-4 mb-lg-0">#THIOCYN</p>
          </div>
            
            
          <div class="col-lg-6 h-100 text-center text-lg-right my-auto" style="height:auto!important">
            <ul class="list-inline mb-0">
                
              <li class="list-inline-item mr-3">
                <a href="https://www.facebook.com/thiocynhaarserum" target="_blank">
                  <i style="color:#000;" class="fab fa-facebook fa-3x"></i>
                </a>
              </li>
                
              <li class="list-inline-item mr-3">
                <a href="https://twitter.com/thiocyn" target="_blank">
                  <i style="color:#000;" class="fab fa-twitter-square fa-3x"></i>
                </a>
              </li>
                
            <li class="list-inline-item mr-3">
                <a href="https://www.instagram.com/thiocyn.haarserum/" target="_blank">
                  <i style="color:#000;" class="fab fa-instagram fa-3x"></i>
                </a>
              </li>
                
            <li class="list-inline-item mr-3">
                <a href="https://plus.google.com/+ThiocynHaarserum-Mittel-gegen-Haarausfall" target="_blank">
                  <i style="color:#000;" class="fab fa-google-plus-square fa-3x"></i>
                </a>
              </li>    
                
            </ul>
          </div>
        </div> 
            
        </div>
    </footer>
      
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script type="text/javascript" src="vendor/telefonvalidator-client/build/js/utils.js"></script>		
	<script type="text/javascript" src="vendor/telefonvalidator-client/build/js/intlTelInput.js"></script>	
    <script src="vendor/google/maps.js"></script>  
    <script type="text/javascript ">

        
        $(document ).ready(function() {
            var autofill_postal_code = "<?= $config[Postal_Code][Autofill];?>";
            console.log("autofill_postal_code", autofill_postal_code);    
			var validate_telefon = "<?= $config[Telefon][Status];?>";
			
			// telefon validation 
			var settings = {
				utilsScript: "vendor/telefonvalidator-php-client/build/js/utils.js",
				preferredCountries: ['de'],			
				onlyCountries:['de','ch','at'],
			};		
			$('#telefon').intlTelInput(settings);
			$('#telefon-mobile').intlTelInput(settings);				

			// Error messages
            $('.errorplz').hide();
			$('.errortelefon').hide();
            
            //Parse config.ini file 
                        
            
            /* JQuery Blur event for the postal code
             * Parameters : 
             * Returns : 
             *    1. fill the city Name on success.
             */
            
            $('.plz').blur(function(){
                var zip = $(this).val();
                var city;
               
                
                if($('.plz').hasClass('wrongplz')){
                    $('.plz').removeClass('wrongplz'); 
                    $('.errorplz').hide();
                }
                // Check for autofill postal code value
                if(autofill_postal_code){      
                    city = checkZipCode(zip);
                    
                    if(city){
                        $('.ort').val(city);
                    }else{
                        $('.ort').val("");      
                        $('.plz').addClass("wrongplz");
                    }
                }    
            });                


            /* Function Name : checkZipCode
             * Parameters : zipcode
             * Returns : 
             *    1. countryName on success .
             *    2. False on faiure.
             */
            
            function checkZipCode(zip){
                var status = true;
                var city ;
                if(zip.length == 5) {
                    var gurl = 'https://maps.googleapis.com/maps/api/geocode/json?address=Germany'+zip+'&key=AIzaSyDA10Y_CEIbkz2OY-Zp7PsBBjKB5YNh77I';
                    $.getJSON({
                        url : gurl,
                        async: false,
                        success:function(response, textStatus){
                            // check the status of the request
                            if(response.status !== "OK") {
                                status = false;         // Postal code not found or wrong postal code.
                            } else{    
                                // Postal code is found
                                status = true
                                var address_components = response.results[0].address_components;
                                $.each(address_components, function(index, component){
                                    var types = component.types;
                                    // Find the city for the postal code
                                    $.each(types, function(index, type){
                                        if(type == 'locality'){
                                            city = component.long_name;
                                            status = true;
                                        }
                                    });
                                });                                 
                            }
                         }
                    });
                } else{        
                    status = false;
                }
                if(status){
                    return city;
                }else {
                    return false;
                }
            }

			/* Desktop Version
			*  Validates the phone number format for specified country. 
			*
			*  Parameters:  Telephone field in the form.
			*
			*  Returns: 
			*		false : shows the error message 
			*       true  : hides the error message
			*/  	
			
			$('#telefon').blur(function() {
				/* Regex for all the special characters and alphabits */
				alpha = /^[a-zA-Z!@#$§?=´:;<>|{}\[\]*~_%&`.,"]*$/;
				tel = $('#telefon'); 
				var error = false;
				telnum = tel.val();
				
				// If telefon validation true 
				if ($.trim(tel.val()) && validate_telefon) {
					console.log(telnum);
				// Check for alphabits and special characters from regex expresssion.
					for(var i = 0;  i < telnum.length; i++){
						var c = telnum.charAt(i);
						if(alpha.test(c)){
							error = true;
						}
					}
					console.log(error);
				// Check for valid number  
					if (!tel.intlTelInput("isValidNumber")) {
						
						$('.errortelefon').show();
					}else {
						if(error){
							$('.errortelefon').show();
						}else{
							$('.errortelefon').hide();
						}	
					}					
				}
			});	
			
			/* Mobile version
			*  Validates the phone number format for specified country. 
			*
			*  Parameters:  Telephone field in the form.
			*
			*  Returns: 
			*		false : shows the error message 
			*       true  : hides the error message
			*/  			
			
			$('#telefon-mobile').blur(function() {
				/* Regex for all the special characters and alphabits */
				alpha = /^[a-zA-Z!@#$§?=´:;<>|{}\[\]*~_%&`.,"]*$/;
				tel = $('#telefon-mobile'); 
				telnum = tel.val();				
				var error = false;
				// If telefon validation true 
				if ($.trim(tel.val()) && validate_telefon) {
				// Check for alphabits and special characters from regex expresssion
					for(var i = 0;  i < telnum.length; i++){
						var c = telnum.charAt(i);
						if(alpha.test(c)){
							error = true;
						}
					}			
				// Check for valid number  
					if (!tel.intlTelInput("isValidNumber")) {
						$('.errortelefon').show();
					}else {
						if(error){
							$('.errortelefon').show();
						}else{
							$('.errortelefon').hide();
						}	
					}					
				}
			});						
			
            /* JQuery submit event for the postal code
             * Parameters : 
             * Returns : 
             *    1. check the postal code and accordingly display the error message for postal code.
             */
            form = $('.check-submit-form');
            form.submit(function(e)  {
               //e.preventDefault();
                var form = $(this);
                var readyforsubmit = true;     
                var plz = $('.plz');
                var ort = $('.ort');
                var email = $('.email');
                
                if(autofill_postal_code) {
                    if(plz.hasClass("wrongplz")){    
                        $('.errorplz').show();
                        return false;
                    }
                }
				if(validate_telefon) {
					if($('.errortelefon').css('display') !== 'none'){
						return false;
					}
				}				
				form.find("input[type='submit']").prop('disabled', true);
            }); 
        });     
       </script>	
  </body>
</html>
<?php
require 'cio.php';

# Facebook Pixel URL
$fbURL = "https://www.facebook.com/tr?id=" . $config[TrackingTools][FacebookPixel] . "&ev=PageView&noscript=1";

# Google Maps URL
$googleURL = "https://maps.googleapis.com/maps/api/js?key=" . $config[TrackingTools][GoogleAPIKey] . "&libraries=places";

#Google Analytics Key
$googleAnalyticsURL = "https://www.googletagmanager.com/gtag/js?id=" . $config[TrackingTools][GoogleAnalytics];
?>

<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Nutzen Sie unser neues FREE EMS® PROGRAMM und trainieren sie bequem von zu Hause aus.">
        <meta name="author" content="StimaWELL">

        <title>GRAMPP - Wir machen's einfach</title>

        <link href="css/creative.css" rel="stylesheet" type="text/css">  
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
        <link rel="stylesheet" href="vendor/telefonvalidator-client/build/css/intlTelInput.css"/>
    </head>
    <script type="text/javascript" src="vendor/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="vendor/telefonvalidator-client/build/js/utils.js"></script>
    <script type="text/javascript" src="vendor/telefonvalidator-client/build/js/intlTelInput.js"></script>
    <script type="text/javascript" src="vendor/google/maps.js"></script>
    <script type="text/javascript" src="vendor/jquery-cookie-master/src/jquery.cookie.js"></script>
    <script type="text/javascript">
        var countdown_status = "<?= $counter_status; ?>";
        var countdown_expire = "<?= $config[GeneralSetting][CountdownExpire];?>";
        var current_stocks = "<?= $current_count_value; ?>";
        var autofill_postal_code = "<?= $config[Postal_Code][Autofill]; ?>";
        var localization_postal_code = "<?= $config[Postal_Code][Localization]; ?>";
        var countdown_expire_message = "<?= $config[GeneralSetting][CountdownExpireMessage] ?>";
        var validate_telefon = "<?= $config[Telefon][Status]; ?>";
        var single_submit_text = "<?= $single_submit_text?>";
        var cookie = "<?= $cookie; ?>";
        var fb_events = "<?=$config[TrackingTools][FacebookEvent] ?>";
        var fb_event = (fb_events.split(",")); // Split the events to array
        var fb_curreny = "<?=$config[TrackingTools][FacebookCurrency] ?>";
        var fb_value = "<?=$config[TrackingTools][FacebookValue] ?>";
    </script>        
    <script language="JavaScript" type="text/javascript" src="cio.js"></script> 
    <script type="text/javascript" src="<?php echo $googleURL; ?>"></script>   	
    <script async src="<?php echo $googleAnalyticsURL; ?>"></script>
    
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', '<?php echo $config[TrackingTools][GoogleAnalytics]; ?>');
    </script>	
    <noscript>
    <img height="1" width="1" style="display:none" src= "<?php echo $fbURL ?>"/>
    </noscript>	
    <body>

        
<div class="container">
    
    <!-- LOGO HEADER -->
    <div class="grampp-header">
    <div class="row">
        <div class="col-sm-12">
            <div class="grampp-logo">
                <a href="https://grampp.net/" target="_blank"><img style="width:300px" src="img/grampp-2017.png" /></a>
            </div>
            
            <div class="auto-logos">
                <a href="https://www.mercedes-benz-grampp.de/content/deutschland/retail-plz9/autohaus-grampp/de/desktop/home.html"><img src="img/mercedes-benz.png" /></a>
                <a href="https://www.grampp-lohr.audi/de.html" target="_blank"><img src="img/audi-logo-weiss.png" /></a>
                <a href="https://www.volkswagen-grampp.de/p_35297.html" target="_blank"><img src="img/vw-logo.png" /></a>
                <a href="https://www.clever-driver-lohr.de/" target="_blank"><img src="img/clever-driver-logo.png" /></a>
            </div>
            
        </div>
    </div>
    </div>
    <!-- /.LOGO HEADER -->
    
    <!-- HEADER IMAGE -->
    <div class="grampp-header-img">
    <div class="row">
        <div class="col-sm-12">
            <img src="img/image001-1.png" />
            <div class="small-info-wrapper">
            <span class="small-info">VW T-Cross Kraftstoffverbrauch kombiniert*: 5,1 - 4,9 l/100km; CO₂-Emissionen kombiniert*: 115-111 g/km; Effizienzklasse: B; Angaben zu den Kraftstoffverbräuchen und CO₂-Emissionen sowie Effizienzklassen bei Spannbreiten in Abhängigkeit vom verwendeten Reifen-/Rädersatz.</span>
            </div>
        </div>
    </div>
    </div>
    <!-- /.HEADER IMAGE -->
    
     
        
    
    <!-- FORM CONTENT -->
    <div class="row">
        <div class="col-sm-8">
        <div style="margin-bottom:20px;"class="highlight-header">Informieren und Gewinnen</div>
            
        <div class="col-sm-4 gwsp-mobile">
            <div class="gewinnspiel-wrapper">
                <center>
                <span>Gewinnspiel*</span>
                <img src="img/gewinnspiel1.jpg" />
                <span>3 x 2 Tickets</span><br>
                <span class="small-info">für 4 Std. im Wonnemar – Erlebnisbad in Marktheidenfeld</span>
                </center>
            </div>
        </div>
       
        
         <form id="api-data-form" action="<?= $_SERVER['PHP_SELF'] ?>" method="post" accept-charset="utf-8" class="form-horizontal" role="form">
                            <input type="hidden" name="contactid" value="<?= $contactid ?>">
                            <input type="hidden" name="checksum" value="<?= $checksum ?>">
                            <input type="hidden" name="mailingid" value="<?= $mailingid ?>">
                
                        <div class="form-group">
                         <div class="col-sm-12">
                             <p>Ich möchte mehr Informationen zum neuen T-Cross von Volkswagen. Für die Kontaktaufnahme gebe ich Ihnen hier meine Kontaktdaten bekannt. Mit der Absendung des Kontaktformulars nehme ich an dem Gewinnspiel teil.</p>
                          </div>       
                        </div>
                                <!-- INPUT VORNAME -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <input style="color:#000;background:transparent;" value="<?php echo ((!empty($standard_001) ? $standard_001 : $_SESSION['data'][$config[MailInOne][Mapping][FIRSTNAME]])); ?>" class="input-fields form-control" id="" name="<?php echo $config[MailInOne][Mapping][FIRSTNAME]; ?>" placeholder="* Ihr Vorname" type="text" required 
                                                   <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>      
                                        </div>
                                    </div>       
                                </div>


                                <!-- INPUT NACHNAME -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">

                                            <input style="color:#000; ;background:transparent;" value="<?php echo ((!empty($standard_002) ? $standard_002 : $_SESSION['data'][$config[MailInOne][Mapping][LASTNAME]])); ?>" class="input-fields form-control" id="nachname" name="<?php echo $config[MailInOne][Mapping][LASTNAME]; ?>" placeholder="* Ihr Nachname" type="text" required
                                                   <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                        </div>
                                    </div>       
                                </div>

                                <!-- INPUT TELEFON -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <input style="color:#000;background:transparent;" value="<?php echo ((!empty($custom_003) ? $custom_003 : $_SESSION['data'][$config[MailInOne][Mapping][Telefon]])); ?>" class="input-fields form-control" id="telefon-mobile"  placeholder="* Ihre Telefonnummer" type="text" required <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?> >
                                            <input value="<?php echo $_SESSION['data'][$config[MailInOne][Mapping][Telefon]] ?>" class="input-fields form-control telefon" id="tel2" name="<?php echo $config[MailInOne][Mapping][Telefon]; ?>" placeholder="* Ihr Telefonnummer" type="text" hidden >   
                                        </div>
                                        <span class="p-light errortelefon" style="color:<?php echo $config[ErrorHandling][ErrorMsg_Color]; ?> ;float:left; padding-bottom:15px;"><?php echo $config[ErrorHandling][ErrorMsgTelefon]; ?></span>
                                    </div>       
                                </div>

                                <!-- INPUT EMAIL -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <input style="color:#000; ;background:transparent;" value="<?php echo ((!empty($email_002) ? $email_002 : $_SESSION['data'][$config[MailInOne][Mapping][EMAIL]])); ?>" class="input-fields form-control email" id="email" name="<?php echo $config[MailInOne][Mapping][EMAIL]; ?>" placeholder="* Ihre E-Mail-Adresse" type="email" required 
                                                   <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                        </div>
                                        <?php if (($_SESSION['error'] === "email") && !empty($_SESSION['error_msg'])) { ?>   
                                            <span class="p-light erroremail"  style="float: left; padding-bottom:15px; color:<?php echo $config[ErrorHandling][ErrorMsg_Color]; ?>"><?php echo $_SESSION['error_msg']; ?></span>
                                        <?php } ?>  										
                                    </div>       
                                </div>

                                <label class="form-group">
                                    <div class="col-sm-12">
                                        <input style="color:#000; border:transparent;background:#545b62;" style="width: 20px; height: 20px;" type="checkbox" required>
                                        <span class="small-info">&nbsp;&nbsp;Sie erklären sich mit der Übersendung Ihrer Anfrage über unser Kontaktformular einverstanden, dass wir Ihre mitgeteilten personenbezogenen Daten zur Beantwortung Ihres Anliegens speichern und verarbeiten. Ihre Einwilligung können Sie jederzeit durch Übersendung einer Nachricht an die im Impressum genannte E-Mailadresse mit Wirkung für die Zukunft widerrufen. Hiermit akzeptiere ich die <a href="https://grampp.net/datenschutz/" target="_blank">Datenschutzerklärung.</a></span>
                                    </div>    
                                </label>    

                            <!--Hidden Fields-->   
                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">URL</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][url]; ?>" value="<?php echo $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>">
                                </div>
                            </div>                                        

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Source</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_source]; ?>" value="<?php echo $_GET['utm_source'] ?>" >
                                </div>
                            </div>  

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Name</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_name]; ?>" value="<?php echo $_GET['utm_name'] ?>">
                                </div>
                            </div>      

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Term</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_term]; ?>" value="<?php echo $_GET['utm_term'] ?>">
                                </div>
                            </div>                             

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Content</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_content]; ?>" value="<?php echo $_GET['utm_content'] ?>">
                                </div>
                            </div>                              

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Medium</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_medium]; ?>" value="<?php echo $_GET['utm_medium'] ?>">
                                </div>
                            </div>    

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Campaign</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_campaign]; ?>" value="<?php echo $_GET['utm_campaign'] ?>">
                                </div>
                            </div>                                               

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">TRAFFICSOURCE</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][trafficsource]; ?>" id="ts" value="<?php echo $_GET['trafficsource'] ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Quelle</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][Quelle]; ?>" id="quelle" value="<?php echo $config[MailInOne][Constants][Quelle]; ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Typ</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" ame="<?php echo $config[MailInOne][Mapping][Typ]; ?>" id="" value="<?php echo $config[MailInOne][Constants][Typ]; ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Segment</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" ame="<?php echo $config[MailInOne][Mapping][Segment]; ?>" id="" value="<?php echo $config[MailInOne][Constants][Segment]; ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Test Mode</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="test_mode" id="" value="<?php echo $_GET['testmode']; ?>">
                                </div>
                            </div>                            

                            <div class="form-group"> 
                                <div class="col-sm-12">
                                    <input style="margin-top: 25px; cursor: pointer; background-color:#01a8a9; border-radius:0px!important; border-color:#01a8a9;" type="submit" class="col-sm-12 btn btn-danger btn-xl js-scroll-trigger" value="Anfrage Senden" name="submit" style="width: 100%;">
                                </div>        
                            </div>
                            <div class="form-group"> 
                                <div class="col-sm-12">
                                    <span class="small-info">*Mit Absenden des ausgefüllten Formulars nehmen Sie automatisch an dem Gewinnspiel teil. Das Gewinnspiel läuft bis zum 28.2.2019. Teilnahmeberechtigt sind alle Personen ab 18 Jahre, ausgeschlossen sind GRAMPP Mitarbeiter. Die Gewinnbenachrichtigung erfolgt per Email. Der Rechtsweg ist ausgeschlossen.</span>
                                </div>        
                            </div>   
<?php if (isset($response) && $response->isSuccess()) { ?>
                                <div class="alert alert-success fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Subscription successful</strong>
                                </div>
<?php } elseif (isset($warning)) { ?>
                                <div class="alert alert-warning fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong style="color: red; z-index: 1000;">Subscription failed</strong>
    <?= $warning['message'] ?>
                                </div>
<?php } elseif (isset($response)) { ?>
                                <div class="alert alert-danger fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Subscription failed</strong>
                                </div>
<?php } ?>
                        </form>    
        </div>
        
        
        
        <div class="col-sm-4 gwsp-desktop">
            <div class="gewinnspiel-wrapper">
                <center>
                <span>Gewinnspiel*</span>
                <img src="img/gewinnspiel1.jpg" />
                <span>3 x 2 Tickets</span><br>
                <span class="small-info">für 4 Std. im Wonnemar – Erlebnisbad in Marktheidenfeld</span>
                </center>
            </div>
            <br><br>
            <div class="grampp-logo">
                <a href="https://grampp.net/" target="_blank"><img style="width:100%" src="img/grampp-2017.png" /></a>
            </div>
        </div>
        
        
    </div>
    <!-- /. FORM CONTENT -->
    <br><br>
    <!-- CONTACTS -->
    <div class="row">
        <div class="col-12">
            <div class="highlight-header">Ihre T-Cross Experten bei Grampp</div>
            <br><br>
        </div>
    </div>
    
    <div class="grampp-contacts">
    <div class="row">
        <div class="col-xs-6 col-sm-3">
            <div class="grampp-contact">
            <img src="img/autohaus-grampp-lohr-klingenmeier.jpg" />
            <p style="padding:5px 0px 5px 0px;">Kevin Klingenmeier</p>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3">
            <div class="grampp-contact">
            <img src="img/matthias-kunkel-neu.jpg" />
            <p style="padding:5px 0px 5px 0px;">Matthias Kunkel</p>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3">
            <div class="grampp-contact">
            <img src="img/thomas-scharf-1.jpg" />
            <p style="padding:5px 0px 5px 0px;">Thomas Scharf</p>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3">
            <div class="grampp-contact">
            <img src="img/stefan-metz-2018.jpg" />
            <p style="padding:5px 0px 5px 0px;">Stefan Metz</p>
            </div>
        </div>
    </div>
    </div>
    <br><br>  
   <!-- /. CONTACTS --> 
    
  </div>  
    
    <!-- STANDORTE -->

<section class="grampp-footer">
    
    <div class="container">
    <div class="row">
    <div class="col-12">
    <center><h3 style="text-decoration:underline;">Standorte</h3></center>
    </div>
    </div>
    </div>
    <br><br> 
 <div class="container">
    <div class="row">
        <div class="col-sm-3">
            <div class="grampp-footer-item">
            <img src="img/mercedes-benz.png" /><br>
            <span><strong>Autohaus Grampp GmbH</strong></span><br>
            <span>Rechtenbacher Straße 17<br>
                  97816 Lohr am Main<br>
                  +49 (0) 9352 5003-0</span>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="grampp-footer-item">
            <img src="img/autohaus-grampp-lohr-vw-audi.png" /><br>
            <span><strong>Peter Grampp GmbH & Co. KG</strong></span><br>
            <span>Bürgermeister-Dr.-Nebel-Str. 19<br>
                 97816 Lohr<br>
                 +49 (0) 9352 8755-0</span>
        </div>
        </div>
         <div class="col-sm-3">
             <div class="grampp-footer-item">
            <img src="img/autohaus-grampp-lohr-vw-audi.png" /><br>
            <span><strong>Peter Grampp GmbH & Co. KG</strong></span><br>
            <span>Am Hammersteig 1<br>
                 97753 Karlstadt<br>
                 +49 (0) 9353 9781-0</span>
        </div>
        </div>
        <div class="col-sm-3">
            <div class="grampp-footer-item">
            <img src="img/clever-driver-logo.png" /><br>
            <span><strong>Clever Driver Lohr</strong></span><br>
            <span>Peter Grampp GmbH & Co. KG<br>
                  Bgm.-Dr.-Nebel-Str. 1a<br>
                  97816 Lohr am Main<br>
                  +49 (0) 9352-8755262</span>
        </div>
        </div>
    </div>
     
     <br><br>
    
    <div class="row">
        <div class="col-sm-3">
            <div class="grampp-footer-item">
            <img src="img/mercedes-benz.png" /><br>
            <span><strong>Autohaus Grampp GmbH</strong></span><br>
            <span>Am Hammersteig 1<br>
                  97753 Karlstadt<br>
                  +49 (0) 9353 9748-0</span>
        </div>
        </div>
        <div class="col-sm-3">
             <div class="grampp-footer-item">
            <h3 style="text-decoration:underline;">Service</h3>
            <li><a href="https://grampp.net/leistungsuebersicht/" target="_blank">Leistungsübersicht</a></li>
            <li><a href="https://grampp.net/autovermietung/" target="_blank">Autovermietung</a></li>
            <li><a href="https://grampp.net/terminanfrage/" target="_blank">Terminanfrage</a></li>
            <li><a href="https://grampp.net/neuwagen/" target="_blank">Neuwagen</a></li>
            <li><a href="https://grampp.net/fahrzeugsuche/" target="_blank">Fahrzeugsuche</a></li>
        </div>
            </div>
         <div class="col-sm-3">
              <div class="grampp-footer-item">
            <h3 style="text-decoration:underline;">Unternehmen</h3>
            <li><a href="https://grampp.net/bewerbungsformular/" target="_blank">Bewerbungsformular</a></li>
            <li><a href="https://grampp.net/ansprechpartner/" target="_blank">Ansprechpartner</a></li>
            <li><a href="https://grampp.net/ausbildung/" target="_blank">Ausbildung</a></li>
            <li><a href="https://grampp.net/presse/" target="_blank">Presse</a></li>
        </div>
             </div>
        <div class="col-sm-3">
             <div class="grampp-footer-item">
            <a href="https://www.facebook.com/autohausgrampp/?fref=ts" target="_blank"><img style="height:30px" src="img/fb-icon.jpg" /></a>
            <a href="https://www.ebay.de/str/autohausergramppvwaudimercedes" target="_blank"><img style="height:30px" src="img/media-ebay-icon.png" /></a>
         </div>
            </div>
    </div>
    <!-- /. STANDORTE -->
     
     <br><br>
    
    <div class="row">
        <div class="col-sm-12">
        <small>* Ergänzende Information zu Verbrauchsangaben: Alle angegebenen Werte wurden nach dem vorgeschriebenen Messverfahren (§ 2 Nr. 5, 6, 6 a Pkw-EnVKV in der jeweils geltenden Fassung) ermittelt. Die Angaben beziehen sich nicht auf ein einzelnes Fahrzeug und sind nicht Bestandteil des Angebots, sondern dienen allein Vergleichszwecken zwischen den verschiedenen Fahrzeugtypen. Weitere aktuelle Informationen zu den einzelnen Fahrzeugen erhalten Sie bei Ihrem Händler. Ermittlung des Verbrauchs auf Grundlage der Serienausstattung. Sonderausstattungen können Verbrauch und Fahrleistungen beeinflussen. Weitere Informationen zum offiziellen Kraftstoffverbrauch und den offiziellen spezifischen CO2-Emissionen neuer Personenkraftwagen können dem „Leitfaden über den Kraftstoffverbrauch, CO2-Emissionen und den Stromverbrauch neuer Personenkraftwagen“ entnommen werden, der an allen Verkaufsstellen und bei Deutschen Automobil Treuhand GmbH (www.dat.de) unentgeltlich erhältlich ist.</small><br><br>
        
            <br><br>
        <center>  
        <p>©2019 Peter Grampp GmbH & Co. KG. Alle Rechte vorbehalten.&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">Datenschutz</a> | <a href="#">Impressum</a></p> 
        </center>  
            
        </div>
    </div>
</div>
        
</section> 

    </body>
</html>        

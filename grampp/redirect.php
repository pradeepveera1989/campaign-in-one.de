<?php
// Get the config data from config.ini       
    $config = parse_ini_file("config.ini", true);
	$currentUrl = parse_url($_SERVER['REQUEST_URI']);
    $redirect_path = $config[GeneralSetting][Redirect][RedirectPath];

    if($config[GeneralSetting][KeepURLParameter]){
        $redirect_path = $redirect_path . $currentUrl["query"];
    }
	$redirect_time = $config[GeneralSetting][Redirect][RedirectTime];

// Webgains
    $webgains = false;
    if($_GET['trafficsource'] === "webgains"){
        $lead_reference = $_GET['lead_reference'];
        $webgains = true;
    }


?>
<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">  
	<?php if($config[GeneralSetting][Redirect][RedirectStatus]){ ?>  
    <meta http-equiv="refresh" content="<?php echo $redirect_time; ?>;url=<?php echo $redirect_path; ?>" />  
	<?php } ?>
     <title>GRAMPP - Wir machen's einfach</title>
    <!-- implementation bootstrap -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- implementation fontawesome icons -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
       <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- implementation simpleline icons -->
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <!-- implementation googlefonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <!-- implementation Animated Header -->
    <!-- implementation custom css -->
    <link href="css/creative.css" rel="stylesheet">
    <!-- implementation animate css -->
    <link href="css/animate.css" rel="stylesheet">
  </head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-16152616-31"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', '<?php echo $config[TrackingTools][GoogleAPIKey]; ?>');
</script>
    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '2069635113314986');
      fbq('track', 'PageView');
      fbq('track', 'Purchase');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=2069635113314986&ev=PageView&noscript=1"
    /></noscript>
<!-- End Facebook Pixel Code -->
  <body>
<script type="text/javascript" src="vendor/jquery-cookie-master/src/jquery.cookie.js"></script>  
    <script type="text/javascript">

       $(document).ready(function () {
	console.log($.cookie('campaign'));
	});

    </script>

<div class="container">
    
    <!-- LOGO HEADER -->
    <div class="grampp-header">
    <div class="row">
        <div class="col-sm-12">
            <div class="grampp-logo">
                <a href="https://grampp.net/" target="_blank"><img style="width:300px" src="img/grampp-2017.png" /></a>
            </div>
            
            <div class="auto-logos">
                <a href="https://www.mercedes-benz-grampp.de/content/deutschland/retail-plz9/autohaus-grampp/de/desktop/home.html"><img src="img/mercedes-benz.png" /></a>
                <a href="https://www.grampp-lohr.audi/de.html" target="_blank"><img src="img/audi-logo-weiss.png" /></a>
                <a href="https://www.volkswagen-grampp.de/p_35297.html" target="_blank"><img src="img/vw-logo.png" /></a>
                <a href="https://www.clever-driver-lohr.de/" target="_blank"><img src="img/clever-driver-logo.png" /></a>
            </div>
            
        </div>
    </div>
    </div>
    <!-- /.LOGO HEADER -->
        
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            
            <h1>Bitte überprüfe dein E-Mail Postfach.</h1>
            <hr>
            <p>Wir haben dir eine E-Mail mit einem Bestätigungslink an deine genannte E-Mail-Adresse gesendet. Solltest du in den nächsten 15 Minuten keine Mail erhalten, schaue bitte auch in dein Spam oder Junk Ordner.</p> 
    
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
    
    
    </div>
      
      <section class="grampp-footer">
    
    <div class="container">
    <div class="row">
    <div class="col-12">
    <center><h3 style="text-decoration:underline;">Standorte</h3></center>
    </div>
    </div>
    </div>
    <br><br> 
 <div class="container">
    <div class="row">
        <div class="col-sm-3">
            <div class="grampp-footer-item">
            <img src="img/mercedes-benz.png" /><br>
            <span><strong>Autohaus Grampp GmbH</strong></span><br>
            <span>Rechtenbacher Straße 17<br>
                  97816 Lohr am Main<br>
                  +49 (0) 9352 5003-0</span>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="grampp-footer-item">
            <img src="img/autohaus-grampp-lohr-vw-audi.png" /><br>
            <span><strong>Peter Grampp GmbH & Co. KG</strong></span><br>
            <span>Bürgermeister-Dr.-Nebel-Str. 19<br>
                 97816 Lohr<br>
                 +49 (0) 9352 8755-0</span>
        </div>
        </div>
         <div class="col-sm-3">
             <div class="grampp-footer-item">
            <img src="img/autohaus-grampp-lohr-vw-audi.png" /><br>
            <span><strong>Peter Grampp GmbH & Co. KG</strong></span><br>
            <span>Am Hammersteig 1<br>
                 97753 Karlstadt<br>
                 +49 (0) 9353 9781-0</span>
        </div>
        </div>
        <div class="col-sm-3">
            <div class="grampp-footer-item">
            <img src="img/clever-driver-logo.png" /><br>
            <span><strong>Clever Driver Lohr</strong></span><br>
            <span>Peter Grampp GmbH & Co. KG<br>
                  Bgm.-Dr.-Nebel-Str. 1a<br>
                  97816 Lohr am Main<br>
                  +49 (0) 9352-8755262</span>
        </div>
        </div>
    </div>
     
     <br><br>
    
    <div class="row">
        <div class="col-sm-3">
            <div class="grampp-footer-item">
            <img src="img/mercedes-benz.png" /><br>
            <span><strong>Autohaus Grampp GmbH</strong></span><br>
            <span>Am Hammersteig 1<br>
                  97753 Karlstadt<br>
                  +49 (0) 9353 9748-0</span>
        </div>
        </div>
        <div class="col-sm-3">
             <div class="grampp-footer-item">
            <h3 style="text-decoration:underline;">Service</h3>
            <li><a href="https://grampp.net/leistungsuebersicht/" target="_blank">Leistungsübersicht</a></li>
            <li><a href="https://grampp.net/autovermietung/" target="_blank">Autovermietung</a></li>
            <li><a href="https://grampp.net/terminanfrage/" target="_blank">Terminanfrage</a></li>
            <li><a href="https://grampp.net/neuwagen/" target="_blank">Neuwagen</a></li>
            <li><a href="https://grampp.net/fahrzeugsuche/" target="_blank">Fahrzeugsuche</a></li>
        </div>
            </div>
         <div class="col-sm-3">
              <div class="grampp-footer-item">
            <h3 style="text-decoration:underline;">Unternehmen</h3>
            <li><a href="https://grampp.net/bewerbungsformular/" target="_blank">Bewerbungsformular</a></li>
            <li><a href="https://grampp.net/ansprechpartner/" target="_blank">Ansprechpartner</a></li>
            <li><a href="https://grampp.net/ausbildung/" target="_blank">Ausbildung</a></li>
            <li><a href="https://grampp.net/presse/" target="_blank">Presse</a></li>
        </div>
             </div>
        <div class="col-sm-3">
             <div class="grampp-footer-item">
            <a href="https://www.facebook.com/autohausgrampp/?fref=ts" target="_blank"><img style="height:30px" src="img/fb-icon.jpg" /></a>
            <a href="https://www.ebay.de/str/autohausergramppvwaudimercedes" target="_blank"><img style="height:30px" src="img/media-ebay-icon.png" /></a>
         </div>
            </div>
    </div>
    <!-- /. STANDORTE -->
     
     <br><br>
    
    <div class="row">
        <div class="col-sm-12">
        <small>* Ergänzende Information zu Verbrauchsangaben: Alle angegebenen Werte wurden nach dem vorgeschriebenen Messverfahren (§ 2 Nr. 5, 6, 6 a Pkw-EnVKV in der jeweils geltenden Fassung) ermittelt. Die Angaben beziehen sich nicht auf ein einzelnes Fahrzeug und sind nicht Bestandteil des Angebots, sondern dienen allein Vergleichszwecken zwischen den verschiedenen Fahrzeugtypen. Weitere aktuelle Informationen zu den einzelnen Fahrzeugen erhalten Sie bei Ihrem Händler. Ermittlung des Verbrauchs auf Grundlage der Serienausstattung. Sonderausstattungen können Verbrauch und Fahrleistungen beeinflussen. Weitere Informationen zum offiziellen Kraftstoffverbrauch und den offiziellen spezifischen CO2-Emissionen neuer Personenkraftwagen können dem „Leitfaden über den Kraftstoffverbrauch, CO2-Emissionen und den Stromverbrauch neuer Personenkraftwagen“ entnommen werden, der an allen Verkaufsstellen und bei Deutschen Automobil Treuhand GmbH (www.dat.de) unentgeltlich erhältlich ist.</small><br><br>
        
            <br><br>
        <center>  
        <p>©2019 Peter Grampp GmbH & Co. KG. Alle Rechte vorbehalten.&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">Datenschutz</a> | <a href="#">Impressum</a></p> 
        </center>  
            
        </div>
    </div>
</div>
        
</section> 

      
      <!--Web Tracking-->
     <script>   
    (function(w,e,b,g,a,i,n,s){w['ITCLKOBJ']=a;w[a]=w[a]||function(){(w[a].q=w[a].q||[]).push(arguments)},w[a].l=1*new Date();i=e.createElement(b),n=e.getElementsByTagName(b)[0];i.async=1;i.src=g;n.parentNode.insertBefore(i,n)})(window,document,'script','https://analytics.webgains.io/clk.min.js','ITCLKQ');
ITCLKQ('set', 'internal.cookie', true);
ITCLKQ('click'); 
    </script>  
    
     <!-- Read the Webgains cookie -->
    <script type="text/javascript ">
        
        // Function to read the cookie
        function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for(var i=0;i < ca.length;i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1,c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
            }
            return null;
        }  
        
        // Check the status of the source 
        var status = "<?=$webgains?>";
        var lead_ref = "";
 
        // Check if the status is true and Webgains cookie is set
        if (readCookie('webgains') && status) { 
           lead_ref = "<?=$lead_reference?>";
        }        

               
    </script>

    <!-- <Webgains Tracking Code> -->
    <!-- <Variablendefinitionen> -->
    <script language="javascript" type="text/javascript">
       
    var wgOrderReference = lead_ref ;
    var wgOrderValue = "0";
    var wgEventID = 1037645;
    var wgComment = "";
    var wgLang = "de_DE";
    var wgsLang = "javascript-client";
    var wgVersion = "1.2";
    var wgProgramID = 269785;
    var wgSubDomain = "track";
    var wgCheckSum = "";
    var wgItems = "";
    var wgVoucherCode = "";
    var wgCustomerID = "";
    var wgCurrency = "EUR";

    console.log(wgOrderReference);

    </script>
    <!-- </Variablendefinitionen> -->

    
    <!-- <Webgains Tracking Code NG> -->
    <script language="javascript" type="text/javascript">
         (function(w,e,b,g,a,i,n,s){w['ITCVROBJ']=a;w[a]=w[a]||function(){
            (w[a].q=w[a].q||[]).push(arguments)},w[a].l=1*new Date();i=e.createElement(b),
            n=e.getElementsByTagName(b)[0];i.async=1;i.src=g;n.parentNode.insertBefore(i,n)
        })(window,document,'script','https://analytics.webgains.io/cvr.min.js','ITCVRQ');
        ITCVRQ('set', 'trk.programId', wgProgramID);
        ITCVRQ('set', 'cvr', {
            value: wgOrderValue,
            currency: wgCurrency,
            language: wgLang,
            eventId: wgEventID,
            orderReference : wgOrderReference,
            comment: wgComment,
            multiple: '',
            checksum: '',
            items: wgItems,
            customerId: wgCustomerID,
            voucherId: wgVoucherCode
        });
        ITCVRQ('conversion');
    </script>
    <!-- </Webgains Tracking Code NG> -->
    <!-- </Webgains Tracking Code> --> 

      
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js">
    </script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js">
    </script>
    <script src="vendor/header-animation/demo-1.js">
    </script>  
    <script src="vendor/header-animation/TweenLite.min.js">
    </script>
    <script src="vendor/header-animation/EasePack.min.js">
    </script>
    <script src="vendor/header-animation/rAF.js">
    </script>
    <script src="vendor/header-animation/demo-1.js">
    </script>  

  </body>

</html>

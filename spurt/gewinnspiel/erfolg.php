
<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="refresh" content="4;url=http://spurtwelt.de/leistungen/car-sponsoring/" />  
    <title>Gewinnspiel 2018 | Teamsportxxl.de</title>
    <!-- implementation bootstrap -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- implementation fontawesome icons -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- implementation simpleline icons -->
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <!-- implementation googlefonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <!-- implementation Animated Header -->
    <!-- implementation custom css -->
    <link href="css/creative.css" rel="stylesheet">
    <!-- implementation animate css -->
    <link href="css/animate.css" rel="stylesheet">
  </head>
    <!-- FACEBOOK PIXEL -->
    
    <!-- ./FACEBOK PIXEL -->
    
    <!-- GOOGLE ANALYTICS -->
    
    <!-- .GOOGLE ANALYTICS -->
  <body>

     <!-- Navigation -->
     <nav class="navbar navbar-light bg-light static-top">
      <div class="container">
          <a class="navbar-brand" style="text-transform: uppercase;" href="https://www.teamsportxxl.de/" target="_blank"><img style="width:150px;" alt="teamsportxxl Gewinnspiel 2018" src="img/teamsportXXL_shop.png"></a>
          <span>JETZT GEWINNEN MIT <strong>TEAMSPORTXXL!</strong></span>
      </div>
    </nav>



    <!-- FORM -->
    <section class="form-container" style="padding-top: 250px; padding-bottom: 250px; height:100%;" id="about">
    <div class="container">  
    <div class="row">
    <div class="col-sm-12">
        <center>
            
            <h1 style="padding: 20px 20px 20px 20px; background-color: rgba(255,255,255,0.7)!important; text-transform: uppercase;">Vielen Dank für deine Anmeldung.</h1>
            
            <p style="padding: 20px 20px 20px 20px; background-color: rgba(255,255,255,0.7)!important;">Gewinner werden per E-Mail benachrichtigt. Teamsportxxl.de wünscht viel Erfolg!</p> 
            
          
        </center>
    </div>
    </div>
    </div>  

</section>  
 <div class="clearspace"></div>     
  <!-- Footer -->
    <footer class="footer bg-light">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 h-100 text-center text-lg-left my-auto" style="height:auto!important">
            <ul class="list-inline mb-2">
                <li class="list-inline-item">
                <a href="https://www.teamsportxxl.de/" target="_blank">Shop</a>
              </li>
              <li class="list-inline-item">&sdot;</li>    
              <li class="list-inline-item">
                <a href="https://www.teamsportxxl.de/Impressum" target="_blank">Impressum</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="https://www.teamsportxxl.de/AGB" target="_blank">AGB</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="https://www.teamsportxxl.de/Datenschutz" target="_blank">Datenschutz</a>
              </li>
            </ul>
            <p class="text-muted small mb-4 mb-lg-0">JETZT GEWINNEN MIT TEAMSPORTXXL!</p>
          </div>
          <div class="col-lg-6 h-100 text-center text-lg-right my-auto" style="height:auto!important">
            <ul class="list-inline mb-0">
              <li class="list-inline-item mr-3">
               <img src="img/claim.png" />
              </li>
             
            </ul>
          </div>
            
        <div class="ds">
        <div class="col-sm-12">
        <div class="container">
        <div class="row">
        <center><span style="font-size: 12px;">* Die Online-Registrierung ist nur bei Abgabe einer Werbeeinwilligung möglich. Sie können aber auch ohne Werbeeinwilligung teilnehmen, wenn Sie uns innerhalb der Teilnahmefrist eine E-Mail an <a href="mailto:widerruf@spurt.de">widerruf@spurt.de</a> senden und in der E-Mail Ihren Namen, Ihre Telefonnummer und den Hauptpreis des Gewinnspiels angeben. Ihre Gewinnchancen werden durch die Art der Teilnahme nicht beeinflusst.</span></center>  
        </div>
        </div>
        </div>
        </div>
            
        </div>   
      </div>
    </footer>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js">
    </script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js">
    </script>
    <script src="vendor/header-animation/demo-1.js">
    </script>  
    <script src="vendor/header-animation/TweenLite.min.js">
    </script>
    <script src="vendor/header-animation/EasePack.min.js">
    </script>
    <script src="vendor/header-animation/rAF.js">
    </script>
    <script src="vendor/header-animation/demo-1.js">
    </script>  

  </body>

</html>
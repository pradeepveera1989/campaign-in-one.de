<?php 

	/* ______________________________________________________________________________________________________________________________________________
	 *
	 MAIL IN ONE - SPURT - Kampagne Gewinnspiel - teamsportxxlGS1805
	 * _______________________________________________________________________________________________________________________________________________
	 */

      //vorname  
      $standard_01 = $_GET["1"];
      //nachname
      $standard_02 = $_GET["2"];
      //email
      $email_02    = $_GET["3"];
      //plz
      $standard_04 = $_GET["4"]; 
      //ort
      $standard_05 = $_GET["5"]; 
      //trafficsource
      $ts = $_GET["trafficsource"];  

      $standard_01  = str_replace(' ','+',$standard_01);
      $standard_001   = base64_decode($standard_01);

      $standard_02  = str_replace(' ','+',$standard_02);
      $standard_002   = base64_decode($standard_02);

      $email_02  = str_replace(' ','+',$email_02);
      $email_002   = base64_decode($email_02);

      $standard_04  = str_replace(' ','+',$standard_04);
      $standard_004   = base64_decode($standard_04);

      $standard_05  = str_replace(' ','+',$standard_05);
      $standard_005   = base64_decode($standard_05);

	// Update with Hubspot 
	require_once('../../php-client/hubspot-php-client/class.contacts.php'); 
	require_once('../../php-client/hubspot-php-client/class.company.php'); 
	require_once('../../php-client/hubspot-php-client/class.deals.php'); 
	   
	// Email Validation
	require_once('../../php-client/emailvalidation-php-client/emailchecker.php');
	  
	// Update with your version of the Maileon PHP API Client
	require_once('../../php-client/maileon-php-client-1.3.1/client/MaileonApiClient.php');

	// Get the config data from config.ini       
	$config = parse_ini_file("config.ini", true);

	$config_MailInOne = array(
		'BASE_URI' => 'https://api.maileon.com/1.0',
		'API_KEY' => $config[MailInOne_Settings][Mapikey],
		'THROW_EXCEPTION' => true,
		'TIMEOUT' => 60,
		'DEBUG' => 'false' // NEVER enable on production
	);

    $contactid = '';
	$checksum = '';
    $mailingid = "";
	$resp;
	session_start();

    if( isset($_GET['contactid']) ) {
		$contactid = $_GET['contactid'];
	} elseif( isset($_POST['contactid']) ) {
		$contactid = $_POST['contactid'];
	}

	if( isset($_GET['checksum']) ) {
		$checksum = $_GET['checksum'];
	} elseif( isset($_POST['checksum']) ) {
		$checksum = $_POST['checksum'];
	}

    if( isset($_GET['mailingid']) ) {
		$checksum = $_GET['mailingid'];
	} elseif( isset($_POST['mailngid']) ) {
		$checksum = $_POST['mailingid'];
	}

	if( isset($_POST['email']) ) {
		
		$email = $_POST['email'];
		// Validate Email address
		$emailchecker = new emailChecker($email);
		$emailchecker->curlRequest();
		$emailchecker->curlResponce();
		$resp = $emailchecker->parseResponce();
		if (!$resp) {
			// invalid email address
			// Copying the data to session and intiating a sesssion variable error.
			$_SESSION['data'] = $_POST;
			$_SESSION['error'] = "email";
			echo '<script type="text/javascript">';
			echo 'window.location ="#form"';
			echo '</script>';  
		} else {
			$_SESSION['error'] = "";
			session_destroy();
			$_SESSION['data'] = array();
		
			$standard_0 = $_POST["anrede"];
			$standard_1 = $_POST["vorname"];
			$standard_2 = $_POST["nachname"];
			$standard_3 = $_POST["plz"];
			$standard_4 = $_POST["ort"];

			$custom_0 = $_POST["quelle"];
			
			$ip = $_SERVER['REMOTE_ADDR'];
			$url = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
			
			$trafficsource = $_POST["ts"];
            
            /* HUBSPOT
            
			//Hubspot Configuration Sync            
			if ($config[HubSpotCredentials][Sync]) {
				$Hapikey = $config[HubSpotCredentials][hubapi];
				$contactHSpot = new HubSpot_Contacts($Hapikey);
				$companyHSpot = new HubSpot_Company($Hapikey);
				$dealHSpot = new HubSpot_Deal($Hapikey);

				// Contact HubSpot
				$contactparmsHSpot = array(
					'salutation' => $standard_0,
					'firstname' => $standard_1,
					'lastname' => $standard_2,
					'zip' => $standard_3,
					'city' => $standard_4,
					'email' => $email,
					'phone' => $custom_3
				);
				// Get the contact by email Id
				$contactByEmailHSpot = $contactHSpot->get_contact_by_email($email);
				if (!is_null($contactByEmailHSpot->vid)) {
					$contactIdHSpot = $contactByEmailHSpot->vid;
					$updatedContactHSpot = $contactHSpot->update_contact($contactIdHSpot, $contactparmsHSpot);

					if (is_null($updatedContactHSpot->vid)) {
						error_log("\n Error:Unable to update the contact details", 3, "error.log");
					}
				} else {
					// Create a new contact on Hubspot
					$contactIdHSpot = $contactHSpot->create_contact($contactparmsHSpot);

					$contactIdHSpot = $contactIdHSpot->vid;
					if (is_null($contactIdHSpot)) {
						error_log("\n Error:Unable to create a contact details", 3, "error.log");
					}
				}

				// Company HubSpot
				$companyparmHSpot = array(
					'name' => $config[HubSpotCompany][name],
					'domain' => $config[HubSpotCompany][domain],
					'description' => $config[HubSpotCompany][description],
					'phone' => $config[HubSpotCompany][phone],
					'zip' => $config[HubSpotCompany][zip],
				);

				$companyparmSearch = array(
					'limit' => 5,
					'requestOptions' => array(
						'properties' => array(
							0 => 'domain',
							1 => 'createdate',
							1 => 'name',
							2 => 'hs_lastmodifieddate',
						),
					),
					'offset' => array(
						'isPrimary' => true,
						'companyId' => 0,
					),
				);

				// HubSpot contact check by email.
				$searchByDomainHSpot = $companyHSpot->get_company_by_domain($config[HubSpotCompany][domain], $companyparmSearch);
				$countDomains = sizeof($searchByDomainHSpot->results);

				// Company does not exist
				if ($countDomains == 0) {

					if (isset($config[HubSpotCompany][domain])) {
						$companyIdHSpot = $companyHSpot->create_company($companyparmHSpot);
						$companyIdHSpot = $companyIdHSpot->companyId;
						if (is_null($companyIdHSpot)) {
							error_log("Error: Unable to create company", 3, "error.log");
						}
					} else {
						// Dont create a new company
						$companyIdHSpot = 0;
					}
				} else {
					// Update the new company 
					foreach ($searchByDomainHSpot->results as $domain) {
						$companyIdHSpot = $domain->companyId;
					}
				}

				// Link Contact to Company
				if (isset($contactIdHSpot)) {
					// Contact Company Association Id = CCA
					if (isset($companyIdHSpot)) {
						$cca = $companyHSpot->assosiate_company_to_contact($contactIdHSpot, $companyIdHSpot);
					}
					if(!isset($companyIdHSpot)){
						$companyIdHSpot = 0;	
					}	
					$dealAssociation = array(
						"associations" => array(
					//		"associatedCompanyIds" => array(
					//			0 => $companyIdHSpot
					//		),
							"associatedVids" => array(
								0 => $contactIdHSpot
							),
						),
					);

					$dealname = $config[HubSpotDeal][dealname]." ".$standard_1." ".$standard_2;
					$dealParms = array(
						"dealname" => $dealname,
						"amount" => $config[HubSpotDeal][amount],
						"segment" => $config[HubSpotDeal][segment],
						"dealstage" => $config[HubSpotDeal][dealstage],
						"pipeline" => $config[HubSpotDeal][pipeline],
							// "hubspot_owner_id" => $config[HubSpotDeal][hubspot_owner_id],
					);

					// Check for the deal
					$dealCreated = false;
					$recentDeals = $dealHSpot->get_recent_deals()->results;
					foreach ($recentDeals as $deals) {
						if ($deals->properties->dealname->value === $dealname) {
							$dealCreated = true;
							$dealId = $deals->dealId;
						}
					}

					if (empty($recentDeals) || !$dealCreated || empty($dealId)) {
						// Associate contact and company with new Deal
						$dcc = $dealHSpot->create_deal($dealAssociation, $dealParms);
						$dealId = $dcc->dealId;
					} 
					$dccontact = $dealHSpot->associate_deal_with_contact($dealId, $contactIdHSpot);
					$dccompany = $dealHSpot->associate_deal_with_company($dealId, $companyIdHSpot);
				}
			}						

           HUBSPOT */
            
            
            if ($config[MailInOne_Settings][Sync]) {				
					
				$contactsService = new com_maileon_api_contacts_ContactsService($config_MailInOne);
				$contactsService->setDebug(false);
            
             function updateContact($email,$custom_0, $config_MailInOne) {

                $debug = FALSE;
                $contactsService = new com_maileon_api_contacts_ContactsService($config_MailInOne);
                $contactsService->setDebug($debug);
                $newContact = new com_maileon_api_contacts_Contact();
                $newContact->email = $email;

                $newContact->custom_fields["Quelle"] = 'teamsportxxlgs1805';

                $response = $contactsService->createContact($newContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, "", "", false);
                return $response;
            }

                $getContact = $contactsService->getContactByEmail($email);
                if ($getContact->isSuccess() && $getContact->getResult()->permission != com_maileon_api_contacts_Permission::$NONE) {


                $response = updateContact($email, $custom_0, $config_MailInOne);


                        echo '<script type="text/javascript">';
                        echo 'window.location.href="https://spurt.campaign-in-one.de/gewinnspiel/redirect.php";';
                        echo '</script>'; 

              } else {
              
            
            if (isset($_POST["thema_handball"]) && $_POST["thema_handball"] == 'check')
            {
                $spurtwelt_handball = true;
                $teamsport_handball = true;
            }
            else
            {
            
                $spurtwelt_handball = false;
                $teamsport_handball = false;
            }
         if (isset($_POST["thema_fussball"]) && $_POST["thema_fussball"] == 'check')
            {
                
                $spurtwelt_fussball = true;
                $teamsport_fussball = true;
            }
            else
            {
               
                $spurtwelt_fussball = false;
                $teamsport_fussball = false;
            }
         if (isset($_POST["thema_volleyball"]) && $_POST["thema_volleyball"] == 'check')
            {
               
                $spurtwelt_volleyball = true;
                $teamsport_volleyball = true;
            }
            else
            { 
                $spurtwelt_volleyball = false;
                $teamsport_volleyball = false;
            }
         if (isset($_POST["thema_turner"]) && $_POST["thema_turner"] == 'check')
            {
               
                $spurtwelt_turner = true;
                $teamsport_turner = true;
            }
            else
            {
                
                $spurtwelt_turner = false;
                $teamsport_turner = false;
            }
         if (isset($_POST["thema_leichtathletik"]) && $_POST["thema_leichtathletik"] == 'check')
            {
                $spurtwelt_leichtathletik = true;
            }
            else
            {
                $spurtwelt_leichtathletik = false;
            }
         if (isset($_POST["thema_sonstige"]) && $_POST["thema_sonstige"] == 'check')
            {
               
                $spurtwelt_sonstige = true;
                $teamsport_sonstige = true;
            }
            else
            {
                
                $spurtwelt_sonstige = false;
                $teamsport_sonstige = false;
            }
        
        if (isset($_POST["thema_alle"]) && $_POST["thema_alle"] == 'check')
            {
                
                $spurtwelt_fussball = true;
                $spurtwelt_handball = true;
                $spurtwelt_sonstige = true;
                $spurtwelt_turner = true;
                $spurtwelt_volleyball = true;
                $spurtwelt_leichtathletik = true;

                $teamsport_fussball = true;
                $teamsport_handball = true;
                $teamsport_sonstige = true;
                $teamsport_turner= true;
                $teamsport_volleyball = true; 
                
            }
                    
                    
					$newContact = new com_maileon_api_contacts_Contact();
					$newContact->email = $email;
					$newContact->anonymous = false;
					$newContact->permission = com_maileon_api_contacts_Permission::$NONE;
					$newContact->standard_fields["SALUTATION"]  = $standard_0;
					$newContact->standard_fields["FIRSTNAME"]   = $standard_1;    
					$newContact->standard_fields["LASTNAME"]    = $standard_2;
					$newContact->standard_fields["ZIP"]         = $standard_3;
					$newContact->standard_fields["CITY"]        = $standard_4;
					$newContact->custom_fields["Quelle"]        = $custom_0;
					
					$newContact->custom_fields["ip_adresse"]    = $ip;
					$newContact->custom_fields["url"]           = $url;
					$newContact->custom_fields["trafficsource"] = $trafficsource;
                    
                    $newContact->custom_fields["Mandant HandballXXL"] = TRUE;
                    $newContact->custom_fields["Mandant Spurtwelt"] = TRUE;
                    $newContact->custom_fields["Mandant TeamsportXXL"] = TRUE; 

                    $newContact->custom_fields["Spurtwelt_Fußball"] = $spurtwelt_fussball;
                    $newContact->custom_fields["Spurtwelt_HandballXXL"] = $spurtwelt_handball;
                    $newContact->custom_fields["Spurtwelt_Sonstige"] = $spurtwelt_sonstige;
                    $newContact->custom_fields["Spurtwelt_Turner"] = $spurtwelt_turner;
                    $newContact->custom_fields["Spurtwelt_Volleyball"] = $spurtwelt_volleyball;
                    $newContact->custom_fields["Spurtwelt_Leichtathletik"] = $spurtwelt_leichtathletik;

                    $newContact->custom_fields["TeamsportXXL_Fußball"] = $teamsport_fussball;
                    $newContact->custom_fields["TeamsportXXL Handball"] = $teamsport_handball;
                    $newContact->custom_fields["TeamsportXXL Sonstige"] = $teamsport_sonstige;
                    $newContact->custom_fields["TeamsportXXL Turner"] = $teamsport_turner;
                    $newContact->custom_fields["TeamsportXXL Volleyball"] = $teamsport_volleyball; 
					
					$response = $contactsService->createContact($newContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, '', '', true, true, $config[MailInOne_Settings][DOIKey]);
					
				
					echo '<script type="text/javascript">';
					echo 'window.location.href="https://spurt.campaign-in-one.de/gewinnspiel/redirect.php";';
					echo '</script>'; 					
                }
            }
        }    
	} else {
		$email = "";
		$standard_0 = "";
		$standard_1 = "";
		$standard_2 = "";
		$standard_3 = "";
        $standard_4 = "";
        $custom_0 = "";
        $custom_1 = "";
        $custom_2 = "";
        $custom_3 = "";
        $ip = "";
        $url = "";
        $trafficsource = "";    
	}
  
?>

<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Wir sind der verlässliche Partner für Sportler, Vereins-mitarbeiter und Industriepartner, die den Sport fördern wollen.">
    <meta name="author" content="Teamsportxxl.de">
      
    <title>Gewinnspiel 2018 | Teamsportxxl.de</title>
      
    <link href="css/creative.css" rel="stylesheet" type="text/css">  
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">    
  </head>

    
    <!-- FACEBOOK PIXEL -->
    
     <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '1741387319271621');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=1741387319271621&ev=PageView&noscript=1"
    /></noscript>
    
    <!-- ./FACEBOK PIXEL -->
    
    <!-- GOOGLE ANALYTICS -->
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-118906696-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
 
  gtag('config', 'UA-118906696-2');
</script>

    <!-- .GOOGLE ANALYTICS -->
    
  <body>
 
    <nav class="nav-top">
       <div class="container">
           <div class="row">
               <span class="nav-top-text"><strong>&#10004;&nbsp;&nbsp;Über 20 Jahre Erfahrung</strong></span>
               <span class="nav-top-text"><strong>&#10004;&nbsp;&nbsp;Rechnungskauf</strong></span>
               <span class="nav-top-text"><strong>&#10004;&nbsp;&nbsp;Versand mit DHL</strong></span>
               <span class="nav-top-text"><strong>&#10004;&nbsp;&nbsp;Günstige Beflockung</strong></span>
               <span class="nav-top-text"><strong>&#10004;&nbsp;&nbsp;Gratisversand ab 100 EUR</strong></span>
               <span class="nav-top-text"><strong>&nbsp;&nbsp;&nbsp;Hotline: 02841 6023930 </strong></span>
           </div>
        </div>   
    </nav>
      
    <nav class="navbar navbar-light bg-light static-top">
      <div class="container">
          <a class="navbar-brand" style="text-transform: uppercase;" href="https://www.teamsportxxl.de/" target="_blank"><img style="width:150px;" alt="teamsportxxl Gewinnspiel 2018" src="img/teamsportXXL_shop.png"></a>
          <span>JETZT GEWINNEN MIT <strong>TEAMSPORTXXL!</strong></span>
      </div>
    </nav>

    <section id="about" class="form-container">
    <div class="container">
    <div class="row">
        
        <div class="col-lg-6 form-container-mobile">       
            <center><img style="margin-top:25px; width: 250px;" src="img/stoerer.png" alt="teamsportxxl"/></center>
        </div>  

        <div class="col-lg-5 gewinn-form">       
            <img style="margin-top:150px;" src="img/stoerer.png" alt="teamsportxxl"/>
        </div>  
        
        <div class="col-lg-7 check-submit-form">     
         <p>&nbsp;</p>
            
            <div class="container">
                <div class="row justify-content-center">
                
                <!-- FORM DESKTOP-->    
               
                <form class="form-desktop" id="api-data-form" action="<?= $_SERVER['PHP_SELF'] ?>" method="post" accept-charset="utf-8" class="form-horizontal" role="form">
				<input type="hidden" name="contactid" value="<?= $contactid ?>">
				<input type="hidden" name="checksum" value="<?= $checksum ?>">
                <input type="hidden" name="mailingid" value="<?= $mailingid ?>">
             
                    <!-- INPUT ANREDE FRAU & HERR -->
					<div class="form-group">
						<div class="col-sm-12">
                            <div class="btn-group">
                                  <label class="btn btn-outline-secondary" style="border: 1px solid transparent;">
                                    <input value="Frau" style="width: 15px; height: 15px;" type="radio" name="anrede" autocomplete="off" required>
                                      <span style="color:#000!important;"> Frau</span>
                                  </label>
                                    <span class="input-group-btn" style="width:15px;"></span> 
                                  <label class="btn btn-outline-secondary" style="border: 1px solid transparent;">
                                    <input value="Herr" style="width: 15px; height: 15px;" type="radio" name="anrede" autocomplete="off">
                                      <span style="color:#000!important;"> Herr</span>
                                  </label>
							</div>  
						</div>       
					</div>
                
                    <!-- INPUT VORNAME & NACHNAME -->
					<div class="form-group">
						<div class="col-sm-12">
							<div class="input-group">
								<input value="<?php echo $_SESSION['data']['vorname'];?>" class="input-fields form-control" id="" name="vorname" placeholder="*Vorname" type="text" required onblur="if (this.value == '') {this.value = '<?= $standard_001?>';}" onfocus="if (this.value == '<?= $standard_001?>') {this.value = '';}" onpaste="return false;" onCopy="return false;" onCut="return false;"> 
                                <span class="input-group-btn" style="width:15px;"></span> 
                                <input value="<?php echo $_SESSION['data']['nachname'];?>" class="input-fields form-control" id="nachname" name="nachname" placeholder="*Nachname" type="text" required onblur="if (this.value == '') {this.value = '<?= $standard_002?>';}" onfocus="if (this.value == '<?= $standard_002?>') {this.value = '';}" onpaste="return false;" onCopy="return false;" onCut="return false;">
							</div>
						</div>       
					</div>
                
                    <!-- INPUT EMAIL & TELEFON -->
					<div class="form-group">
						<div class="col-sm-12">
							<div class="input-group">
								<input value="<?php echo  $_SESSION['data']['email'];?>" class="input-fields form-control email" id="email" name="email" placeholder="*E-Mail-Adresse" type="email" required onblur="if (this.value == '') {this.value = '<?= $email_002?>';}" onfocus="if (this.value == '<?= $email_002?>') {this.value = '';}" onpaste="return false;" onCopy="return false;" onCut="return false;"> 
							</div>
                            <?php if($_SESSION['error'] === "email"){ ?>   
                                <span class="p-light erroremail"  style="color:<?php echo $config[Email_Address][ErrorMsg_Color]; ?>"><?php echo $config[Email_Address][ErrorMsg]; ?></span>
                            <?php } ?>  							
						</div>       
					</div>
                    
                     <script>
                                   function setC(checked) {
                                        var elm = document.getElementById('check-all');
                                        if (checked != elm.checked) {
                                            elm.click();
                                        }
                                    }
                                </script>
                    
                        <div style="padding: 5px 0px 15px 0px" class="checkbox">
                           <div class="col-sm-12">
                               <strong>Thema:</strong><br>
                               <label><input name="thema_alle" aria-label="..." id="check-all" type="checkbox" value="check" checked>Alle Themen&nbsp;</label>
                               <label><input onclick="setC(false)" name="thema_handball" aria-label="..." id="check-1" type="checkbox" value="check" >Handball&nbsp;</label> 
                               <label><input onclick="setC(false)" name="thema_fussball" aria-label="..." id="check-2" type="checkbox" value="check" >Fußball&nbsp;</label>
                               <label><input onclick="setC(false)" name="thema_volleyball" aria-label="..." id="check-3" type="checkbox" value="check" >Volleyball&nbsp;</label>
                               <label><input onclick="setC(false)" name="thema_turner" aria-label="..." id="check-4" type="checkbox" value="check" >Turner&nbsp;</label>
                               <label><input onclick="setC(false)" name="thema_leichtathletik" aria-label="..." id="check-5" type="checkbox" value="check" >Leichtathletik&nbsp;</label>
                               <label><input onclick="setC(false)" name="thema_sonstige" aria-label="..." id="check-6" type="checkbox" value="check" >Sonstige&nbsp;</label>            
				            </div>
				            </div>
                    
                    <label class="form-group">
                        <div class="col-sm-12">
                        <input style="width: 20px; height: 20px;" type="checkbox" required>
                            <span class="p-dark">&nbsp;&nbsp;Ich bin einverstanden, dass die SPURT GmbH, Industriedenkmal Rheinpreussen, Schacht 4, Franz-Haniel-Straße 20, 47443 Moers, meine bei der Gewinnspielregistrierung eingetragenen Daten nutzt und mir E-Mails zuschickt bzw. mich anruft, um mir Angebote aus den Bereichen Teamsport und Car-Sponsoring zukommen zu lassen. Diese Einwilligung* kann ich jederzeit widerrufen, etwa durch einen Brief an die oben genannte Adresse oder durch eine E-Mail an <a href="mailto:widerruf@spurt.de">widerruf@spurt.de.</a> Anschließend wird jede werbliche Nutzung unterbleiben.</span>
                        </div>    
                    </label>
                    
    
                      <div class="form-group hidden">
                      <label class="col-sm-2 control-label" for="">TRAFFICSOURCE</label>
                      <div class="col-sm-10">
                      <input class="form-control"  value="<?= $ts?>" placeholder="" type="text" name="ts" id="ts" value="">
                      </div>
                      </div>
                
                      <div class="form-group hidden">
                      <label class="col-sm-2 control-label" for="">Quelle</label>
                      <div class="col-sm-10">
                      <input class="form-control"  placeholder="" type="text" name="quelle" id="quelle" value="teamsportxxlgs1805">
                      </div>
                      </div>
                
  
				<div class="form-group"> 
                    <div class="col-sm-12">
					<input style="margin-top: 25px; cursor: pointer;" type="submit" class="col-sm-12 btn btn-danger btn-xl js-scroll-trigger" value="Jetzt teilnehmen und gewinnen!" name="submit" style="width: 100%;">
                </div>        
				</div>
               <?php if( isset($response) && $response->isSuccess() ) { ?>
					<div class="alert alert-success fade in">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<strong>Subscription successful</strong>
					</div>
				<?php } elseif( isset($warning) ) { ?>
					<div class="alert alert-warning fade in">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<strong style="color: red; z-index: 1000;">Subscription failed</strong>
						<?= $warning['message'] ?>
					</div>
				<?php } elseif( isset($response) ) { ?>
					<div class="alert alert-danger fade in">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<strong>Subscription failed</strong>
					</div>
				<?php } ?>
			</form>  
                    
                    
                          <!-- FORM MOBILE-->    
                             <div class="col-lg-6 form-mobile check-submit-form">     
                            <form id="api-data-form" action="<?= $_SERVER['PHP_SELF'] ?>" method="post" accept-charset="utf-8" class="form-horizontal" role="form">
                            <input type="hidden" name="contactid" value="<?= $contactid ?>">
                            <input type="hidden" name="checksum" value="<?= $checksum ?>">
                            <input type="hidden" name="mailingid" value="<?= $mailingid ?>">

                                <!-- INPUT ANREDE FRAU & HERR -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="btn-group">
                                              <label class="btn btn-outline-secondary" style="border: 1px solid transparent;">
                                                <input value="Frau" style="width: 15px; height: 15px;" type="radio" name="anrede" autocomplete="off" required> Frau
                                              </label>
                                                <span class="input-group-btn" style="width:15px;"></span> 
                                              <label class="btn btn-outline-secondary" style="border: 1px solid transparent;">
                                                <input value="Herr" style="width: 15px; height: 15px;" type="radio" name="anrede" autocomplete="off"> Herr
                                              </label>
                                        </div>  
                                    </div>       
                                </div>

                                <!-- INPUT VORNAME -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <input value="<?php echo  $_SESSION['data']['vorname'];?>" class="input-fields form-control" id="" name="vorname" placeholder="*Vorname" type="text" required onblur="if (this.value == '') {this.value = '<?= $standard_001?>';}"     onfocus="if (this.value == '<?= $standard_001?>') {this.value = '';}" onpaste="return false;" onCopy="return false;" onCut="return false;">                                             
                                        </div>
                                    </div>       
                                </div>
                                
                                
                                <!-- INPUT NACHNAME -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <input value="<?php echo  $_SESSION['data']['nachname'];?>" class="input-fields form-control" id="nachname" name="nachname" placeholder="*Nachname" type="text" required onblur="if (this.value == '') {this.value = '<?= $standard_002?>';}" onfocus="if (this.value == '<?= $standard_002?>') {this.value = '';}" onpaste="return false;" onCopy="return false;" onCut="return false;">
                                        </div>
                                    </div>       
                                </div>

                                <!-- INPUT EMAIL -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <input value="<?php echo  $_SESSION['data']['email'];?>" class="input-fields form-control email" id="email" name="email" placeholder="*E-Mail-Adresse" type="email" required onblur="if (this.value == '') {this.value = '<?= $email_002?>';}" onfocus="if (this.value == '<?= $email_002?>') {this.value = '';}" onpaste="return false;" onCopy="return false;" onCut="return false;"> 
                                        </div>
                                        <?php if($_SESSION['error'] === "email"){ ?>   
                                             <span class="p-light erroremail"  style="color:<?php echo $config[Email_Address][ErrorMsg_Color]; ?>"><?php echo $config[Email_Address][ErrorMsg]; ?></span>
                                         <?php } ?>  										
                                    </div>       
                                </div>
                                
                                <script>
                                   function setA(checked) {
                                        var elm1 = document.getElementById('check-all-mobile');
                                        if (checked != elm1.checked) {
                                            elm1.click();
                                        }
                                    }
                                </script>
                                
                               <div style="padding: 5px 0px 15px 0px" class="checkbox">
                               <div class="col-sm-12">
                                   <strong>Thema:</strong><br>
                                   <label><input name="thema_alle" aria-label="..." id="check-all-mobile" type="checkbox" value="check" checked>Alle Themen&nbsp;</label><br>
                                   <label><input onclick="setA(false)" name="thema_handball" aria-label="..." id="check-1" type="checkbox" value="check" >Handball&nbsp;</label> <br>
                                   <label><input onclick="setA(false)" name="thema_fussball" aria-label="..." id="check-2" type="checkbox" value="check" >Fußball&nbsp;</label><br>
                                   <label><input onclick="setA(false)" name="thema_volleyball" aria-label="..." id="check-3" type="checkbox" value="check" >Volleyball&nbsp;</label><br>
                                   <label><input onclick="setA(false)" name="thema_turner" aria-label="..." id="check-4" type="checkbox" value="check" >Turner&nbsp;</label><br>
                                   <label><input onclick="setA(false)" name="thema_leichtathletik" aria-label="..." id="check-5" type="checkbox" value="check" >Leichtathletik&nbsp;</label><br>
                                   <label><input onclick="setA(false)" name="thema_sonstige" aria-label="..." id="check-6" type="checkbox" value="check" >Sonstige&nbsp;</label><br>            
                                </div>
                                </div>
                               
                                <label class="form-group">
                                    <div class="col-sm-12">
                                    <input style="width: 20px; height: 20px;" type="checkbox" required>
                                        <span class="p-dark">&nbsp;&nbsp;Ich bin einverstanden, dass die SPURT GmbH, Industriedenkmal Rheinpreussen, Schacht 4, Franz-Haniel-Straße 20, 47443 Moers, meine bei der Gewinnspielregistrierung eingetragenen Daten nutzt und mir E-Mails zuschickt bzw. mich anruft, um mir Angebote aus den Bereichen Teamsport und Car-Sponsoring zukommen zu lassen. Diese Einwilligung* kann ich jederzeit widerrufen, etwa durch einen Brief an die oben genannte Adresse oder durch eine E-Mail an <a href="mailto:widerruf@spurt.de">widerruf@spurt.de.</a> Anschließend wird jede werbliche Nutzung unterbleiben.</span>
                                    </div>    
                                </label>

                                  <div class="form-group hidden">
                                  <label class="col-sm-2 control-label" for="">TRAFFICSOURCE</label>
                                  <div class="col-sm-10">
                                  <input class="form-control"  value="<?= $ts?>" placeholder="" type="text" name="ts" id="ts" value="">
                                  </div>
                                  </div>

                                  <div class="form-group hidden">
                                  <label class="col-sm-2 control-label" for="">Quelle</label>
                                  <div class="col-sm-10">
                                  <input class="form-control"  placeholder="" type="text" name="quelle" id="quelle" value="teamsportxxlgs1805">
                                  </div>
                                  </div>


                            <div class="form-group"> 
                                <div class="col-sm-12">
                                <input style="margin-top: 25px; cursor: pointer;" type="submit" class="col-sm-12 btn btn-danger btn-xl js-scroll-trigger" value="Jetzt teilnehmen!" name="submit" style="width: 100%;">
                            </div>        
                            </div>
                           <?php if( isset($response) && $response->isSuccess() ) { ?>
                                <div class="alert alert-success fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Subscription successful</strong>
                                </div>
                            <?php } elseif( isset($warning) ) { ?>
                                <div class="alert alert-warning fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong style="color: red; z-index: 1000;">Subscription failed</strong>
                                    <?= $warning['message'] ?>
                                </div>
                            <?php } elseif( isset($response) ) { ?>
                                <div class="alert alert-danger fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Subscription failed</strong>
                                </div>
                            <?php } ?>
                        </form>          
                        
                        
                    </div>
                </div>
            </div>
         </div>        
            
         <p>&nbsp;</p>    
    
        </div>
        </div>
    </section> 
      

       <section>
         <div style="height:100%!important; width:100%!important; padding: 25px 25px 25px 25px!important;" class="clearspace">     
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center"> 
                    <h2 style="color:#fff!important;">Unter allen Teilnehmern werden Gewinne im Wert von mehr als 1.500€ verlost:</h2>
                    <h2 style="color:#fff!important;"><i class="fas fa-angle-double-right"></i>&nbsp;Platz 1-30: 50€-Einkaufsgutschein&nbsp;<i class="fas fa-angle-double-left"></i></h2>
                    <h2 style="color:#fff!important;"><i class="fas fa-angle-double-right"></i>&nbsp;Platz 31-70: 10%-Rabattgutschein&nbsp;<i class="fas fa-angle-double-left"></i></h2>
                     <h2 style="color:#fff!important;">für deinen nächsten Einkauf unter teamsportxxl.de</h2>   
                        
              </div>     
             </div>
            </div>
            </div>
        </section> 

    

    <section class="partner-container"  style="background-image: url('img/bg4.jpg');">
    <div class="container">
    <div class="row">
         <div class="col-sm-6 model-feature">       
            <h2>ÜBER 20 JAHRE ERFAHRUNG IM SPORT</h2>
            <hr>
            <p class="lead mb-0">Wir sind der verlässliche Partner für Sportler, Vereinsmitarbeiter und Industriepartner, die den Sport fördern wollen. Durch unsere langjährige direkte Verbindung zum Landessportbund NRW wissen wir, worauf es im Vereinsleben ankommt. Unsere Mitarbeiter sind selbst in verschiedenen Sportbereichen tätig und beraten dich gern!</p><br>
        </div>  
        
            </div>
        </div>  
    </section>  
    
	  <section>
         <div style="height:100%!important; padding: 25px 25px 25px 25px!important;" class="clearspace">
            <div class="col-lg-12 text-center">
           
            <div class="container">
                <div class="row">
                 <!--<div class="overlay"></div>-->
      <div class="container">
        <div class="row">
          <div class="col-xl-12 mx-auto">
            <h2 style="color: #fff;">Gewinnspielzeitraum: 15.05.2018 bis 15.07.2018 - 23:59 Uhr.</h2>
          </div>
            
          </div>
        </div>
          </div>     
         </div>
        </div>
        </div>
    </section> 
       
    <section class="partner-container" style="background-position: left!important; background-image: url('img/bg.jpg');">
    <div class="container">
    <div class="row">
                <div class="col-sm-6 text-center">
                    <!--<img style="width: 350px;" class="hidden-model" src="img/stimawell-model_3.png" alt="ems-training zu Hause"/>-->
                </div>
        <div class="col-sm-6  model-feature">       
              <h2>PARTNERCLUB</h2>
              <hr>
              <p class="lead mb-0">Wir sind da, wenn du etwas brauchst! Durch unser Partnerclub-Angebot profitieren du und deine Vereinsmitglieder zum Beispiel von dauerhaft garantierten Sonderrabatten auf Original-Markenartikel. Dabei spielt das Volumen der Bestellungen, die du oder deine Sportkameraden tätigen, keine Rolle. Lasse dich von uns beraten und spare viel Geld.</p>
        </div>  
            </div>
        </div>  
    </section>  
      
   
      
      <section>
        <div class="clearspace"></div>
    </section>
  
    <section class="testimonials text-center bg-light">
      <div class="container">
                        <p>&nbsp;</p>

          <h3 style="color: #3c3b3b;">TOP MARKEN QUALITÄT</h3> 
             
              
              <p>&nbsp;</p>
        <div class="row">
          <div class="col-lg-3">
            <div class=" mx-auto mb-5 mb-lg-0">  
              <img style="width: 200px;" class="img-fluid  mb-3" src="img/hersteller_erima.png" alt="herstelle erima">
       
            </div>
          </div>
          <div class="col-lg-3">
            <div class=" mx-auto mb-5 mb-lg-0">
              <img style="width: 200px;" class="img-fluid  mb-3" src="img/hersteller_hummel.png" alt="herstelle hummel">
              
            </div>
          </div>
          <div class="col-lg-3">
            <div class="mx-auto mb-5 mb-lg-0">
              <img style="width: 200px;" class="img-fluid  mb-3" src="img/hersteller_jako.png" alt="herstelle jako">
              
            </div>
          </div>
            <div class="col-lg-3">
            <div class="mx-auto mb-5 mb-lg-0">
              <img style="width: 200px;" class="img-fluid  mb-3" src="img/hersteller_puma.png" alt="herstelle puma">
              
            </div>
          </div>
        </div>
      </div>
    </section> 
 
   <section>
         <div style="height:100%!important; padding: 25px 25px 25px 25px!important;" class="clearspace">
            <div class="col-lg-12 text-center">
           
            <div class="container">
                <div class="row">
                 <!--<div class="overlay"></div>-->
      <div class="container">
        <div class="row">
          <div class="col-xl-12 mx-auto">
              <p>&nbsp;</p>
            <h2 style="color: #fff;">DAS TEMSPORTXXL.DE GEWINNSPIEL 2018<br> JETZT TEILNEHMEN UND GEWINNEN!</h2>
          </div>
            <p>&nbsp;</p>
          <div class="col-md-12">
            <a href="#about" class="btn btn-text btn-danger">TEILNEHMEN & GEWINNEN</a>
               <p>&nbsp;</p>
          </div>
          </div>
        </div>
          </div>     
         </div>
        </div>
        </div>
    </section> 
      
    <footer class="footer bg-light">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 h-100 text-center text-lg-left my-auto" style="height:auto!important">
            <ul class="list-inline mb-2">
                <li class="list-inline-item">
                <a href="https://www.teamsportxxl.de/" target="_blank">Shop</a>
              </li>
              <li class="list-inline-item">&sdot;</li>    
              <li class="list-inline-item">
                <a href="https://www.teamsportxxl.de/Impressum" target="_blank">Impressum</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="https://www.teamsportxxl.de/AGB" target="_blank">AGB</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="https://www.teamsportxxl.de/Datenschutz" target="_blank">Datenschutz</a>
              </li>
		      <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="teilnahmebedingungen.php" target="_blank">Teilnahmebedingungen</a>
              </li>
            </ul>
            <p class="text-muted small mb-4 mb-lg-0">JETZT GEWINNEN MIT TEAMSPORTXXL!</p>
          </div>
          <div class="col-lg-6 h-100 text-center text-lg-right my-auto" style="height:auto!important">
            <ul class="list-inline mb-0">
              <li class="list-inline-item mr-3">
               <img src="img/claim.png" />
              </li>
             
            </ul>
          </div>
            
        <div class="ds">
        <div class="col-sm-12">
        <div class="container">
        <div class="row">
        <center><span style="font-size: 12px;">* Die Online-Registrierung ist nur bei Abgabe einer Werbeeinwilligung möglich. Sie können aber auch ohne Werbeeinwilligung teilnehmen, wenn Sie uns innerhalb der Teilnahmefrist eine E-Mail an <a href="mailto:widerruf@spurt.de">widerruf@spurt.de</a> senden und in der E-Mail Ihren Namen, Ihre Telefonnummer und den Hauptpreis des Gewinnspiels angeben. Ihre Gewinnchancen werden durch die Art der Teilnahme nicht beeinflusst.</span></center>  
        </div>
        </div>
        </div>
        </div>
            
        </div>   
      </div>
    </footer>
  
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5oWnvqIPHFPQF6Zp7ooEj0QxSAYhOwv0"></script>
    <script src="vendor/google/maps.js"></script>  
    <script type="text/javascript ">

        
        $(document ).ready(function() {
            var autofill_postal_code = "<?= $config[Postal_Code][Autofill];?>";
            console.log("autofill_postal_code", autofill_postal_code);            
            $('.errorplz').hide();
            
            //Parse config.ini file 
                        
            
            /* JQuery Blur event for the postal code
             * Parameters : 
             * Returns : 
             *    1. fill the city Name on success.
             */
            
            $('.plz').blur(function(){
                var zip = $(this).val();
                var city;
               
                
                if($('.plz').hasClass('wrongplz')){
                    $('.plz').removeClass('wrongplz'); 
                    $('.errorplz').hide();
                }
                // Check for autofill postal code value
                if(autofill_postal_code){      
                    city = checkZipCode(zip);
                    
                    if(city){
                        $('.ort').val(city);
                    }else{
                        $('.ort').val("");      
                        $('.plz').addClass("wrongplz");
                    }
                }    
            });                


            /* Function Name : checkZipCode
             * Parameters : zipcode
             * Returns : 
             *    1. countryName on success .
             *    2. False on faiure.
             */
            
            function checkZipCode(zip){
                var status = true;
                var city ;
                if(zip.length == 5) {
                    var gurl = 'https://maps.googleapis.com/maps/api/geocode/json?address=Germany'+zip+'&key=AIzaSyC5oWnvqIPHFPQF6Zp7ooEj0QxSAYhOwv0&components=country:DE';
                    $.getJSON({
                        url : gurl,
                        async: false,
                        success:function(response, textStatus){
                            // check the status of the request
                            if(response.status !== "OK") {
                                status = false;         // Postal code not found or wrong postal code.
                            } else{    
                                // Postal code is found
                                status = true
                                var address_components = response.results[0].address_components;
                                $.each(address_components, function(index, component){
                                    var types = component.types;
                                    // Find the city for the postal code
                                    $.each(types, function(index, type){
                                        if(type == 'locality'){
                                            city = component.long_name;
                                            status = true;
                                        }
                                    });
                                });                                 
                            }
                         }
                    });
                } else{        
                    status = false;
                }
                if(status){
                    return city;
                }else {
                    return false;
                }
            }
          
            /* JQuery submit event for the postal code
             * Parameters : 
             * Returns : 
             *    1. check the postal code and accordingly display the error message for postal code.
             */
            form = $('.check-submit-form');
            form.submit(function(e)  {
               //e.preventDefault();
                var form = $(this);
                var readyforsubmit = true;     
                var plz = $('.plz');
                var ort = $('.ort');
                var email = $('.email');
                
                if(autofill_postal_code) {
                    if(plz.hasClass("wrongplz")){    
                        $('.errorplz').show();
                        return false;
                    }
                }
            }); 
        });     
       </script>				
  </body>
</html>
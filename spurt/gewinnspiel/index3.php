<?php

	/* ______________________________________________________________________________________________________________________________________________
	 *
	 MAIL IN ONE - STIMAWELL - SEA LP 1 - STADT: zuhause - 1804
	 * _______________________________________________________________________________________________________________________________________________
	 */

      //vorname  
      $standard_01 = $_GET["1"];
      //nachname
      $standard_02 = $_GET["2"];
      //email
      $email_02    = $_GET["3"];
      //plz
      $standard_04 = $_GET["4"]; 
      //ort
      $standard_05 = $_GET["5"]; 
      //trafficsource
      $ts = $_GET["trafficsource"];  

      $standard_01  = str_replace(' ','+',$standard_01);
      $standard_001   = base64_decode($standard_01);

      $standard_02  = str_replace(' ','+',$standard_02);
      $standard_002   = base64_decode($standard_02);

      $email_02  = str_replace(' ','+',$email_02);
      $email_002   = base64_decode($email_02);

      $standard_04  = str_replace(' ','+',$standard_04);
      $standard_004   = base64_decode($standard_04);

      $standard_05  = str_replace(' ','+',$standard_05);
      $standard_005   = base64_decode($standard_05);

	// Update with Hubspot 
	require_once('../../../php-client/hubspot-php-client/class.contacts.php'); 
	require_once('../../../php-client/hubspot-php-client/class.company.php'); 
	require_once('../../../php-client/hubspot-php-client/class.deals.php'); 
	   
	// Email Validation
	require_once('../../../php-client/emailvalidation-php-client/emailchecker.php');
	  
	// Update with your version of the Maileon PHP API Client
	require_once('../../../php-client/maileon-php-client-1.3.1/client/MaileonApiClient.php');

	// Get the config data from config.ini       
	$config = parse_ini_file("config.ini", true);

	$config_MailInOne = array(
		'BASE_URI' => 'https://api.maileon.com/1.0',
		'API_KEY' => $config[MailInOne_Settings][Mapikey],
		'THROW_EXCEPTION' => true,
		'TIMEOUT' => 60,
		'DEBUG' => 'false' // NEVER enable on production
	);

    $contactid = '';
	$checksum = '';
    $mailingid = "";
	$resp;
	session_start();

    if( isset($_GET['contactid']) ) {
		$contactid = $_GET['contactid'];
	} elseif( isset($_POST['contactid']) ) {
		$contactid = $_POST['contactid'];
	}

	if( isset($_GET['checksum']) ) {
		$checksum = $_GET['checksum'];
	} elseif( isset($_POST['checksum']) ) {
		$checksum = $_POST['checksum'];
	}

    if( isset($_GET['mailingid']) ) {
		$checksum = $_GET['mailingid'];
	} elseif( isset($_POST['mailngid']) ) {
		$checksum = $_POST['mailingid'];
	}

	if( isset($_POST['email']) ) {
		
		$email = $_POST['email'];
		// Validate Email address
		$emailchecker = new emailChecker($email);
		$emailchecker->curlRequest();
		$emailchecker->curlResponce();
		$resp = $emailchecker->parseResponce();
		if (!$resp) {
			// invalid email address
			// Copying the data to session and intiating a sesssion variable error.
			$_SESSION['data'] = $_POST;
			$_SESSION['error'] = "email";
			echo '<script type="text/javascript">';
			echo 'window.location ="#form"';
			echo '</script>';  
		} else {
			$_SESSION['error'] = "";
			session_destroy();
			$_SESSION['data'] = array();
		
			$standard_0 = $_POST["anrede"];
			$standard_1 = $_POST["vorname"];
			$standard_2 = $_POST["nachname"];
			$standard_3 = $_POST["plz"];
			$standard_4 = $_POST["ort"];

			$custom_0 = $_POST["quelle"];
			$custom_1 = $_POST["typ"];
			$custom_2 = $_POST["segment"];
			$custom_3 = $_POST["telefon"];

			$ip = $_SERVER['REMOTE_ADDR'];
			$url = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
			
			$trafficsource = $_POST["ts"];
			//Hubspot Configuration Sync            
			if ($config[HubSpotCredentials][Sync]) {
				$Hapikey = $config[HubSpotCredentials][hubapi];
				$contactHSpot = new HubSpot_Contacts($Hapikey);
				$companyHSpot = new HubSpot_Company($Hapikey);
				$dealHSpot = new HubSpot_Deal($Hapikey);

				// Contact HubSpot
				$contactparmsHSpot = array(
					'salutation' => $standard_0,
					'firstname' => $standard_1,
					'lastname' => $standard_2,
					'zip' => $standard_3,
					'city' => $standard_4,
					'email' => $email,
					'phone' => $custom_3
				);
				// Get the contact by email Id
				$contactByEmailHSpot = $contactHSpot->get_contact_by_email($email);
				if (!is_null($contactByEmailHSpot->vid)) {
					$contactIdHSpot = $contactByEmailHSpot->vid;
					$updatedContactHSpot = $contactHSpot->update_contact($contactIdHSpot, $contactparmsHSpot);

					if (is_null($updatedContactHSpot->vid)) {
						error_log("\n Error:Unable to update the contact details", 3, "error.log");
					}
				} else {
					// Create a new contact on Hubspot
					$contactIdHSpot = $contactHSpot->create_contact($contactparmsHSpot);

					$contactIdHSpot = $contactIdHSpot->vid;
					if (is_null($contactIdHSpot)) {
						error_log("\n Error:Unable to create a contact details", 3, "error.log");
					}
				}

				// Company HubSpot
				$companyparmHSpot = array(
					'name' => $config[HubSpotCompany][name],
					'domain' => $config[HubSpotCompany][domain],
					'description' => $config[HubSpotCompany][description],
					'phone' => $config[HubSpotCompany][phone],
					'zip' => $config[HubSpotCompany][zip],
				);

				$companyparmSearch = array(
					'limit' => 5,
					'requestOptions' => array(
						'properties' => array(
							0 => 'domain',
							1 => 'createdate',
							1 => 'name',
							2 => 'hs_lastmodifieddate',
						),
					),
					'offset' => array(
						'isPrimary' => true,
						'companyId' => 0,
					),
				);

				// HubSpot contact check by email.
				$searchByDomainHSpot = $companyHSpot->get_company_by_domain($config[HubSpotCompany][domain], $companyparmSearch);
				$countDomains = sizeof($searchByDomainHSpot->results);

				// Company does not exist
				if ($countDomains == 0) {

					if (isset($config[HubSpotCompany][domain])) {
						$companyIdHSpot = $companyHSpot->create_company($companyparmHSpot);
						$companyIdHSpot = $companyIdHSpot->companyId;
						if (is_null($companyIdHSpot)) {
							error_log("Error: Unable to create company", 3, "error.log");
						}
					} else {
						// Dont create a new company
						$companyIdHSpot = 0;
					}
				} else {
					// Update the new company 
					foreach ($searchByDomainHSpot->results as $domain) {
						$companyIdHSpot = $domain->companyId;
					}
				}

				// Link Contact to Company
				if (isset($contactIdHSpot)) {
					// Contact Company Association Id = CCA
					if (isset($companyIdHSpot)) {
						$cca = $companyHSpot->assosiate_company_to_contact($contactIdHSpot, $companyIdHSpot);
					}
					if(!isset($companyIdHSpot)){
						$companyIdHSpot = 0;	
					}	
					$dealAssociation = array(
						"associations" => array(
					//		"associatedCompanyIds" => array(
					//			0 => $companyIdHSpot
					//		),
							"associatedVids" => array(
								0 => $contactIdHSpot
							),
						),
					);

					$dealParms = array(
						"dealname" => $config[HubSpotDeal][dealname],
						"amount" => $config[HubSpotDeal][amount],
						"segment" => $config[HubSpotDeal][segment],
						"dealstage" => $config[HubSpotDeal][dealstage],
						"pipeline" => $config[HubSpotDeal][pipeline],
							// "hubspot_owner_id" => $config[HubSpotDeal][hubspot_owner_id],
					);

					// Check for the deal
					$dealCreated = false;
					$recentDeals = $dealHSpot->get_recent_deals()->results;
					foreach ($recentDeals as $deals) {
						if ($deals->properties->dealname->value === $config[HubSpotDeal][dealname]) {
							$dealCreated = true;
							$dealId = $deals->dealId;
						}
					}

					if (empty($recentDeals) || !$dealCreated || empty($dealId)) {
						// Associate contact and company with new Deal
						$dcc = $dealHSpot->create_deal($dealAssociation, $dealParms);
						$dealId = $dcc->dealId;
					} 
					$dccontact = $dealHSpot->associate_deal_with_contact($dealId, $contactIdHSpot);
					$dccompany = $dealHSpot->associate_deal_with_company($dealId, $companyIdHSpot);
				}
			}						

			// MailInOne Sync
			if ($config[MailInOne_Settings][Sync]) {				
					
				$contactsService = new com_maileon_api_contacts_ContactsService($config_MailInOne);
				$contactsService->setDebug(false);
				
				
				function updateContact($email, $config_MailInOne) {
			
					$debug = FALSE;
					$contactsService = new com_maileon_api_contacts_ContactsService($config_MailInOne);
					$contactsService->setDebug($debug);
					$newContact = new com_maileon_api_contacts_Contact();
					$newContact->email = $email;
					
					$newContact->custom_fields["Quelle"] = 'adwordslp1zuhause';
				   
					$response = $contactsService->createContact($newContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, "", "", false);
					return $response;
				}


				$getContact = $contactsService->getContactByEmail($email);
				if( $getContact->isSuccess() && $getContact->getResult()->permission != com_maileon_api_contacts_Permission::$NONE ) {
					
					
				   $response = updateContact($email, $config_MailInOne);
					
					echo '<script type="text/javascript">';
					echo 'window.location.href="https://stimawell.campaign-in-one.de/ems-training/zuhause/redirect.php";';
					echo '</script>'; 

				} else {
					$newContact = new com_maileon_api_contacts_Contact();
					$newContact->email = $email;
					$newContact->anonymous = false;
					$newContact->permission = com_maileon_api_contacts_Permission::$NONE;
					$newContact->standard_fields["SALUTATION"]  = $standard_0;
					$newContact->standard_fields["FIRSTNAME"]   = $standard_1;    
					$newContact->standard_fields["LASTNAME"]    = $standard_2;
					$newContact->standard_fields["ZIP"]         = $standard_3;
					$newContact->standard_fields["CITY"]        = $standard_4;
					$newContact->custom_fields["Quelle"]        = $custom_0;
					$newContact->custom_fields["Typ"]           = $custom_1;
					$newContact->custom_fields["Segment"]       = $custom_2;
					$newContact->custom_fields["Telefon"]       = $custom_3;
					$newContact->custom_fields["ip_adresse"]    = $ip;
					$newContact->custom_fields["url"]           = $url;
					$newContact->custom_fields["trafficsource"] = $trafficsource;
					
					$response = $contactsService->createContact($newContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, '', '', true, true, $config[MailInOne_Settings][DOIKey]);
					
						#$transactionsService = new com_maileon_api_transactions_TransactionsService($config);
						#$transactionsService->setDebug(FALSE);
						#$transaction = new com_maileon_api_transactions_Transaction();
						#$transaction->contact = new com_maileon_api_transactions_ContactReference();
						#$transaction->type = 1;   
						#$transaction->contact->email = $email;
						#$transactions = array($transaction);
						#$transaction->content["FIBO2018"] = $fibo2018;
						#$transactionsService->createTransactions($transactions, true, false);
					
					#header('Location: https://stimawell.campaign-in-one.de/ems-training/muenchen/redirect.php');
					echo '<script type="text/javascript">';
					echo 'window.location.href="https://stimawell.campaign-in-one.de/ems-training/zuhause/redirect.php";';
					echo '</script>'; 					
                }
            }
        }    
	} else {
		$email = "";
		$standard_0 = "";
		$standard_1 = "";
		$standard_2 = "";
		$standard_3 = "";
        $standard_4 = "";
        $custom_0 = "";
        $custom_1 = "";
        $custom_2 = "";
        $custom_3 = "";
        $ip = "";
        $url = "";
        $trafficsource = "";    
	}
  
?>

<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Nutzen Sie unser neues FREE EMS® PROGRAMM und trainieren sie bequem von zu Hause aus.">
    <meta name="author" content="StimaWELL">
      
    <title>EMS Training zu Hause</title>
      
    <link href="css/creative.css" rel="stylesheet" type="text/css">  
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">    
  </head>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfPExLIePbHmy_rtDbHD406iuQ46n-SMA&libraries=places"></script>
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '2069635113314986');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=2069635113314986&ev=PageView&noscript=1"
    /></noscript>
    
  <body>
      
    <nav class="nav-top">
       <div class="container">
           <div class="row">
               <span class="nav-top-text"><strong>&#10004;&nbsp;&nbsp;40 JAHRE MADE IN GERMANY</strong></span>
               <span class="nav-top-text"><strong>&#10004;&nbsp;&nbsp;DIREKT VOM HERSTELLER</strong></span>
               <span class="nav-top-text"><strong>&#10004;&nbsp;&nbsp;SERVICE: 06443 4369914</strong></span>
               <div class="col-lg-4 h-100 text-center text-lg-right my-auto">
                <ul class="list-inline mb-0">
                  <li class="list-inline-item mr-3">
                    <a href="https://www.facebook.com/stimawellems/" target="_blank">
                      <i class="fa fa-facebook fa fa-fw"></i>
                    </a>
                  </li>
                  <li class="list-inline-item mr-3">
                    <a href="https://www.instagram.com/stimawell.ems/" target="_blank">
                      <i class="fa fa-instagram fa fa-fw"></i>
                    </a>
                  </li>
                </ul>
              </div>
           </div>
        </div>   
    </nav>
      
    <nav class="navbar navbar-light bg-light static-top">
      <div class="container">
          <a class="navbar-brand" style="text-transform: uppercase;" href="https://www.stimawell-ems.de/" target="_blank"><img style="width:200px;" alt="EMS-Training-zuhause" src="img/logo.svg"></a>
          <span>EMS TRAINING <strong>ZUHAUSE</strong></span>
      </div>
    </nav>
      
    <section>
        <div class="clearspace"></div>
    </section>

    <section class="form-container">
    <div class="container">
    <div class="row">
        <div style="padding-top: 90px;" class="col-sm-6 gewinn-form">       
            <h1 style="color: #3c3b3b;">EMS-Training zu Hause</h1> 
            <hr>
            <p style="padding-bottom:20px;" class="lead mb-0">Nutze unser neues <strong>FREE EMS® PROGRAMM</strong> und trainiere bequem von zu Hause aus.</p> 
            <ul>
              <li>Nutze unsere Erfahrung aus 40 Jahren.</li>
              <li>Medizinisch geprüfte Geräte direkt vom Hersteller.</li>
              <li>Ruf an unter <strong>06443 4369914</strong> um mehr zu erfahren.</li>
            </ul>      
            <a href="#form"><input style="margin-top: 25px; cursor: pointer;" type="submit" class="col-sm-12 btn btn-danger btn-xl js-scroll-trigger" value="Jetzt kostenlos Probetraining vereinbaren" style="width: 100%;"></a>
        </div>  
        <div class="col-sm-6 gewinn-form text-center">
            <img style="width: 350px;" src="img/stimawell-model_2.png" alt="ems-training zu Hause"/>
        </div>
        </div>
        </div>  
    </section> 
  
    <section class="form-container-mobile">  
    <div class="container">
    <div class="row">
        <div style="padding-top: 25px;" class="col-sm-12 text-center">       
            <h1 style="color: #3c3b3b;">EMS-Training zu Hause</h1> 
            <center><img class="img-fluid" src="img/partner-hero-image.png" alt="ems-training zu Hause"></center>
            <p style="padding-bottom:20px; padding-top:20px;" class="lead mb-0">Nutze unser neues <strong>FREE EMS® PROGRAMM</strong><br> und trainiere bequem von zu Hause aus.</p> 
              <li>Nutze unsere Erfahrung aus 40 Jahren.</li>
              <li>Medizinisch geprüfte Geräte direkt vom Hersteller.</li>
              <li>Ruf an unter <strong>06443 4369914</strong> um mehr zu erfahren.</li>
            <div class="btn-desktop">
                <a href="#form"><input style="margin-top: 25px;  margin-bottom: 25px; cursor: pointer;" type="submit" class="col-sm-12 btn btn-danger btn-xl js-scroll-trigger" value="Jetzt kostenlos Probetraining vereinbaren" name="" style="width: 100%;"></a>
            </div>
            <div class="btn-mobile">
                <a href="#form"><input style="margin-top: 25px;  margin-bottom: 25px; cursor: pointer;" type="submit" class="col-sm-12 btn btn-danger btn-xl js-scroll-trigger" value="Kostenlos Probetraining vereinbaren" name="" style="width: 100%;"></a>
            </div>
        </div>  
       
     </div>
    </div>  
    </section>   
      
    <section>
        <div class="clearspace"></div>
    </section>
         
    <section class="partner-container">
    <div class="container">
    <div class="row">
                <div class="col-sm-6 text-center">
                    <img class="partner-img" style="width: 350px;" src="img/stimawell-philipp-akguezel.png" alt="ems-experte philipp akgüzel zu Hause"/>
                    <center><img class="img-fluid partner-img-mobile" src="img/stimawell-philipp-akguezel-mobile.jpg" alt="ems-experte philipp akgüzel zu Hause"></center>
                </div>
        <div style="padding-top: 25px;" class="col-sm-6">       
     
            <h2>Dein persönlicher Ansprechpartner und EMS-Experte für dein Training zu Hause ist <span style="color:#00a2db">Philipp Akgüzel</span>.</h2>
            <hr>
			<p>Mein Name ist Philipp Akgüzel. Ich bin 39 Jahre alt. Nachdem ich in meiner Jugend 15 Jahre Fußball gespielt habe, bin ich zum Mountainbike- und Rennradsport gekommen. Seit einigen Jahren bin ich begeisterter „EMS-Sportler“.</p>
			<p>Nach meiner schulischen Ausbildung habe ich BWL studiert und arbeite seither in diesen Berufsfeldern. Seit 2016 bin ich in der EMS-Branche beschäftigt. Als qualifizierter Medizinprodukteberater freue ich mich täglich aufs Neue darauf, unseren Kunden mit Rat, Tat und als Problemlöser beiseite zu stehen.</p>
        </div>         
    </div>
    </div>  
    </section>
	  
	<section>
        <div class="clearspace"></div>
    </section>
  
    <section class="features-video">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="videoWrapper">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/18xXcSH0cY4" frameborder="0" allow=" encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>   
                <div class="col-lg-6" style="padding-top:50px;">     
                    
                  <h3 style="color: #3c3b3b;">Unser EMS-Programm bringt dich schneller zu deinen Zielen.</h3> 
                  <hr>
                  <p>Die Wirksamkeit von EMS-Training wurde in vielen Studien nachgewiesen. Dein zertifizierter Trainer wählt aus einer Vielzahl von StimaWELL® EMS-Programmen deine passenden aus und begleitet dich auf dem Weg zu deinen Zielen.</p>
                        
                </div>
                </div>
                <p>&nbsp;</p>
            <div class="btn-desktop">
                <center><a href="#form"><input style="margin-top: 25px;  margin-bottom: 25px; cursor: pointer;" type="submit" class="col-sm-8 btn btn-danger btn-xl js-scroll-trigger" value="Jetzt kostenlos Probetraining vereinbaren" name="" style="width: 100%;"></a></center>
            </div>
            <div class="btn-mobile">
                <a href="#form"><input style="margin-top: 25px;  margin-bottom: 25px; cursor: pointer;" type="submit" class="col-sm-12 btn btn-danger btn-xl js-scroll-trigger" value="Kostenlos Probetraining vereinbaren" name="" style="width: 100%;"></a>
            </div>
          </div>    
    </section>

    <section class="testimonials text-center bg-light">
      <div class="container">
          <h3 style="color: #3c3b3b;">Einfach mal ausprobieren!</h3> 
              <hr>
              <p>Erreiche deine Trainingsziele schneller und bequemer durch die Kombination aus der regelmäßigen, persönlichen Betreuung durch deinen EMS-Experten und dem eigenständigen EMS-Training mit deinem virtuellen Trainer zu Hause oder unterwegs.</p>
              <p>&nbsp;</p>
        <div class="row">
          <div class="col-lg-4">
            <div class="testimonial-item mx-auto mb-5 mb-lg-0">
              <img class="img-fluid rounded-circle mb-3" src="img/testimonials-1.jpg" alt="ems-experte nico lorenz zu Hause">
              <h5>Margaret E.</h5>
              <p class="font-weight-light mb-0"><i>"Ich liebe StimaWELL!"</i></p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="testimonial-item mx-auto mb-5 mb-lg-0">
              <img class="img-fluid rounded-circle mb-3" src="img/testimonials-2.jpg" alt="ems-experte nico lorenz zu Hause">
              <h5>Fred S.</h5>
              <p class="font-weight-light mb-0"><i>"StimaWELL ist klasse!"</i></p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="testimonial-item mx-auto mb-5 mb-lg-0">
              <img class="img-fluid rounded-circle mb-3" src="img/testimonials-3.jpg" alt="ems-experte nico lorenz zu Hause">
              <h5>Sarah	W.</h5>
              <p class="font-weight-light mb-0"><i>"StimaWELL rockt!"</i></p>
            </div>
          </div>
        </div>
      </div>
    </section> 
	  
	<section>
        <div class="clearspace"></div>
    </section>

    <section class="partner-container">
    <div class="container">
    <div class="row">
         <div class="col-sm-6 model-feature">       
            <h2>Weil Fitness unkompliziert und bezahlbar sein soll.</h2>
            <hr>
            <p class="lead mb-0">Erlebe die neue, freie Art des EMS-Trainings. Erreiche deine Trainingsziele schneller und bequemer. Kombiniere die regelmäßige, persönliche Betreuung durch deinen EMS-Experten und dein eigenständiges EMS-Training mit dem virtuellen Trainer zu Hause oder unterwegs.</p><br>
        </div>  
                <div class="col-sm-6 text-center">
                    <img style="width: 350px;" src="img/stimawell-model_1.png" alt="ems-training zu Hause"/>
                </div>
            </div>
        </div>  
    </section>  
    
	<section>
        <div class="clearspace"></div>
    </section>
       
    <section class="partner-container" style="background-image: url('img/ems-bg.jpg');">
    <div class="container">
    <div class="row">
                <div class="col-sm-6 text-center">
                    <img style="width: 350px;" class="hidden-model" src="img/stimawell-model_3.png" alt="ems-training zu Hause"/>
                </div>
        <div  class="col-sm-6 model-feature">       
              <h2>Vielseitige Anwendung um deine Ziele schneller zu erreichen.</h2>
              <hr>
              <p class="lead mb-0">Die EMS Technologie ermöglicht es dir Ziele verschiedenster Art schneller zu erreichen. Die Kombination aus persönlicher Betreuung durch deinen EMS-Experten und eigenständigem EMS-Training machen es möglich. Wo und wann du willst!</p>
        </div>  
            </div>
        </div>  
    </section>  
      
    <section>
        <div class="clearspace"></div>
    </section>
  
    <section class="features-maps" id="form">
            <center><h3>Vereinbare jetzt dein kostenloses EMS-Training in deiner Nähe.</h3></center>
            <p>&nbsp;</p>
            <!-- MAP -->
            <div class="container">
                <div class="row justify-content-center">
                
                <!-- FORM DESKTOP-->    
                <div class="col-lg-8 check-submit-form">     
                <form class="form-desktop" id="api-data-form" action="<?= $_SERVER['PHP_SELF'] ?>" method="post" accept-charset="utf-8" class="form-horizontal" role="form">
				<input type="hidden" name="contactid" value="<?= $contactid ?>">
				<input type="hidden" name="checksum" value="<?= $checksum ?>">
                <input type="hidden" name="mailingid" value="<?= $mailingid ?>">
             
                    <!-- INPUT ANREDE FRAU & HERR -->
					<div class="form-group">
						<div class="col-sm-12">
                            <div class="btn-group">
                                  <label class="btn btn-outline-secondary" style="border: 1px solid transparent;">
                                    <input value="Frau" style="width: 15px; height: 15px;" type="radio" name="anrede" autocomplete="off" required> Frau
                                  </label>
                                    <span class="input-group-btn" style="width:15px;"></span> 
                                  <label class="btn btn-outline-secondary" style="border: 1px solid transparent;">
                                    <input value="Herr" style="width: 15px; height: 15px;" type="radio" name="anrede" autocomplete="off"> Herr
                                  </label>
							</div>  
						</div>       
					</div>
                
                    <!-- INPUT VORNAME & NACHNAME -->
					<div class="form-group">
						<div class="col-sm-12">
							<div class="input-group">
								<input value="<?php echo $_SESSION['data']['vorname'];?>" class="input-fields form-control" id="" name="vorname" placeholder="*Vorname" type="text" required onblur="if (this.value == '') {this.value = '<?= $standard_001?>';}" onfocus="if (this.value == '<?= $standard_001?>') {this.value = '';}" onpaste="return false;" onCopy="return false;" onCut="return false;"> 
                                <span class="input-group-btn" style="width:15px;"></span> 
                                <input value="<?php echo $_SESSION['data']['nachname'];?>" class="input-fields form-control" id="nachname" name="nachname" placeholder="*Nachname" type="text" required onblur="if (this.value == '') {this.value = '<?= $standard_002?>';}" onfocus="if (this.value == '<?= $standard_002?>') {this.value = '';}" onpaste="return false;" onCopy="return false;" onCut="return false;">
							</div>
						</div>       
					</div>
                
                    <!-- INPUT PLZ & ORT -->
					<div class="form-group">
						<div class="col-sm-12">
							<div class="input-group">
								 <input value="<?php echo  $_SESSION['data']['plz'];?>" class="input-fields form-control  plz" id="plz" name="plz" placeholder="*PLZ" type="number" required onblur="if (this.value == '') {this.value = '<?= $standard_004?>';}" onfocus="if (this.value == '<?= $standard_004?>') {this.value = '';}" onpaste="return false;" onCopy="return false;" onCut="return false;">
                                
                                <span class="input-group-btn" style="width:15px;"></span> 
                                <input value="<?php echo  $_SESSION['data']['ort'];?>" class="input-fields form-control ort" id="ort" name="ort" placeholder="*Ort" type="text" required onblur="if (this.value == '') {this.value = '<?= $standard_005?>';}" onfocus="if (this.value == '<?= $standard_005?>') {this.value = '';}" onpaste="return false;" onCopy="return false;" onCut="return false;">
							</div>
							 <span class="p-light errorplz" style="color:<?php echo $config[Postal_Code][ErrorMsg_Color]; ?>"><?php echo $config[Postal_Code][ErrorMsg]; ?></span>
						</div>       
					</div>
                
                    <!-- INPUT EMAIL & TELEFON -->
					<div class="form-group">
						<div class="col-sm-12">
							<div class="input-group">
								<input value="<?php echo  $_SESSION['data']['email'];?>" class="input-fields form-control email" id="email" name="email" placeholder="*E-Mail-Adresse" type="email" required onblur="if (this.value == '') {this.value = '<?= $email_002?>';}" onfocus="if (this.value == '<?= $email_002?>') {this.value = '';}" onpaste="return false;" onCopy="return false;" onCut="return false;"> 
                                <span class="input-group-btn" style="width:15px;"></span> 
                                <input value="<?php echo  $_SESSION['data']['telefon'];?>" class="input-fields form-control" id="telefon" name="telefon" placeholder="*Telefonnummer" type="number" required onpaste="return false;" onCopy="return false;" onCut="return false;">
							</div>
                            <?php if($_SESSION['error'] === "email"){ ?>   
                                <span class="p-light erroremail"  style="color:<?php echo $config[Email_Address][ErrorMsg_Color]; ?>"><?php echo $config[Email_Address][ErrorMsg]; ?></span>
                            <?php } ?>  							
						</div>       
					</div>
                    
                    <label class="form-group">
                        <div class="col-sm-12">
                        <input style="width: 20px; height: 20px;" type="checkbox" required>
                            <span class="p-dark">&nbsp;&nbsp;Ich bin einverstanden, dass die Schwa-Medico Medizinische Apparate Vertriebsgesellschaft mbH, Gehrnstraße 4, 35630 Ehringhausen, meine eingetragenen Daten nutzt und mir E-Mails zuschickt bzw. mich anruft, um mir Angebote aus dem Bereich EMS-Training (Zubehör, Training, Fachliteratur) zukommen zu lassen. Diese Einwilligung kann ich jederzeit widerrufen, etwa durch einen Brief an die oben genannte Adresse oder durch eine E-Mail an <a href="mailto:widerruf@schwa-medico.de">widerruf@schwa-medico.de.</a> Anschließend wird jede werbliche Nutzung unterbleiben.</span>
                        </div>    
                    </label>
                      
                      <div class="form-group hidden">
                      <label class="col-sm-2 control-label" for="">TRAFFICSOURCE</label>
                      <div class="col-sm-10">
                      <input class="form-control"  value="<?= $ts?>" placeholder="" type="text" name="ts" id="ts" value="">
                      </div>
                      </div>
                
                      <div class="form-group hidden">
                      <label class="col-sm-2 control-label" for="">Quelle</label>
                      <div class="col-sm-10">
                      <input class="form-control"  placeholder="" type="text" name="quelle" id="quelle" value="adwordslp1zuhause">
                      </div>
                      </div>
                
                    <div class="form-group hidden">
                      <label class="col-sm-2 control-label" for="">Typ</label>
                      <div class="col-sm-10">
                      <input class="form-control"  placeholder="" type="text" name="typ" id="" value="B2C">
                      </div>
                      </div>
                
                <div class="form-group hidden">
                      <label class="col-sm-2 control-label" for="">Segment</label>
                      <div class="col-sm-10">
                      <input class="form-control"  placeholder="" type="text" name="segment" id="" value="Lead">
                      </div>
                      </div>
                
                    
				<div class="form-group"> 
                    <div class="col-sm-12">
					<input style="margin-top: 25px; cursor: pointer;" type="submit" class="col-sm-12 btn btn-danger btn-xl js-scroll-trigger" value="Jetzt kostenlos Probetraining vereinbaren" name="submit" style="width: 100%;">
                </div>        
				</div>
               <?php if( isset($response) && $response->isSuccess() ) { ?>
					<div class="alert alert-success fade in">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<strong>Subscription successful</strong>
					</div>
				<?php } elseif( isset($warning) ) { ?>
					<div class="alert alert-warning fade in">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<strong style="color: red; z-index: 1000;">Subscription failed</strong>
						<?= $warning['message'] ?>
					</div>
				<?php } elseif( isset($response) ) { ?>
					<div class="alert alert-danger fade in">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<strong>Subscription failed</strong>
					</div>
				<?php } ?>
			</form>  
                    
                    
                          <!-- FORM MOBILE-->    
                             <div class="col-lg-6 form-mobile check-submit-form">     
                            <form id="api-data-form" action="<?= $_SERVER['PHP_SELF'] ?>" method="post" accept-charset="utf-8" class="form-horizontal" role="form">
                            <input type="hidden" name="contactid" value="<?= $contactid ?>">
                            <input type="hidden" name="checksum" value="<?= $checksum ?>">
                            <input type="hidden" name="mailingid" value="<?= $mailingid ?>">

                                <!-- INPUT ANREDE FRAU & HERR -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="btn-group">
                                              <label class="btn btn-outline-secondary" style="border: 1px solid transparent;">
                                                <input value="Frau" style="width: 15px; height: 15px;" type="radio" name="anrede" autocomplete="off" required> Frau
                                              </label>
                                                <span class="input-group-btn" style="width:15px;"></span> 
                                              <label class="btn btn-outline-secondary" style="border: 1px solid transparent;">
                                                <input value="Herr" style="width: 15px; height: 15px;" type="radio" name="anrede" autocomplete="off"> Herr
                                              </label>
                                        </div>  
                                    </div>       
                                </div>

                                <!-- INPUT VORNAME -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <input value="<?php echo  $_SESSION['data']['vorname'];?>" class="input-fields form-control" id="" name="vorname" placeholder="*Vorname" type="text" required onblur="if (this.value == '') {this.value = '<?= $standard_001?>';}"     onfocus="if (this.value == '<?= $standard_001?>') {this.value = '';}" onpaste="return false;" onCopy="return false;" onCut="return false;">                                             
                                        </div>
                                    </div>       
                                </div>
                                
                                
                                <!-- INPUT NACHNAME -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <input value="<?php echo  $_SESSION['data']['nachname'];?>" class="input-fields form-control" id="nachname" name="nachname" placeholder="*Nachname" type="text" required onblur="if (this.value == '') {this.value = '<?= $standard_002?>';}" onfocus="if (this.value == '<?= $standard_002?>') {this.value = '';}" onpaste="return false;" onCopy="return false;" onCut="return false;">
                                        </div>
                                    </div>       
                                </div>
                                

                                <!-- INPUT PLZ -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                             <input value="<?php echo  $_SESSION['data']['plz'];?>" class="input-fields form-control plz" id="plz" name="plz" placeholder="*PLZ" type="number" required onblur="if (this.value == '') {this.value = '<?= $standard_004?>';}"        onfocus="if (this.value == '<?= $standard_004?>') {this.value = '';}" onpaste="return false;" onCopy="return false;" onCut="return false;">
                                        </div>
										<span class="p-light errorplz" style="color:<?php echo $config[Postal_Code][ErrorMsg_Color]; ?>"><?php echo $config[Postal_Code][ErrorMsg]; ?></span>     
                                    </div>       
                                </div>
                                
                                 <!-- INPUT ORT -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <input value="<?php echo  $_SESSION['data']['ort'];?>" class="input-fields form-control ort" id="ort" name="ort" placeholder="*Ort" type="text" required onblur="if (this.value == '') {this.value = '<?= $standard_005?>';}" onfocus="if (this.value == '<?= $standard_005?>') {this.value = '';}" onpaste="return false;" onCopy="return false;" onCut="return false;">
                                        </div>
                                    </div>       
                                </div>

                                <!-- INPUT EMAIL -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <input value="<?php echo  $_SESSION['data']['email'];?>" class="input-fields form-control email" id="email" name="email" placeholder="*E-Mail-Adresse" type="email" required onblur="if (this.value == '') {this.value = '<?= $email_002?>';}" onfocus="if (this.value == '<?= $email_002?>') {this.value = '';}" onpaste="return false;" onCopy="return false;" onCut="return false;"> 
                                        </div>
                                        <?php if($_SESSION['error'] === "email"){ ?>   
                                             <span class="p-light erroremail"  style="color:<?php echo $config[Email_Address][ErrorMsg_Color]; ?>"><?php echo $config[Email_Address][ErrorMsg]; ?></span>
                                         <?php } ?>  										
                                    </div>       
                                </div>
                                
                                <!-- INPUT TELEFON -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <input value="<?php echo  $_SESSION['data']['telefon'];?>" class="input-fields form-control" id="telefon" name="telefon" placeholder="*Telefonnummer" type="number" required onpaste="return false;" onCopy="return false;" onCut="return false;">
                                        </div>
                                    </div>       
                                </div>


                                <label class="form-group">
                                    <div class="col-sm-12">
                                    <input style="width: 20px; height: 20px;" type="checkbox" required>
                                        <span class="p-dark">&nbsp;&nbsp;Ich bin einverstanden, dass die Schwa-Medico Medizinische Apparate Vertriebsgesellschaft mbH, Gehrnstraße 4, 35630 Ehringhausen, meine eingetragenen Daten nutzt und mir E-Mails zuschickt bzw. mich anruft, um mir Angebote aus dem Bereich EMS-Training (Zubehör, Training, Fachliteratur) zukommen zu lassen. Diese Einwilligung kann ich jederzeit widerrufen, etwa durch einen Brief an die oben genannte Adresse oder durch eine E-Mail an <a href="mailto:widerruf@schwa-medico.de">widerruf@schwa-medico.de.</a> Anschließend wird jede werbliche Nutzung unterbleiben.</span>
                                    </div>    
                                </label>

                                  <div class="form-group hidden">
                                  <label class="col-sm-2 control-label" for="">TRAFFICSOURCE</label>
                                  <div class="col-sm-10">
                                  <input class="form-control"  value="<?= $ts?>" placeholder="" type="text" name="ts" id="ts" value="">
                                  </div>
                                  </div>

                                  <div class="form-group hidden">
                                  <label class="col-sm-2 control-label" for="">Quelle</label>
                                  <div class="col-sm-10">
                                  <input class="form-control"  placeholder="" type="text" name="quelle" id="quelle" value="adwordslp1zuhause">
                                  </div>
                                  </div>

                                <div class="form-group hidden">
                                  <label class="col-sm-2 control-label" for="">Typ</label>
                                  <div class="col-sm-10">
                                  <input class="form-control"  placeholder="" type="text" name="typ" id="" value="B2C">
                                  </div>
                                  </div>

                            <div class="form-group hidden">
                                  <label class="col-sm-2 control-label" for="">Segment</label>
                                  <div class="col-sm-10">
                                  <input class="form-control"  placeholder="" type="text" name="segment" id="" value="Lead">
                                  </div>
                                  </div>


                            <div class="form-group"> 
                                <div class="col-sm-12">
                                <input style="margin-top: 25px; cursor: pointer;" type="submit" class="col-sm-12 btn btn-danger btn-xl js-scroll-trigger" value="Kostenlos Probetraining vereinbaren!" name="submit" style="width: 100%;">
                            </div>        
                            </div>
                           <?php if( isset($response) && $response->isSuccess() ) { ?>
                                <div class="alert alert-success fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Subscription successful</strong>
                                </div>
                            <?php } elseif( isset($warning) ) { ?>
                                <div class="alert alert-warning fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong style="color: red; z-index: 1000;">Subscription failed</strong>
                                    <?= $warning['message'] ?>
                                </div>
                            <?php } elseif( isset($response) ) { ?>
                                <div class="alert alert-danger fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Subscription failed</strong>
                                </div>
                            <?php } ?>
                        </form>          
                        
                        
                    </div>
                </div>
            </div>
         </div>        
    </section>  
 
   <section>
        <div class="clearspace"></div>
    </section>
      
    <footer class="footer bg-light">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 h-100 text-center text-lg-left my-auto" style="height:auto!important">
            <ul class="list-inline mb-2">
              <li class="list-inline-item">
                <a href="https://www.stimawell-ems.de/impressum" target="_blank">Impressum</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="https://www.stimawell-ems.de/agb" target="_blank">AGB</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="https://www.stimawell-ems.de/datenschutz" target="_blank">Datenschutz</a>
              </li>
            </ul>
            <p class="text-muted small mb-4 mb-lg-0">EMS Training zu Hause</p>
          </div>
          <div class="col-lg-6 h-100 text-center text-lg-right my-auto" style="height:auto!important">
            <ul class="list-inline mb-0">
              <li class="list-inline-item mr-3">
                <a href="https://www.facebook.com/stimawellems/" target="_blank">
                  <i class="fa fa-facebook fa-2x fa-fw"></i>
                </a>
              </li>
              <li class="list-inline-item mr-3">
                <a href="https://www.instagram.com/stimawell.ems/" target="_blank">
                  <i class="fa fa-instagram fa-2x fa-fw"></i>
                </a>
              </li>
            </ul>
          </div>
            
        </div>   
      </div>
    </footer>
  
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5oWnvqIPHFPQF6Zp7ooEj0QxSAYhOwv0"></script>
    <script src="vendor/google/maps.js"></script>  
    <script type="text/javascript ">

        
        $(document ).ready(function() {
            var autofill_postal_code = "<?= $config[Postal_Code][Autofill];?>";
            console.log("autofill_postal_code", autofill_postal_code);            
            $('.errorplz').hide();
            
            //Parse config.ini file 
                        
            
            /* JQuery Blur event for the postal code
             * Parameters : 
             * Returns : 
             *    1. fill the city Name on success.
             */
            
            $('.plz').blur(function(){
                var zip = $(this).val();
                var city;
               
                
                if($('.plz').hasClass('wrongplz')){
                    $('.plz').removeClass('wrongplz'); 
                    $('.errorplz').hide();
                }
                // Check for autofill postal code value
                if(autofill_postal_code){      
                    city = checkZipCode(zip);
                    
                    if(city){
                        $('.ort').val(city);
                    }else{
                        $('.ort').val("");      
                        $('.plz').addClass("wrongplz");
                    }
                }    
            });                


            /* Function Name : checkZipCode
             * Parameters : zipcode
             * Returns : 
             *    1. countryName on success .
             *    2. False on faiure.
             */
            
            function checkZipCode(zip){
                var status = true;
                var city ;
                if(zip.length == 5) {
                    var gurl = 'https://maps.googleapis.com/maps/api/geocode/json?address=Germany'+zip+'&key=AIzaSyC5oWnvqIPHFPQF6Zp7ooEj0QxSAYhOwv0&components=country:DE';
                    $.getJSON({
                        url : gurl,
                        async: false,
                        success:function(response, textStatus){
                            // check the status of the request
                            if(response.status !== "OK") {
                                status = false;         // Postal code not found or wrong postal code.
                            } else{    
                                // Postal code is found
                                status = true
                                var address_components = response.results[0].address_components;
                                $.each(address_components, function(index, component){
                                    var types = component.types;
                                    // Find the city for the postal code
                                    $.each(types, function(index, type){
                                        if(type == 'locality'){
                                            city = component.long_name;
                                            status = true;
                                        }
                                    });
                                });                                 
                            }
                         }
                    });
                } else{        
                    status = false;
                }
                if(status){
                    return city;
                }else {
                    return false;
                }
            }
          
            /* JQuery submit event for the postal code
             * Parameters : 
             * Returns : 
             *    1. check the postal code and accordingly display the error message for postal code.
             */
            form = $('.check-submit-form');
            form.submit(function(e)  {
               //e.preventDefault();
                var form = $(this);
                var readyforsubmit = true;     
                var plz = $('.plz');
                var ort = $('.ort');
                var email = $('.email');
                
                if(autofill_postal_code) {
                    if(plz.hasClass("wrongplz")){    
                        $('.errorplz').show();
                        return false;
                    }
                }
            }); 
        });     
       </script>				
  </body>
</html>
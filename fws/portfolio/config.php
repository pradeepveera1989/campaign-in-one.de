<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of config
 *
 * @author Pradeep
 */
class config{
    
    protected $path;
    public $config;
    
    public function __construct($path) {
       
       if(empty($path)){
            
            $this->config = parse_ini_file("config.ini", true, INI_SCANNER_TYPED );
       }else{
           
            $this->config = parse_ini_file($path, true, INI_SCANNER_TYPED );
       }
      
    }
    public function getIniLocation(){
        
        return $this->config[GeneralSetting][iniLocation];
    }
    
    public function getAllConfig(){
        
        return $this->config;
    }

    public function getConfigCounterStatus(){
    
        $counter_status = strtotime(date('F j, Y, g:i a')) <= strtotime($this->config[GeneralSetting][CountdownExpire]);
        return $counter_status;
    }
    
    public function getConfigCounter(){
 
        return[
            "expire" => $this->config[GeneralSetting][CountdownExpire],
            "expire_msg" => $this->config[GeneralSetting][CountdownExpireMessage],
            "max_offers" => $this->config[GeneralSetting][MaxOffers],
            "sold_offers" => $this->config[GeneralSetting][SoldOffers],
        ];
    }
    
    public function getConfigAssignmentManagerStatus(){
        
        return $this->config[AssignmentManager][Status];
    }    
    
    public function getConfigAssignmentManager(){
        
        return $this->config[AssignmentManager];
    }

    public function getConfigMailInOneStatus(){
        
        return $this->config[MailInOne][Sync];
    }
    
    public function getConfigMailInOneEvent(){
        
        return $this->config[MailInOneEvent];
    }     
        
    public function getConfigMailInOne(){
        
        return $this->config[MailInOne];
    }
    
    public function getConfigHubspotStatus(){

        return $this->config[HubSpot][Sync];
    }
    
    public function getConfigHubspot(){
        
        return $this->config[HubSpot];
    }
    
    public function getConfigSplitTestStatus(){
        
        $return = false;
        
        if($this->config[Splittest][RunSplittest] && !$this->config[Varient][VarientFile]){
            
            $return = true;
        }
        return $return;
    }   
    
    public function getConfigSplitTest(){
        
        return $this->config[Splittest];
    }       
    
    public function getConfigKeepURLParms(){
        
        return $this->config[GeneralSetting][KeepURLParameter];
    }
    
    public function getConfigGeneralSettings(){
        
        return $this->config[GeneralSetting];
    }

    public function getConfigPostalStatus(){
        
        return $this->config[Postal_Code][Autofill];
    }
    
    public function getConfigPostalCode(){
        
        return $this->config[Postal_Code];
    }    
    
    public function getConfigTrackingTools(){
        
        return $this->config[TrackingTools];
    }   

    public function getConfigTelefonStatus(){
        
        return $this->config[Telefon][Status];
    }
    
    public function getConfigTelefon(){
        
        return $this->config[Telefon];
    }

    public function getConfigForwardEmail(){
        
        return $this->config[Email_Address][ForwardEmail];
    }    
    
    public function getConfigEmail(){
        
        return $this->config[Email_Address];
    }
    
    
    public function getConfigDatabase(){
        
        return $this->config[Database];
    }
    
    public function getConfigSingleSubmit(){
        
        return $this->config[GeneralSetting][SingleSubmit];
    }
    
    public function getConfigSingleSubmitText(){
        
        return $this->config[GeneralSetting][SingleSubmitErrorTxt];
    }    
    
    public function getConfigSupportPreFilling(){
        
        return $this->config[GeneralSetting][SupportPreFilling];
    }
 
    public function getConfigLeadView(){
        
        return $this->config[LeadView];
    }
    
    public function getConfigUpdateLeadView(){
        
        return $this->config[LeadView][UpdateLeadView];
    }    
}
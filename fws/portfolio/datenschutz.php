<!DOCTYPE html>
<html lang="de">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>FWS | Ihr Spezialist für Kapitalanlagen und Pflegeimmobilien</title>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Cinzel" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">  
	  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="css/creative.css" rel="stylesheet">
  </head>

  <body id="page-top">
       <!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1741387319271621');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=1741387319271621&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-67495742-9"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-67495742-9');
</script>

       <!-- DESKTOP NAV -->


       
        <!-- NAVBAR -->      
        <nav>

            <div class="container">
                <div class="row">
                    <div class="col-lg-4 logo">
                        <a class="navbar-brand js-scroll-trigger" href="https://kapitalanlage-pflegeimmobilien.net/portfolio/#page-top"><img style="width: 390px !important; height: 80px !important; margin: 5px 15px;" src="img/kp_logo.png"></a>
                    </div>
                    <div class="text-center col-lg-8 text-right" style="padding-top: 37px;">
                        <h6 class="open-sans">Jetzt kostenlos informieren: <span style="color: #005577 !important; font-size: 1.4rem;"><i style="margin-left: 8px; margin-right: 2px;" class="fas fa-phone"></i> <strong>0800 1140069</strong></span></h6>
                    </div>
                </div>
            </div>
        </nav>     

        <!-- MOBILE HEADER -->

        <div class="container mobile-head">    
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <img style="width:100%;margin-bottom:15px;" src="img/kp_logo.png">
                        <center><h6 class="open-sans">Jetzt kostenlos informieren: <br><span style="color: #005577 !important; font-size: 1.4rem;"><i style="margin-left: 8px; margin-right: 2px;" class="fas fa-phone"></i> <strong><a href="tel:08001140069">0800 1140069</a></strong></span></h6></center>
                    </div>
                </div>       
            </div>  
        </div>
        <!-- /. MOBILE HEADER -->


        <div class="row">
            <div class="col-12">

                <div style="color:#fff; background-color:#557700; padding:5px 5px 5px 5px; font-size:15px!important;">

                    <center><i class="fas fa-check"></i> Über 33.000 Kundenberatungen &nbsp;&nbsp;<i class="fas fa-check"></i> Über 20 Jahre ihr verlässlicher Partner&nbsp;&nbsp;<i class="fas fa-check"></i> Kostenlos und unverbindlich informieren</center>
                </div>
            </div>
        </div>


      
<div class="clearspace"></div>  
      

    <section class="bg-primary" id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto">
              
             <p><small><a href="index.php">zurück</a></small></p> 
              
            
            
            
            
            
            
            
            
            
<h3 class="section-heading-alt open-sans">VERARBEITUNG PERSONENBEZOGENER DATEN</h3>
<h4 class="section-heading-alt open-sans"><strong>Datenschutzerklärung</strong></h4>

Mit der Übermittlung Ihrer Daten über das Formular erklären Sie sich einverstanden, dass die Flash Web Solutions e.K., Kastanienweg 20, 72639 Neuffen und ihre Kooperationspartner, ihre eingetragenen Daten nutzt und ihnen E-Mails zuschickt bzw. Sie anruft, um ihnen Angebote aus dem Bereich Kapitalanlageprodukte (insbesondere Pflegeimmobilien) zukommen zu lassen. Diese Einwilligung können Sie jederzeit widerrufen, etwa durch einen Brief, Adresse siehe Impressum, oder per Email an <a href="mailto:fws@web-creatorandconsulting.de">fws(at)web-creatorandconsulting(dot)de</a>. Anschließend wird jede werbliche Nutzung unterbleiben.<br><br>			  
			  
Der Datenschutz ist uns ein besonderes Anliegen. Unsere Aktivitäten zur Erfüllung der Anforderungen der Datenschutz-Grundverordnung (DS-GVO), des Bundesdatenschutzgesetzes (BDSG), des Telemediengesetzes (TMG) und des Sozialgesetzbuchs X (SGB X) sind an dem Ziel ausgerichtet, unserem Respekt vor Ihrer Privat- und Persönlichkeitssphäre Ausdruck zu verleihen.<br><br>

<strong>1. Verantwortliche Stelle für die Datenverarbeitungen und Kontaktmöglichkeiten des Datenschutzbeauftragten</strong><br><br>

Verantwortliche Stelle ist die <br><br>

Flash Web Solutions e.K<br><br>

Kastanienweg 20<br><br>

72639 Neuffen<br><br>

E-Mail: <a href="mailto:fws@web-creatorandconsulting.de">fws(at)web-creatorandconsulting(dot)de</a><br><br>

Telefon: +49 7025 / 911 07 40<br><br><br><br>


              <strong>2. Was personenbezogene Daten sind und woher wir sie erhalten</strong><br><br>

„Personenbezogene Daten“ sind alle Informationen, die sich auf eine identifizierte oder identifizierbare natürliche Person (im Folgenden „betroffene Person“) beziehen; als identifizierbar wird eine natürliche Person angesehen, die direkt oder indirekt, insbesondere mittels Zuordnung zu einer Kennung wie einem Namen, zu einer Kennnummer, zu Standortdaten, zu einer Online-Kennung oder zu einem oder mehreren besonderen Merkmalen identifiziert werden kann, die Ausdruck der physischen, physiologischen, genetischen, psychischen, wirtschaftlichen, kulturellen oder sozialen Identität dieser natürlichen Person sind.<br><br>

a) Daten, die beim Besuch der Internetseite anfallen<br><br>

Sie können unsere Internetseiten grundsätzlich ohne die Angabe personenbezogener Daten einsehen. Wir speichern hierbei standardmäßig ausschließlich die Website, von der aus Sie zu unserem Angebot gelangt sind, den Namen Ihres Internet-Service-Providers, welche Websites Sie innerhalb unseres Internet-Angebots besucht haben sowie Datum und Dauer Ihres Besuches. Hierfür werden für die Dauer Ihres Besuches im Arbeitsspeicher Ihres Computers kleine Dateien (so genannte Cookies) abgelegt. Es handelt sich dabei um so genannte temporäre Session-Cookies. Die entsprechenden Daten werden dabei auf Servern unseres Dienstleisters Host Europe, Hansestrasse 111, 51149 Köln gespeichert. Die Session-Cookies werden automatisch gelöscht, sobald Sie Ihr Browser-Fenster schließen. Wir erstellen mithilfe der Session-Cookies eine so genannte Session-ID für interne statistische Zwecke. Die gewonnenen Daten sind vollständig anonymisiert, enthalten also keinerlei Möglichkeit Sie als Person zu identifizieren.<br><br>

Ihre IP-Adresse sowie ein Zeitstempel werden aus Sicherheitsgründen gespeichert und lediglich für interne Zwecke genutzt. Die IP-Adresse ist dabei eine maschinenbezogene Kennung, die eine Aussage über den verwendeten Rechner für den Internet-Zugang oder benutzten Internet-Gateway macht, und zwar zum Zeitpunkt der Online-Abfrage. Der Begriff Zeitstempel bezeichnet einen Wert in einem definierten Format, der einem Ereignis (beispielsweise dem Senden oder Empfangen einer Nachricht, der Modifikation von Daten u. a.) einen Zeitpunkt zuordnet. Der Zweck eines Zeitstempels ist es, für Menschen oder Computer deutlich zu machen, wann welche Ereignisse eingetreten sind.<br><br>

b) Personenbezogene Daten, die Sie uns über ein Formular mitteilen<br><br>

Wir erhalten Ihre personenbezogenen Daten, wenn Sie diese Daten in die Formulare auf unserer Seite eingeben und uns so zur Verfügung stellen. Welche Daten dies genau sind hängt davon ab, für welche Dienstleistung Sie sich genau interessieren.<br><br>

Sie können sich einerseits für einen Newsletter anmelden. Insofern erheben wir folgende Daten:<br><br>

-           Ihr Geschlecht,<br><br>

-           Ihren vollständigen und richtigen Namen,<br><br>

-           Ihre E-Mail-Adresse,<br><br>

-           Ihre Firma (nicht fakultativ)<br><br>

-           Quelle - URL / genaue Adresse unserer Webseite<br><br>

-           IP Adresse Ihres Rechners bei der Eintragung Ihrer Daten<br><br>

-           Zeitstempel bei der Eintragung Ihrer Daten<br><br>

-           IP Adresse Ihres Rechners bei der Bestätigung mit dem Double-Opt-In Verfahrens<br><br>

-           Zeitstempels Ihres Rechners bei der Durchführung des Double-Opt-In Verfahrens<br><br>

Sie können ferner eine Erstberatung bestellen. Dann erheben wir neben den vorgenannten Adressen noch ihre Telefonnummer.<br><br>

Ferner kann es sein, dass auf unserer Website ein Webinar angeboten wird. Auch insofern erheben wir dann die vorgenannten Daten. <br><br>

              <strong>3. Zu welchen Zwecken wir Ihre personenbezogenen Daten verwenden</strong><br><br>

Personenbezogene Daten dürfen von uns nur mit Ihrer oder einer gesetzlichen Erlaubnis aus der DS-GVO, dem BDSG oder einem anderen den Datenschutz regelnden Gesetzen verarbeitet werden.<br><br>

a) Verarbeitung aufgrund Ihrer Einwilligung (Art. 6 Abs. 1 a) DS-GVO<br><br>

Wir verarbeiten Ihre Daten aufgrund einer Einwilligung, wenn Sie einen Newsletter abonnieren bzw. sich für eine Erstberatung anmelden. In diesem Fall erhalten Sie entweder den Newsletter oder Sie werden von uns in der gewünschten Weise werblich kontaktiert. Nach einem Widerruf einer Einwilligung werden die Daten nicht weiter werblich genutzt. Sie werden aber noch bis zu drei Jahren gespeichert, um die Rechtmäßigkeit von früheren Kontaktaufnahmen im Zweifelsfall beweisen zu können.<br><br>

b) Verarbeitung zur Erfüllung von vertraglichen Pflichten (Art. 6 Abs. 1 b) DS-GVO)<br><br>

Die Erhebung Ihrer personenbezogenen Daten bei einem Webinar dient uns in erster Linie dazu, das Webinar mit Ihnen durchführen zu können.<br><br> 

              <strong>4. Wer Ihre Daten empfängt und wann eine Übermittlung in Drittländer erfolgt</strong><br><br>

In den folgenden Unterpunkten erklären wir, wem wir Ihre Daten mitteilen und wann wir eine Übermittlung in sog. Drittländer vornehmen. Drittländer sind solche Länder, die außerhalb des europäischen Wirtschaftsraums liegen. Intern werden Ihre Daten bei uns von mehreren Abteilungen verarbeitet. An externe Dienstleister übermitteln wir Daten vor allem, da wir manche Leistungen nicht oder nicht sinnvoll selbst erbringen können. Wir haben externe Dienstleister, die wir für alle unsere Datenverarbeitungen einsetzen und externe Dienstleister, an die nur dann Daten gegeben werden, wenn Sie sich für eine bestimmte Vermittlung entscheiden.<br><br>

Intern haben alle Abteilungen Zugriff auf Ihre Daten, welche die Daten benötigen, um die oben genannten Zwecke zu erfüllen. Zudem setzen wir externe Dienstleister ein, um die Daten zu diesen Zwecken zu verarbeiten. Diese externen Dienstleister sind vor allem Anbieter von IT-Dienstleistungen und Telekommunikation sowie Telefonnummern- und Adressvalidierungsdienste. Außerdem setzen wir zum technischen Versand unseres E-Mail-Newsletters externe Dienstleister ein. <br><br>

Unsere Daten werden zum einen über den Hosting Dienst Host Europe GmbH, Hansestraße 111, 51149 Köln, gespeichert. Insofern gibt es eine ADV. Allerdings setzen wir zur Speicherung der bei der Registrierung erhobenen Daten setzen wir den Dienst Windows Office 365 ein. Das hat zur Folge, dass die Daten auf einem Server der Microsoft Coorporation gespeichert werden. Mit der Microsoft Coorportation wird ein umfassender Vertrag zur Auftragsdatenverarbeitung (ADV) geschlossen, der die EU-Standardvertragsklauseln ebenso wie die Selbstzertifizierung gemäß dem Safe-Harbor-Abkommen zwischen den USA und der EU umfasst.<br><br> 

Zur  Speicherung von Daten und zur Versendung von E-Mails setzen wir auch die Dienstleister XQueue GmbH, Christian-Pleß-Str. 11-13, 63069 Offenbach am Main, und die HubSpot, Inc., 25 First Street 2nd Floor, Cambridge, MA 02141 United States, und Mailocator s.r.o, Piletická 486, 503 41 Hradec Králové, ein. Mit allen drei Gesellschaften wurden Auftragsdatenvereinbarungen geschlossen.<br><br> 

              <strong>5. Wie lange Ihre Daten gespeichert werden </strong><br><br>

Die Daten werden gelöscht, wenn sie für den benötigten Zweck nicht mehr genötigt werden.<br><br> 

Haben Sie nur eine einmalige Beratung beantragt, werden Ihre Daten sechs Monate nach der Eintragung gelöscht.<br><br>

Haben Sie unseren E-Mail-Newsletter abonniert, sei es aufgrund Ihrer Einwilligung, oder weil Sie Kunde bei uns sind, speichern wir die für die Versendung erforderlichen Daten bis drei Jahre nach Ihrer Abmeldung. Allerdings werden wir die Daten nach dem Widerruf der Einwilligung nicht mehr zu Werbezwecken einsetzen, sondern nur noch Nachweiszwecken speichern.<br><br>

              <strong>6. Ihre Betroffenenrechte und Ihr Widerruf einer Einwilligung</strong><br><br>

Die Datenschutz-Grundverordnung garantiert Ihnen gewisse Rechte, die Sie uns gegenüber geltend machen können. Sie haben das Recht: <br><br>

-           von uns eine Bestätigung darüber zu verlangen, ob Sie betreffende personenbezogene Daten verarbeitet werden und wenn ja, die näheren Umstände der Datenverarbeitung (Art. 15 DS-GVO: Auskunftsrecht der betroffenen Person),<br><br>

-           von uns unverzüglich die Berichtigung Sie betreffender unrichtiger personenbezogener Daten zu verlangen. Dabei haben Sie unter Berücksichtigung der Zwecke der Verarbeitung auch das Recht, die Vervollständigung unvollständiger personenbezogener Daten – auch mittels einer ergänzenden Erklärung – zu verlangen (Art. 16 DS-GVO: Recht auf Berichtigung),<br><br>

-           von uns zu verlangen, dass Sie betreffende personenbezogene Daten unverzüglich gelöscht werden (Art. 17 DS-GVO: Recht auf Löschung),<br><br>

 -           von uns die Einschränkung der Verarbeitung zu verlangen (Art. 18 DS-GVO: Recht auf Einschränkung der Verarbeitung), <br><br>

-           im Falle der Verarbeitung aufgrund einer Einwilligung oder zur Erfüllung eines Vertrags, die Sie betreffenden personenbezogenen Daten, die Sie uns bereitgestellt haben, in einem strukturierten, gängigen und maschinenlesbaren Format zu erhalten, und diese Daten einem anderen Verantwortlichen ohne Behinderung durch uns zu übermitteln oder die Daten direkt an den anderen Verantwortlichen zu übermitteln, soweit dies technisch machbar ist (Art. 20 DS-GVO: Recht auf Datenübertragbarkeit), <br><br>

-           aus Gründen, die sich aus ihrer besonderen Situation ergeben, jederzeit gegen die Verarbeitung Sie betreffender personenbezogener Daten, die für die Wahrung einer Aufgabe im öffentlichen Interesse erforderlich ist, oder die in Ausübung öffentlicher Gewalt erfolgt, Widerspruch einzulegen (Art. 21 DS-GVO: Widerspruchsrecht),<br><br>

-           jederzeit Beschwerde bei einer Aufsichtsbehörde, insbesondere in dem Mitgliedstaat ihres Aufenthaltsorts, ihres Arbeitsplatzes oder des Orts des mutmaßlichen Verstoßes, einzulegen, wenn Sie der Ansicht sind, dass die Verarbeitung der sie betreffenden personenbezogenen Daten gegen geltendes Recht verstößt (Art. 77 DSGVO i. V. m. § 19 BDSG: Recht auf Beschwerde bei einer Aufsichtsbehörde).<br><br>

Wenn Sie uns eine Einwilligung erteilt haben, haben Sie schließlich das Recht, Ihre Einwilligung jederzeit zu widerrufen. Alle Datenverarbeitungen die wir bis zu Ihrem Widerruf vorgenommen haben, bleiben in diesem Fall rechtmäßig. Zu diesem Zweck können Sie einfach den in jeder Mail enthaltenen Link anklicken und sich von dem E-Mail-Dienst abmelden oder eine Nachricht an <a href="mailto:fws@web-creatorandconsulting.de">fws(at)web-creatorandconsulting(dot)de</a> schicken. Wenn Sie uns in dieser Nachricht mitteilen, künftig keine E-Mails erhalten zu wollen, werden wir an die von Ihnen angegebene E-Mail-Adresse keine Nachrichten mehr versenden. Hiervon unberührt sind E-Mails, die wir Ihnen zur Erfüllung eines etwaig mit Ihnen geschlossenen Vertrags übermitteln (zum Beispiel von Ihnen angefragte Vergleichsangebote).<br><br>

              <strong>7. Ihre Pflicht zur Bereitstellung von personenbezogenen Daten</strong><br><br>

Sie haben keine vertragliche, oder gesetzliche Pflicht uns personenbezogene Daten bereitzustellen. Allerdings sind wir ohne die von Ihnen mitgeteilten Daten nicht in der Lage, Ihnen unsere vertraglichen Leistungen und Services anzubieten.<br><br>

              <strong>8. Bestehen von automatisierten Entscheidungsfindungen (einschließlich Profiling)</strong><br><br>

Wir verwenden keine automatisierte Entscheidungsfindungen.<br><br>

              <strong>9. Sicherheit</strong><br><br>

Wir setzen technische und organisatorische Sicherheitsmaßnahmen ein, um zu gewährleisten, dass Ihre persönlichen Daten vor Verlust, unrichtigen Veränderungen oder unberechtigten Zugriffen Dritter geschützt sind. In jedem Fall haben von unserer Seite nur berechtigte Personen Zutritt zu Ihren persönlichen Daten, und dies auch nur insoweit, als es im Rahmen der oben genannten Zwecke erforderlich ist. Die Sicherheitsmaßnahmen werden ständig den verbesserten technischen Möglichkeiten angepasst.<br><br>

              <strong>10. Welche internetspezifischen Datenverarbeitungen anfallen</strong><br><br>

a) Cookies <br><br>

Im Rahmen unserer Internetseiten verwenden wir so genannte Cookies. Hierbei handelt es sich um kleine Dateien, die auf Ihrer Festplatte gespeichert werden und durch welche uns bestimmte Informationen zufließen. Bei diesen Informationen handelt es sich insbesondere um den Login (Ihren Besuch), das Datum und die Zeit Ihres Besuches bei uns, die Cookie-Nummer sowie die URL der Website, von der aus Sie auf unsere Angebotsseiten gelangt sind. Die entsprechenden Daten werden dabei auf Servern unseres Dienstleisters gespeichert.<br><br>

Der Einsatz von Cookies ermöglicht es uns zunächst, Sie zu erkennen. Darüber hinaus können wir durch die Cookies unser Angebot optimal an Ihre individuellen Bedürfnisse anpassen. Außerdem verwenden wir Cookies, um die statistische Häufigkeit der Aufrufe der verschiedenen Seiten unseres Internet-Angebots sowie die allgemeine Navigation aufzuzeichnen. <br><br>

Falls Sie die Verwendung von Cookies jedoch unterbinden wollen, besteht dazu bei Ihrem Browser die Möglichkeit, die Annahme und Speicherung neuer Cookies zu verhindern. Unser Angebot können Sie auch in diesem Fall nutzen, doch kann es zu einer Einschränkung des Leistungsumfangs kommen. Um herauszufinden, wie dies bei dem von Ihnen verwendeten Browser funktioniert, benutzen Sie bitte die Hilfe-Funktion des jeweiligen Browsers oder wenden Sie sich an den Hersteller. Wir empfehlen Ihnen jedoch, die Cookies-Funktionen eingeschaltet zu lassen, da Ihnen nur dann das hohe Niveau des Benutzerkomforts, um das wir uns ständig bemühen, in vollem Umfang zu Gute kommt.<br><br>

b) Facebook Pixel <br><br>

Wir verwenden das Facebook-Pixel der Facebook Ireland Limited, 4 Grand Canal Square, Dublin 2, Irland auf unseren Seiten. Dieses bietet mehrere Funktionen an, von der wir die Funktion Custom Audiences benutzen. Hiermit kann Ihnen – wenn Sie ein Nutzerkonto bei Facebook haben – auf Facebook gezielt unsere Werbung angezeigt werden. Das Facebook Pixel stellt hierzu eine direkte Verbindung ihrer Seite mit den Facebook-Servern her. Dabei werden Ihre Nutzungsdaten an Facebook zu Analyse- und Marketingzwecken übermittelt. Haben Sie ein Facebook-Konto, so können diese Daten Ihnen zugeordnet werden. Durch das Tracking können Ihre Aktivitäten von Facebook so unter Umständen über mehrere Seiten verfolgt werden. Wir haben keinerlei Einfluss auf eventuelle Verarbeitungen von personenbezogenen Daten, da diese allein im Verantwortungsbereich von Facebook liegen. Nähere Informationen zur Erhebung und Nutzung der Daten durch Facebook finden Sie in den Datenschutzhinweisen von Facebook unter www.facebook.com/policy.php.<br><br>

Wenn Sie der Nutzung von Facebook Website Custom Audiences widersprechen möchten, können Sie die entsprechenden Einstellungen in Ihrem Facebook Nutzerkonto vornehmen unter:<br><br>

www.facebook.com/ads/website_custom_audiences/.<br><br>

c) Facebook Social Plugins<br><br>

Dieses Angebot verwendet Social Plugins („Plugins“) des sozialen Netzwerkes facebook.com, welches von der Facebook Inc., 1601 S. California Ave, Palo Alto, CA 94304, USA betrieben wird („Facebook“). Die Plugins sind an einem der Facebook-Logos erkennbar (weißes „f“ auf blauer Kachel oder ein „Daumen hoch“-Zeichen) oder sind mit dem Zusatz „Facebook Social Plugin“ gekennzeichnet. Die Liste und das Aussehen der Facebook Social Plugins kann hier eingesehen werden: developers.facebook.com/docs/plugins/.<br><br>

Wenn ein Nutzer eine Website dieses Angebots aufruft, die ein solches Plugin enthält, baut sein Browser eine direkte Verbindung mit den Servern von Facebook auf. Der Inhalt des Plugins wird von Facebook direkt an Ihren Browser übermittelt und von diesem in die Website eingebunden. Der Anbieter hat daher keinen Einfluss auf den Umfang der Daten, die Facebook mit Hilfe dieses Plugins erhebt und informiert die Nutzer daher entsprechend seinem Kenntnisstand:<br><br>

Durch die Einbindung der Plugins erhält Facebook die Information, dass ein Nutzer die entsprechende Seite des Angebots aufgerufen hat. Ist der Nutzer bei Facebook eingeloggt, kann Facebook den Besuch seinem Facebook-Konto zuordnen. Wenn Nutzer mit den Plugins interagieren, zum Beispiel den Like Button betätigen oder einen Kommentar abgeben, wird die entsprechende Information von Ihrem Browser direkt an Facebook übermittelt und dort gespeichert. Falls ein Nutzer kein Mitglied von Facebook ist, besteht trotzdem die Möglichkeit, dass Facebook seine IP-Adresse in Erfahrung bringt und speichert. Laut Facebook wird in Deutschland nur eine anonymisierte IP-Adresse gespeichert.<br><br>

Zweck und Umfang der Datenerhebung und die weitere Verarbeitung und Nutzung der Daten durch Facebook sowie die diesbezüglichen Rechte und Einstellungsmöglichkeiten zum Schutz der Privatsphäre der Nutzer können diese den Datenschutzhinweisen von Facebook entnehmen: www.facebook.com/about/privacy/.<br><br>

Wenn ein Nutzer Facebookmitglied ist und nicht möchte, dass Facebook über dieses Angebot Daten über ihn sammelt und mit seinen bei Facebook gespeicherten Mitgliedsdaten verknüpft, muss er sich vor dem Besuch des Internetauftritts bei Facebook ausloggen.<br><br>

Ebenfalls ist es möglich Facebook-Social-Plugins mit Add-ons für Ihren Browser zu blocken, zum Beispiel mit dem „Facebook Blocker“.<br><br>

d) Google Analytics<br><br>

Diese Website und weitere Online-Angebote von finanzen.de benutzen den Webanalysedienst Google Analytics und den Dienst Google Remarketing, beides Angebote der Google Inc. („Google“). Durch den Dienst Google Remarketing werden Nutzer, die unsere Internetseiten und Onlinedienste bereits besucht und sich für das Angebot interessiert haben, durch zielgerichtete Werbung auf den Seiten des Google Partner Netzwerks erneut angesprochen. Die Einblendung der Werbung erfolgt durch den Einsatz von Cookies. Mit Hilfe der Textdateien kann das Nutzerverhalten beim Besuch der Website analysiert und anschließend für gezielte Produktempfehlungen und interessenbasierte Werbung genutzt werden. Auch Google Analytics verwendet Cookies. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website (einschließlich Ihrer IP-Adresse) werden an einen Server von Google in den USA übertragen und dort gespeichert. Wir setzen aber Google Analytics selbstverständlich mit der Funktion anonymizeIP ein, wodurch die IP-Adresse durch Maskierung absolut anonymisiert wird.<br><br>

Google wird die Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten für die Websitebetreiber zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen zu erbringen. Auch wird Google diese Informationen gegebenenfalls an Dritte übertragen, sofern dies gesetzlich vorgeschrieben ist oder soweit Dritte diese Daten im Auftrag von Google verarbeiten. Drittanbieter, einschließlich Google, verwenden die in den Cookies gespeicherten Informationen im Rahmen des Dienstes Google Remarketing auch zum Schalten von Anzeigen auf anderen Websites auf Grundlage vorheriger Besuche eines Nutzers auf dieser Website. Google wird in keinem Fall Ihre ohnehin maskierte IP-Adresse mit anderen Daten von Google in Verbindung bringen.<br><br>

Sie können die Installation der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software oder mittels der Installation eines Browser-Add-ons (abrufbar unter: https://tools.google.com/dlpage/gaoptout?hl=de) verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich nutzen können. Durch die Nutzung dieser Website erklären Sie sich mit der Bearbeitung der über Sie erhobenen Daten durch Google in der zuvor beschriebenen Art und Weise und zu dem zuvor benannten Zweck einverstanden.<br><br>

e) Google AdWords<br><br>

Unsere Webseite nutzt AdWords, eine Technologie von Google.<br><br>

Wir verwenden zum einen das Conversion-Tracking. Das bedeutet, dass wir mittels eines Cookies erkennen können, über welche Anzeige des AdWords-Netzwerk Sie zu uns gelangt sind und welches Verhalten so auf unserer Seite ausgelöst wurde. Für uns ergibt sich so ein besseres Bild über die Wirksamkeit unserer Anzeigen und wir nutzen die Daten für Zwecke der Statistik und Marktforschung. Diese Daten sind für uns anonym. Das bedeutet, wir können Sie nicht Ihrer Person zuordnen.<br><br>

Unsere Website nutzt zum anderen die Remarketing-Funktion von Google Adwords. Diese Funktion hilft uns dabei unseren Besuchern interessenbezogene Werbeanzeigen zu präsentieren. Ihr Browser speichert dazu Cookies auf Ihrem Endgerät, die es uns ermöglichen Sie wiederzuerkennen, wenn Sie Websites aufrufen, die dem Werbenetzwerk von Google angehören. So können Ihnen Werbeanzeigen präsentiert werden, die sich auf Inhalte beziehen, welche Sie sich zuvor auf anderen Websites angesehen haben. Wir erhalten hierdurch keine personenbezogenen Daten von Ihnen.<br><br>

Sie können die Remarketing Funktion deaktivieren, indem Sie die entsprechenden Einstellungen unter<br><br>

http://www.google.com/settings/ads<br><br>

vornehmen.<br><br>

f) Google +1-Schaltfläche<br><br>

Dieses Angebot verwendet die „+1“-Schaltfläche des sozialen Netzwerkes Google Plus, welches von der Google Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, United States betrieben wird („Google“). Der Button ist an dem Zeichen „+1“ auf weißem oder farbigem Hintergrund erkennbar.<br><br>

Wenn ein Nutzer eine Website dieses Angebotes aufruft, die eine solche Schaltfläche enthält, baut der Browser eine direkte Verbindung mit den Servern von Google auf. Der Inhalt der „+1“-Schaltfläche wird von Google direkt an seinen Browser übermittelt und von diesem in die Website eingebunden. Der Anbieter hat daher keinen Einfluss auf den Umfang der Daten, die Google mit der Schaltfläche erhebt. Laut Google werden ohne einen Klick auf die Schaltfläche keine personenbezogenen Daten erhoben. Nur bei eingeloggten Mitgliedern werden solche Daten, unter anderem die IP-Adresse, erhoben und verarbeitet.<br><br>

Zweck und Umfang der Datenerhebung und die weitere Verarbeitung und Nutzung der Daten durch Google sowie Ihre diesbezüglichen Rechte und Einstellungsmöglichkeiten zum Schutz Ihrer Privatsphäre können die Nutzer Googles Datenschutzhinweisen zu der „+1“-Schaltfläche entnehmen: www.google.com/intl/de/+/policy/+1button.html und den FAQ: www.google.com/intl/de/+1/button/.<br><br>

i) Twitter<br><br>

Dieses Angebot nutzt die Schaltflächen des Dienstes Twitter. Diese Schaltflächen werden angeboten durch die Twitter Inc., 795 Folsom St., Suite 600, San Francisco, CA 94107, USA. Sie sind an Begriffen wie „Twitter“ oder „Folge“, verbunden mit einem stilisierten blauen Vogel, erkennbar. Mit Hilfe der Schaltflächen ist es möglich, einen Beitrag oder eine Seite dieses Angebotes bei Twitter zu teilen oder dem Anbieter bei Twitter zu folgen.<br><br>

Wenn ein Nutzer eine Website dieses Internetauftritts aufruft, die einen solchen Button enthält, baut sein Browser eine direkte Verbindung mit den Servern von Twitter auf. Der Inhalt der Twitter-Schaltflächen wird von Twitter direkt an den Browser des Nutzers übermittelt. Der Anbieter hat daher keinen Einfluss auf den Umfang der Daten, die Twitter mit Hilfe dieses Plugins erhebt und informiert die Nutzer entsprechend seinem Kenntnisstand. Nach diesem werden lediglich die IP-Adresse des Nutzers sowie die URL der jeweiligen Website beim Bezug des Buttons mit übermittelt, aber nicht für andere Zwecke als die Darstellung des Buttons genutzt.<br><br>

Weitere Informationen hierzu finden sich in der Datenschutzerklärung von Twitter unter twitter.com/privacy.<br><br>

<strong>Stand: Februar 2018</strong><br><br>
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
 <p><small><a href="index.php">zurück</a></small></p> 
          </div>
          
        </div>
      </div>
    </section>
      
<div class="clearspace"></div>       
<div class="clearspace-trans"></div> 
<div class="clearspace-trans"></div> 
<!-- FOOTER -->
        <section id="contact">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 mx-auto text-center">
                        <div style="margin-bottom: 50px;">
                            <p><small><strong>*Staatlich gesicherte Miete:<br></strong>
                                    Die Pflegeimmobilie gehört zu den Sozialimmobilien, die im Rahmen des SGB XI (Sozialgesetzbuch-Elftes Buch-Soziale Pflegeversicherung) als förderungswürdig gelten. Kann der Bewohner sein Pflegeappartements nicht mehr selbst finanzieren, springt daher der Staat ein. Das bedeutet: der Betreiber (Ihr Mieter) kann sich stets darauf verlassen, dass er für belegte Appartements seine kalkulierten Einnahmen erhält.</small></p>
                        </div>
                        <div class="footer-image"><img src="img/fws_logo.png"></div>
                        <div class="clearspace-trans"></div>
                        <h5 class="section-heading open-sans"><b>Kapitalanlage-Pflegeimmobilien <br>ist ein Produkt der FLASH-WEB-SOLUTIONS e.K.</b></h5>
                        <hr class="my-4">
                        <h2 class="section-heading">Ihr Spezialist für Kapitalanlagen Pflegeimmobilien</h2>
                        <div class="clearspace-trans"></div>
                        <div class="footer-image"><img src="img/sicherheit.png"></div>
                        <div class="clearspace-trans"></div>
                        <div class="clearspace-trans"></div>
                        <div class="clearspace-trans"></div>
                        <h5>
                            <a href="impressum.php" target="_blank">IMPRESSUM</a> | 
                            <a href="https://kapitalanlage-pflegeimmobilien.net/portfolio/#page-top">KONTAKT</a> | 
                            <a href="datenschutz.php" target="_blank">DATENSCHUTZ</a> | 
                            <a href="optionen.php" target="_blank">OPTIONEN</a> | 
                            <a href="ueber-fws.php" target="_blank">ÜBER UNS</a>
                        </h5>
                        <p>&nbsp;</p>
                    </div>
                </div>
            </div>
        </section>


    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>

  </body>

</html>

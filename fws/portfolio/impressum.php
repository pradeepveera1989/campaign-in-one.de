<!DOCTYPE html>
<html lang="de">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>FWS | Ihr Spezialist für Kapitalanlagen und Pflegeimmobilien</title>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Cinzel" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">  
	  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="css/creative.css" rel="stylesheet">
  </head>

  <body id="page-top">
 <!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '166146667427092');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=166146667427092&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-67495742-9"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-67495742-9');
</script>
    <   <!-- DESKTOP NAV -->


        <!-- NAVBAR -->      
        <nav>

            <div class="container">
                <div class="row">
                    <div class="col-lg-4 logo">
                        <a class="navbar-brand js-scroll-trigger" href="https://kapitalanlage-pflegeimmobilien.net/portfolio/#page-top"><img style="width: 390px !important; height: 80px !important; margin: 5px 15px;" src="img/kp_logo.png"></a>
                    </div>
                    <div class="text-center col-lg-8 text-right" style="padding-top: 37px;">
                        <h6 class="open-sans">Jetzt kostenlos informieren: <span style="color: #005577 !important; font-size: 1.4rem;"><i style="margin-left: 8px; margin-right: 2px;" class="fas fa-phone"></i> <strong>0800 1140069</strong></span></h6>
                    </div>
                </div>
            </div>
        </nav>     

        <!-- MOBILE HEADER -->

        <div class="container mobile-head">    
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <img style="width:100%;margin-bottom:15px;" src="img/kp_logo.png">
                        <center><h6 class="open-sans">Jetzt kostenlos informieren: <br><span style="color: #005577 !important; font-size: 1.4rem;"><i style="margin-left: 8px; margin-right: 2px;" class="fas fa-phone"></i> <strong><a href="tel:08001140069">0800 1140069</a></strong></span></h6></center>
                    </div>
                </div>       
            </div>  
        </div>
        <!-- /. MOBILE HEADER -->


        <div class="row">
            <div class="col-12">

                <div style="color:#fff; background-color:#557700; padding:5px 5px 5px 5px; font-size:15px!important;">

                    <center><i class="fas fa-check"></i> Über 33.000 Kundenberatungen &nbsp;&nbsp;<i class="fas fa-check"></i> Über 20 Jahre ihr verlässlicher Partner&nbsp;&nbsp;<i class="fas fa-check"></i> Kostenlos und unverbindlich informieren</center>
                </div>
            </div>
        </div>


<div class="clearspace"></div>  
      

    <section class="bg-primary" id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto">
              
              <p><small><a href="index.php">zurück</a></small></p>

             <h2 class="section-heading-alt">Impressum</h2>
              
<p>Flash Web Solutions e.K.<br>
Inhaber Rüdiger Finn<br>
Kastanienweg 20<br>
72639 Neuffen<br>
Fon   +49 7025 / 911 07 40<br>
Fax   +49 7025 / 911 07 41<br>
USt-ID: DE193451333<br><br>

E-mail: <a href="mailto:fws@web-creatorandconsulting.de">fws(at)web-creatorandconsulting(dot)de</a><br>

<br></p>
              
<h2 class="section-heading-alt">Haftungsausschluss</h2><br><br>

<h2 class="section-heading-alt">1. Inhalt des Onlineangebotes</h2>

Der Autor übernimmt keinerlei Gewähr für die Aktualität, Korrektheit, Vollständigkeit oder Qualität der bereitgestellten Informationen. Haftungsansprüche gegen den Autor, welche sich auf Schäden materieller oder ideeller Art beziehen, die durch die Nutzung oder Nichtnutzung der dargebotenen Informationen bzw. durch die Nutzung fehlerhafter und unvollständiger Informationen verursacht wurden, sind grundsätzlich ausgeschlossen, sofern seitens des Autors kein nachweislich vorsätzliches oder grob fahrlässiges Verschulden vorliegt. Alle Angebote sind freibleibend und unverbindlich. Der Autor behält es sich ausdrücklich vor, Teile der Seiten oder das gesamte Angebot ohne gesonderte Ankündigung zu verändern, zu ergänzen, zu löschen oder die Veröffentlichung zeitweise oder endgültig einzustellen.<br><br>
    
<h2 class="section-heading-alt">2. Verweise und Links</h2>

Bei direkten oder indirekten Verweisen auf fremde Webseiten ("Hyperlinks"), die außerhalb des Verantwortungsbereiches des Autors liegen, würde eine Haftungsverpflichtung ausschließlich in dem Fall in Kraft treten, in dem der Autor von den Inhalten Kenntnis hat und es ihm technisch möglich und zumutbar wäre, die Nutzung im Falle rechtswidriger Inhalte zu verhindern. Der Autor erklärt hiermit ausdrücklich, dass zum Zeitpunkt der Linksetzung keine illegalen Inhalte auf den zu verlinkenden Seiten erkennbar waren. Auf die aktuelle und zukünftige Gestaltung, die Inhalte oder die Urheberschaft der verlinkten/verknüpften Seiten hat der Autor keinerlei Einfluss. Deshalb distanziert er sich hiermit ausdrücklich von allen Inhalten aller verlinkten /verknüpften Seiten, die nach der Linksetzung verändert wurden. Diese Feststellung gilt für alle innerhalb des eigenen Internetangebotes gesetzten Links und Verweise sowie für Fremdeinträge in vom Autor eingerichteten Gästebüchern, Diskussionsforen, Linkverzeichnissen, Mailinglisten und in allen anderen Formen von Datenbanken, auf deren Inhalt externe Schreibzugriffe möglich sind. Für illegale, fehlerhafte oder unvollständige Inhalte und insbesondere für Schäden, die aus der Nutzung oder Nichtnutzung solcherart dargebotener Informationen entstehen, haftet allein der Anbieter der Seite, auf welche verwiesen wurde, nicht derjenige, der über Links auf die jeweilige Veröffentlichung lediglich verweist.<br><br>
    
<h2 class="section-heading-alt">3. Urheber- und Kennzeichenrecht</h2>

Der Autor ist bestrebt, in allen Publikationen die Urheberrechte der verwendeten Bilder, Grafiken, Tondokumente, Videosequenzen und Texte zu beachten, von ihm selbst erstellte Bilder, Grafiken, Tondokumente, Videosequenzen und Texte zu nutzen oder auf lizenzfreie Grafiken, Tondokumente, Videosequenzen und Texte zurückzugreifen. Alle innerhalb des Internetangebotes genannten und ggf. durch Dritte geschützten Marken- und Warenzeichen unterliegen uneingeschränkt den Bestimmungen des jeweils gültigen Kennzeichenrechts und den Besitzrechten der jeweiligen eingetragenen Eigentümer. Allein aufgrund der bloßen Nennung ist nicht der Schluss zu ziehen, dass Markenzeichen nicht durch Rechte Dritter geschützt sind! Das Copyright für veröffentlichte, vom Autor selbst erstellte Objekte bleibt allein beim Autor der Seiten. Eine Vervielfältigung oder Verwendung solcher Grafiken, Tondokumente, Videosequenzen und Texte in anderen elektronischen oder gedruckten Publikationen ist ohne ausdrückliche Zustimmung des Autors nicht gestattet.<br><br>
    
<h2 class="section-heading-alt">4. Datenschutz</h2>

Sofern innerhalb des Internetangebotes die Möglichkeit zur Eingabe persönlicher oder geschäftlicher Daten (Emailadressen, Namen, Anschriften) besteht, so erfolgt die Preisgabe dieser Daten seitens des Nutzers auf ausdrücklich freiwilliger Basis. Die Inanspruchnahme und Bezahlung aller angebotenen Dienste ist - soweit technisch möglich und zumutbar - auch ohne Angabe solcher Daten bzw. unter Angabe anonymisierter Daten oder eines Pseudonyms gestattet. Die Nutzung der im Rahmen des Impressums oder vergleichbarer Angaben veröffentlichten Kontaktdaten wie Postanschriften, Telefon- und Faxnummern sowie Emailadressen durch Dritte zur Übersendung von nicht ausdrücklich angeforderten Informationen ist nicht gestattet. Rechtliche Schritte gegen die Versender von sogenannten Spam-Mails bei Verstössen gegen dieses Verbot sind ausdrücklich vorbehalten.<br><br>
    
<h2 class="section-heading-alt">5. Rechtswirksamkeit dieses Haftungsausschlusses</h2>

Dieser Haftungsausschluss ist als Teil des Internetangebotes zu betrachten, von dem aus auf diese Seite verwiesen wurde. Sofern Teile oder einzelne Formulierungen dieses Textes der geltenden Rechtslage nicht, nicht mehr oder nicht vollständig entsprechen sollten, bleiben die übrigen Teile des Dokumentes in ihrem Inhalt und ihrer Gültigkeit davon unberührt.<br><br>

 <p><small><a href="index.php">zurück</a></small></p>           
            
            

          </div>
        </div>
      </div>
    </section>
      
<div class="clearspace"></div>       
<div class="clearspace-trans"></div> 
<div class="clearspace-trans"></div> 
      
 <!-- FOOTER -->
        <section id="contact">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 mx-auto text-center">
                        <div style="margin-bottom: 50px;">
                            <p><small><strong>*Staatlich gesicherte Miete:<br></strong>
                                    Die Pflegeimmobilie gehört zu den Sozialimmobilien, die im Rahmen des SGB XI (Sozialgesetzbuch-Elftes Buch-Soziale Pflegeversicherung) als förderungswürdig gelten. Kann der Bewohner sein Pflegeappartements nicht mehr selbst finanzieren, springt daher der Staat ein. Das bedeutet: der Betreiber (Ihr Mieter) kann sich stets darauf verlassen, dass er für belegte Appartements seine kalkulierten Einnahmen erhält.</small></p>
                        </div>
                        <div class="footer-image"><img src="img/fws_logo.png"></div>
                        <div class="clearspace-trans"></div>
                        <h5 class="section-heading open-sans"><b>Kapitalanlage-Pflegeimmobilien <br>ist ein Produkt der FLASH-WEB-SOLUTIONS e.K.</b></h5>
                        <hr class="my-4">
                        <h2 class="section-heading">Ihr Spezialist für Kapitalanlagen Pflegeimmobilien</h2>
                        <div class="clearspace-trans"></div>
                        <div class="footer-image"><img src="img/sicherheit.png"></div>
                        <div class="clearspace-trans"></div>
                        <div class="clearspace-trans"></div>
                        <div class="clearspace-trans"></div>
                        <h5>
                            <a href="impressum.php" target="_blank">IMPRESSUM</a> | 
                            <a href="https://kapitalanlage-pflegeimmobilien.net/portfolio/#page-top">KONTAKT</a> | 
                            <a href="datenschutz.php" target="_blank">DATENSCHUTZ</a> | 
                            <a href="optionen.php" target="_blank">OPTIONEN</a> | 
                            <a href="ueber-fws.php" target="_blank">ÜBER UNS</a>
                        </h5>
                        <p>&nbsp;</p>
                    </div>
                </div>
            </div>
        </section>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>

  </body>

</html>

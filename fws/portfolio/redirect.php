<?php
$config = parse_ini_file("config.ini", true);

$webgains = false;
if ($_GET['trafficsource'] === "webgains") {
    $webgains = true;
}

$lead_reference = $_GET['lead_reference'];
?>


<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>FWS | Ihr Spezialist für Kapitalanlagen und Pflegeimmobilien</title>
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Cinzel" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">  
        <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
        <link href="css/creative.css" rel="stylesheet">

        <!-- Google Analytics -->
        <?php include_once 'clients/tracking/google/google.php'; ?>        

        <!-- Facebook Pixel -->
        <?php include_once('clients/tracking/facebook/facebook.php'); ?>          
        
    </head>


    <!-- Outbrain Tracking -->
    <?php $config[TrackingTools][EnableOutbrain] ? include_once'clients/tracking/outbrain/outbrain_redirect.php' : ' '; ?>

    <body id="page-top">
        <!--Adeblo Tracking-->
        <?php $config[TrackingTools][EnableAdeblo] ? include_once'clients/tracking/adeblo/adeblo.php' : ' ' ?>
        
        <!-- Remarketing Target360 Tracking -->
        <?php $config[TrackingTools][EnableTarget360] ? include_once 'clients/tracking/target360/target_redirect.php' : ' ' ?>;    

       <!-- DESKTOP NAV -->


       
        <!-- NAVBAR -->      
        <nav>

            <div class="container">
                <div class="row">
                    <div class="col-lg-4 logo">
                        <a class="navbar-brand js-scroll-trigger" href="https://kapitalanlage-pflegeimmobilien.net/portfolio/#page-top"><img style="width: 390px !important; height: 80px !important; margin: 5px 15px;" src="img/kp_logo.png"></a>
                    </div>
                    <div class="text-center col-lg-8 text-right" style="padding-top: 37px;">
                        <h6 class="open-sans">Jetzt kostenlos informieren: <span style="color: #005577 !important; font-size: 1.4rem;"><i style="margin-left: 8px; margin-right: 2px;" class="fas fa-phone"></i> <strong>0800 1140069</strong></span></h6>
                    </div>
                </div>
            </div>
        </nav>     


        <!-- MOBILE HEADER -->

        <div class="container mobile-head">    
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <img style="width:100%;margin-bottom:15px;" src="img/kp_logo.png">
                        <center><h6 class="open-sans">Jetzt kostenlos informieren: <br><span style="color: #005577 !important; font-size: 1.4rem;"><i style="margin-left: 8px; margin-right: 2px;" class="fas fa-phone"></i> <strong><a href="tel:08001140069">0800 1140069</a></strong></span></h6></center>
                    </div>
                </div>       
            </div>  
        </div>
        <!-- /. MOBILE HEADER -->


        <div class="row">
            <div class="col-12">

                <div style="color:#fff; background-color:#557700; padding:5px 5px 5px 5px; font-size:15px!important;">

                    <center><i class="fas fa-check"></i> Über 33.000 Kundenberatungen &nbsp;&nbsp;<i class="fas fa-check"></i> Über 20 Jahre ihr verlässlicher Partner&nbsp;&nbsp;<i class="fas fa-check"></i> Kostenlos und unverbindlich informieren</center>
                </div>
            </div>
        </div>


        <div class="clearspace"></div>  


        <section class="bg-primary" id="about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 mx-auto text-center">
                        <h2 class="section-heading">BITTE ÜBERPRÜFEN SIE IHR E-MAIL POSTFACH.</b></h2>
                        <hr class="light my-4">

                        <p>&nbsp;</p>
                        <p>Wir haben Ihnen eine E-Mail mit einem Bestätigungslink an Ihre genannte E-Mail-Adresse gesendet. Sollten Sie in den nächsten 15 Minuten keine Mail erhalten, sehen Sie bitte auch im Spam oder Junk Ordner nach.</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                    </div>
                </div>
            </div>
        </section>

        <div class="clearspace"></div>       
        <div class="clearspace-trans"></div> 
        <div class="clearspace-trans"></div> 

<!-- FOOTER -->
        <section id="contact">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 mx-auto text-center">
                        <div style="margin-bottom: 50px;">
                            <p><small><strong>*Staatlich gesicherte Miete:<br></strong>
                                    Die Pflegeimmobilie gehört zu den Sozialimmobilien, die im Rahmen des SGB XI (Sozialgesetzbuch-Elftes Buch-Soziale Pflegeversicherung) als förderungswürdig gelten. Kann der Bewohner sein Pflegeappartements nicht mehr selbst finanzieren, springt daher der Staat ein. Das bedeutet: der Betreiber (Ihr Mieter) kann sich stets darauf verlassen, dass er für belegte Appartements seine kalkulierten Einnahmen erhält.</small></p>
                        </div>
                        <div class="footer-image"><img src="img/fws_logo.png"></div>
                        <div class="clearspace-trans"></div>
                        <h5 class="section-heading open-sans"><b>Kapitalanlage-Pflegeimmobilien <br>ist ein Produkt der FLASH-WEB-SOLUTIONS e.K.</b></h5>
                        <hr class="my-4">
                        <h2 class="section-heading">Ihr Spezialist für Kapitalanlagen Pflegeimmobilien</h2>
                        <div class="clearspace-trans"></div>
                        <div class="footer-image"><img src="img/sicherheit.png"></div>
                        <div class="clearspace-trans"></div>
                        <div class="clearspace-trans"></div>
                        <div class="clearspace-trans"></div>
                        <h5>
                            <a href="impressum.php" target="_blank">IMPRESSUM</a> | 
                            <a href="https://kapitalanlage-pflegeimmobilien.net/portfolio/#page-top">KONTAKT</a> | 
                            <a href="datenschutz.php" target="_blank">DATENSCHUTZ</a> | 
                            <a href="optionen.php" target="_blank">OPTIONEN</a> | 
                            <a href="ueber-fws.php" target="_blank">ÜBER UNS</a>
                        </h5>
                        <p>&nbsp;</p>
                    </div>
                </div>
            </div>
        </section>
        <!-- </Webgains Tracking Code> -->    
        <?php $config[TrackingTools][EnableWebGains] ? include_once 'clients/tracking/webgains/webgains_redirect.php' : ' '; ?>    
        <!--Check for WebGains tracking -->
        <!-- </Webgains Tracking Code NG> -->
        <!-- </Webgains Tracking Code> -->   

        

        <!-- Bootstrap core JavaScript -->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Plugin JavaScript -->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
        <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
        <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

        <!-- Custom scripts for this template -->
        <script src="js/creative.min.js"></script>

    </body>

</html>

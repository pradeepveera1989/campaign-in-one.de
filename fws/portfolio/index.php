<?php
require "cio.php";
?>
<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Ihr Spezialist für Kapitalanlagen und Pflegeimmobilien">
        <meta name="author" content="flash web solutions">
        <title>FWS | Ihr Spezialist für Kapitalanlagen und Pflegeimmobilien</title> 
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/creative.css" rel="stylesheet">  
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Cinzel" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">  
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link rel="stylesheet" href="vendor/telefonvalidator-client/build/css/intlTelInput.css" />		
        <!-- TrustBox script -->
        <script type="text/javascript" src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js" async></script>
        <!-- End TrustBox script -->


    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="vendor/telefonvalidator-client/build/js/utils.js"></script>		
    <script type="text/javascript" src="vendor/telefonvalidator-client/build/js/intlTelInput.js"></script>
    <script src="vendor/google/maps.js"></script>  
    <script type="text/javascript" src="vendor/jquery-cookie-master/src/jquery.cookie.js"></script>  
    <script type="text/javascript">
        var countdown_status = "<?= $counter_status; ?>";
        var countdown_expire = "<?= $config[GeneralSetting][CountdownExpire]; ?>";
        var current_stocks = "<?= $current_count_value; ?>";
        var autofill_postal_code = "<?= $config[Postal_Code][Autofill]; ?>";
        var localization_postal_code = "<?= $config[Postal_Code][Localization]; ?>"
        var countdown_expire_message = "<?= $config[GeneralSetting][CountdownExpireMessage] ?>";
        var validate_telefon = "<?= $config[Telefon][Status]; ?>";
        var single_submit_text = "<?= $single_submit_text ?>"
        var cookie = "<?= $cookie; ?>";
        var webgains_cookie = "<?= $webgains_cookie ?>";
        var fb_events = "<?= $config[TrackingTools][FacebookEvent] ?>";
        var fb_event = (fb_events.split(",")); // Split the events to array
        var fb_curreny = "<?= $config[TrackingTools][FacebookCurrency] ?>";
        var fb_value = "<?= $config[TrackingTools][FacebookValue] ?>";
    </script>            
    <script language="JavaScript" type="text/javascript" src="cio.js"></script>   
        <!-- Google Analytics -->
        <?php include_once 'clients/tracking/google/google.php'; ?>        

        <!-- Facebook Pixel -->
        <?php include_once('clients/tracking/facebook/facebook.php'); ?>      

    </head>
    <!-- Outbrain Tracking -->
    <?php $config[TrackingTools][EnableOutbrain] ? include_once('clients/tracking/outbrain/outbrain_index.php') : " "; ?>

    <body id="page-top">
        <!-- Web Gains Tracking -->
        <?php $config[TrackingTools][EnableWebGains] ? include_once 'clients/tracking/webgains/webgains_index.php' : " "; ?>

        <!-- Remarketing Target360 Tracking -->
        <?php $config[TrackingTools][EnableTarget360] ? include_once 'clients/tracking/target360/target_index.php' : ' ' ?>;      

        <div class="stoerer"><img src="img/stoerer.png" /></div>

        <!-- DESKTOP NAV -->


        <!-- NAVBAR -->      
        <nav>

            <div class="container">
                <div class="row">
                    <div class="col-lg-4 logo">
                        <a class="navbar-brand js-scroll-trigger" href="#page-top"><img style="width: 390px !important; height: 80px !important; margin: 5px 15px;" src="img/kp_logo.png" alt="logo"></a>
                    </div>
                    <div class="text-center col-lg-8 text-right" style="padding-top: 37px;">
                        <h6 class="open-sans">Jetzt kostenlos informieren: <span style="color: #005577 !important; font-size: 1.4rem;"><i style="margin-left: 8px; margin-right: 2px;" class="fas fa-phone"></i> <strong>0800 1140069</strong></span></h6>
                    </div>
                </div>
            </div>
        </nav>     


        <!-- MOBILE HEADER -->

        <div class="container mobile-head">    
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <img style="width:100%;margin-bottom:15px;" src="img/kp_logo.png" alt="logo">
                        <center><h6 class="open-sans">Jetzt kostenlos informieren: <br><span style="color: #005577 !important; font-size: 1.4rem;"><i style="margin-left: 8px; margin-right: 2px;" class="fas fa-phone"></i> <strong><a href="tel:08001140069">0800 1140069</a></strong></span></h6></center>
                    </div>
                </div>       
            </div>  
        </div>
        <!-- /. MOBILE HEADER -->


        <div class="row">
            <div class="col-12">

                <div style="color:#fff; background-color:#557700; padding:5px 5px 5px 5px; font-size:15px!important;">

                    <center><i class="fas fa-check"></i> Über 33.000 Kundenberatungen &nbsp;&nbsp;<i class="fas fa-check"></i> Über 20 Jahre ihr verlässlicher Partner&nbsp;&nbsp;<i class="fas fa-check"></i> Kostenlos und unverbindlich informieren</center>
                </div>
            </div>
        </div>


        <!-- MASTHEADER --> 
        <header class="masthead d-flex">
            <div class="container my-auto">
                <div class="row">

                    <div class="col-lg-6 intro-text-container">
                        <div class="intro-text">

                            <h1 class="header-h1"><strong>BIS ZU 5% RENDITE P.A. SICHERN!</strong></h1>   

                            <h2 class="header-h2"><strong>5% staatlich gesicherte Rendite mit Pflegeimmobilien*</strong></h2>
                            <div style=" width: 100%;   background-color: #FFF;">
                                <!-- TrustBox widget - Micro Review Count -->
                                <div stlye="padding: 0px!important;" class="trustpilot-widget" data-locale="de-DE" data-template-id="5419b6a8b0d04a076446a9ad" data-businessunit-id="5c45a53103166e0001020dc3" data-style-height="50px" data-style-width="100%" data-theme="light" data-stars="1,2,3,4,5">

                                    <a href="https://de.trustpilot.com/review/kapitalanlage-pflegeimmobilien.net" target="_blank">Trustpilot</a>

                                </div>
                                <!-- End TrustBox widget -->
                            </div>
                        </div> 
                    </div>
                    <div class="col-lg-1">
                        <div></div>
                    </div>
                    <!-- FORM HEADER --> 
                    <div class="col-lg-5 form-head shadow">
        
                        <center><h6 class="open-sans" id="single-submit"><strong>Informieren Sie sich unverbindlich!</strong></h6></center>

                        <form id="api-data-form" action="<?= $_SERVER['PHP_SELF'] ?>" method="post"  accept-charset="" class="form-horizontal check-submit-form" role="form">
                            <input type="hidden" name="contactid" value="<?= $contactid ?>">
                            <input type="hidden" name="checksum" value="<?= $checksum ?>">
                            <input type="hidden" name="mailingid" value="<?= $mailingid ?>">




                            <!-- INPUT ANREDE FRAU & HERR -->
                            <div class="form-group">


                                <div style="width:100%;"class="btn-group btn-group-toggle" data-toggle="buttons">
                                    <label style="padding-left: 0px!important; cursor:pointer; border:1px solid #cccccc; width:100%;" class="btn btn-outline-secondary active">
                                        <input style="color:#c5c5c5;" value="Frau" type="radio" name="<?php echo $config[MailInOne][Mapping][SALUTATION]; ?>" id="option1" autocomplete="off" checked> Frau
                                    </label>



                                    <label style="cursor:pointer; border:1px solid #cccccc; width:100%;" class="btn btn-outline-secondary">
                                        <input style="color:#c5c5c5;" value="Herr" type="radio"name="<?php echo $config[MailInOne][Mapping][SALUTATION]; ?>" id="option2" autocomplete="off"> Herr
                                    </label>

                                </div>

                            </div>


                            <div style="margin-top:-15px;" class="input-group">



                                <input style="height:30px;" value="<?php echo ((!empty($vorname) ? $vorname : $_SESSION['data']['vorname'])); ?>" class="input-fields form-control" id="name" name="<?php echo $config[MailInOne][Mapping][FIRSTNAME]; ?>" placeholder="*Vorname" type="text" required
                                       <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>> 


                                       <span class="input-group-btn" style="width:5px;"></span> 


                                <input style="height:30px;" value="<?php echo ((!empty($nachname) ? $nachname : $_SESSION['data']['nachname'])); ?>" class="input-fields form-control" id="name" name="<?php echo $config[MailInOne][Mapping][LASTNAME]; ?>" placeholder="*Nachname" type="text" required 
                                       <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>


                            </div>




                            <!-- INPUT ANSCHRIFT STRASSE & NUMMER -->

                            <div style="margin-top:5px;" class="input-group">

                                <input style="height:30px;" value="<?php echo ((!empty($strasse) ? $strasse : $_SESSION['data']['strasse'])); ?>" class="input-fields form-control" id="name" name="<?php echo $config[MailInOne][Mapping][ADDRESS]; ?>" placeholder="*Straße" type="text" required <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                       <span class="input-group-btn" style="width:5px;"></span> 
                                <input style="height:30px;" value="<?php echo ((!empty($hnr) ? $hnr : $_SESSION['data']['nummer'])); ?>" class="input-fields form-control" id="name" name="<?php echo $config[MailInOne][Mapping][HNR]; ?>" placeholder="*Hausnummer" type="text" <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>

                            </div>


                            <div style="margin-top:5px;" class="input-group">
                                <input style="height:30px;" value="<?php echo ((!empty($plz) ? $plz : $_SESSION['data']['plz'])); ?>" class="input-fields form-control plz" id="plz" name="<?php echo $config[MailInOne][Mapping][ZIP]; ?>" placeholder="*PLZ" type="text" required <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>> 
                                       <span class="input-group-btn" style="width:5px;"></span> 
                                <input style="height:30px;" value="<?php echo ((!empty($ort) ? $ort : $_SESSION['data']['ort'])); ?>" class="input-fields form-control ort" id="ort" name="<?php echo $config[MailInOne][Mapping][CITY]; ?>" placeholder="*Ort" type="text" <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>

                                       <span class="p-light errorplz" style="color:<?php echo $config[ErrorHandling][ErrorMsg_Color]; ?>"><?php echo $config[ErrorHandling][ErrorMsgPostalcode]; ?></span>     

                            </div>



                            <div style="margin-top:5px;" class="input-group">


                                <input style="width:100%!important; height:30px;" value="<?php echo ((!empty($email) ? $email : $_SESSION['data']['email'])); ?>" class="input-fields form-control email" id="email" name="<?php echo $config[MailInOne][Mapping][EMAIL]; ?>" placeholder="*E-Mail Adresse" type="text" required <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>


                            </div>



                            <div style="margin-top:5px;" class="input-group">  

                                <input style="width:100%!important; height:30px;"  value="<?php echo ((!empty($telefon) ? $telefon : $_SESSION['data'][$config[MailInOne][Mapping][Telefon]])); ?>" class="input-fields form-control" id="telefon"  placeholder="*Telefonnummer" type="text" required <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?> >


                                       <input value="<?php echo $_SESSION['data'][$config[MailInOne][Mapping][Telefon]] ?>" class="input-fields form-control telefon" id="tel2" name="<?php echo $config[MailInOne][Mapping][Telefon]; ?>" placeholder="*Telefonnummer" type="text" hidden >   

                                							
                            </div>
							<span class="p-light errortelefon" style="color:<?php echo $config[ErrorHandling][ErrorMsg_Color]; ?> ;"><?php echo $config[ErrorHandling][ErrorMsgTelefon]; ?></span>
                                <?php if (($_SESSION['error'] === "email") && !empty($_SESSION['error_msg'])) { ?>   
                                    <span class="p-light erroremail"  style=" font-size: 0.7em; line-height: 2; color:<?php echo $config[ErrorHandling][ErrorMsg_Color]; ?>"><?php echo $_SESSION['error_msg']; ?></span>
                                <?php } ?>  









                            <div class="form-group">
                                <div style="margin-top:10px;"class="col-sm-12">
                                    <input name="agb" required type="checkbox" value="check">&nbsp;&nbsp;<span style="font-size:14px!important;" class="text-left"><small>Unter <a href="datenschutz.php">"Datenschutz"</a> finden Sie unsere Informationen gemäß Art. 13 DSGVO über den Datenschutz im Zusammenhang mit unserem Kontaktformular. Bitte bestätigen Sie die Kenntnisnahme.</small></span>
                                </div>
                            </div>

                            <!-- TRAFFIC QUELLE MANUEL -->

                            <style>
                                .hidden{display: none!important;} 
                            </style>

                            <!--Hidden Fields-->   
                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">URL</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][url]; ?>" value="<?php echo $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>">
                                </div>
                            </div>                                        

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Source</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_source]; ?>" value="<?php echo $_GET['utm_source'] ?>">
                                </div>
                            </div>  

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Name</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_name]; ?>" value="<?php echo $_GET['utm_name'] ?>">
                                </div>
                            </div>      

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Term</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_term]; ?>" value="<?php echo $_GET['utm_term'] ?>">
                                </div>
                            </div>                             

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Content</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_content]; ?>" value="<?php echo $_GET['utm_content'] ?>">
                                </div>
                            </div>                              

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Medium</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_medium]; ?>" value="<?php echo $_GET['utm_medium'] ?>">
                                </div>
                            </div>    

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Campaign</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_campaign]; ?>" value="<?php echo $_GET['utm_campaign'] ?>">
                                </div>
                            </div>                                               

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">TRAFFICSOURCE</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][trafficsource]; ?>" id="ts" value="<?php echo $_GET['trafficsource'] ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Quelle</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][Quelle]; ?>" id="quelle" value="<?php echo $config[MailInOne][Constants][Quelle]; ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Typ</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][Typ]; ?>" id="" value="<?php echo $config[MailInOne][Constants][Typ]; ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Segment</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][Segment]; ?>" id="" value="<?php echo $config[MailInOne][Constants][Segment]; ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Test Mode</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="test_mode" id="" value="<?php echo $_GET['testmode']; ?>">
                                </div>
                            </div>



                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Interesse</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="interesse" id="interesse" value="Pflegeimmo">
                                </div>
                            </div>


                            <div class="form-group">
                                <button  style="cursor: pointer; width:100%;" class="btn-creative btn btn-primary btn-lg btn-block login-button" id="button" name="submit" type="submit" value="submit">INFORMATIONEN ANFORDERN</button>
                            </div>

                        </form>
                    </div>   
                </div>
            </div>
        </header>

        <div class="clearspace"></div>   

        <!-- VORTEILE FEATURES -->
        <section id="services">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 class="section-heading">Ihre Vorteile bei Investition in eine Pflegeimmobilie</h2>
                        <hr class="light my-4">
                    </div>
                </div>
            </div>

            <div class="clearspace-trans"></div>      

            <div class="container">
                <div class="row">
                    <div class="text-center col-sm-3">
                        <i class="fa fa-4x fa-university text-primary mb-3 sr-icons"></i>
                        <center>
                            <h5 class="bullets text-center open-sans">Staatliche Refinanzierung und Absicherung über das Sozialgesetzbuch</h5>
                        </center>
                    </div>
                    <div class="text-center col-sm-3">
                        <i class="fa fa-4x fa-shield-alt text-primary mb-3 sr-icons"></i>
                        <h5 class="bullets text-center open-sans">Einnahmesicherheit durch langjährigen Mietvertrag</h5>
                        </center>
                    </div>
                    <div class="text-center col-sm-3">
                        <i class="fa fa-4x fa-eur text-primary mb-3 sr-icons"></i>
                        <center>
                            <h5 class="bullets text-center open-sans">Top-Rendite bis zu 5% p.a.</h5>
                        </center>
                    </div>
                    <div class="text-center col-sm-3">
                        <i class="fa fa-4x fa-gavel text-primary mb-3 sr-icons"></i>
                        <center>
                            <h5 class="bullets text-center open-sans">Absicherung der Geldanlage durch Grundbucheintrag</h5>
                        </center>
                    </div>
                </div>
            </div>
        </section>  

        <!-- FINANZIERUNG -->
        <section class="finanzierung">
            <div class="container">
                <center>
                    <h2 class="mb-4">Investitions Möglichkeiten</h2> 
                    <hr class="light my-4" style="padding-bottom: 20px;">
                </center>
                <div class="row">
                    <div class="col-sm-12 col-md-1"></div>
                    <div class="col-sm-12 col-md-10">
                        <p>Es ist heute schwieriger denn je, Geld sicher und gewinnbringend anzulegen. Die günstigen Finanzierungszinsen sind der einzige wirkliche Vorteil in dieser besonderen Zeit. Sie hebeln Ihre Rendite auf das eingesetzte Eigenkapital, profitieren von der Inflation, sichern Ihre Geldwerte ab, Sie müssen sich weniger Gedanken um geldpolitische Auswirkungen machen und senken Ihre Steuerlast. In Verbindung mit einem Pflegeobjekt tätigen Sie für die nächsten Jahre eine sinnvolle Geldanlage in eine Immobilienklasse, die gebraucht wird wie keine andere.</p>
                    </div>
                    <div class="col-sm-12 col-md-1"></div>
                </div>
                <div class="row" style="margin-top: 25px;">
                    <div class="col-sm-12 col-md-6 option1">
                        <center>
                            <h4><strong>Option 1</strong></h4>
                            <hr class="light" style="padding-bottom: 10px;">
                            <h5><strong>Kauf mit Eigenkapital ab 120.000€</strong></h5>
                            <p style="margin-top: 15px;">Ab einem Eigenkapital von 120.000€ können Sie direkt investieren.</p>
                        </center>
                    </div>
                    <div class="col-sm-12 col-md-6 option2">
                        <center>
                            <h4><strong>Option 2</strong></h4>
                            <hr class="dark" style="padding-bottom: 10px;">
                            <h5><strong>Finanzierung ab 10.000€ Eigenkapital</strong></h5>
                            <p style="margin-top: 15px;">Ab einem Eigenkapital von 10.000€ ist eine Finanzierung über ein Darlehen möglich. Eine Beispielrechnung zur Finanzierung finden Sie 
                                <a style="color: #FFF; text-decoration: underline;" href="optionen.php">hier</a>.</p>
                        </center>
                    </div>
                </div>
                <div class="row" style="margin-top: 20px;">
                    <div class="col-sm-12 col-md-1"></div>
                    <div class="col-sm-12 col-md-10">
                        <p><strong><span style="color: #005577;">Tipp:</span></strong> Zusätzlich zu dem KfW Darlehen sind in den meisten Fällen Tilgungszuschüsse von 5.000 Euro bis 10.000 Euro möglich, d.h. Ihre Rendite wird sich dadurch weiter erhöhen. (Quelle Homepage KfW: Programm 153 „Energieeffizient Bauen“ <br>(Stand: 22.02.2018) <a href="#page-top">Wir beraten Sie gerne unverbindlich.</a></p>
                    </div>
                    <div class="col-sm-12 col-md-1"></div>
                </div>
            </div>
        </section>

        <!-- UNSERE PFLEGEIMMOBILIEN -->
        <section class="beispielobjekte bg-dark text-white">
            <div class="container">
                <center>
                    <h2 class="mb-4">Auszug aus unserem Pflegeimmobilien-Portfolio</h2> 
                    <hr class="dark my-4" style="padding-bottom: 20px;">
                </center>
            </div>

            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>

                </ol>
                <div class="carousel-inner">

                    <div class="carousel-item active">
                        <div class="container">
                            <div class="row" style="padding-bottom: 50px;">
                                <div class="col-sm-12 col-md-6 text-center slider-bild-links">
                                    <img class="img-fluid" src="img/senioren-immobilien-zwenkau.jpg" alt="Seniorenquartier Zwenkau">
                                </div>
                                <div class="col-sm-12 col-md-6 slider-text-rechts" style="padding-top: 13px;">
                                    <div style="min-height: 50px;">
                                        <h5><strong>Seniorenresidenz Zwenkau</strong></h5><br>
                                    </div>
                                    <p><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;Betreiber: Alloheim Seniorenresidenzen GmbH</p>
                                    <p><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;Pachtvertrag: 25 Jahre + 1 x 5 Jahre Verlängerungsoption</p>
                                    <p><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;Mietrendite: ab 4,0 %</p>
                                    <p><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;Haupthaus: 76 Einzelapartments und 13 Doppelzimmer, Anbau 26 Einzelapartments</p>
                                    <p><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;Baujahr: Juni 2011 (Haupthaus), Juni 2017 (Anbau)</p>
                                    <p><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;Energiebedarf Haupthaus: 52,9 kWh (m²a), Energieeffizienzklasse B</p>
                                    <p><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;Energiebedarf Anbau: 19,4 kWh (m²a), Energieeffizienzklasse A+</p>
                                </div>
                            </div>
                        </div>
                    </div>
    

                    <div class="carousel-item">
                        <div class="container">
                            <div class="row" style="padding-bottom: 50px;">
                                <div class="col-sm-12 col-md-6 text-center slider-bild-links">
                                    <img class="img-fluid" src="img/senioren-immobilien-badl.png" alt="Seniorenquartier Düren">
                                </div>
                                <div class="col-sm-12 col-md-6 slider-text-rechts" style="padding-top: 13px;">
                                    <div style="min-height: 50px;">
                                        <h5><strong>Lebens- und Gesundheitszentrum „Haus Invita“ Bremen-Huchting</strong></h5><br>
                                    </div>
                                    <p><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;Betreiber: WH Care Bremen Huchting GmbH</p>
                                    <p><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;22 Jahre Pachtvertrag + 1 x 5 Jahre Verlängerungsoption</p>
                                    <p><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;Mietrendite: 4,00 %</p>
                                    <p><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;84 Einzelpflegeapartments</p>
                                    <p><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;Fertigstellung: bereits fertiggestellt in 2012 – sofortige Mieteinnahme</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="container">
                            <div class="row" style="padding-bottom: 50px;">
                                <div class="col-sm-12 col-md-6 text-center slider-bild-links">
                                    <img class="img-fluid" src="img/senioren-immobilien-erlangen.jpg" alt="Seniorenwohnanlage Erlangen">
                                </div>
                                <div class="col-sm-12 col-md-6 slider-text-rechts" style="padding-top: 13px;">
                                    <div style="min-height: 50px;">
                                        <h5><strong>Malteserstift St. Elisabeth, Erlangen</strong></h5><br>
                                    </div>
                                    <p><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;Betreiber: Malteser Waldkrankenhaus Erlangen, gemeinnütz. GmbH</p>
                                    <p><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;Pachtvertrag: 20 Jahre + 2 x 5 Jahre Verlängerungsoption</p>
                                    <p><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;Mietrendite: ab 4,15 %</p>
                                    <p><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;102 Einzelpflegeapartments, 24 betreute Wohnungen</p>
                                    <p><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;Fertigstellung: voraussichtlich Juli 2020</p>
                                    <p><i class="fa fa-angle-right"></i>&nbsp;&nbsp;&nbsp;Energiebedarf: liegt noch nicht vor – wird nachgereicht</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>

            <div class="container" style="margin-top: 20px;">
                <center>
                    <a style="text-decoration:none!important;" href="#top">
                        <div class="col-9">
                            <button style="cursor: pointer;" class="btn-creative btn btn-primary btn-lg btn-block login-button" id="button"  type="submit" >INFORMATIONEN ANFORDERN</button>
                        </div>
                    </a>
                </center>
            </div>
        </section>

        <section class="testimonials bg-light">
            <div class="container">
                <center>
                    <h2 class="mb-4">Das sagen unsere Kunden</h2> 
                    <hr class="light my-4">
                </center>
            </div>
            <div id="carousel2Indicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators indicators-blue">
                    <li data-target="#carousel2Indicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel2Indicators" data-slide-to="1"></li>
                    <li data-target="#carousel2Indicators" data-slide-to="2"></li>
                    <li data-target="#carousel2Indicators" data-slide-to="3"></li>
                </ol>
                <div class="carousel-inner">

                    <div class="carousel-item active">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links2">
                                    <img class="img-fluid rounded-circle" src="img/testimonials_t-lischke.jpg" alt="testimonial t lischke">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts2">
                                    <h5><strong>T. Lischke</strong></h5>
                                    <p style="color: #ffcc00;"><i class="fa fa-star"></i>&nbsp;&nbsp;<i class="fa fa-star"></i>&nbsp;&nbsp;<i class="fa fa-star"></i>&nbsp;&nbsp;<i class="fa fa-star"></i>&nbsp;&nbsp;<i class="fa fa-star"></i></p>
                                    <p><i>"Ab dem ersten Kontakt absolut professionelle Beratung. Vielen Dank daher nochmal für die schnelle und kompetente Unterstützung hinsichtlich der Beantwortung all unserer Fragen. Absolut empfehlenswert."</i></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links2">
                                    <img class="img-fluid rounded-circle" src="img/testimonials_hj-ernst.jpg" alt="testimonial hj ernst">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts2">
                                    <h5><strong>H.-J. Ernst</strong></h5>
                                    <p style="color: #ffcc00;"><i class="fa fa-star"></i>&nbsp;&nbsp;<i class="fa fa-star"></i>&nbsp;&nbsp;<i class="fa fa-star"></i>&nbsp;&nbsp;<i class="fa fa-star"></i>&nbsp;&nbsp;<i class="fa fa-star"></i></p>
                                    <p><i>"Vom ersten Kontakt, über das Angebot, bis zum Kaufabschluss wurden wir durch das FWS-Team optimal beraten. Für unsere Fragen wurde sich immer Zeit genommen und man hat uns mit Rat und Tat unterstützt. Wir sind sicher, eine gute Kapitalanlage gefunden zu haben und können diesen Service uneingeschränkt weiterempfehlen."</i></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links2">
                                    <img class="img-fluid rounded-circle" src="img/testimonials_holger-heidi-m.jpg" alt="testimonial holger heidi m">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts2">
                                    <h5><strong>Holger & Heidi M.</strong></h5>
                                    <p style="color: #ffcc00;"><i class="fa fa-star"></i>&nbsp;&nbsp;<i class="fa fa-star"></i>&nbsp;&nbsp;<i class="fa fa-star"></i>&nbsp;&nbsp;<i class="fa fa-star"></i>&nbsp;&nbsp;<i class="fa fa-star"></i></p>
                                    <p><i>"Wir hatten den Eindruck, dass es nicht nur um den Verkauf, also vorrangig das eigene Interesse des Verkäufers geht, sondern auch um unsere Wünsche sowie faire Interessenabwägung unter Berücksichtigung sachlicher Aspekte."</i></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links2">
                                    <img class="img-fluid rounded-circle" src="img/testimonials_anja-f.jpg" alt="testimonial anja f">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts2">
                                    <h5><strong>Anja F.</strong></h5>
                                    <p style="color: #ffcc00;"><i class="fa fa-star"></i>&nbsp;&nbsp;<i class="fa fa-star"></i>&nbsp;&nbsp;<i class="fa fa-star"></i>&nbsp;&nbsp;<i class="fa fa-star"></i>&nbsp;&nbsp;<i class="fa fa-star"></i></p>
                                    <p><i>"Absolut professionelle, fachkundige und vertrauensvolle Beratung. Alle Fragen im Zusammenhang mit dem Erwerb einer Pflegeimmobilie wurden sehr kompetent und seriös beantwortet. Ich würde hier wieder eine Pflegeimmobilie kaufen."</i></p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <a class="carousel-control-prev" href="#carousel2Indicators" role="button" data-slide="prev">
                    <span id="previous-blue" class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel2Indicators" role="button" data-slide="next">
                    <span id="next-blue" class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>
        </section>

        <!-- BULLETLIST --> 
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <div class="bullet-group">
                            <i class="fa fa-2x fa-check-square text-primary sr-icons"></i> <span class="bullet-point open-sans2">&nbsp;&nbsp;Top-Rendite bis zu 5% p.a.</span>
                            <p class="bullet-text">Bei einer Investition in Pflegeimmobilien können Sie sich auf Top-Renditen von bis zu 5% bezogen auf den Kaufpreis freuen. Besonders in einer Zeit in der niedrige Zinsen den Kapitalmarkt beherrschen ist eine hohe Rendite von bis zu 5% sehr erfreulich. Natürlich ist die Rendite jedoch standort- und objektabhängig, sodass auch durchaus eine höhere Rendite möglich ist. Da es sich um eine Kapitalanlage mit Staatsgarantie handelt, ist die Rendite zusätzlich besonders stabil.<br>
                                Die Besteuerung ist deutlich günstiger als bei Kapitalerträgen (Zinsen, Aktiengewinne, etc.), die mit ca. 25 % zzgl. Kirchensteuer und Solidaritätszuschlag besteuert werden. Einnahmen aus Vermietung und Verpachtung sind insbesondere für Rentner interessant, da diese in der Regel einen niedrigen Grenzsteuersatz haben.  
                            </p>
                        </div>
                        <div class="bullet-group">
                            <i class="fa fa-2x fa-check-square text-primary sr-icons"></i> <span class="bullet-point open-sans2">&nbsp;&nbsp;Steigende Mieteinnahmen bei Inflation</span>
                            <p class="bullet-text">Bei ihrer Kapitalanlage in Pflegeimmobilien genießen Sie Inflationsschutz durch indexierte Mietverträge.  So können die Mietkosten immer an die wirtschaftlichen Veränderungen der Lebenshaltungskosten angepasst werden.  
                            </p>
                        </div>
                        <div class="bullet-group">
                            <i class="fa fa-2x fa-check-square text-primary sr-icons"></i> <span class="bullet-point open-sans2">&nbsp;&nbsp;Staatlich abgesicherte Mieten</span>
                            <p class="bullet-text">Wenn Sie in Pflegeobjekte investieren, sind die Mieten für den Betreiber der Immobilie und somit auch für Sie durch eine staatliche Refinanzierung gesichert. Sprich wenn ein Bewohner aufgrund finanzieller Engpässe nicht für die Kosten für Pflege und Unterkunft aufkommen kann, bezahlen Pflegekassen und/oder das Sozialamt hierfür. Außerdem ist Ihre Investition durch den vorgenommenen Grundbucheintrag abgesichert.
                            </p>
                        </div>
                        <div class="bullet-group">
                            <i class="fa fa-2x fa-check-square text-primary sr-icons"></i> <span class="bullet-point open-sans2">&nbsp;&nbsp;Günstige Finanzierung</span>
                            <p class="bullet-text">Wer sein Kapital nicht auf einen Schlag in Pflegeimmobilien anlegen möchte, der kann auf eine günstige Finanzierung zurückgreifen. Viele Neubauprojekte sind zudem KfW-förderfähig. Sie erhelten somit sehr günstige Zinssätze und einen Tilgungszuschuss zwischen 5.000 € und 15.000 €.    
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">

                        <div class="bullet-group">
                            <i class="fa fa-2x fa-check-square text-primary sr-icons"></i> <span class="bullet-point open-sans2">&nbsp;&nbsp;Keine versteckten Kosten</span>
                            <p class="bullet-text">Bei einer Kapitalanlage in Pflegeimmobilien warten keine versteckten Kosten auf Sie. Die Maklercourtage ist immer im Kaufpreis enthalten, Sie zahlen also keine separate Vermittlungsgebühr. Die Varwaltungs- und Instandhaltungskosten sind für die Eigentümergemeinschaft deutlich geringer als bei Wohnimmobilien.  
                            </p>
                        </div>
                        <div class="bullet-group">
                            <i class="fa fa-2x fa-check-square text-primary sr-icons"></i> <span class="bullet-point open-sans2">&nbsp;&nbsp;Steuervorteile durch Abschreibung</span>
                            <p class="bullet-text">Mit einer Investition in Altenheime oder Pflegeappartements haben Sie ebenfalls steuerliche Vorteile. Egal ob Neu- oder Altbauten: Sie können jährlich zwischen 2 Prozent der Gebäudeanschaffungs- sowie der Nebenkosten absetzen. Der Grundstücksanteil – der bei der Abschreibung vom Kaufpreis abgezogen wird – ist meist deutlich geringer als bei Wohnimmobilien. Zudem enthält der Kaufpreis des Apartments häufig einen Inventarkostenzuschuss. In diesem Fall kann das Inventar 10 Jahre lang zu je 10% jährlich abgeschrieben werden. Der Prozentsatz ist bei Neubauten höher als bei Altbauten.
                            </p>
                        </div>
                        <div class="bullet-group">
                            <i class="fa fa-2x fa-check-square text-primary sr-icons"></i> <span class="bullet-point open-sans2">&nbsp;&nbsp;Ihre Vorsorge im Alter</span>
                            <p class="bullet-text">Die Investition in Pflegeimmobilien ist außerdem für Sie interessant, da sowohl Sie als Investor, als auch Ihre engen Angehörigen ein spezielles Wohnrecht im Alter auf Pflegeplätze in den jeweiligen Pflegeheimen haben.     
                            </p>
                        </div>
                        <div class="bullet-group">
                            <i class="fa fa-2x fa-check-square text-primary sr-icons"></i> <span class="bullet-point open-sans2">&nbsp;&nbsp;Kaufpreise ab 120.000 €</span>
                            <p class="bullet-text">Es ist heute schwieriger denn je, Geld sicher und gewinnbringend anzulegen. Die günstigen
                                Finanzierungszinsen sind der einzige wirkliche Vorteil in dieser besonderen Zeit. Sie hebeln Ihre Rendite auf das eingesetzte Eigenkapital, profitieren von der Inflation, sichern Ihre Geldwerte ab, müssen sich weniger Gedanken um geldpolitische Auswirkungen machen und senken Ihre Steuerlast. In Verbindung mit einem Pflegeobjekt tätigen Sie für die nächsten Jahre eine sinnvolle Geldanlage in eine Immobilienklasse, die gebraucht wird wie keine andere.   
                            </p>
                        </div>
                    </div>
                </div>
            </div>    
        </section>

        <!-- VORTEILE 2 -->     
        <section class="bg-dark text-white">
            <div class="container text-center">
                <h2 class="mb-4">Die drei größten Vorteile für Sie:</h2>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="bullet-group">
                                <div class="number">
                                    1
                                </div>
                                <div class="number-point">
                                    Hohe Sicherheit
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="bullet-group">
                                <div class="number">
                                    2
                                </div>
                                <div class="number-point">
                                    Langfristig ausgelegte Mietverträge<br>
                                    (über 20 Jahre)
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="bullet-group">
                                <div class="number">
                                    3
                                </div>
                                <div class="number-point">
                                    Staatlich gesicherte Mieteinnahme<br>
                                    - Rendite ca. 5% p.a.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
                <div class="clearspace-trans"></div>
                <div class="clearspace-trans"></div>

                <center><a style="text-decoration:none!important;" href="#top"><div class="col-9"><button style="cursor: pointer;" class="btn-creative btn btn-primary btn-lg btn-block login-button" id="button"  type="submit" >INFORMATIONEN ANFORDERN</button></div></a></center>

            </div>
        </section>

        <!-- VERZINSUNG -->
        <section class="bg-primary">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6"><img class="verzinsung-img" src="img/graph.png"></div>
                    <div class="col-sm-6">
                        <h2 class="section-heading-alt">Verzinsung</h2>
                        <p class="section-heading-detail mb-4">Als beste langfristige Kapitalanlagen galten lange Bundesanleihen und Lebensversicherungen.<br>
                            <br>
                            Im aktuellen Marktumfeld sind hier allerdings aktuell nur niedrige Renditen zu erwarten.</p>
                    </div>
                </div>
            </div>
        </section>

        <!-- HINTERGRUND INFORMATION -->    
        <section class="hintergrund">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 mx-auto text-center">
                        <h2 class="section-heading-1">Hintergrund-Informationen</h2>
                        <hr class="light my-4">
                        <p class="mb-4">Bis zum Jahr 2050 wird sich die Bevölkerung um 10 Mio. Menschen verringern und der Wohnungsbedarf entsprechend anpassen. Die Zahl der über 80-jährigen Menschen wird sich aber verdoppeln. <b>Experten sprechen daher bei Pflege von einem der letzten Wachstumsmärkte.</b> Normalerweise sind Investitionen dieser Art nur für Großinvestoren zugänglich.<br>
                            <br>
                            Die Pflegeeinrichtungen entstehen an diversen Standorten in ganz Deutschland. Nach Fertigstellung bieten die stationären Pflegeplätze den Seniorinnen und Senioren in freundlicher Atmosphäre ein neues Zuhause.</p>
                    </div>
                </div>
            </div>
        </section>

        <section id="services">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 class="section-heading">Pflegeheime stehen vor Investmentboom</h2>
                        <hr class="light my-4">
                        <p><i>"Der deutsche Pflegeheimmarkt steht vor einem neuen Investmentboom, Nach einer Studie (...) werden in den kommenden fünf Jahren bis zu 7 Mrd. Euro in diese Gesundheitsimmobilien fließen."</i></p>
                        <p>&nbsp;</p>
                        <center><a style="text-decoration:none!important;" href="#top"><div class="col-9"><button style="cursor: pointer;" class="btn-creative btn btn-primary btn-lg btn-block login-button" id="button"  type="submit" >INFORMATIONEN ANFORDERN</button></div></a></center>
                    </div>
                </div>
            </div>
        </section>

        <section class="info">
            <div class="container">
                <div class="row">
                    <center>
                        <div class="col-md-10 col-sm-12 info-text">
                            <h1 class="info-h1">Kapitalanlage Pflegeimmobilien</h1>
                            <h2 class="info-h2">Top-Rendite bis 5%, keine versteckten Kosten, sichere Anlage!</h2>
                            <hr class="dark my-4">
                            <p>Rentable Kapitalanlagen sind heutzutage schwer zu finden. Wer sein Geld anlegen möchte, hat viele Möglichkeiten. Doch während die Börse stark schwankend ist, Festgeld, Tagesgeld und sonstige sichere Geldanlagen zu niedrige Prozentsätze verbriefen, so wird der Investmentmarkt in Pflegeimmobilien immer attraktiver.</p>
                            <p>Bei der Kapitalanlage in Pflegeimmobilien erwarten Sie zahlreiche Vorteile. So zum Beispiel auch bei denkmalgeschützten Immobilien oder Studentenwohnungen. Doch Pflegeimmobilien haben einen ganz besonderen Vorteil. Denn laut einer Prognose des statistischen Bundesamtes sollen bis zum Jahr 2030 die Anzahl der Pflegebedürftigen in Deutschland auf bis zu 3,4 Millionen Menschen wachsen. Somit sollen bis zu 320.000 neue Plätze in Pflegeheimen benötigt werden. Das liegt insbesondere daran, dass durch die immer besser werdende medizinische Versorgung die Zahl älterer Menschen deutlich zunimmt. Zum Vergleich: Ende 2015 belief sich diese Zahl auf rund 2,86 Millionen pflegebedürftigen Menschen.</p><br>

                            <p><strong>Was sind Pflegeimmobilien?</strong></p>
                            <p>Unter Pflegeimmobilien versteht man vollstationäre Einrichtungen, in der ältere und pflegebedürftige Menschen beheimatet werden. Bei einer Kapitalanlage in Pflegeimmobilien handelt es sich also um eine Investition in Objekte wie Senioren- und Pflegeheime, Pflegeappartements, Wohnheime mit Pflege-Möglichkeiten für Senioren oder einem Mix aus Wohn- und Pflegeheim.</p>
                            <p>Laut des statistischen Bundesamtes ist heutzutage der Bedarf an Pflegeimmobilien nicht einmal zu 50% gedeckt. Bis zum Jahr 2030 sprechen Experten sogar davon, dass sich der Anteil der über 80-jährigen verdoppeln und bis zum Jahr 2050 sogar verdreifachen wird. Aus der Diskrepanz von einem zu geringen Angebot an Pflegeimmobilien und einer zu hohen Nachfrage an Pflegeplätzen entsteht eine besonders interessante Möglichkeit der Kapitalanlage.</p><br>

                            <p><strong>Kapitalanlage Pflegeimmobilien – darum lohnt es sich zu investieren</strong></p>
                            <p>Es gibt zahlreiche Gründe, die dafür sprechen, in Pflegeimmobilien zu investieren. Der Immobilienmarkt ist zur Zeit besonders beliebt, wenn es darum geht, sein Geld anzulegen. Jedoch haben Sie bei Seniorenheime oder Pflegeappartements ganz besondere Vorteile. Denn Sie sind lediglich der Vermieter und nicht der Betreiber dieser Einrichtungen.</p>
                            <p>Das bedeutet für Sie, dass Sie ganz bequem Monat für Monat Ihre Miete erhalten, auch wenn einzelne Zimmer oder Wohneinheiten leer stehen. Sie müssen sich nicht mit den Anfragen für neue Mieter beschäftigen, denn auch das macht der Betreiber. Sie erstellen weder eine Nebenkostenabrechnung, noch sind Sie für die Instandsetzung der verschiedenen Wohnbereiche zuständig.</p>
                            <p>Mit dem jeweiligen Mieter/Betreiber der Seniorenheime oder Pflegeappartements haben Sie zudem langfristige Mietverträge von über 20 Jahren. Da die Mieter von Pflegeimmobilien jedoch eine Gewissheit auf die Zahlung von staatlichen Pflegegeldern haben, sind Mietausfälle fast ausgeschlossen.</p>
                            <p>Die „Anlageobjekte mit sozialem Mehrwert“ sind außerdem interessant, da Investoren in den jeweiligen Pflegeheimen auch für sich selbst und sogar für Angehörige ein spezielles Wohnrecht im Alter haben.</p><br>
                            <p><strong>Alle Vorteile im Überblick</strong></p>
                            <p>Kapitalanlage in Pflegeimmobilien zählen zu den sichersten Kapitalanlagen und halten keine versteckten Kosten für Sie bereit – weder eine Maklercourtage, noch Verwaltungsgebühren fallen für Sie an. Wenn Sie Ihr Geld in Pflegeimmobilien anlegen, zahlt sich das wortwörtlich für Sie aus und ist eine besondere Investition in die Zukunft. Neben dem sozialen Aspekt legen Sie Ihr Geld nicht nur sicher, sondern auch mit Top-Renditen an.</p>
                            <p>Warum Ihr Geld so sicher angelegt ist? Das liegt an der konjunkturunabhängigen Situation und einer großen Nachfrage an Pflegeplätzen. Auch ein besonders langfristig angelegter Vertrag mit Pflege-Experten wie AWO, Johanniter oder Vitanas von bis zu 20 Jahren, die zudem staatlich gefördert werden, machen Ihre sichere Position deutlich.</p>
                            <p>&nbsp;</p>
                            <center>
                                <p><strong>Ihre Investition in Pflegeimmobilien mit FWS!</strong></p>
                                <p>Sie möchten sich näher über das Thema Kapitalanlage in Pflegeimmobilien informieren?<br> 
                                    Holen Sie sich jetzt ganz unverbindlich Informationen ein!</p>
                                <p>&nbsp;</p>
                                <a style="text-decoration:none!important;" href="#top"><div class="col-9"><button style="cursor: pointer;" class="btn-creative btn btn-primary btn-lg btn-block login-button" id="button"  type="submit" >INFORMATIONEN ANFORDERN</button></div></a>
                            </center>
                            <p>&nbsp;</p>
                        </div>
                    </center>
                </div>
            </div>
        </section> 

        <div class="clearspace-trans"></div> 
        <div class="clearspace-trans"></div>  

        <!-- FOOTER -->
        <section id="contact">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 mx-auto text-center">
                        <div style="margin-bottom: 50px;">
                            <p><small><strong>*Staatlich gesicherte Miete:<br></strong>
                                    Die Pflegeimmobilie gehört zu den Sozialimmobilien, die im Rahmen des SGB XI (Sozialgesetzbuch-Elftes Buch-Soziale Pflegeversicherung) als förderungswürdig gelten. Kann der Bewohner sein Pflegeappartements nicht mehr selbst finanzieren, springt daher der Staat ein. Das bedeutet: der Betreiber (Ihr Mieter) kann sich stets darauf verlassen, dass er für belegte Appartements seine kalkulierten Einnahmen erhält.</small></p>
                        </div>
                        <div class="footer-image"><img src="img/fws_logo.png"></div>
                        <div class="clearspace-trans"></div>
                        <h5 class="section-heading open-sans"><b>Kapitalanlage-Pflegeimmobilien <br>ist ein Produkt der FLASH-WEB-SOLUTIONS e.K.</b></h5>
                        <hr class="my-4">
                        <h2 class="section-heading">Ihr Spezialist für Kapitalanlagen Pflegeimmobilien</h2>
                        <div class="clearspace-trans"></div>
                        <div class="footer-image"><img src="img/sicherheit.png"></div>
                        <div class="clearspace-trans"></div>
                        <div class="clearspace-trans"></div>
                        <div class="clearspace-trans"></div>
                        <h5>
                            <a href="impressum.php" target="_blank">IMPRESSUM</a> | 
                            <a href="#top">KONTAKT</a> | 
                            <a href="datenschutz.php" target="_blank">DATENSCHUTZ</a> | 
                            <a href="optionen.php" target="_blank">OPTIONEN</a> | 
                            <a href="ueber-fws.php" target="_blank">ÜBER UNS</a>
                        </h5>
                        <p>&nbsp;</p>
                    </div>
                </div>
            </div>
        </section>



        <script type='text/javascript'>
            $(document).ready(function () {
                $('.carousel2').carousel({
                    interval: 2000
                })
            });
        </script>


        <!--script src="libraries/ini-master/ini.js"></script-->  
        <!--script src="jquery/campaign.js"></script-->  


        <script>

            $('.close').click(function () {
                $('#modal-video').hide();
                $('#modal-video iframe').attr("src", jQuery("#modal-video iframe").attr("src"));
            });
        </script>



        <script>

            $(window).scroll(function () {
                var scroll = $(window).scrollTop();

                if (scroll > 660) {
                    $(".nav-bottom").addClass("change"); // you don't need to add a "." in before your class name
                } else {
                    $(".nav-bottom").removeClass("change");
                }
            });
        </script>





        <script type="text/javascript ">



        </script>	


    </body>
</html>

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class forwardMail {

    function __construct() {
     
        $this->config = new config("config.ini");
        $this->config_mail = $this->config->getConfigEmail();
    }
    
    
    public function getMailParams($params){
        
        if(empty($params)){
            
            return false;
        }

        $this->config_mailon = $this->config->getConfigMailInOne()[Mapping];

        return [
            "to" => $this->config_mail[ForwardEmailRecipient],
            "from" => $this->config_mail[ForwardEmailSender],
            "produkt1" => $params[$this->config_mailon[Product1]],
            "produkt2" => $params[$this->config_mailon[Product2]],
            "produkt3" => $params[$this->config_mailon[Product3]],
            "produkt4" => $params[$this->config_mailon[Product4]],
            "produkt5" => $params[$this->config_mailon[Product5]],
            "produkt6" => $params[$this->config_mailon[Product6]],
            "produkt7" => $params[$this->config_mailon[Product7]],
            "produkt8" => $params[$this->config_mailon[Product8]],
            "produkt9" => $params[$this->config_mailon[Product9]],
            "produkt10" => $params[$this->config_mailon[Product10]],
            "anrede" => $params[$this->config_mailon[SALUTATION]],
            "vorname" => $params[$this->config_mailon[FIRSTNAME]],
            "nachname" => $params[$this->config_mailon[LASTNAME]],
            "telefon" => $params[$this->config_mailon[Telefon]],
            "email" => $params[$this->config_mailon[EMAIL]],    
            "geburtsdatum" => $params[$this->config_mailon[Geburtsdatum]],
            "telefon" => $params[$this->config_mailon[Telefon]],
            "mobiltelefon" => $params[$this->config_mailon[Mobiltelefon]],
            "strasse" => $params[$this->config_mailon[ADDRESS]],
            "nummer" => $params[$this->config_mailon[HNR]],
            "plz" => $params[$this->config_mailon[ZIP]],
            "ort" => $params[$this->config_mailon[CITY]],
            "beruf" => $params[$this->config_mailon[Beruf]],
			"derzeitVersichertBei" => $params[$this->config_mailon[DerzeitVersichertBei]],
			"einkommen" => $params[$this->config_mailon[Einkommen]],
			"kommentar" => $params[$this->config_mailon[Kommentar]],
			"kommentarIntern" => $params[$this->config_mailon[KommentarIntern]],
			"ip"  => $params[$this->config_mailon[ip_adresse]],
			"adressIdLieferant" => $params[$this->config_mailon[AdressIdLieferant]],  
            "subject" => $params[$this->config_mailon[trafficsource]]
        ];
    }
    
    
    public function getMailMessage(){
        
        return $this->mail_params["vorname"] ." ". $this->mail_params["nachname"] . "\n"              
            . "Produkt1:"      . $this->mail_params["produkt1"] . "\n"               				
			. "Produkt2:"      . $this->mail_params["produkt2"] . "\n"
			. "Produkt3:"      . $this->mail_params["produkt3"] . "\n"
			. "Produkt4:"      . $this->mail_params["produkt4"] . "\n"
			. "Produkt5:"      . $this->mail_params["produkt5"] . "\n"
			. "Produkt6:"      . $this->mail_params["produkt6"] . "\n"
			. "Produkt7:"      . $this->mail_params["produkt7"] . "\n"
			. "Produkt8:"      . $this->mail_params["produkt8"] . "\n"
			. "Produkt9:"      . $this->mail_params["produkt9"] . "\n"
			. "Produkt10:"     . $this->mail_params["produkt10"] . "\n"
			. "Anrede:"        . $this->mail_params["anrede"] . "\n"
			. "Name:"          . $this->mail_params["nachname"] . "\n"
			. "Vorname:"       . $this->mail_params["vorname"] . "\n"
			. "Geburtsdatum:"  . $this->mail_params["geburtsdatum"] . "\n"
			. "Telefon:"       . $this->mail_params["telefon"] . "\n"
			. "Mobiltelefon:"  . $this->mail_params["mobiltelefon"] . "\n"
			. "Strasse:"       . $this->mail_params["strasse"] . "\n"
			. "Nummer:"        . $this->mail_params["nummer"] . "\n"
			. "PLZ:"           . $this->mail_params["plz"] . "\n"
			. "Ort:"           . $this->mail_params["ort"] . "\n"
			. "Beruf:"         . $this->mail_params["beruf"] . "\n"
			. "derzeitVersichertBei:"   . $this->mail_params["derzeitVersichertBei"] . "\n"
			. "Einkommen:"     . $this->mail_params["einkommen"] . "\n"
			. "Email:"         . $this->mail_params["email"] . "\n"
			. "Kommentar:"     . $this->mail_params["kommentar"] . "\n"
			. "KommentarIntern:"     . $this->mail_params["kommentarIntern"] . "\n"
			. "IP:"            . $this->mail_params["ip"] . "\n"
			. "AdressIdLieferant:"     . $this->mail_params["adressIdLieferant"] . "\n" ; 
              
    }
    
    
    public function forwardEmail($params){
        
        $this->mail_params = $this->getMailParams($params);

        if(empty($this->mail_params)){
                
            return false;
        }

        $this->mail_msg = $this->getMailMessage();
        
        $this->headers = "From:" . $this->mail_params["from"];
        
        $result = mail(
                $this->mail_params["to"], 
                $this->mail_params["subject"], 
                $this->mail_msg, 
                $this->headers);   
        
        return true;
    }
}

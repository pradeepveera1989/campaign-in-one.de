<?php
	// Update with your version of and path to the Maileon PHP API Client
	// http://dev.maileon.com/api-clients/
	require_once("maileon-php-client-1.3.2/client/MaileonApiClient.php");
    
	$config = array(
		"BASE_URI" => "https://api.maileon.com/1.0",
		"API_KEY" => "9aa9d110-8448-4536-a9b6-427db2d43061", // Insert API key
		"THROW_EXCEPTION" => true,
		"TIMEOUT" => 60,
		"DEBUG" => false, // NEVER enable on production
		
		// CC addresses to notify
		// Note: every entry must be an active contact in the Maileon account
		"CC" => array(
			 "marc.kresin@treaction.de"     
			// ...
		)
	);
	
	// Read the sent JSON body from PHP's I/O stream
	$input = fopen('php://input', 'r');
	$temp = fopen('php://temp', 'r+');
	
	stream_copy_to_stream($input, $temp);  		 // Save the input stream into PHP's build in temp
	rewind($temp);						   		 // and rewind
	$contentAsJSON = stream_get_contents($temp); // so we can finally read
    $content = json_decode($contentAsJSON); 	 // and decode it
	
	// Prepare the transaction(s)
	$transactionsService = new com_maileon_api_transactions_TransactionsService($config);
	$transactions = array();
	
	// Create a transaction for each CC address
	foreach( $config["CC"] as $cc ) { 
		$transaction = new com_maileon_api_transactions_Transaction();
		$transaction->type = 8; // Insert the transaction id of the event        
		
		$transaction->contact = new com_maileon_api_transactions_ContactReference();           
		$transaction->contact->email = $cc;
		
		// Transfer sent contact information into transaction payload
         
        $transaction->content["email"] = $content->email;
        $transaction->content["SALUTATION"] = $content->salutation; 
        $transaction->content["FIRSTNAME"] = $content->firstname;
        $transaction->content["LASTNAME"] = $content->lastname;
        $transaction->content["ZIP"] = $content->zip;   
        $transaction->content["CITY"] = $content->city;
        $transaction->content["ADDRESS"] = $content->address;
        $transaction->content["HNR"] = $content->hnr;
        $transaction->content["Telefon"] = $content->Telefon;
        $transaction->content["Quelle"] = $content->Quelle;    
        $transaction->content["trafficsource"] = $content->trafficsource;  

		// Pool all transactions
		$transactions[] = $transaction;
	}
	
	// Send transaction(s)
	$response = $transactionsService->createTransactions($transactions);
	
	// Validate return value
	if( $response->isSuccess() ) {
		// Transactions have been sent successfully; nothing else to do (except for logging maybe?)
	} else {
		// An error occured while sending the transaction(s)
		// Add error handling (log the error, send a different transaction for an error notification etc.)
	}
?>

<p>This is a webhook</p>
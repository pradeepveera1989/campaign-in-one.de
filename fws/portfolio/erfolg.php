<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>FWS | Ihr Spezialist für Kapitalanlagen und Pflegeimmobilien</title>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Cinzel" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">  
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="css/creative.css" rel="stylesheet">
  </head>

  <body id="page-top">
     <!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '166146667427092');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=166146667427092&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-67495742-9"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-67495742-9');
</script>

    <!-- Nvigation -->
    <nav>
    <div class="container">
    <div class="row">
    <div class="col-lg-3 mx-auto text-right logo"><a class="navbar-brand js-scroll-trigger" href="#page-top"><img src="img/fws_logo.png" /></a></div>   
    <div class="col-lg-9 mx-auto nav-head"><h5>IHR SPEZIALIST FÜR KAPITALANLAGEN UND PFLEGEIMMOBILIEN</h4></div>
    </div>     
    </div>
    </nav>

<div class="clearspace"></div>  
      

    <section class="bg-primary" id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading">Vielen Dank!</b></h2>
            <hr class="light my-4">
            <h2 class="section-heading">Ihre Anmeldung war erfolgreich</b></h2>
            <p>&nbsp;</p>
            <p>Wir werden uns in Kürze bei Ihnen melden. Mit freundlichen Grüßen.</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          </div>
        </div>
      </div>
    </section>
      
<div class="clearspace"></div>       
<div class="clearspace-trans"></div> 
<div class="clearspace-trans"></div> 
      
      
    <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <img src="img/fws_logo.png"> 
<div class="clearspace-trans"></div>  
            <h5 class="section-heading"><b>FLASH-WEB-SOLUTIONS e.K.</b></h5>
            <hr class="my-4">
            <h2 class="section-heading">Ihr Spezialist für Kapitalanlagen Pflegeimmobilien</h2>  
<div class="clearspace-trans"></div>                
            <img src="img/sicherheit.png">
<div class="clearspace-trans"></div>  
<div class="clearspace-trans"></div>
<div class="clearspace-trans"></div>                
            <h5>
				<a href="impressum.php" target="_blank">IMPRESSUM</a> | 
				<a href="#" target="_blank">KONTAKT</a> | 
				<a href="datenschutz.php" target="_blank">DATENSCHUTZ</a> | 
			  	<a href="ueber-fws.php" target="_blank">ÜBER UNS</a>
			</h5>
          </div>
        </div>
    
    </section>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>

  </body>

</html>

﻿<?php


	/* ______________________________________________________________________________________________________________________________________________
	 *
	 MAIL IN ONE - FWS - LANDINGPAGE - ANGEBOT - 1804
	 * _______________________________________________________________________________________________________________________________________________
	 */

	// Get the config data from config.ini       
	$config = parse_ini_file("config3.ini", true);

	#Database
	$database = $config[Database][DbName]; 
	$user = $config[Database][User]; 
	$dbname = $config[Database][DbName]; 
	$password = $config[Database][Password]; 
	$host = $config[Database][Host]; 	
	
	require_once('/is/htdocs/wp13026946_P396R5TO8O/www/campaign-in-one.de/php-client/splittest-php-client/splittest-php-client.php'); 
	require_once('/is/htdocs/wp13026946_P396R5TO8O/www/campaign-in-one.de/php-client/splittest-php-client/functions.php'); 
 	
 	# SplitTest
	# $config[Varient][VarientFile] is enabled only for varients page
	if($config[Splittest][RunSplittest] && !$config[Varient][VarientFile]){
		
		# Create folders for the Splittest 
		# Executed only for the 1st time.
		$spTestName = $config[Splittest][Name];
		$varients = $config[Splittest][NumberOfVariants];
		$spTestName = $config[Splittest][Name];
		$dir = getcwd();

		while($varients > 0){
			$folder = $spTestName.'/V'.$varients.'/';
			if(!is_dir($folder)){
				mkdir( $folder, 0777, true);
			}			
			copyDirectory( $dir, $folder , $spTestName);
			if(!modifyFile($folder.'config.ini')){
				
				#something went wrong
				return false;
			}			
			if(!modifyFile($folder.'config3.ini')){
				
				#something went wrong
				return false;
			}
			$varients = $varients - 1 ;
		}
		
		#Create a new object for Split Test 
		$spTest = new splitTest($spTestName, $config[Splittest][NumberOfVariants]);

		if(!($spTest->tableExists())){
			# Excutes only once while creating the splitTest
			$spTest->createTable();
			$spTestIndex = 1;
		}else {
			#Get the current value of split Test index
			$spTest->getCurrentIndex();
		}	
		
		$spTestNewIndex = $spTest->getNewIndex($spTestIndex);
		$spTestNewLink = $spTestName.'/V'.$spTestNewIndex.'/index3.php';
		
		#Get a new splitTest index
		if($spTestNewLink){
			# Divides the URL and appends the new splittest index and appends the parameters.
			$url_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
			$php_parts = explode('/', $_SERVER['REQUEST_URI']);
			$index = $php_parts[sizeof($php_parts)-1];
			array_pop($php_parts);
			$link = implode($php_parts, "/");			
			
			# Generate a new validate link.
			$redirectURL = "https://".$_SERVER['HTTP_HOST'].$link."/".$spTestNewLink."?".$url_parts[1];	


			#Finally redirects the URL to new Link.
   			echo '<script type="text/javascript">';
            echo "window.top.location='".$redirectURL."'";
			echo '</script>';    
		}
	
	}else {
		
		//vorname  
		$standard_01 = $_GET["1"];
		//nachname
		$standard_02 = $_GET["2"];
		//email
		$email_02    = $_GET["3"];
		//zip
		$standard_04 = $_GET["4"]; 
		//city
		$standard_05 = $_GET["5"]; 
		//telefon
		$custom_03 = $_GET["6"];	
		//Strasse
		$standard_07 = $_GET["7"]; 
		//HNR
		$standard_08 = $_GET["8"]; 	
		//salutaion
		$standard_09 = $_GET["9"]; 	
		
		//trafficsource
		$ts = $_GET["trafficsource"];  
		  
		$standard_01 = str_replace(' ', '+', $standard_01);
		$standard_02 = str_replace(' ', '+', $standard_02);
		$email_02 = str_replace(' ', '+', $email_02);
		$standard_04 = str_replace(' ', '+', $standard_04);
		$standard_05 = str_replace(' ', '+', $standard_05);
		$custom_03 = str_replace(' ', '+', $custom_03);	
		$standard_07 = str_replace(' ', '+', $standard_07);	
		$standard_08 = str_replace(' ', '+', $standard_08);	

		// Update with Hubspot 
		require_once('/is/htdocs/wp13026946_P396R5TO8O/www/campaign-in-one.de/php-client/hubspot-php-client/class.contacts.php'); 
		require_once('/is/htdocs/wp13026946_P396R5TO8O/www/campaign-in-one.de/php-client/hubspot-php-client/class.company.php'); 
		require_once('/is/htdocs/wp13026946_P396R5TO8O/www/campaign-in-one.de/php-client/hubspot-php-client/class.deals.php'); 
		   
		// Email Validation
		require_once('/is/htdocs/wp13026946_P396R5TO8O/www/campaign-in-one.de/php-client/emailvalidation-php-client/emailchecker.php');
		  
		// Update with your version of the Maileon PHP API Client
		require_once('/is/htdocs/wp13026946_P396R5TO8O/www/campaign-in-one.de/php-client/maileon-php-client-1.3.1/client/MaileonApiClient.php');
		require_once('/is/htdocs/wp13026946_P396R5TO8O/www/campaign-in-one.de/php-client/webhook.php');

		// Get the config data from config.ini       
		#$config = parse_ini_file("config.ini", true);

		#Prefilling 
		if($config[GeneralSetting][SupportPreFilling]){
			
			$standard_001 = base64_decode($standard_01);
			$standard_002 = base64_decode($standard_02);
			$email_002 = base64_decode($email_02);
			$standard_004 = base64_decode($standard_04);
			$standard_005 = base64_decode($standard_05);
			$custom_003 = base64_decode($custom_03);	
			$standard_007 = base64_decode($standard_07);	
			$standard_008 = base64_decode($standard_08);	
			$standard_009 = base64_decode($standard_09);
		
		}
		$configMailInOne = array(
			'BASE_URI' => 'https://api.maileon.com/1.0',
			'API_KEY' => $config[MailInOne_Settings][Mapikey],
			'THROW_EXCEPTION' => true,
			'TIMEOUT' => 60,
			'DEBUG' => 'false' // NEVER enable on production
		);

		$contactid = '';
		$checksum = '';
		$mailingid = "";
		$resp;


		session_start();
		
		if (isset($_GET['contactid'])) {
			$contactid = $_GET['contactid'];
		} elseif (isset($_POST['contactid'])) {
			$contactid = $_POST['contactid'];
		}

		if (isset($_GET['checksum'])) {
			$checksum = $_GET['checksum'];
		} elseif (isset($_POST['checksum'])) {
			$checksum = $_POST['checksum'];
		}

		if (isset($_GET['mailingid'])) {
			$checksum = $_GET['mailingid'];
		} elseif (isset($_POST['mailngid'])) {
			$checksum = $_POST['mailingid'];
		}
		
		#Mapping Name from HTML fields
		$emailName = $config[MailInOne_Settings][Mapping][EMAIL]; 
		$anreda = $config[MailInOne_Settings][Mapping][SALUTATION]; 
		$vorname = $config[MailInOne_Settings][Mapping][FIRSTNAME]; 
		$nachname = $config[MailInOne_Settings][Mapping][LASTNAME]; 
		$plz = $config[MailInOne_Settings][Mapping][ZIP]; 
		$ort = $config[MailInOne_Settings][Mapping][CITY];  
		$strasse = $config[MailInOne_Settings][Mapping][ADDRESS];  
		$nummer = $config[MailInOne_Settings][Mapping][HNR];  
		$telefon = $config[MailInOne_Settings][Mapping][TELEFON];  							
		$quelle = $config[MailInOne_Settings][Mapping][Quelle]; 
		$typ = $config[MailInOne_Settings][Mapping][Typ]; 
		$segment = $config[MailInOne_Settings][Mapping][Segment]; 	

		#Autofill postal code
		$postalCodeAutofill = $config[Postal_Code][Autofill]; 
		$postalCodeErrorMsg = $config[Postal_Code][ErrorMsg]; 
		$postalCodeErrorColor = $config[Postal_Code][ErrorMsg_Color];
		
		#Email Validation
		$EmailValidationColor = $config[Email_Address][ErrorMsg_Color];
		$EmailValidationMessage = $config[Email_Address][ErrorMsg];

		#MailInOne Settings 
		$mailInOneSync = $config[MailInOne_Settings][Sync];		

		if (isset($_POST[$emailName])) {
		   
			$email = $_POST[$emailName];

			$emailchecker = new emailChecker($email);
			$emailchecker->curlRequest();
			$emailchecker->curlResponce();
			$resp = $emailchecker->parseResponce();

			if (!$resp) {
				// invalid email address
				// Copying the data to session and intiating a sesssion variable error.
				$_SESSION['data'] = $_POST;
				$_SESSION['error'] = "email";
				echo '<script type="text/javascript">';
				echo 'window.location ="#form"';
				echo '</script>';  
			} else {
				$_SESSION['error'] = "";
				session_destroy();	

				$_SESSION['data'] = array();
				$standard_0 = $_POST[$anreda];
				$standard_1 = $_POST[$vorname];
				$standard_2 = $_POST[$nachname];
				$standard_3 = $_POST[$plz];
				$standard_4 = $_POST[$ort];
				$standard_5 = $_POST[$strasse];
				$standard_6 = $_POST[$nummer];
				$standard_7 = $_POST[$telefon];	
				$custom_0 = $_POST[$quelle];
				$custom_1 = $_POST["interesse"];
				$custom_3 = $_POST[$telefon];

				$ip = $_SERVER['REMOTE_ADDR'];
				$url = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
				$trafficsource = $_POST["ts"];

				// MailInOne Sync
				if ($mailInOneSync) {
					
					// Mail-in-one Configuration			
					$contactsService = new com_maileon_api_contacts_ContactsService($configMailInOne);
					$contactsService->setDebug(false);
								
					$getContact = $contactsService->getContactByEmail($email);
					
					if( $getContact->isSuccess() && $getContact->getResult()->permission != com_maileon_api_contacts_Permission::$NONE ) {
						
						#Update the contact in Mailinone
						
						$to =            "mail@fws-services.de, fws@web-creatorandconsulting.de";
					#	$to =            "pradeep.veera@treaction.de";
						$from =          "admin@kapitalanlage-pflegeimmobilien.net";

						$produkt1 =      'Pflegeimmobilie';
						$produkt2 =      '';
						$produkt3 =      '';
						$produkt4 =      '';
						$produkt5 =      '';
						$produkt6 =      '';
						$produkt7 =      '';
						$produkt8 =      '';
						$produkt9 =      '';
						$produkt10 =     '';
						$anrede =       $standard_0;
						$vorname =      $standard_1;
						$nanchname =    $standard_2;
						$geburtsdatum = '0000-00-00';
						$telefon =      $standard_7;
						$mobiltelefon = '';
						$strasse =      $standard_5;
						$nummer =       $standard_6;
						$plz =          $standard_3;
						$ort =          $standard_4;
						$beruf =        '';
						$verzischertbei = '';
						$einkommen = '';
						$email =        $email;
						$kommentar = '';
						$kommentarIntern = '';
						$IP = '';
						$AdressIdLieferant =  $trafficsource;


						$subject = $trafficsource;

						$message = $first_name 
							.  " " . 
						$last_name 

							. "Produkt1:"      . $produkt1 . "\n" 
							. "Produkt2:"      . $produkt2 . "\n" 
							. "Produkt3:"      . $produkt3 . "\n" 
							. "Produkt4:"      . $produkt4 . "\n" 
							. "Produkt5:"      . $produkt5 . "\n" 
							. "Produkt6:"      . $produkt6 . "\n" 
							. "Produkt7:"      . $produkt7 . "\n" 
							. "Produkt8:"      . $produkt8 . "\n" 
							. "Produkt9:"      . $produkt9 . "\n" 
							. "Produkt10:"     . $produkt10 . "\n" 
							. "Anrede:"        . $standard_0 . "\n" 
							. "Name:"          . $standard_2 . "\n"
							. "Vorname:"       . $standard_1 . "\n"
							. "Geburtsdatum:"  . $geburtsdatum . "\n"
							. "Telefon:"       . $standard_7 . "\n"
							. "Mobiltelefon:"  . $mobiltelefon . "\n" 
							. "Strasse:"       . $standard_5 . "\n"
							. "Nummer:"        . $standard_6 . "\n"
							. "PLZ:"           . $standard_3 . "\n"
							. "Ort:"           . $standard_4 . "\n"
							. "Beruf:"         . $beruf . "\n" 
							. "derzeitVersichertBei:". $verzischertbei . "\n" 
							. "Einkommen:"     . $einkommen . "\n" 
							. "Email:"         . $email . "\n"
							. "Kommentar:"     . $kommentar . "\n"
							. "KommentarIntern:"     . $kommentarIntern . "\n"
							. "IP:"            . $IP . "\n"
							. "AdressIdLieferant:"     . $AdressIdLieferant . "\n";



						$headers = "From:" . $from;

						mail($to,$subject,$message,$headers);
						
						$date = date("Y-m-d");
						
						$debug = FALSE;
						$contactsService = new com_maileon_api_contacts_ContactsService($configMailInOne);
						$contactsService->setDebug($debug);
						$newContact = new com_maileon_api_contacts_Contact();
						$newContact->email = $email;
						
						$newContact->standard_fields["SALUTATION"] = $standard_0;
						$newContact->standard_fields["FIRSTNAME"] = $standard_1;    
						$newContact->standard_fields["LASTNAME"] = $standard_2;
						$newContact->standard_fields["ZIP"] = $standard_3;
						$newContact->standard_fields["CITY"] = $standard_4;
						$newContact->standard_fields["ADDRESS"] = $standard_5;
						$newContact->standard_fields["HNR"] = $standard_6;
				   
						$newContact->custom_fields["Telefon"] = $custom_3;
						$newContact->custom_fields["ip_adresse"] = $ip;
						$newContact->custom_fields["url"] = $url;
						$newContact->custom_fields["trafficsource"] = $trafficsource;
						
						$newContact->custom_fields["Quelle"] = 'fwslp1804';
						$newContact->custom_fields["Interesse"] = 'Pflegeimmo';
						$newContact->custom_fields["ip_adresse"] = $ip;
						$newContact->custom_fields["url"] = $url;
						$newContact->custom_fields["trafficsource"] = $trafficsource;
						$newContact->custom_fields["Erfassungsdatum"] = $date;
						
						$response = $contactsService->createContact($newContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, "", "", false);

   						echo '<script type="text/javascript">';
						echo 'window.location.href="https://kapitalanlage-pflegeimmobilien.net/angebot/redirect.php";';
						echo '</script>';  	 				
						
					}else {				
					
						$to =            "mail@fws-services.de, fws@web-creatorandconsulting.de";
					#	$to =             "pradeep.veera@treaction.de";
						$from =          "admin@kapitalanlage-pflegeimmobilien.net";

						$produkt1 =      'Pflegeimmobilie';
						$produkt2 =      '';
						$produkt3 =      '';
						$produkt4 =      '';
						$produkt5 =      '';
						$produkt6 =      '';
						$produkt7 =      '';
						$produkt8 =      '';
						$produkt9 =      '';
						$produkt10 =     '';
						$anrede =       $standard_0;
						$vorname =      $standard_1;
						$nanchname =    $standard_2;
						$geburtsdatum = '0000-00-00';
						$telefon =      $standard_7;
						$mobiltelefon = '';
						$strasse =      $standard_5;
						$nummer =       $standard_6;
						$plz =          $standard_3;
						$ort =          $standard_4;
						$beruf =        '';
						$verzischertbei = '';
						$einkommen = '';
						$email =        $email;
						$kommentar = '';
						$kommentarIntern = '';
						$IP = '';
						$AdressIdLieferant =  $trafficsource;


						$subject = $trafficsource;

						$message = $first_name 
							.  " " . 
						$last_name 


							. "Produkt1:"      . $produkt1 . "\n" 
							. "Produkt2:"      . $produkt2 . "\n" 
							. "Produkt3:"      . $produkt3 . "\n" 
							. "Produkt4:"      . $produkt4 . "\n" 
							. "Produkt5:"      . $produkt5 . "\n" 
							. "Produkt6:"      . $produkt6 . "\n" 
							. "Produkt7:"      . $produkt7 . "\n" 
							. "Produkt8:"      . $produkt8 . "\n" 
							. "Produkt9:"      . $produkt9 . "\n" 
							. "Produkt10:"     . $produkt10 . "\n" 
							. "Anrede:"        . $standard_0 . "\n" 
							. "Name:"          . $standard_2 . "\n"
							. "Vorname:"       . $standard_1 . "\n"
							. "Geburtsdatum:"  . $geburtsdatum . "\n"
							. "Telefon:"       . $standard_7 . "\n"
							. "Mobiltelefon:"  . $mobiltelefon . "\n" 
							. "Strasse:"       . $standard_5 . "\n"
							. "Nummer:"        . $standard_6 . "\n"
							. "PLZ:"           . $standard_3 . "\n"
							. "Ort:"           . $standard_4 . "\n"
							. "Beruf:"         . $beruf . "\n" 
							. "derzeitVersichertBei:". $verzischertbei . "\n" 
							. "Einkommen:"     . $einkommen . "\n" 
							. "Email:"         . $email . "\n"
							. "Kommentar:"     . $kommentar . "\n"
							. "KommentarIntern:"     . $kommentarIntern . "\n"
							. "IP:"            . $IP . "\n"
							. "AdressIdLieferant:"     . $AdressIdLieferant . "\n";



						$headers = "From:" . $from;
				
						mail($to,$subject,$message,$headers);
						
						$date = date("Y-m-d");
						#var_dump($date);
						$newContact = new com_maileon_api_contacts_Contact();
						$newContact->email = $email;
						$newContact->anonymous = false;
						$newContact->permission = com_maileon_api_contacts_Permission::$NONE;
						
						$newContact->standard_fields["SALUTATION"] = $standard_0;
						$newContact->standard_fields["FIRSTNAME"] = $standard_1;    
						$newContact->standard_fields["LASTNAME"] = $standard_2;
						$newContact->standard_fields["ZIP"] = $standard_3;
						$newContact->standard_fields["CITY"] = $standard_4;
						$newContact->standard_fields["ADDRESS"] = $standard_5;
						$newContact->standard_fields["HNR"] = $standard_6;
						
						$newContact->custom_fields["Quelle"] = $custom_0; 
						$newContact->custom_fields["Interesse"] = $custom_1;
						$newContact->custom_fields["Telefon"] = $custom_3;
						$newContact->custom_fields["ip_adresse"] = $ip;
						$newContact->custom_fields["url"] = $url;
						$newContact->custom_fields["trafficsource"] = $trafficsource;
						$newContact->custom_fields["Erfassungsdatum"] = $date;
					  
						$response = $contactsService->createContact($newContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, '', '', true, true, 'S70UpPp3');
						
						
   						echo '<script type="text/javascript">';
						echo 'window.location.href="https://kapitalanlage-pflegeimmobilien.net/angebot/redirect.php";';
						echo '</script>';  	 														
					}
				}    
			}	
		}
	} 
?>
<?php

	# Facebook Pixel URL
	$facebookpixel = $config[TrackingTools][FacebookPixel];
	$fbURL = "https://www.facebook.com/tr?id=".$config[TrackingTools][FacebookPixel]."&ev=PageView&noscript=1";
	
	# Google Maps URL
	$googleKey = $config[TrackingTools][GoogleAPIKey];
	$googleURL = "https://maps.googleapis.com/maps/api/js?key=".$config[TrackingTools][GoogleAPIKey]."&libraries=places";

	#Google Analytics Key
	$googleAnalyticsKey = $config[TrackingTools][GoogleAnalytics];
	$googleAnalyticsURL = "https://www.googletagmanager.com/gtag/js?id=".$config[TrackingTools][GoogleAnalytics];
?>	 
	 
<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Ihr Spezialist für Kapitalanlagen und Pflegeimmobilien">
    <meta name="author" content="flash web solutions">
    <title>FWS | Ihr Spezialist für Kapitalanlagen und Pflegeimmobilien</title> 
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/creative.css" rel="stylesheet">  
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Cinzel" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">  
	  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<link rel="stylesheet" href="vendor/telefonvalidator-client/build/css/intlTelInput.css" />		
  </head>
    
    <!-- Facebook Pixel Code -->
    <script type="text/javascript" src=<?php echo $googleURL; ?></script>   	
	<script async src= <?php echo $googleAnalyticsURL;?>></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', '<?php echo $googleAnalyticsKey; ?>');
	</script>	
	<script>
	  /*Get the facebook pixel code from config.ini */
		var facebookpixel = <?=$facebookpixel ?>;
		!function(f,b,e,v,n,t,s)
		  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		  n.queue=[];t=b.createElement(e);t.async=!0;
		  t.src=v;s=b.getElementsByTagName(e)[0];
	      s.parentNode.insertBefore(t,s)}(window, document,'script',
		  'https://connect.facebook.net/en_US/fbevents.js');
		  fbq('init', <?=$facebookpixel ?> );
		  fbq('track', 'PageView'); 
	</script>
	<noscript>
		<img height="1" width="1" style="display:none" src= <?php echo $fbURL ?>
	/></noscript>		
<!-- End Facebook Pixel Code -->


<body id="page-top">
  	
  <!-- NAVBAR -->      
        <nav>

            <div class="container">
                <div class="row">
                    <div class="col-lg-4 logo">
                        <a class="navbar-brand js-scroll-trigger" href="https://kapitalanlage-pflegeimmobilien.net/portfolio/#page-top"><img style="width: 390px !important; height: 80px !important; margin: 5px 15px;" src="img/kp_logo.png"></a>
                    </div>
                    <div class="text-center col-lg-8 text-right" style="padding-top: 37px;">
                        <h6 class="open-sans">Jetzt kostenlos informieren: <span style="color: #005577 !important; font-size: 1.4rem;"><i style="margin-left: 8px; margin-right: 2px;" class="fas fa-phone"></i> <strong>0800 1140069</strong></span></h6>
                    </div>
                </div>
            </div>
        </nav>     


        <!-- MOBILE HEADER -->

        <div class="container mobile-head">    
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <img style="width:100%;margin-bottom:15px;" src="img/kp_logo.png">
                        <center><h6 class="open-sans">Jetzt kostenlos informieren: <br><span style="color: #005577 !important; font-size: 1.4rem;"><i style="margin-left: 8px; margin-right: 2px;" class="fas fa-phone"></i> <strong><a href="tel:08001140069">0800 1140069</a></strong></span></h6></center>
                    </div>
                </div>       
            </div>  
        </div>
        <!-- /. MOBILE HEADER -->


        <div class="row">
            <div class="col-12">

                <div style="color:#fff; background-color:#557700; padding:5px 5px 5px 5px; font-size:15px!important;">

                    <center><i class="fas fa-check"></i> Über 33.000 Kundenberatungen &nbsp;&nbsp;<i class="fas fa-check"></i> Über 20 Jahre ihr verlässlicher Partner&nbsp;&nbsp;<i class="fas fa-check"></i> Kostenlos und unverbindlich informieren</center>
                </div>
            </div>
        </div>

	
<header class="masthead2 d-flex">
	<div class="container my-auto">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading2">ÜBER FWS</h2>
				<hr class="dark">
			</div>
		</div>
	</div>
</header>
        
    <!-- Über FWS -->    
<section>
	<div class="container">
		<div class="row">
			<center>
			<div class="col-sm-12 col-md-9" style="text-align: left;">
				<p><small><a href="index.php">zurück</a></small></p><br>
				
				<p><strong>Unser vorrangiges Unternehmensziel ist es, zusammen mit Ihnen den Zukunfts-/und Wachstumsmarkt Nr. 1 der Pflegeimmobilien zu erschließen.</strong></p>
				<p>Schon seit 2010, als einer der Ersten in Deutschland, entdeckten wir die äußert interessante Möglichkeit Geld gewinnbringend und sicher in Pflegeimmobilien als Teileigentum zu investieren. Was früher nur Großinvestoren und institutionellen Anlegern (wie Banken, Fondsgesellschaften und Versicherungen) vorbehalten war, welche im Millionenbereich investiert haben und weiterhin tun, war nun auch den Menschen mit kleineren Portmonees möglich. Der Zugang zum lukrativen Pflegeimmobilienmarkt für uns "Otto-Normal-Verbraucher" wurde erlangt, indem man die Pflegeheime in Teileigentum aufgeteilt hat (analog wie bei einem ganz normalen Mehrfamilienhaus). Somit war der Weg für die Kleinanleger in diesem Segment geebnet.</p><br>
				
				<p>Die Pflegeimmobilie ist auch im Volksmund als "Sorglos-Immobilie" bekannt, da sie noch weitere Vorteile einer "normalen Kapitalanlageimmobilie" bietet, da aufgrund der ständig wachsenden Anzahl älterer Menschen, in Zukunft immer mehr Pflegeheime benötigt werden. Ein echter Wachstumsmarkt vor sozialem Hintergrund. Eine Übersicht der positiven Aspekte einer Pflegeimmobilie sehen Sie hier:</p>
				<p><strong>Solide Renditen:</strong><br>
				In der Regel wird mit Renditen von 4-6% p.a. auf den Kaufpreis gerechnet.</p>
				<p><strong>Inflationsschutz:</strong><br>
				Die Mietverträge sind indexiert. Das bedeutet, dass die Miete in regelmäßigen Intervallen an den steigenden Preisindex angepasst wird. Meist sind diese Intervalle fünfjährig angelegt.</p>
				<p><strong>Mietsicherheit:</strong><br>
				Bei Pflegeimmobilie besteht der Mietvertrag meist nicht mit dem Bewohner, sondern mit dem Betreiber. Dieser pachtet die ganze Einrichtung für rund 20 Jahre und zahlt auch dann Miete, wenn das Apartment nicht belegt ist.</p>
				<p><strong>Geringe Instandhaltung:</strong><br>
				Für die Instandhaltung ist zum größten Teil der Betreiber der Pflegeeinrichtung verantwortlich. Das ist beispielsweise bei Renovierungen oder Sanierungen, die die investierte Pflegeimmobilie betreffen, der Fall. Der Investor ist lediglich anteilig für „Dach und Fach“ zuständig.</p>
				<p><strong>Geringer Verwaltungsaufwand:</strong><br>
				Vermietertypische Aufgaben werden deutlich reduziert. Es entfallen beispielsweise die Nebenkostenabrechnung und die Mietersuche.</p>
				<p><strong>Geringe Konjunkturabhängigkeit:</strong><br>
				Da die Nachfrage nach Pflegeplätzen stark steigen wird, sind Pflegeimmobilien weniger konjunkturabhängig als Wohn- und Gewerbeimmobilien.</p>
				<p><strong>Exklusives Belegrecht:</strong><br>
				Investoren haben das vorteilhafte Recht, die Pflegeimmobilie oder eine andere Immobilie desselben Betreibers im Bedarfsfall für sich zu beanspruchen. Dieses Recht gilt nicht nur für den Investor selbst, sondern auch für seine Familienangehörigen.</p>
				<p><strong>Steuervorteile:</strong><br>
				Sie können die Immobilie und das Inventar abschreiben und die Finanzierungszinsen absetzen. Wie bei anderen Immobilien ist nach 10 Jahren ein steuerfreier Verkauf möglich.</p>
				<p><strong>Grundbucheintrag:</strong><br>
				Wie eine Eigentumswohnung können Sie eine Pflegeimmobilie jederzeit verkaufen, vererben oder verschenken. Ihr Kapital ist somit nicht fest gebunden, wenn Sie es dringend brauchen.</p><br>
				
				<p>Wir kennen auch die Alternativen zu dieser Kapitalanlage, da wir gemeinsam berufliche und akademische Bildung und Erfahrung in den Bereichen Bank, Fondsmanagement Immobilien, Versicherung und Börsenhandel haben. Als unabhängiger Vermittler (wir sind an keine Bauträger oder Projektentwickler gebunden) arbeiten wir jedoch aus Überzeugung ausschließlich im Bereich der Pflegeimmobilie.</p>
				<p>Wir sondieren für Sie den Markt hinsichtlich zukunftsträchtiger Pflegeimmobilien als Anlageobjekt. Hierbei prüfen wir insbesondere Standort, Bauqualität und Betreiber der Pflegeeinrichtungen, d.h. wir prüfen die Pflegeimmobilien auf "Herz und Nieren" bevor wir Sie Ihnen anbieten oder empfehlen. Mit unserer persönlichen Unterstützung und unserem fachlichem Know-how bieten wir Ihnen eine ehrliche und zielorientierte Beratung, und für Sie als Käufer sind unsere Dienstleistungen selbstverständlich garantiert kostenfrei.<br>
				Wir erstellen Ihnen eine fundierte Berechnung, aufgrund Ihrer Einkommens- und Steuersituation, zu Ihrem geplanten Investment in eine Pflegeimmobilie-Kapitalanlage.<br>
				Unser qualifiziertes Team berät Sie ehrlich und unabhängig.<br>
				Wir sind ein qualifiziertes Team , welches die einzelnne Objekte aus verschiedenen Sichtweisen für Sie analysiert und Ihnen eine fundierte und ehrliche Empfehlung ausspricht.<br>
				Vereinbaren Sie mit uns einen unverbindlichen Telefontermin - Sie werden es nicht bereuen.</p><br>
				
				<p><small><a href="index.php">zurück</a></small></p>
			</div>
			</center>
		</div>
	</div>    
</section>
    
<!-- Rüdiger Finn -->     
<section class="bg-dark text-white">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-6">
				<img src="img/ruediger-finn-lang.jpg" style="width:100%; margin-top: 5px; margin-bottom: 15px;">
			</div>
			<div class="col-sm-12 col-md-6">
				<h5 class="open-sans" style="margin-bottom: 15px; color: #FFFFFF;">Ihr Spezialist für Kapitalanlage in Pflegeimmobilien: Rüdiger Finn</h5>
				<i><p>"Als gelernter Bankkaufmann habe ich die ganzen Facetten des Bankwesen kennengelernt. Auf meinen weiteren beruflichen Stationen, u.a. als Geschäftführer der Mehrwert Makler GmbH, habe ich mir dann in den Bereichen Versicherungen, Kapitalanlagen und Immobilien spezielles Fachwissen angeeignet bzw. das vorhandene weiter vertieft.</p>
				<p>Seit 1999 führe ich erfolgreich die FWS, welche im Jahr 2010 das Hauptaugenmerk auf den interessanten und zugleich lukrativen Kapitalanlagebereich der Pflegeimmobilien ausgerichtet hat. Durch unsere langjährige Erfahrung und Branchenkenntnis im Pflegebereich liegt heute unsere Kernkompetenz genau in diesem Segment.</p>
				<p>Aufgrund des demographischen Wandels wird der Bedarf an Pflegeimmobilien weiter zunehmen. Und wenn Sie durch eine Investition Pflegewohnraum zur Verfügung stellen,  werden Sie nicht nur eine gute Rendite erzielen sondern der positive Nebeneffekt ist, dass Sie sich auch gesellschaftlich engagieren. Deshalb sprechen wir auch von einer "Rendite mit sozialer Verantwortung".</p>
				<p>Ich freue mich Ihnen unsere kostenlose Beratung, zum Thema Kapitalanlage Pflegeimmobilie, anbieten zu dürfen."</p></i>
			</div>
		</div>
	</div>    
</section> 

<!-- FOOTER -->
        <section id="contact">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 mx-auto text-center">
                        <div style="margin-bottom: 50px;">
                            <p><small><strong>*Staatlich gesicherte Miete:<br></strong>
                                    Die Pflegeimmobilie gehört zu den Sozialimmobilien, die im Rahmen des SGB XI (Sozialgesetzbuch-Elftes Buch-Soziale Pflegeversicherung) als förderungswürdig gelten. Kann der Bewohner sein Pflegeappartements nicht mehr selbst finanzieren, springt daher der Staat ein. Das bedeutet: der Betreiber (Ihr Mieter) kann sich stets darauf verlassen, dass er für belegte Appartements seine kalkulierten Einnahmen erhält.</small></p>
                        </div>
                        <div class="footer-image"><img src="img/fws_logo.png"></div>
                        <div class="clearspace-trans"></div>
                        <h5 class="section-heading open-sans"><b>Kapitalanlage-Pflegeimmobilien <br>ist ein Produkt der FLASH-WEB-SOLUTIONS e.K.</b></h5>
                        <hr class="my-4">
                        <h2 class="section-heading">Ihr Spezialist für Kapitalanlagen Pflegeimmobilien</h2>
                        <div class="clearspace-trans"></div>
                        <div class="footer-image"><img src="img/sicherheit.png"></div>
                        <div class="clearspace-trans"></div>
                        <div class="clearspace-trans"></div>
                        <div class="clearspace-trans"></div>
                        <h5>
                            <a href="impressum.php" target="_blank">IMPRESSUM</a> | 
                            <a href="https://kapitalanlage-pflegeimmobilien.net/portfolio/#page-top">KONTAKT</a> | 
                            <a href="datenschutz.php" target="_blank">DATENSCHUTZ</a> | 
                            <a href="optionen.php" target="_blank">OPTIONEN</a> | 
                            <a href="ueber-fws.php" target="_blank">ÜBER UNS</a>
                        </h5>
                        <p>&nbsp;</p>
                    </div>
                </div>
            </div>
        </section>

    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="js/creative.min.js"></script>
	<script type="text/javascript" src="vendor/telefonvalidator-client/build/js/utils.js"></script>		
	<script type="text/javascript" src="vendor/telefonvalidator-client/build/js/intlTelInput.js"></script>
    <script type="text/javascript ">

        
        $(document ).ready(function() {
            var autofill_postal_code = "<?= $config[Postal_Code][Autofill];?>";
			var validate_telefon = "<?= $config[Telefon][Status];?>";
			
			// telefon validation 
			var settings = {
				utilsScript: "vendor/telefonvalidator-client/build/js/utils.js",
				preferredCountries: ['de'],			
				onlyCountries:['de','ch','at'],
			};		
			//$('#telefon').intlTelInput(settings);	

			// Error messages
            $('.errorplz').hide();
			$('.errortelefon').hide();
            
            //Parse config.ini file 
                        
            
            /* JQuery Blur event for the postal code
             * Parameters : 
             * Returns : 
             *    1. fill the city Name on success.
             */
            
            $('.plz').blur(function(){
                var zip = $(this).val();
                var city;
               
                
                if($('.plz').hasClass('wrongplz')){
                    $('.plz').removeClass('wrongplz'); 
                    $('.errorplz').hide();
                }
                // Check for autofill postal code value
                if(autofill_postal_code){      
                    city = checkZipCode(zip);
                    
                    if(city){
                        $('.ort').val(city);
                    }else{
                        $('.ort').val("");      
                        $('.plz').addClass("wrongplz");
                    }
                }    
            });                


            /* Function Name : checkZipCode
             * Parameters : zipcode
             * Returns : 
             *    1. countryName on success .
             *    2. False on faiure.
             */
            
            function checkZipCode(zip){
                var status = true;
                var city ;
                if(zip.length == 5) {
               //     var gurl = 'https://maps.googleapis.com/maps/api/geocode/json?address=Germany'+zip+'&key=AIzaSyDA10Y_CEIbkz2OY-Zp7PsBBjKB5YNh77I&components=country:DE';
                    var gurl = 'https://maps.googleapis.com/maps/api/geocode/json?address=Germany'+zip+'&key=AIzaSyDA10Y_CEIbkz2OY-Zp7PsBBjKB5YNh77I';					
                    $.getJSON({
                        url : gurl,
                        async: false,
                        success:function(response, textStatus){
                            // check the status of the request
                            if(response.status !== "OK") {
                                status = false;         // Postal code not found or wrong postal code.
                            } else{    
                                // Postal code is found
								console.log(response);
                                status = true
                                var address_components = response.results[0].address_components;
                                $.each(address_components, function(index, component){
                                    var types = component.types;
                                    // Find the city for the postal code
                                    $.each(types, function(index, type){
                                        if(type == 'locality'){
                                            city = component.long_name;
                                            status = true;
                                        }
                                    });
                                });                                 
                            }
                         }
                    });
                } else{        
                    status = false;
                }
                if(status){
                    return city;
                }else {
                    return false;
                }
            }

			/* Desktop Version
			*  Validates the phone number format for specified country. 
			*
			*  Parameters:  Telephone field in the form.
			*
			*  Returns: 
			*		false : shows the error message 
			*       true  : hides the error message
			*/  	
			

			
            /* JQuery submit event for the postal code
             * Parameters : 
             * Returns : 
             *    1. check the postal code and accordingly display the error message for postal code.
             */
            form = $('.check-submit-form');
            form.submit(function(e)  {
               //e.preventDefault();
                var form = $(this);
                var readyforsubmit = true;     
                var plz = $('.plz');
                var ort = $('.ort');
                var email = $('.email');
                
                if(autofill_postal_code) {
                    if(plz.hasClass("wrongplz")){    
                        $('.errorplz').show();
                        return false;
                    }
                }
						
				form.find("input[type='submit']").prop('disabled', true);
            }); 
        });     
       </script>
    	

  </body>
</html>

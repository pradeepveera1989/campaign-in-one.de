<?php
   
$config = parse_ini_file('config.ini', true, INI_SCANNER_TYPED );


class config{
    
    protected $path;
    public $config;
    
    public function __construct($path) {
        
       $this->config = parse_ini_file($path, true, INI_SCANNER_TYPED );
    }
    
    public function getConfigAssignmentManager(){
        
        return $this->config[AssignmentManager];
    }
    
    public function getConfigMailInOne(){
        
        return $this->config[MailInOne];
    }
    
    public function getConfigHubspot(){
        
        return $this->config[HubSpot];
    }
    
    public function getConfigGeneralSettings(){
        
        return $this->config[GeneralSetting];
    }
    
    public function getConfigPostalCode(){
        
        return $this->config[Postal_Code];
    }    
    
    public function getConfigTrackingTools(){
        
        return $this->config[TrackingTools];
    }   
    
    public function getConfigTelefon(){
        
        return $this->config[Telefon];
    }
    
    public function getConfigEmail(){
        
        return $this->config[Email_Address];
    }
    
    public function getConfigDatabase(){
        
        return $this->config[Database];
    }
}

#Mapping Name from HTML fields
$email = $config[MailInOne_Settings][Mapping][EMAIL];
$anreda = $config[MailInOne_Settings][Mapping][SALUTATION];
$vorname = $config[MailInOne_Settings][Mapping][FIRSTNAME];
$nachname = $config[MailInOne_Settings][Mapping][LASTNAME];
$adresse = $config[MailInOne_Settings][Mapping][ADDRESS];
$plz = $config[MailInOne_Settings][Mapping][ZIP];
$ort = $config[MailInOne_Settings][Mapping][CITY];
$telefon = $config[MailInOne_Settings][Mapping][TELEFON];
$quelle = $config[MailInOne_Settings][Mapping][Quelle];
$typ = $config[MailInOne_Settings][Mapping][Typ];
$segment = $config[MailInOne_Settings][Mapping][Segment];
$ts = $config[MailInOne_Settings][Mapping][trafficsource];
$url = $config[MailInOne_Settings][Mapping][URL];
$ip = $config[MailInOne_Settings][Mapping][IP];
$datum = $config[MailInOne_Settings][Mapping][Datum];
$trafficsource = $config[MailInOne_Settings][Mapping][trafficsource];
$partner = $config[MailInOne_Settings][Mapping][Partner];

// Hubspot
$hubspotcontact = $config[MailInOne_Settings][Mapping][HubspotContactID];
$hubspotcompany = $config[MailInOne_Settings][Mapping][HubspotCompanyID];
$hubspotdeal = $config[MailInOne_Settings][Mapping][HubspotDealID];

// Assignement Manager
$vertrieb = $config[MailInOne_Settings][Mapping][Vertriebsmitarbeiter];
$vertriebid = $config[MailInOne_Settings][Mapping][Vertriebsmitarbeiterid];


// Parter Details
$partneremail = $config[MailInOne_Settings][Mapping][Partneremail];
$partnername = $config[MailInOne_Settings][Mapping][Partnername];
$partneroffnungszeiten = $config[MailInOne_Settings][Mapping][Partneroffnungszeiten];
$partnerort = $config[MailInOne_Settings][Mapping][Partnerort];
$partnerplz = $config[MailInOne_Settings][Mapping][Partnerplz];
$partnerstrasse = $config[MailInOne_Settings][Mapping][Partnerstrasse];
$partnertelefon = $config[MailInOne_Settings][Mapping][Partnertelefon];

//Constants
$constts = $config[MailInOne_Settings][Constants][trafficsource];
$consttyp = $config[MailInOne_Settings][Constants][Typ];
$constsegment = $config[MailInOne_Settings][Constants][Segment];
$constquelle = $config[MailInOne_Settings][Constants][Quelle];

#Database
$dbname = $config[Database][DbName];
$dbuser = $config[Database][User];
$dbpass = $config[Database][Password];
$dbhost = $config[Database][Host];


#AssignmentManager
$assignmentstatus = $config[AssignmentManager][Status];
$defaultmanager = $config[AssignmentManager][Vertriebsmitarbeiter];

#GeneralSetting
$supportprefilling = ( $config[GeneralSetting][SupportPreFilling] == (true))? true : '';
$cutcopypaste = ($config[GeneralSetting][CutCopyandPaste] == (true))? true : '';
$updateexistingrecord = ($config[GeneralSetting][UpdateExistingRecord] == (true))? true : '';
$redirectstatus = $config[GeneralSetting][Redirect][RedirectStatus];
$redirectpath = $config[GeneralSetting][Redirect][RedirectPath];
$redirecttime = $config[GeneralSetting][Redirect][RedirectTime];
$successpage = $config[GeneralSetting][SuccessPage];
$keepurlparameters = ($config[GeneralSetting][KeepURLParameter] == (true))? true : '';

        
#Hubspot
$hubspotsync = ($config[HubSpotCredentials][Sync] == (true))? true : '';
$hubspotkey = $config[HubSpotCredentials][hubapi];
$hubspotid = $config[HubSpotCredentials][hubspot_owner_id];
$hubspotdealname = $config[HubSpotDeal][dealname];
$hubspotdealamount = $config[HubSpotDeal][amount];
$hubspotdealsegement = $config[HubSpotDeal][segment];
$hubspotdealtype = $config[HubSpotDeal][dealtype];
$hubspotdealstage = $config[HubSpotDeal][dealstage];
$hubspotdealclosedate = $config[HubSpotDeal][closedate];
$hubspotdealpipeline = $config[HubSpotDeal][pipeline];
$hubspotdealqualitaet = $config[HubSpotDeal][qualitaet];
        

#Mail-in-One
$maileonstatus = ( $config[MailInOne_Settings][Sync] == (true))? true : '';
$maileonkey = $config[MailInOne_Settings][Mapikey];
$maileonconfig = array(
    'BASE_URI' => 'https://api.maileon.com/1.0',
    'API_KEY' => $maileonkey,
    'THROW_EXCEPTION' => true,
    'TIMEOUT' => 60,
    'DEBUG' => 'false' // NEVER enable on production
);
$maileonconstant = $config[MailInOne_Settings][Constants];
$maileondoikey = $config[MailInOne_Settings][DOIKey];

#TackingTools
$facebookpixel = $config[TrackingTools][FacebookPixel];
$googleKey = $config[TrackingTools][GoogleAPIKey];
$googleAnalyticsKey = $config[TrackingTools][GoogleAnalytics];


#Postal code
$plzstatus = ($config[Postal_Code][Autofill] == (true))? true : '';
$plzerrcolor = $config[Postal_Code][ErrorMsg_Color];
$plzerr = $config[Postal_Code][ErrorMsg];


#Splittest
$splittestname = $config[Splittest][Name];
$splitteststatus = ($config[Splittest][RunSplittest]  == (true))? true : '';
$splittestvarients = $config[Splittest][NumberOfVariants];       


#Telefon
$telefonstatus = ($config[Telefon][Status] == (true))? true : '';
$telefonerrcolor = $config[Telefon][ErrorMsg_Color];
$telefonerr = $config[Telefon][ErrorMsg];


#Email Adress
$emailerrormsg = $config[Email_Address][ErrorMsg];
$emailmsgcolor = $config[Email_Address][ErrorMsg_Color];





<?php

class db {
    private $conn;
	private $host;
	private $user;
	private $password;
	private $baseName;
    private $query;
    
	function __construct() {
        $this->host = "localhost";
        $this->user = "db13026946-cio";
        $this->password  = "6gRjTSD5";
        $this->dbName = "db13026946-cio";
        $this->conn = false;
        $this->query = "";
        $this->connect();
	}
    
    private function connect(){
		if (!$this->conn) {
			$this->conn = mysqli_connect($this->host, $this->user, $this->password, $this->dbName);	
          
			if (!$this->conn) {
				$this->status_fatal = true;
				echo 'Connection BDD failed';
				die();
			} 
			else {
				$this->status_fatal = false;
			}
           
		}
		return $this->conn;        
    }
    
	public function disconnect() {
		if ($this->conn) {
			@pg_close($this->conn);
		}
	}
	
    private function getSelectQueryStatement($select=array(), $table, $where=array()){
        if(isset($select) && isset($table)){
            $select = (count($select) > 1)? implode(", ", $select) : implode($select);
            $where = (count($w) > 1)? implode(",", $where) : implode($where);            
            
            $this->query = "select ". $select . " from " . $table . " where ". $where;
            
        }else{
            $this->query = "";
            
        }
    }
    
	public function getOne($select, $table, $where) { // getOne function: when you need to select only 1 line in the database
        $cnx = $this->conn;
        
		if (!$cnx || $this->status_fatal) {
			echo 'GetOne -> Connection BDD failed';
			die();
		}
        
        $this->getSelectQueryStatement($select, $table, $where);
        $cur = mysqli_query($cnx, $this->query);
        
		if ($cur == FALSE) {		
			$errorMessage = mysqli_error($cnx); 
			$this->handleError($query, $errorMessage);
            
		} 
		else {
   			$this->Error=FALSE;
			$this->BadQuery="";
			$tmp = mysqli_fetch_array($cur, MYSQLI_ASSOC ); 
			$return = $tmp;
        }
		@mysqli_free_result($cur);
		return $return;
	}
	
	public function getAll($query) { // getAll function: when you need to select more than 1 line in the database
		$cnx = $this->conn;
		if (!$cnx || $this->status_fatal) {
			echo 'GetAll -> Connection BDD failed';
			die();
		}
		
		mysql_query("SET NAMES 'utf8'");
		$cur = mysqli_query($cnx, $query);
		$return = array();
		
		while($data = mysqli_fetch_assoc($cur)) { 
			array_push($return, $data);
		} 
 
		return $return;
	}
    
    public function getInsertQueryStatement($table, $value){
        $val = array();
        $col = array();
        if(isset($table) && isset($value)){
            
            foreach($value as $key => $value){
                array_push($col, $key);
                array_push($val, "'".$value."'");
            }
          
            $val = (count($val) > 1)? implode(", ", $val) : implode($val);
            $col = (count($col) > 1)? implode(", ", $col) : implode($col);
            $this->query = "INSERT INTO ".$table." (" . $col . ") VALUES (".$val.")";
           # var_dump($this->query);
        }
    }
    
    private function getUpdateQueryStatement($table ,$value ,$where){
        if(isset($table) && isset($value)){
            
            foreach ($value as $key => $value) {
                $val = $key ." = ". $value;
            }

            foreach($where as $key => $value){
                array_push($where, $key ." = ". $value);
            }            
            
            
            $this->query = "UPDATE ".$table. " SET " .$val. " WHERE " .$where;
        }else{
            $this->query = false;
        }
    }
	
	public function execute($table ,$value ,$where ,$operation ,$use_slave=false) { // execute function: to use INSERT or UPDATE
		$cnx = $this->conn;
       
		if (!$cnx||$this->status_fatal) {
			return null;
		}
        if($operation === "update"){
            $this->getUpdateQueryStatement($table ,$value ,$where);
            
        }elseif($operation === "insert"){
            $this->getInsertQueryStatement($table, $value);
            
        } 
		$cur = @mysqli_query($cnx, $this->query);
		if ($cur == FALSE) {
			$ErrorMessage = @mysqli_last_error($cnx);
			$this->handleError($query, $ErrorMessage);
		}
		else {
			$this->Error=FALSE;
			$this->BadQuery="";
			$this->NumRows = mysqli_affected_rows();
			return $cur;
		}
		@mysqli_free_result($cur);
	}
	
	function handleError($query, $str_erreur) {
		$this->Error = TRUE;
		$this->BadQuery = $query;
		if ($this->Debug) {
			echo "Query : ".$query."<br>";
			echo "Error : ".$str_erreur."<br>";
		}
	}    
}

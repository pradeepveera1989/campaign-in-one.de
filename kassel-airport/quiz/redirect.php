<?php
// Get the config data from config.ini       
    $config = parse_ini_file("config.ini", true);
	$currentUrl = parse_url($_SERVER['REQUEST_URI']);
    $redirect_path = $config[GeneralSetting][Redirect][RedirectPath];

    if($config[GeneralSetting][KeepURLParameter]){
        $redirect_path = $redirect_path . $currentUrl["query"];
    }
	$redirect_time = $config[GeneralSetting][Redirect][RedirectTime];
?>
<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">  
	<?php if($config[GeneralSetting][Redirect][RedirectStatus]){ ?>  
    <meta http-equiv="refresh" content="<?php echo $redirect_time; ?>;url=<?php echo $redirect_path; ?>" />  
	<?php } ?>
    <title>Kassel Airport</title>
    <!-- implementation bootstrap -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- implementation fontawesome icons -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
       <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- implementation simpleline icons -->
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <!-- implementation googlefonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <!-- implementation Animated Header -->
    <!-- implementation custom css -->
    <link href="css/creative.css" rel="stylesheet">
    <!-- implementation animate css -->
    <link href="css/animate.css" rel="stylesheet">
  </head>
    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '2069635113314986');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=2069635113314986&ev=PageView&noscript=1"
    /></noscript>
<!-- End Facebook Pixel Code -->
  <body>

        <section class="ksf-head">
                <center><img src="img/ksf-logo.svg" /></center>
        </section>
        
        
        
        
        
        <!-- MOBILE HEADER -->
        
        <div class="mobile-header">        
        <div class="container">
            <div class="row">

                <div class="col-12">
                    <center><img style="width:100%;" src="img/suitcase.png" alt="flughafen-kassel"/></center>
                </div>

            </div>
        </div>
        </div>        
        
        <!-- /. MOBILE HEADER -->
        
  
 <div class="container">
          
            
    <div class="row">
        <div class="col-sm-4">
            <div class="suitcase">
             <img src="img/suitcase.png" alt="flughafen-kassel"/>
            </div>
           
        </div>
        
        
       
        <div class="col-sm-2"></div>
   
        
        <div class="intro-redirect col-sm-6" >
            
            <h3 style="color: #FCDE1D;"><img style="width:35px;" src="img/dreieck.svg" />&nbsp;&nbsp;Vielen Dank!</h3>
            <p>&nbsp;</p>
            <h4>Ihre Anmeldung ist fast abgeschlossen.</h4>
          
            <p>Soeben haben Sie eine E-Mail von uns erhalten. Sollten Sie keine E-Mail erhalten haben, schauen Sie bitte auch in Ihrem Spamordner nach. Klicken Sie in dieser E-Mail bitte auf den angegebenen Link oder kopieren diesen in Ihren Browser, um Ihre Identität zu bestätigen.</p>

        </div>
    </div>
    </div>     
        
       
        
        
                 
            
<section class="ksf-foot-below"> 
       <div class="container">    
      <div class="row">
        <div class="col-sm-4">
            <img src="img/ksf-logo-white.svg" />
          </div>
        
    
        <div class="col-sm-8">
          <div class="legal">
            <center><a href="https://www.kassel-airport.aero/de/inhalte-metanavigation-seitenfuss/impressum" target="_blank">Impressum</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.kassel-airport.aero/de/inhalte-metanavigation-seitenfuss/datenschutz" target="_blank">Datenschutz</a></center>
           </div> 
          </div>
    </div>      
    </div>    
</section>      
 
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js">
    </script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js">
    </script>
    <script src="vendor/header-animation/demo-1.js">
    </script>  
    <script src="vendor/header-animation/TweenLite.min.js">
    </script>
    <script src="vendor/header-animation/EasePack.min.js">
    </script>
    <script src="vendor/header-animation/rAF.js">
    </script>
    <script src="vendor/header-animation/demo-1.js">
    </script>  

  </body>

</html>
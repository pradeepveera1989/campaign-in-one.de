<?php
require "cio.php";
?>

<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="JETZT TEILNEHMEN UND AB KASSEL AUF DIE INSEL">
        <meta name="author" content="Flughafen Kassel">

        <title>Kassel Airport | Jetzt gewinnen!</title>

        <link href="css/creative.css" rel="stylesheet" type="text/css">  
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">    
        <link rel="stylesheet" href="vendor/telefonvalidator-client/build/css/intlTelInput.css"/>

        <script type="text/javascript" src="vendor/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="vendor/telefonvalidator-client/build/js/utils.js"></script>
        <script type="text/javascript" src="vendor/telefonvalidator-client/build/js/intlTelInput.js"></script>
        <script type="text/javascript" src="vendor/bxslider/src/js/jquery.bxslider.js "></script>
        <script type="text/javascript" src="vendor/google/maps.js"></script>
        <script type="text/javascript" src="vendor/jquery-cookie-master/src/jquery.cookie.js"></script>
        <script type="text/javascript">
            var countdown_status = "<?= $counter_status; ?>";
            var countdown_expire = "<?= $config[GeneralSetting][CountdownExpire]; ?>";
            var countdown_expire_period = "<?= $config[GeneralSetting][MaximumCountdownperiod]; ?>";
            var current_stocks = "<?= $current_count_value; ?>";
            var countdown_end_date = "<?= $current_end_date ?>";
            var autofill_postal_code = "<?= $config[Postal_Code][Autofill]; ?>";
            var localization_postal_code = "<?= $config[Postal_Code][Localization]; ?>"
            var countdown_expire_message = "<?= $config[GeneralSetting][CountdownExpireMessage] ?>";
            var validate_telefon = "<?= $config[Telefon][Status]; ?>";
            var single_submit = "<?= $config[GeneralSetting][SingleSubmit]; ?>"
            var single_submit_text = "<?= $single_submit_text ?>"
            var cookie = "<?= $cookie; ?>";
            var webgains_cookie = "<?= $webgains_cookie ?>";
        </script>
        <script language="JavaScript" type="text/javascript" src="cio.js"></script>   
        <!-- Google Analytics -->
        <?php include_once 'clients/tracking/google/google.php'; ?>        

        <!-- Facebook Pixel -->
        <?php include_once('clients/tracking/facebook/facebook.php'); ?>      
    </head>

    <!-- Outbrain Tracking -->
    <?php $config[TrackingTools][EnableOutbrain] ? include_once('clients/tracking/outbrain/outbrain_index.php') : " "; ?>

    <body>
        <!-- Web Gains Tracking -->
        <?php $config[TrackingTools][EnableWebGains] ? include_once 'clients/tracking/webgains/webgains_index.php' : " "; ?>

        <!-- Remarketing Target360 Tracking -->
        <?php $config[TrackingTools][EnableTarget360] ? include_once 'clients/tracking/target360/target_index.php' : ' ';?>      

        
        
         <!-- MOBILE HEADER -->
        <div class="mobile-header">        
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <center><img style="width:50%;" src="img/ksf-logo.svg" alt="flughafen-kassel"/></center>
                </div>
            </div>
        </div>
        </div>        
       <!-- /.MOBILE HEADER -->
        
        <section class="ksf-header">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="ksf-head"><img src="img/Kassel_Airport_logo.svg" /></div>
                    </div>
                </div>
            </div>
        </section>
        
        <section class="ksf-header-background">
                <div><img src="img/header.jpg" /></div>   
        </section>
        
         <div class="container">
                <div class="row">
 
        
        <div class="intro col-sm-12">
            
            <h2 style="padding:50px 0px 42px 0px;">Gewinnen Sie 1x2 Flugtickets nach Sylt und weitere tolle Preise!</h2>
          
            <p style="padding-bottom: 30px;">Machen Sie sich bereit für die Insel. Ab Juni geht es nämlich mit Rhein-Neckar Air von Kassel in unter 60 Minuten direkt nach Westerland – und Sie können dabei sein! **</p>
            
            <h3 style="padding-bottom: 30px;" >Wie das geht? Ganz einfach:</h3>
            <h3 style="padding-bottom: 10px;">1. Namen, Anschrift und E-Mail-Adresse eintragen</h3>
            <h3 style="padding-bottom: 10px;">2. Häkchen setzen und auf „Jetzt teilnehmen und ab Kassel auf die Insel“ klicken</h3>
            <h3 style="padding-bottom: 30px;" >3. Mit etwas Glück 1x2 Flugtickets nach Sylt oder einen anderen tollen Preis erhalten.</h3>
            
            <div style="max-width:900px;"><img style="width:100%;" src="img/platzhalter.jpg" /></div>
            
            <p style="padding-top: 30px; padding-bottom: 30px;">Neben Flugtickets haben Sie auch die Chance auf eines von zehn personalisierten Handtüchern oder einen von zehn persönlich gebrandeten Liegestühlen. So zieht der Urlaub auch bei Ihnen zu Hause ein!</p>
            
            <h3 style="padding-bottom: 30px;" >Nur noch Daten eingeben und mit etwas Glück Flugtickets gewinnen!</h3>
        
            <form id="api-data-form" action="<?= $_SERVER['PHP_SELF'] ?>" method="post" accept-charset="utf-8" class="form-horizontal" role="form">
                            <input type="hidden" name="contactid" value="<?= $contactid ?>">
                            <input type="hidden" name="checksum" value="<?= $checksum ?>">
                            <input type="hidden" name="mailingid" value="<?= $mailingid ?>">

                            <!-- INPUT ANREDE FRAU & HERR -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="btn-group">
                                        <?php
                                        if ($_SESSION['data']['anrede'] === "Frau") {
                                            $frau = "checked";
                                        } else {
                                            $herr = "checked";
                                        }
                                        ?>										
                                        <label class="btn btn-outline-secondary" style="border: 1px solid transparent;">
                                            <input value="Frau" style="width: 15px; height: 15px;" type="radio" name="<?php echo $config[MailInOne][Mapping][SALUTATION]; ?>" autocomplete="off" required <?php echo $frau; ?>> Frau
                                        </label>
                                        <span class="input-group-btn" style="width:15px;"></span> 
                                        <label class="btn btn-outline-secondary" style="border: 1px solid transparent;">
                                            <input value="Herr" style="width: 15px; height: 15px;" type="radio" name="<?php echo $config[MailInOne][Mapping][SALUTATION]; ?>" autocomplete="off" <?php echo $herr ?>> 
                                            Herr
                                        </label>
                                    </div>  
                                </div>       
                            </div>

                    <div class="row">
                            <!-- INPUT VORNAME -->
                           <div class="col-sm-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">

                                        <input value="<?php echo ((!empty($standard_001) ? $standard_001 : $_SESSION['data'][$config[MailInOne][Mapping][FIRSTNAME]])); ?>" class="ksf-input input-fields form-control" id="" name="<?php echo $config[MailInOne][Mapping][FIRSTNAME]; ?>" placeholder="*Vorname" type="text" required 
                                               <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>      
                                    </div>
                                </div>       
                            </div>
                            </div>

                            <!-- INPUT NACHNAME -->
                            
                         <div class="col-sm-4">            
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">

                                        <input  value="<?php echo ((!empty($standard_002) ? $standard_002 : $_SESSION['data'][$config[MailInOne][Mapping][LASTNAME]])); ?>" class="ksf-input input-fields form-control" id="nachname" name="<?php echo $config[MailInOne][Mapping][LASTNAME]; ?>" placeholder="*Nachname" type="text" required
                                                <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                    </div>
                                </div>       
                            </div>
                         </div>
                         
                            <div class="col-sm-4">
                            <!-- INPUT EMAIL -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <input value="<?php echo ((!empty($email_002) ? $email_002 : $_SESSION['data'][$config[MailInOne][Mapping][EMAIL]])); ?>" class="ksf-input input-fields form-control email" id="email" name="<?php echo $config[MailInOne][Mapping][EMAIL]; ?>" placeholder="*E-Mail-Adresse" type="email" required 
                                               <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                    </div>
                                    <?php if (($_SESSION['error'] === "email") && !empty($_SESSION['error_msg'])) { ?>   
                                        <span class="p-light erroremail"  style="float: left; padding-bottom:15px; color:<?php echo $config[ErrorHandling][ErrorMsg_Color]; ?>"><?php echo $_SESSION['error_msg']; ?></span>
                                    <?php } ?>  										
                                </div>       
                            </div>
                         </div>
                    </div>
                            <!--Hidden Fields-->   
                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">URL</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][url]; ?>" value="<?php echo $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>">
                                </div>
                            </div>                                        

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Source</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_source]; ?>" value="<?php echo $_GET['utm_source'] ?>" >
                                </div>
                            </div>  

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Name</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_name]; ?>" value="<?php echo $_GET['utm_name'] ?>">
                                </div>
                            </div>      

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Term</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_term]; ?>" value="<?php echo $_GET['utm_term'] ?>">
                                </div>
                            </div>                             

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Content</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_content]; ?>" value="<?php echo $_GET['utm_content'] ?>">
                                </div>
                            </div>                              

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Medium</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_medium]; ?>" value="<?php echo $_GET['utm_medium'] ?>">
                                </div>
                            </div>    

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Campaign</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_campaign]; ?>" value="<?php echo $_GET['utm_campaign'] ?>">
                                </div>
                            </div>                                               

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">TRAFFICSOURCE</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][trafficsource]; ?>" id="ts" value="<?php echo $_GET['trafficsource'] ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Quelle</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][Quelle]; ?>" id="quelle" value="<?php echo $config[MailInOne][Constants][Quelle]; ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Typ</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" ame="<?php echo $config[MailInOne][Mapping][Typ]; ?>" id="" value="<?php echo $config[MailInOne][Constants][Typ]; ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Segment</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" ame="<?php echo $config[MailInOne][Mapping][Segment]; ?>" id="" value="<?php echo $config[MailInOne][Constants][Segment]; ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Test Mode</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="test_mode" id="" value="<?php echo $_GET['testmode']; ?>">
                                </div>
                            </div>
            
                             <label class="form-group">
                                <div style="padding-top: 30px;" class="col-sm-12 einwilligung">
                                    <input class="checkbox" style="width: 20px; height: 20px;" type="checkbox" required>
                                    
                                    <span class="p-dark">&nbsp;&nbsp;Ja, senden Sie mir gratis die "Airport-News" mit spannenden Last-Minute-Angeboten, Events am Kassel Airport und tollen Gewinn-Aktionen<br> per E-Mail.</span><br><br>
                                    
                                    <span class="p-dark-small"><small>Ich bin einverstanden, dass die Flughafen GmbH Kassel, Fieseler-Storch-Straße 40, 34379 Calden, meine eingetragenen Daten nutzt und mir E-Mails mit Infos zum Flughafen, Aktionen und Reiseangeboten schickt. Diese Einwilligung kann ich jederzeit widerrufen, etwa durch einen Brief an die oben genannte Adresse oder durch eine E-Mail an <a style="color:#069CD7;" href="maito:widerruf@kassel-airport.aero">widerruf@kassel-airport.aero</a>. Anschließend wird jede werbliche Nutzung unterbleiben.**</small></span>
                                </div>    
                            </label>   

                          
                             <div class="form-group"> 
                                <div class="col-sm-12 btn-1">
                                    <input style="" type="submit" class="ksf-btn col-sm-12 btn btn-xl js-scroll-trigger" value="JETZT TEILNEHMEN UND AB KASSEL AUF DIE INSEL" name="submit" style="width: 100%;">
                                </div>        
                            </div>
                            <p>&nbsp;</p>

                            <div class="form-group"> 
                                <div class="col-sm-12 star">
                                    <span class="p-dark-small"><small>Laufzeit der Verlosung: 01.03.2019 bis 30.04.2019. Reisezeit: Die Tickets sind für Flüge im Reisezeitraum vom 08.06.2019 bis 31.08.2019 gültig. 
                                        
                                    <br><br>**Die Online-Registrierung ist nur bei Abgabe einer Werbeeinwilligung möglich. Sie können aber auch ohne Werbeeinwilligung teilnehmen, wenn Sie uns innerhalb der Teilnahmefrist eine E-Mail an <a style="color:#069CD7;" href="mailto:widerruf@kassel-airport.aero">widerruf@kassel-airport.aero</a> senden und in der E-Mail Ihren Namen, Ihre Telefonnummer und den Hauptpreis des Gewinnspiels angeben. Ihre Gewinnchancen werden durch die Art der Teilnahme nicht beeinflusst.</small></span>
                                </div>        
                            </div>    
            
            
                            <?php if (isset($response) && $response->isSuccess()) { ?>
                                <div class="alert alert-success fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Subscription successful</strong>
                                </div>
                            <?php } elseif (isset($warning)) { ?>
                                <div class="alert alert-warning fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong style="color: red; z-index: 1000;">Subscription failed</strong>
                                    <?= $warning['message'] ?>
                                </div>
                            <?php } elseif (isset($response)) { ?>
                                <div class="alert alert-danger fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Subscription failed</strong>
                                </div>
                            <?php } ?>
                        </form> 
        
        
      </div>
    </div>
    </div>     
        
      
        <section class="ksf-teilnahmebedingungen">
               
       <div class="container">        
        <div class="row">
            <div class="col-sm-12">
           
                
<div class="accordion ksf-accordion" id="accordionExample"  data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
  <div class="">
    <div class="" id="headingOne">
        
           <span style="font-size:20px; color:#919CB0">Teilnahmebedingungen</span><img style="padding-top:10px; float:right; width:6px;" src="img/left.svg" />
    
    </div>

    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="card-body">
        <span style="color:#919CB0; font-weight:400;">
            
        1. Veranstalter des Gewinnspiels ist der Kasseler Airport, Fieseler-Storch-Straße, 34379 Calden.<br><br>
        
        2. An dem Gewinnspiel nimmt ein Nutzer teil, indem er sich bis zum angegebenen Teilnahmeschluss über die Gewinnspielseite registriert und dabei seine Teilnahmedaten (Name, Vorname, E-Mail-Adresse) angibt und an der Gewinnspiel 2018 teilgenommen hat und eine Einwilligung zur E-Mail-Werbung und zur werblichen Nutzer seiner Daten (im Folgenden: Werbeeinwilligung) erklärt. Alternativ ist auch eine Teilnahme ohne eine Werbeeinwilligung möglich, wenn ein Nutzer innerhalb der Teilnahmefrist eine E-Mail an die E-Mail-Adresse <a href="mailto:widerruf@kassel-airport.aero">widerruf@kassel-airport.aero</a> unter Angabe seines Namens und des Hauptpreises des Gewinnspiels sendet. Die Form der Teilnahme (also Online-Registrierung mit Werbeeinwilligung oder E-Mail-Registrierung ohne Werbeeinwilligung) hat auf die Gewinnchancen keinen Einfluss. <br><br>
          
        3. Jeder Teilnehmer kann nur einmal an einem Gewinnspiel teilnehmen. Mitarbeitern des Kasseler Airports und ihren unmittelbaren Angehörigen (Eltern, Kinder, Ehepartner, Geschwister) ist eine Teilnahme untersagt. Zudem ist eine Teilnahme unter Angabe einer falschen E-Mail-Adresse unzulässig.<br> <br>
          
        4. Der Gewinner wird per Verlosung unter allen zugelassenen Teilnehmern (vgl. Ziff. 3) wöchentlich ermittelt. <br><br>
          
        5. Der oder die Gewinner werden per E-Mail über den Gewinn informiert. In diesem Zusammenhang werden Sie aufgefordert, ihren vollen Namen zu benennen. Bei einer Gewinnmitteilung per E-Mail wird der Gewinner aufgefordert, innerhalb von zwei Wochen seine Postadresse zu benennen. Reagiert der Gewinner darauf nicht, gilt die Gewinnmitteilung per E-Mail als gescheitert und der Gewinnanspruch verfällt. <br><br>
          
        6. Stellt sich nach der Gewinnermittlung heraus, dass ein Nutzer unter Verstoß gegen Ziff. 3 teilgenommen hat, kann die Gewinnermittlung wiederholt werden. <br><br>
          
        7. Sofern in den Gewinnspielinformationen nichts anderes mitgeteilt wird, wird ein Gewinn auf Kosten des Kasseler Airports an den Gewinnspielteilnehmer verschickt. <br><br>
          
        8. Es gilt deutsches Recht.
        </span>
      </div>
    </div>
  </div>
  </div>               
                
             </div>
        </div>
       </div>      
        </section>
          <p>&nbsp;</p>       





<section class="ksf-foot"> 
<div class="container">
    <div class="legal">
    <div class="row">
    
        <div class="col-sm-2"><a href="https://www.kassel-airport.aero/" target="_blank"><span style="color:#919CB0;"> &lt; Zur Webseite</span></a></div>
        <div class="col-sm-2"><a href="https://www.kassel-airport.aero/de/fluege-urlaubsangebote/reiseangebote" target="_blank">Unsere Angebote</a></div>
        <div class="col-sm-2"><a href="https://www.kassel-airport.aero/de/inhalte-metanavigation-seitenfuss/impressum" target="_blank">Impressum</a></div>
        <div class="col-sm-2"><a href="https://www.kassel-airport.aero/de/inhalte-metanavigation-seitenfuss/datenschutz" target="_blank">Datenschutz</a></div>
        <div class="col-sm-2"><a href="https://www.kassel-airport.aero/de/inhalte-metanavigation-seitenfuss/datenschutz" target="_blank">AGB</a></div>
        <div class="col-sm-2"></div>
    </div>
    </div>
</div>
 </section>  
                 



        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="vendor/telefonvalidator-client/build/js/utils.js"></script>		
        <script type="text/javascript" src="vendor/telefonvalidator-client/build/js/intlTelInput.js"></script>	
        <script src="vendor/google/maps.js"></script>  
        <script type="text/javascript" src="vendor/jquery-cookie-master/src/jquery.cookie.js"></script>  


    </body>
</html>        

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of assignmentManager
 *
 * @author Pradeep
 */

#require '../config.php';

class assignmentManager {
    
    var $assignmentmanager;
    var $assignmentmanagerid;
    var $verbindung;
    var $sqlQuery;
    var $result;
        
    function __construct(){
        
        $config = new config();
        $config_db = $config->getConfigDatabase();
        $this->host = $config_db[Host];
        $this->user = $config_db[User];
        $this->password  = $config_db[Password];
        $this->dbName = $config_db[DbName]; 
        $this->connect();        
    }
    
    private function connect(){
        try{  
            if (!$this->conn) {
                $this->conn = mysqli_connect($this->host, $this->user, $this->password, $this->dbName);	

                if (!$this->conn) {
                    throw new Exception("Assignment Manager: Could not establish database connection" .PHP_EOL);   
                    die();
                }
            }
        }catch (Exception $e) {
            echo $e->getMessage();
        }        
        
		return $this->conn;        
    }
    
    private function excuteQuery(){
        try{
            
            if(!isset($this->query)){
                throw new Exception("Assignment Manager: Invalid SQL query" .PHP_EOL);   
            }
            $res = $this->conn->query($this->query);
            $this->result = mysqli_fetch_array($res, MYSQLI_ASSOC );     
            
        }catch (Exception $e) {
            
            echo $e->getMessage();
        }
    }
    
    public function getAssignmentManager($postalCode, $defaultmanager ){
        
        try{    
            
            if(!isset($postalCode)){
                throw new Exception("Assignment Manager: Invalid postal code" .PHP_EOL);   
            }
            $this->query = "SELECT `AssigneValue` FROM `AssignmentManager` WHERE `PostalCodeFrom` <= '" . $postalCode . "' AND `PostalCodeTo` >= '" . $postalCode . "' LIMIT 0, 30 ";
          
            $this->excuteQuery();
            
            if(!isset($this->result)){  
                
                 # Default value for Vertriebsmitarbeiter
                $this->assignmentmanager = $defaultmanager;
            } else {
                $row = $this->result;
                $this->assignmentmanager = $row["AssigneValue"];
            }
            
        }catch (Exception $e) {
            echo $e->getMessage();
        }            
        return $this->assignmentmanager;        
    }
    
    
    
    public function getAssignmentManagerId(){
        
        try {
            
            if(!isset($this->assignmentmanager)){
                
                throw new Exception("Assignment Manager: Invalid Assignment Manager" .PHP_EOL);  
            }
            $this->query = "SELECT `HubspotId` FROM `AssignmentManagerInfo` WHERE `AssignmentManager` LIKE '" . $this->assignmentmanager . "'";
            $this->excuteQuery();
            if(!isset($this->result)){  
                
                throw new Exception("Assignment Manager: Invalid SQL query" .PHP_EOL);  
            }
            $row = $this->result;
            $this->assignmentmanagerid = $row["HubspotId"];
            
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        
        return  $this->assignmentmanagerid;     
    }
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of autoload
 *
 * @author Pradeep
 */

//

class Autoload
{
    public static $path = "../clients";
    
    public static function hubspotLoader($class)
    {
        require '../clients/hubspot/'.$class.'.php';
    }


    public static function maileonLoader($className)
    {
        require '../clients/maileon/'.$class.'.php';        
    }


    public static function databaseLoader($className)
    {
        require '../clients/database/'.$class.'.php';               
    }    
    
}

//spl_autoload_register('Autoload::hubspotLoader');
//spl_autoload_register('Autoload::maileonLoader');
//spl_autoload_register('Autoload::databaseLoader');
//spl_autoload_register('Autoload::DatabaseLoader');
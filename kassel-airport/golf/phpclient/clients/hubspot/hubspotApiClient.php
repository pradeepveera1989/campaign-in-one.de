<?php

/*
* hubspotApiClient.php
*
* HubspotApiClient does the following.
*   1. Creates a Contact in Hubspot
*   2. Creates a Deal in Hubspot
*   3. Creates a Company in Hubspot
*   
*/

class hubspotApiClient {
    public $contact_id;
    public $company_id;
    public $deal_id;
    public $key;
    public $status;
    
	function __construct($key) {  
        
        $config = new config();
        $this->config_hubspot = $config->getConfigHubspot();
        $this->config_mailinone = $config->getConfigMailInOne();
        $this->key = $this->config_hubspot[hubapi];  
	} 

  /**
  * @Function  validateHubspotKey.
  *
  * @Description Validates the Hubspot key by trying to create an object using the Key.
  *
  * @return void
  * 
  */      
    private function validateHubspotKey($key){
        try{
            $hb = new HubSpot_Deal($key);    
            $res = $hb->get_recent_deals()->results;
            if(is_null($res)){
                throw new Exception("Hubspot : Invalid Hubspot Key".PHP_EOL);          
            }              
        } catch (Exception $e) {
            $this->errorKey = true;
            echo $e->getMessage();
        }
    }    


  /**
  * @Function  getContactParams.
  *
  * @Description Maps the contact Parameters required to create a contact.
  *
  * @return array of contact parameters.
  * 
  */      
    private function getContactParams($params) {
        
        $return = false;
        $hb_params = $this->config_mailinone[Mapping];
        
        if (empty($hb_params[EMAIL])) {
            
            return $return;
        }       

        $return = array(
            'firstname' => $params[$hb_params[FIRSTNAME]],
            'lastname' => $params[$hb_params[LASTNAME]],
            'zip' => $params[$hb_params[ZIP]],
            'city' => $params[$hb_params[CITY]],
            'email' => $params[$hb_params[EMAIL]],
            'phone' => $params[$hb_params[Telefon]],
            'hubspot_owner_id' => $params[$this->config_hubspot[hubspot_owner_id]],
        );
        return $return;
    }

      
  /**
  * @Function  getCompanyParams.
  *
  * @Description Maps the Company Parameters required to create a Company.
  *
  * @return array of Company parameters.
  * 
  */  
    private function getCompanyParams() {
        
        $return = false;
        $hb_params = $this->config_hubspot[Company];
        
        if(empty($hb_params['name']) || empty($hb_params['domain'])){
            
           return $return;
        }
        $return = array(
            'name' => $hb_params['name'],
            'domain' => $hb_params['domain'],
            'description' => $hb_params['description'],
            'phone' => $hb_params['phone'],
            'zip' => $hb_params['zip'],
        );
        return $return; 
    }

  /**
  * @Function  getCompanySearchParams.
  *
  * @Description Maps the Company search Parameters required to search the existence of company.
  *
  * @return array of Company parameters.
  * 
  */     
    private function getCompanySearchParams() {
        
        return array(
            'limit' => 5,
            'requestOptions' => array(
                'properties' => array(
                    0 => 'domain',
                    1 => 'createdate',
                    1 => 'name',
                    2 => 'hs_lastmodifieddate',
                ),
            ),
            'offset' => array(
                'isPrimary' => true,
                'companyId' => 0,
            ),
        );
    }

  /**
  * @Function  getDealParams.
  *
  * @Description Maps the Deal Parameters required to create a deal.
  *
  * @return array of deal parameters.
  * 
  */    
    private function getDealParams($param) {
        
        $return = false;
        $hb_deal = $this->config_hubspot[Deal];
        $hb = $this->config_mailinone[Mapping];
        
        
        if(empty($hb_deal[name])){
            
           return $return;
        }
        
        $dealname = $hb_params[name] .' '. $param[$hb[FIRSTNAME]] .' '. $param[$hb[LASTNAME]] .' '. $param[$hb[CITY]] .' '.  date("Y-m-d");
        
        $return = array(
            "dealname" => $hb_deal[name],
            "amount" => $hb_deal[amount],
            "segment" => $hb_deal[segment],
            "dealstage" => $hb_deal[dealstage],
            "pipeline" => $hb_deal[pipeline],
            "qualitaet" => $hb_deal[qualitaet],
            "hubspot_owner_id" => $param[$hb[Vertriebsmitarbeiterid]],
        );
        
        return $return;
    }

  /**
  * @Function  getDealAssociations.
  *
  * @Description Maps the Deal Association Parameters required to create a deal.
  *
  * @return array of deal associations parameters.
  * 
  */        
    private function getDealAssociations() {
        $return = false;
        
        if(!isset($this->contact_id)){
            return false;
        }
        
        $return = array(
            "associations" => array(
                "associatedVids" => array(
                    0 => $this->contact_id
                ),
            ),
        );
        return $return;
    }

  /**
  * @Function  getCompanyDomain.
  *
  * @Description get the domain of the email addresse.
  *
  * @return domain name of email.
  * 
  */ 
    private function getCompanyDomain($param){
        
        return substr(strrchr($param['email'], "@"), 1);
    }


  /**
  * @Function  createContact.
  *
  * @Description Creates the contact in Hubspot
  *
  * @return contact id of Hubspot.
  * 
  */     
    public function createContact($params) {
        
        $contact_parms = $this->getContactParams($params);
        try{
            if(!$contact_parms){
                
                throw new Exception("Hubspot : Invalid Contact Parameters" .PHP_EOL);     
            }
            $contact = new HubSpot_Contacts($this->key);
            $contact_exists = $contact->get_contact_by_email($contact_parms["email"]);    

            if (isset($contact_exists->vid)) {
                
                $this->contact_id = $contact_exists->vid;                                        //  Contact exist in Hubspot
                $contact->update_contact($this->contact_id, $contact_parms);                     //  Does not return anything on update

            } else {
                
                $this->contact_id = $contact->create_contact($contact_parms);                   // Create a new contact on Hubspot
                $this->contact_id = $this->contact_id->vid;
            }
            
            if (($this->contact_id->status === "error") || ($updated_contact->status === "error")) {
                
                throw new Exception("Hubspot Contact: ". $this->message .PHP_EOL);     
            }            
                
            
        } catch (Exception $e) {
         
            echo $e->getMessage();
        }
        
        return $this->contact_id; 
    }

  /**
  * @Function  createCompany.
  *
  * @Description Creates the Company in Hubspot
  *
  * @return company id of Hubspot.
  * 
  */      
    public function createCompany($params) {
        
        $company_parms = $this->getCompanyParams($params);
        try{
            if(!$company_parms){
                throw new Exception("Hubspot : Invalid Company Parameters".PHP_EOL);     
            }

            $company = new HubSpot_Company($this->key);
            $company_exits = $company->get_company_by_domain($params["domain"], $this->getCompanySearchParams());

            if (count($company_exits->results) == 0) {
                $this->company_id = $company->create_company($company_parms);   // Company doesnot exists in Hubspot

                if ($this->company_id->status === "error") {
                    throw new Exception("Hubspot : ". $this->message .PHP_EOL);     
                }

                $this->company_id = $this->company_id->companyId;                // Company is created
            } else {

                foreach ($company_exits->results as $domain) {                    // Company exits in Hubspot
                    $this->company_id = $domain->companyId;
                }
            }     
            return $this->company_id;
            
        } catch (Exception $e) {
            
           throw new Exception("Hubspot Company: ". $this->message .PHP_EOL);  
        }        
    }

  /**
  * @Function  createDeal.
  *
  * @Description Creates the Deal in Hubspot
  *
  * @return deal id of Hubspot.
  * 
  */        
    public function createDeal($params) {
        
        $deal_parms = $this->getDealParams($params);
        try{
            
            if(!$deal_parms){
                
                throw new Exception("Hubspot : Invalid Deal Parameters".PHP_EOL);     
            }            
            $deal = new HubSpot_Deal($this->key);
            $recentDeals = $deal->get_recent_deals()->results;               // Get all the recent Deals
            foreach ($recentDeals as $deals) {
                
                if ($deals->properties->dealname->value === $deal_parms['dealname']) {
                    
                    $this->deal_id = $deals->dealId;
                }
            }    
            if (!isset($this->deal_id)) {               
                $this->deal_id = $deal->create_deal($this->getDealAssociations(), $deal_parms);   // Associate contact and company with new Deal
                if($this->deal_id->status === "error"){
                     throw new Exception("Hubspot :" . $this->message.PHP_EOL);     
                }
                $this->deal_id = json_decode($this->deal_id)->dealId;
            }         
            
        } catch (Exception $e) {
            
            throw new Exception("Hubspot Deal: ". $this->message .PHP_EOL);  
        }

        return $this->deal_id;
    }
    
  /**
  * @Function  associateDealWithContact.
  *
  * @Description Associate Deal with Contact in hubspot
  *
  * @return void.
  * 
  */     
    public function associateDealWithContact($params){
        
        $hb = $this->config_mailinone[Mapping];
        
        try {
            
            if(!isset($params[$hb[HubspotContactID]]) || !isset($params[$hb[HubspotDealID]])){
                
                throw new Exception("Hubspot : Invalid Deal Id or Contact Id ".PHP_EOL);     
            }
            $ass = new HubSpot_Deal($this->key);
            $ass->associate_deal_with_contact($params[$hb[HubspotContactID]], $params[$hb[HubspotDealID]]);
        } catch (Exception $e) {
            
               echo $e->getMessage();
        }
    }

    public function handleError($error_msg) {
		$this->Error = TRUE;
		$this->message = $error_msg;
        throw new Exception("Hubspot error: ". $this->message .PHP_EOL);
        return false;
	} 

}


?>
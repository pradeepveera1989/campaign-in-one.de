<?php

/*
* maileonApiClient.php
*
* MailonApiClient used to create contact service or transactions.
*   1. Inserts or updates a contact data
*   2. Creates a contact Event.
*   
*/
#require '../../vendor/autoload.php';
class maileonApiClient{
    public $mailinone;
    public $doi;
    public $config;
    public $custom_params;
    public $stnd_params;
    public $hubspot_contact_id;
    public $email;
    
       
    
	function __construct() {     
        
        $config = new config();
        $this->config_mailinone = $config->getConfigMailInOne();
        $this->getConfigParams();
	} 

 /**
  * @Function  getConfigParams.
  *
  * @Description create config parameter required for Maileon
  *
  * @return void
  * 
  */    
     
    private function getConfigParams(){
        
        try {

            if(empty($this->config_mailinone)){

                throw new Exception("Could not Load the Mailinone parameters");
            }       
            
            $this->mailinone = [
                'BASE_URI' => 'https://api.maileon.com/1.0',
                'API_KEY' => $this->config_mailinone["Mapikey"],
                'THROW_EXCEPTION' => true,
                'TIMEOUT' => 60,
                'DEBUG' => 'false' // NEVER enable on production                
            ];
            $this->doi = $this->config_mailinone["DOIKey"];
                        
        } catch (Exception $e) {
            
            $e->getMessage();
        }
    }

 /**
  * @Function  createContactService.
  *
  * @Description Initiate Contact Service to insert or update a contact in Maileon
  *
  * @return void
  * 
  */       
    private function createContactService() {

        try {
            if(count($this->mailinone) == 0){
                throw new Exception("Maileon: Invalid Config Parameters ".PHP_EOL);     
            }

            $this->contact_service = new com_maileon_api_contacts_ContactsService($this->mailinone);
            com_maileon_api_contacts_Permission::init();
            com_maileon_api_contacts_SynchronizationMode::init();
            $this->contact_service->setDebug(false);
            $newContact = new com_maileon_api_contacts_Contact();
            $newContact->email = $this->email;
            $newContact->anonymous = false;
            $newContact->permission = com_maileon_api_contacts_Permission::$DOI_PLUS;
            
            foreach ($this->stnd_params as $key => $value){
                $newContact->standard_fields[$key] = $value;
            }
            
            foreach ($this->custom_params as $key => $value){
                $newContact->custom_fields[$key] = $value;
            }    
            
            foreach ($this->constant_params as $key => $value){
                $newContact->custom_fields[$key] = $value;
            }    
            
            $this->response = $this->contact_service->createContact($newContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, '', '', true, true, $this->doi);   
              
            if(!($this->response->isSuccess())){
                
               $this->response = false;
               throw new Exception("Maileon: Contact could not be created or updated ".PHP_EOL);     
            }
            
        } catch (Exception $e) {
             echo $e->getMessage(); 
        }       
    }
    


    
  /**
  * @Function  createTransaction.
  *
  * @Description Verifies the config and throw an exception if the config is missing.
  *              Initiates the transaction service with respective email adress.
  *
  * @return void
  * 
  */      
    
    private function createTransaction(){
        
        try {
            if(count($this->config) == 0){
                throw new Exception("Maileon: Invalid Config Parameters ".PHP_EOL);     
            }
            
            $transactionsService = new com_maileon_api_transactions_TransactionsService($this->mailinone);   
            $transactionsService->setDebug(FALSE);
            $transaction = new com_maileon_api_transactions_Transaction();
            $transaction->contact = new com_maileon_api_transactions_ContactReference($this->contact_service);
            $transaction->type = $this->type;
            $transaction->contact->email = $this->email;
            $transactions = array($transaction);
            foreach ($this->event_params as $key => $value){
                   $transaction->content[$key] = $value;
            }
            
            $response = $transactionsService->createTransactions($transactions, true, false);

        } catch (Exception $e) {
            echo $e->getMessage(); 
        }
    }

  /**
  * @Function  getContactStatus.
  *
  * @Description Verifies the Maileon where the contact is present or
  *              not based on Email address of the contact.
  *
  * @return contact Id on success
  */      
        
    private function getContactStatus($email){
        $return = false;
        
        $contact_service = new com_maileon_api_contacts_ContactsService($this->mailinone);
        $contact_service->setDebug(false);
        $getContact = $contact_service->getContactByEmail($email);
        if(!($getContact->getResult()->id)){
            return $return;
        }
        $return = $getContact->getResult()->id;
        return $return;
    }
  
    
    
  private function getEventType($params){
      
      return $params['event_type'];
  }  
  /**
  * @Function  createEventPartnerLead.
  *
  * @Description Intiate the contact event for the Partner Lead.
  *              1. Inserts the contact in the system if the contact is missing.
  *              2. Intiate the tranaction service with Partner contact Email adress 
  *            
  * @return array of parameter
  * 
  */        
    public function createEvent($params){

        $this->type = $this->getEventType($params); 
            
        try{
            
            // Contact needs to be updated all the time.
            $this->insertContact($params);
            
            if(!$this->response->isSuccess()){
                
                return false;
            }
            
            $this->createTransaction();

            
            
        } catch (Exception $e) {
            echo $e->getMessage(); 
        } 
        
        return true;
    }    

  
  /**
  * @Function  getStandardParams.
  *
  * @Description Maps the Standard parameters required to create a contact in Maileon
  *            
  * @return array of parameter
  * 
  */        
    public function getStandardParams($param){
        
        $return = false;
        
        $stand_parms = $this->config_mailinone[Mapping];
        
        if(empty($param[$stand_parms[EMAIL]])){
            return $return;
        }
        $this->email = $param[$stand_parms[EMAIL]];
      
        return array(
            "TITLE" => $param[$stand_parms[TITLE]],
            "SALUTATION" => $param[$stand_parms[SALUTATION]],
            "FIRSTNAME" => $param[$stand_parms[FIRSTNAME]],
            "LASTNAME" => $param[$stand_parms[LASTNAME]],
            "ADDRESS" => $param[$stand_parms[ADDRESS]],
            "ZIP" => $param[$stand_parms[ZIP]],
            "CITY" => $param[$stand_parms[CITY]]
        );

    }


  /**
  * @Function  getCustomParams.
  *
  * @Description Maps the Custom  parameters required to create a contact in Maileon
  *            
  * @return array of parameter
  * 
  */      
    public function getCustomParams($custom) {

        $custom_parms = $this->config_mailinone[Mapping];     
        return array(
            "Vertriebsmitarbeiter" => $custom[$custom_parms[Vertriebsmitarbeiter]],
            "Telefon" => $custom[$custom_parms[Telefon]],
            "ip_adresse" => $custom[$custom_parms[ip_adresse]],
            "Datum" => $custom[$custom_parms[Datum]],
            "url" => $custom[$custom_parms[url]],
            "trafficsource" => $custom[$custom_parms[trafficsource]],
            "Typ" => $custom[$custom_parms[Typ]],
            "Segment" => $custom[$custom_parms[Segment]],
            "Quelle" => $custom[$custom_parms[Quelle]],
            "HubspotContactID" => $custom[$custom_parms[HubspotContactID]],
            "HubspotDealID" => $custom[$custom_parms[HubspotDealID]],
            "quiz_freiesparken" => $custom[$custom_parms[quiz_freiesparken]],
            "quiz_reservierung_liegen" => $custom[$custom_parms[quiz_reservierung_liegen]],
            "quiz_schnelle_anreise" => $custom[$custom_parms[quiz_schnelle_anreise]],
            "quiz_sushi_vom_band" => $custom[$custom_parms[quiz_sushi_vom_band]],
            "quiz_flugzeug_parken" => $custom[$custom_parms[quiz_flugzeug_parken]],
            "quiz_gratis_sportgepaeck" => $custom[$custom_parms[quiz_gratis_sportgepaeck]]
            
        );
    }
    
    public function getConstantParams(){
        
        $constant_params = $this->config_mailinone[Constants];
        return $constant_params;
    }
    

  /**
  * @Function  insertContact.
  *
  * @Description inserts the contact data to the by creating contact service.
  *              Maps the Standard and Custom parameters of the contact. 
  *              Intiates the contact service only if there are standard parameters.
  *            
  * @return response data of contact service.
  * 
  */     
    public function insertContact($params){
        
        $this->stnd_params = $this->getStandardParams($params);
        $this->custom_params = $this->getCustomParams($params);
        $this->constant_params = $this->getConstantParams();
        
        try{
            
            if(!$this->stnd_params){  
                
                throw new Exception("Maileon: Invalid Contact Parameters ".PHP_EOL);     
            }
           
            $this->createContactService();
            $return = $this->response;
                        
        } catch (Exception $e) {
            echo $e->getMessage();    
        }
        return $return;        
    }
   
}

?>
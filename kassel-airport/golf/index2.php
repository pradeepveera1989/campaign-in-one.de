<?php
require "vendor/autoload.php";
require "libraries/autoload.php";

session_start();

// Get the Config file path
$config_path = "config.ini";
$conf = new config($config_path);
$config = $conf->getAllConfig();

spl_autoload_register('Autoload::generalFunctionLoader');
$func = new generalFunctions();
$func->getUTMParsamFromURL($_GET);
$single_submit_text = $config[GeneralSetting][SingleSubmitErrorTxt];
$jetzt_kaufen = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '#form'; 
// Page counter
$counter_status = $conf->getConfigCounterStatus();
if ($counter_status) {

    $counter = new counter();
    $url = "https://" . $_SERVER[HTTP_HOST] . "/" . explode('/', $_SERVER['REQUEST_URI'])[1];
    $current_count_value = $counter->getCounterValue($url);
}

//Split Test
if ($conf->getConfigSplitTestStatus()) {

    spl_autoload_register('Autoload::splitTestLoader');
    $currentURL = getcwd();
    $spTest = new splitTestClient($config[Splittest][Name], $config[Splittest][NumberOfVariants]);
    $redirect_url = $spTest->getRedirectURL($currentURL);
}

// Post Request
if (isset($_POST[$config[MailInOne][Mapping][EMAIL]])) {

    $params = array();

    // Email Validation
    $validate = new emailValidatorApiClient($_POST[$config[MailInOne][Mapping][EMAIL]]);
    $validate->curlRequest();
    $resp = $validate->curlResponce();

    if (!$resp) {
        // invalid email address
        // Copying the data to session and intiating a sesssion variable error.
        $_SESSION['data'] = $_POST;
        $_SESSION['error'] = "email";
        $url = $_POST[$config[MailInOne][Mapping][url]] . '#form';
        $func->redirectPage($url);
    } else {

        $_SESSION['error'] = "";

        // Map the POST params to Maileon Params
        $maileon_params = $config[MailInOne][Mapping];

        foreach ($maileon_params as $mp) {
            $params[$mp] = $_POST[$mp];
        }

        // Check for UTM Params and load from SESSION 
        if ($func->checkSession()) {
            $params = $func->checkUTMParamsInPOST($params);
        }

        // Get the date 
        $params[$config[MailInOne][Mapping][Datum]] = date('d.m.Y \\u\\m h:i:s a', time());
        $params[$config[MailInOne][Mapping][ip_adresse]] = $_SERVER['REMOTE_ADDR'];
        $params["url_params"] = parse_url($params[$config[MailInOne][Mapping][url]])["query"];

        // Set Cookie

        if($conf->getConfigSingleSubmit()){
            $single_submit_text = $config[GeneralSetting][SingleSubmitErrorTxt];
            $func->setCookieAsEmail($params[$config[MailInOne][Mapping][EMAIL]]);            
        }
        // Keep URL Parameters
        $success_page = $config[GeneralSetting][SuccessPage];

        if ($conf->getConfigKeepURLParms()) {
            $success_page .= '?' . $params["url_params"];
        }

        // Update the Counter Value
#        $new_counter_value = $current_count_value - 1;
#        $resp = $counter->updateCounterValue($url, $new_counter_value);

        // Assignment Manager
#        if ($conf->getConfigAssignmentManagerStatus()) {

#            $assignmentmanager = new assignmentManager();
#            $vertriebsmitarbeiter = $assignmentmanager->getAssignmentManager(
#                    $params[$config[MailInOne][Mapping][ZIP]], $config[AssignmentManager][Vertriebsmitarbeiter]
#            );
#            $vertriebsmitarbeiterid = $assignmentmanager->getAssignmentManagerId();
#            $params[$config[MailInOne][Mapping][Vertriebsmitarbeiter]] = $vertriebsmitarbeiter;
#            $params[$config[MailInOne][Mapping][Vertriebsmitarbeiterid]] = $vertriebsmitarbeiterid;
#        }
        
        //Hubspot Configuration Sync
        if ($conf->getConfigHubspotStatus()) {

            spl_autoload_register('Autoload::hubspotLoader');
            $hubspot = new hubspotApiClient();
            $params[$config[MailInOne][Mapping][HubspotContactID]] = $hubspot->createContact($params);
            $params[$config[MailInOne][Mapping][HubspotDealID]] = $hubspot->createDeal($params);
            $hubspot->associateDealWithContact($params);
        }

        // Forward Email
        if ($conf->getConfigForwardEmail()) {

            $mail = new forwardMail();
            $s = $mail->forwardEmail($params);
        }

        // Maileon Configuration Sync
        if ($conf->getConfigMailInOneStatus()) {

            spl_autoload_register('Autoload::maileonLoader');
            $maileon = new maileonApiClient();
            $params["event_type"] = 11;
            $response = $maileon->createEvent($params);

            // Redirect the Page
            $func->redirectPage($success_page);
        }
    }
}



# Facebook Pixel URL
$fbURL = "https://www.facebook.com/tr?id=" . $config[TrackingTools][FacebookPixel] . "&ev=PageView&noscript=1";

# Google Maps URL
$googleURL = "https://maps.googleapis.com/maps/api/js?key=" . $config[TrackingTools][GoogleAPIKey] . "&libraries=places";

#Google Analytics Key
$googleAnalyticsURL = "https://www.googletagmanager.com/gtag/js?id=" . $config[TrackingTools][GoogleAnalytics];
?>

<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Ab Kassel Airport in die Sonne: Informationen zu Flugplan, Reiseangeboten und Online-Buchung.">
        <meta name="author" content="Kassel Airport">

        <title>Kassel Airport</title>

        <link href="css/creative.css" rel="stylesheet" type="text/css">  
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
        <link rel="stylesheet" href="libraries/telefonvalidator-client/build/css/intlTelInput.css"/>
    </head>

    <script type="text/javascript" src="<?php echo $googleURL; ?>"></script>   	
    <script async src="<?php echo $googleAnalyticsURL; ?>"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', '<?php echo $config[TrackingTools][GoogleAPIKey]; ?>');
    </script>	
    <script>
        /*Get the facebook pixel code from config.ini */
        var facebookpixel = <?= $config[TrackingTools][FacebookPixel] ?>;
        !function (f, b, e, v, n, t, s)
        {
            if (f.fbq)
                return;
            n = f.fbq = function () {
                n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq)
                f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
                'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', <?= $config[TrackingTools][FacebookPixel] ?>);
        fbq('track', 'PageView');
        fbq('track', 'CompleteRegistration');
        fbq('track', 'Lead');
        
    </script>
    <noscript>
    <img height="1" width="1" style="display:none" src= "<?php echo $fbURL ?>"/>
    </noscript>	

    <script>
        // Set the date we're counting down to
        var countDownDate = new Date("<?= $config[GeneralSetting][CountdownExpire]; ?>").getTime();

        // Update the count down every 1 second
        var x = setInterval(function () {

            // Get todays date and time
            var now = new Date().getTime();

            // Find the distance between now an the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result in the element with id="demo"
            document.getElementById("demo").innerHTML = days + " Tage " + hours + " Stunden "
                    + minutes + " Min " + seconds + " Sek ";

            // If the count down is finished, write some text 
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("demo").innerHTML = "<?= $config[GeneralSetting][CountdownExpireMessage]; ?>";
            }
        }, 1000);
    </script>


    <body>
        
        
        <!-- MOBILE HEADER -->
        <div class="mobile-header">        
        <div class="container">
            <div class="row">

                <div class="col-12">
                    <center><img style="width:50%;" src="img/ksf-logo.svg" alt="flughafen-kassel"/></center>
                </div>

            </div>
        </div>
        </div>        
       <!-- /.MOBILE HEADER -->
        

        <section class="ksf-header">
            <div class="container">
                <div class="row">
      
                    <div class="col-12">
                        <div class="ksf-head"><img src="img/Kassel_Airport_logo.svg" /></div>
                    </div>
   
                </div>
            </div>
        </section>
        
        <section class="ksf-header-background">
                <div><img src="img/bg-golf.png" /></div>   
        </section>
        
        
        
     
        
  
 <div class="container">
    <div class="row">
 
        
        <div class="intro col-sm-12">
            
            <h2 style="padding:50px 0px 42px 0px;">Jetzt bei unseren Gewinnspiel-Fragen die richtigen Treffer landen!</h2>
          
            <p style="padding-bottom: 50px;">Jeden Monat haben Sie die Chance auf 5x2 Tickets zu Traumzielen direkt ab Kassel. Freuen Sie sich auf eine tolle Zeit auf Fuerteventura, Gran Canaria oder Teneriffa.</p>
            
            <h3 style="padding-bottom: 30px;" >Welche 3 Vorteile bietet eine Reise ab Kassel Airport?</h3>
            
            
             <form id="api-data-form" action="<?= $_SERVER['PHP_SELF'] ?>" method="post" accept-charset="utf-8" class="form-horizontal" role="form">
                            <input type="hidden" name="contactid" value="<?= $contactid ?>">
                            <input type="hidden" name="checksum" value="<?= $checksum ?>">
                            <input type="hidden" name="mailingid" value="<?= $mailingid ?>">
                 
                            <div style="padding-bottom: 50px;" class="container">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <input class="single-checkbox" type="checkbox" name="<?php echo $config[MailInOne][Mapping][quiz_freiesparken]; ?>" value="true"/>
                                        <span class="checkbox-text">&nbsp;Parkplätze gratis&nbsp;</span></br>       
                                        <input class="single-checkbox" type="checkbox" name="<?php echo $config[MailInOne][Mapping][quiz_schnelle_anreise]; ?>" value="true" />
                                        <span class="checkbox-text">&nbsp;Schnelle Anreise & Check-In&nbsp;</span></br>     
                                    </div>
                                    <div class="col-sm-4">
                                        <input class="single-checkbox" type="checkbox" name="<?php echo $config[MailInOne][Mapping][quiz_reservierung_liegen]; ?>" value="true" />
                                        <span class="checkbox-text">&nbsp;Vor-Reservierung der Liegen&nbsp;</span></br>     
                                        <input class="single-checkbox" type="checkbox" name="<?php echo $config[MailInOne][Mapping][quiz_gratis_sportgepaeck]; ?>" value="true" />
                                        <span class="checkbox-text">&nbsp;Sportgepäck gratis&nbsp;</span></br>    
                                    </div>
                                    <div class="col-sm-4">
                                        <input class="single-checkbox" type="checkbox" name="<?php echo $config[MailInOne][Mapping][quiz_sushi_vom_band]; ?>" value="true" />
                                        <span class="checkbox-text">&nbsp;Sushi direkt vom Gepäckband&nbsp;</span></br>    
                                        <input class="single-checkbox" type="checkbox" name="<?php echo $config[MailInOne][Mapping][quiz_flugzeug_parken]; ?>" value="true" />
                                        <span class="checkbox-text">&nbsp;Flugzeug selber ausparken&nbsp;</span></br>   
                                    </div>
                                </div>
                            </div>
                          
                            <h3 style="padding-bottom: 30px;">Nur noch Daten eingeben und mit etwas Glück Flugtickets gewinnen!</h3>
                       
                            
                 
                 
                             <!-- INPUT ANREDE FRAU & HERR -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="btn-group">
                                        <?php
                                        if ($_SESSION['data']['anrede'] === "Frau") {
                                            $frau = "checked";
                                        } else {
                                            $herr = "checked";
                                        }
                                        ?>										
                                        <label class="btn btn-outline-secondary" style="border: 1px solid transparent;">
                                            <input value="Frau" style="width: 15px; height: 15px;" type="radio" name="<?php echo $config[MailInOne][Mapping][SALUTATION]; ?>" autocomplete="off" required<?php echo $frau; ?>> <span style="color:#000;">Frau</span>
                                        </label>
                                        <span class="input-group-btn" style="width:15px;"></span> 
                                        <label class="btn btn-outline-secondary" style="border: 1px solid transparent;">
                                            <input value="Herr" style="width: 15px; height: 15px;" type="radio" name="<?php echo $config[MailInOne][Mapping][SALUTATION]; ?>" autocomplete="off" <?php echo $herr ?>> 
                                            <span style="color:#000;">Herr</span>
                                        </label>
                                    </div>  
                                </div>       
                            </div>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <!-- INPUT VORNAME -->
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <div class="input-group">

                                                        <input value="<?php echo ((!empty($standard_001) ? $standard_001 : $_SESSION['data'][$config[MailInOne][Mapping][FIRSTNAME]])); ?>" class="input-fields form-control ksf-input" id="" name="<?php echo $config[MailInOne][Mapping][FIRSTNAME]; ?>" placeholder="Vorname" type="text" required 
                                                               <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>      
                                                    </div>
                                                </div>       
                                            </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <!-- INPUT NACHNAME -->
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <div class="input-group">

                                                        <input  value="<?php echo ((!empty($standard_002) ? $standard_002 : $_SESSION['data'][$config[MailInOne][Mapping][LASTNAME]])); ?>" class="input-fields form-control ksf-input" id="nachname" name="<?php echo $config[MailInOne][Mapping][LASTNAME]; ?>" placeholder="Nachname" type="text" required
                                                                <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                                    </div>
                                                </div>       
                                            </div>
                                    </div>
                                    <div class="col-sm-4">
                                            <!-- INPUT EMAIL -->
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <div class="input-group">
                                                        <input value="<?php echo ((!empty($email_002) ? $email_002 : $_SESSION['data'][$config[MailInOne][Mapping][EMAIL]])); ?>" class="input-fields form-control email ksf-input" id="email" name="<?php echo $config[MailInOne][Mapping][EMAIL]; ?>" placeholder="E-Mail-Adresse" type="email" required 
                                                               <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                                    </div>
                                                <?php if ($_SESSION['error'] === "email") { ?>   
                                                                                        <span class="p-light erroremail"  style="color:<?php echo $config[Email_Address][ErrorMsg_Color]; ?>"><?php echo $config[Email_Address][ErrorMsg]; ?></span>
                                                <?php } ?>  										
                                                </div>       
                                            </div>
                                    </div>
                                </div>
        
                         

                            <label class="form-group">
                                <div style="padding-top: 30px;" class="col-sm-12 einwilligung">
                                    <input class="checkbox" style="width: 20px; height: 20px;" type="checkbox" required>
                                    
                                    <span class="p-dark">&nbsp;&nbsp;Ja, senden Sie mir gratis die "Airport-News" mit spannenden Last-Minute-Angeboten, Events am Kassel Airport und tollen Gewinn-Aktionen<br> per E-Mail.</span><br><br>
                                    
                                    <span class="p-dark-small"><small>Ich bin einverstanden, dass die Flughafen GmbH Kassel, Fieseler-Storch-Straße 40, 34379 Calden , meine eingetragenen Daten nutzt und mir E-Mails mit Infos zum Flughafen, Aktionen und Reiseangeboten schickt. Diese Einwilligung kann ich jederzeit widerrufen, etwa durch einen Brief an die oben genannte Adresse oder durch eine E-Mail an <a style="color:#069CD7;" href="maito:widerruf@kassel-airport.aero">widerruf@kassel-airport.aero</a>. Anschließend wird jede werbliche Nutzung unterbleiben.**</small></span>
                                </div>    
                            </label>   

                            <p>&nbsp;</p>
                 

                            <!--Hidden Fields-->   
                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">URL</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][url]; ?>" value="<?php echo $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>">
                                </div>
                            </div>                                        

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Source</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_source]; ?>" value="<?php echo $_GET['utm_source'] ?>" >
                                </div>
                            </div>  

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Name</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_name]; ?>" value="<?php echo $_GET['utm_name'] ?>">
                                </div>
                            </div>      

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Term</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_term]; ?>" value="<?php echo $_GET['utm_term'] ?>">
                                </div>
                            </div>                             

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Content</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_content]; ?>" value="<?php echo $_GET['utm_content'] ?>">
                                </div>
                            </div>                              

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Medium</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_medium]; ?>" value="<?php echo $_GET['utm_medium'] ?>">
                                </div>
                            </div>    

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Campaign</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_campaign]; ?>" value="<?php echo $_GET['utm_campaign'] ?>">
                                </div>
                            </div>                                               

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">TRAFFICSOURCE</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][trafficsource]; ?>" id="ts" value="<?php echo $_GET['trafficsource'] ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Quelle</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][Quelle]; ?>" id="quelle" value="<?php echo $config[MailInOne][Constants][Quelle]; ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Typ</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" ame="<?php echo $config[MailInOne][Mapping][Typ]; ?>" id="" value="<?php echo $config[MailInOne][Constants][Typ]; ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Segment</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" ame="<?php echo $config[MailInOne][Mapping][Segment]; ?>" id="" value="<?php echo $config[MailInOne][Constants][Segment]; ?>">
                                </div>  
                            </div>

                            <div class="form-group"> 
                                <div class="col-sm-12 btn-1">
                                    <input style="" type="submit" class="ksf-btn col-sm-12 btn btn-xl js-scroll-trigger" value="Jetzt teilnehmen und ab Kassel in die Sonne" name="submit" style="width: 100%;">
                                </div>        
                            </div>
                            <p>&nbsp;</p>

                            <div class="form-group"> 
                                <div class="col-sm-12 star">
                                    
    
                                    
                                    <span class="p-dark-small"><small>*Laufzeit der Monats-Verlosungen: 01.12.2018 bis 28.02.2019. Reisezeit: Die Tickets sind für Flüge im Reisezeitraum vom 01.12.2018 bis 28.02.2019 gültig. Ferientermine ausgeschlossen. Flüge und Tickets nach Verfügbarkeit..<br><br>**Die Online-Registrierung ist nur bei Abgabe einer Werbeeinwilligung möglich. Sie können aber auch ohne Werbeeinwilligung teilnehmen, wenn Sie uns innerhalb der Teilnahmefrist eine E-Mail an <a style="color:#069CD7;" href="mailto:widerruf@kassel-airport.aero">widerruf@kassel-airport.aero</a> senden und in der E-Mail Ihren Namen, Ihre Telefonnummer und den Hauptpreis des Gewinnspiels angeben. Ihre Gewinnchancen werden durch die Art der Teilnahme nicht beeinflusst..
                                        </small></span>
                                </div>        
                            </div>    
                            
                            <p>&nbsp;</p>
                            
<?php if (isset($response) && $response->isSuccess()) { ?>
                                <div class="alert alert-success fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Subscription successful</strong>
                                </div>
<?php } elseif (isset($warning)) { ?>
                                <div class="alert alert-warning fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong style="color: red; z-index: 1000;">Subscription failed</strong>
    <?= $warning['message'] ?>
                                </div>
<?php } elseif (isset($response)) { ?>
                                <div class="alert alert-danger fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Subscription failed</strong>
                                </div>
<?php } ?>
                        </form>          
            
        </div>
    </div>
    </div>     
        
       
        
            
 <section class="ksf-teilnahmebedingungen">
               
       <div class="container">        
        <div class="row">
            <div class="col-sm-12">
           
                
<div class="accordion ksf-accordion" id="accordionExample"  data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
  <div class="">
    <div class="" id="headingOne">
        
           <span style="font-size:20px; color:#919CB0">Teilnahmebedingungen</span><img style="padding-top:10px; float:right; width:6px;" src="img/left.svg" />
    
    </div>

    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="card-body">
        <span style="color:#919CB0; font-weight:400;">
            
        1. Veranstalter des Gewinnspiels ist der Kasseler Airport, Fieseler-Storch-Straße, 34379 Calden.<br><br>
        
        2. An dem Gewinnspiel nimmt ein Nutzer teil, indem er sich bis zum angegebenen Teilnahmeschluss über die Gewinnspielseite registriert und dabei seine Teilnahmedaten (Name, Vorname, E-Mail-Adresse) angibt und an der Gewinnspiel 2018 teilgenommen hat und eine Einwilligung zur E-Mail-Werbung und zur werblichen Nutzer seiner Daten (im Folgenden: Werbeeinwilligung) erklärt. Alternativ ist auch eine Teilnahme ohne eine Werbeeinwilligung möglich, wenn ein Nutzer innerhalb der Teilnahmefrist eine E-Mail an die E-Mail-Adresse <a href="mailto:widerruf@kassel-airport.aero">widerruf@kassel-airport.aero</a> unter Angabe seines Namens und des Hauptpreises des Gewinnspiels sendet. Die Form der Teilnahme (also Online-Registrierung mit Werbeeinwilligung oder E-Mail-Registrierung ohne Werbeeinwilligung) hat auf die Gewinnchancen keinen Einfluss. <br><br>
          
        3. Jeder Teilnehmer kann nur einmal an einem Gewinnspiel teilnehmen. Mitarbeitern des Kasseler Airports und ihren unmittelbaren Angehörigen (Eltern, Kinder, Ehepartner, Geschwister) ist eine Teilnahme untersagt. Zudem ist eine Teilnahme unter Angabe einer falschen E-Mail-Adresse unzulässig.<br> <br>
          
        4. Der Gewinner wird per Verlosung unter allen zugelassenen Teilnehmern (vgl. Ziff. 3) wöchentlich ermittelt. <br><br>
          
        5. Der oder die Gewinner werden per E-Mail über den Gewinn informiert. In diesem Zusammenhang werden Sie aufgefordert, ihren vollen Namen zu benennen. Bei einer Gewinnmitteilung per E-Mail wird der Gewinner aufgefordert, innerhalb von zwei Wochen seine Postadresse zu benennen. Reagiert der Gewinner darauf nicht, gilt die Gewinnmitteilung per E-Mail als gescheitert und der Gewinnanspruch verfällt. <br><br>
          
        6. Stellt sich nach der Gewinnermittlung heraus, dass ein Nutzer unter Verstoß gegen Ziff. 3 teilgenommen hat, kann die Gewinnermittlung wiederholt werden. <br><br>
          
        7. Sofern in den Gewinnspielinformationen nichts anderes mitgeteilt wird, wird ein Gewinn auf Kosten des Kasseler Airports an den Gewinnspielteilnehmer verschickt. <br><br>
          
        8. Es gilt deutsches Recht.
        </span>
      </div>
    </div>
  </div>
  </div>               
                
             </div>
        </div>
       </div>      
        </section>
          <p>&nbsp;</p>       





<section class="ksf-foot"> 
<div class="container">
    <div class="legal">
    <div class="row">
    
        <div class="col-sm-2"><a href="https://www.kassel-airport.aero/" target="_blank"><span style="color:#919CB0;"> &lt; Zur Webseite</span></a></div>
        <div class="col-sm-2"><a href="https://www.kassel-airport.aero/de/fluege-urlaubsangebote/reiseangebote" target="_blank">Unsere Angebote</a></div>
        <div class="col-sm-2"><a href="https://www.kassel-airport.aero/de/inhalte-metanavigation-seitenfuss/impressum" target="_blank">Impressum</a></div>
        <div class="col-sm-2"><a href="https://www.kassel-airport.aero/de/inhalte-metanavigation-seitenfuss/datenschutz" target="_blank">Datenschutz</a></div>
        <div class="col-sm-2"><a href="https://www.kassel-airport.aero/de/inhalte-metanavigation-seitenfuss/datenschutz" target="_blank">AGB</a></div>
        <div class="col-sm-2"></div>
    </div>
    </div>
</div>
 </section>  





    

        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="libraries/telefonvalidator-client/build/js/utils.js"></script>		
        <script type="text/javascript" src="libraries/telefonvalidator-client/build/js/intlTelInput.js"></script>	
        <script src="vendor/google/maps.js"></script>  
        <script type="text/javascript" src="libraries/jquery-cookie-master/src/jquery.cookie.js"></script>  
        
        
        <script>  
                                                $('.single-checkbox').on('change', function() {
                                           if($('.single-checkbox:checked').length > 3) {
                                               this.checked = false;
                                               
                                           }
                                        });

                                       

            </script>
        <script>

                                    $('.close').click(function () {
                                        $('#modal-video').hide();
                                        $('#modal-video iframe').attr("src", jQuery("#modal-video iframe").attr("src"));
                                    });
        </script>



        <script>

            $(window).scroll(function () {
                var scroll = $(window).scrollTop();

                if (scroll > 660) {
                    $(".nav-bottom").addClass("change"); // you don't need to add a "." in before your class name
                } else {
                    $(".nav-bottom").removeClass("change");
                }
            });
        </script>






        <script type="text/javascript">

            $(document).ready(function () {
                var countdown_status = "<?= $counter_status ?>";
                var current_stocks = "<?= $current_count_value; ?>";
                var autofill_postal_code = "<?= $config[Postal_Code][Autofill]; ?>";
                var countdown_expire_message = "<?= $config[GeneralSetting][CountdownExpireMessage] ?>";
                var validate_telefon = "<?= $config[Telefon][Status]; ?>";
                var single_submit_text = "<?= $single_submit_text?>"
                var cookie = "<?= $cookie; ?>";
                console.log("submt text",single_submit_text);

                // telefon validation 
                var settings = {
                    utilsScript: "../libraries/telefonvalidator-client/build/js/utils.js",
                    preferredCountries: ['de'],
                    onlyCountries: ['de', 'ch', 'at'],
                };
                $('#telefon').intlTelInput(settings);
                $('#telefon-mobile').intlTelInput(settings);

                // Error messages
                $('.errorplz').hide();
                $('.errortelefon').hide();

                //Cookie 
                if ($.cookie('campaign')) {
                    disableContactForm(single_submit_text);
                }

                // Countdown counter
                if (countdown_status != 1 || current_stocks <= 0) {
                    disableContactForm(countdown_expire_message);
                }

               
                /* JQuery Blur event for the postal code
                 * Parameters : 
                 * Returns : 
                 *    1. fill the city Name on success.
                 */

                $('.plz').blur(function () {
                    var zip = $(this).val();
                    var city;
                    console.log(zip);

                    if ($('.plz').hasClass('wrongplz')) {
                        $('.plz').removeClass('wrongplz');
                        $('.errorplz').hide();
                    }
                    // Check for autofill postal code value
                    if (autofill_postal_code) {
                        city = checkZipCode(zip);

                        if (city) {
                            $('.ort').val(city);
                        } else {
                            $('.ort').val("");
                            $('.plz').addClass("wrongplz");
                        }
                    }
                });


                /* Function Name : checkZipCode
                 * Parameters : zipcode
                 * Returns : 
                 *    1. countryName on success .
                 *    2. False on faiure.
                 */

                function checkZipCode(zip) {
                    console.log("checkZipCode");
                    var status = true;
                    var city;
                    if (zip.length == 5) {
                        var gurl = 'https://maps.googleapis.com/maps/api/geocode/json?address=Germany' + zip + '&key=AIzaSyDA10Y_CEIbkz2OY-Zp7PsBBjKB5YNh77I';
                        $.getJSON({
                            url: gurl,
                            async: false,
                            success: function (response, textStatus) {
                                // check the status of the request
                                if (response.status !== "OK") {
                                    status = false;         // Postal code not found or wrong postal code.
                                } else {
                                    // Postal code is found
                                    status = true
                                    var address_components = response.results[0].address_components;
                                    $.each(address_components, function (index, component) {
                                        var types = component.types;
                                        // Find the city for the postal code
                                        $.each(types, function (index, type) {
                                            if (type == 'locality') {
                                                city = component.long_name;
                                                status = true;
                                            }
                                        });
                                    });
                                }
                            }
                        });
                    } else {
                        status = false;
                    }
                    if (status) {
                        return city;
                    } else {
                        return false;
                    }
                }

                /* Desktop Version
                 *  Validates the phone number format for specified country. 
                 *
                 *  Parameters:  Telephone field in the form.
                 *
                 *  Returns: 
                 *		false : shows the error message 
                 *       true  : hides the error message
                 */

                $('#telefon').blur(function () {
                    /* Regex for all the special characters and alphabits */
                    alpha = /^[a-zA-Z!@#$§?=´:;<>|{}\[\]*~_%&`.,"]*$/;
                    tel = $('#telefon');
                    tel2 = $('#tel2');
                    var error = false;
                    telnum = tel.val();

                    // If telefon validation true 
                    if ($.trim(tel.val()) && validate_telefon) {
                        console.log(telnum);
                        // Check for alphabits and special characters from regex expresssion.
                        for (var i = 0; i < telnum.length; i++) {
                            var c = telnum.charAt(i);
                            if (alpha.test(c)) {
                                error = true;
                            }
                        }

                        if (telnum.charAt(0) == 0) {
                            console.log("Inside");
                            telnum = telnum.substring(1);
                        }

                        console.log("After removiing", telnum)

                        // Get the Conuntry code     
                        var ext = tel.intlTelInput("getSelectedCountryData").dialCode;
                        var num = "+" + ext + ' ' + telnum;
                        tel2.val(num);


                        // Check for valid number  
                        if (!tel.intlTelInput("isValidNumber")) {

                            $('.errortelefon').show();
                        } else {
                            if (error) {
                                $('.errortelefon').show();
                            } else {
                                $('.errortelefon').hide();
                            }
                        }
                    }
                });

                /* Mobile version
                 *  Validates the phone number format for specified country. 
                 *
                 *  Parameters:  Telephone field in the form.
                 *
                 *  Returns: 
                 *		false : shows the error message 
                 *       true  : hides the error message
                 */

                $('#telefon-mobile').blur(function () {
                    /* Regex for all the special characters and alphabits */
                    alpha = /^[a-zA-Z!@#$§?=´:;<>|{}\[\]*~_%&`.,"]*$/;
                    tel = $('#telefon-mobile');
                    tel2 = $('#tel2');
                    telnum = tel.val();
                    var error = false;
                    // If telefon validation true 
                    if ($.trim(tel.val()) && validate_telefon) {
                        // Check for alphabits and special characters from regex expresssion
                        for (var i = 0; i < telnum.length; i++) {
                            var c = telnum.charAt(i);
                            if (alpha.test(c)) {
                                error = true;
                            }
                        }
                        if (telnum.charAt(0) == 0) {
                            console.log("Inside");
                            telnum = telnum.substring(1);
                        }

                        console.log("After removiing", telnum)

                        // Get the Conuntry code     
                        var ext = tel.intlTelInput("getSelectedCountryData").dialCode;
                        console.log("extention", ext);
                        var num = "+" + ext + ' ' + telnum;
                        console.log("num", num);
                        tel2.val(num);

                        // Check for valid number  
                        if (!tel.intlTelInput("isValidNumber")) {
                            $('.errortelefon').show();
                        } else {
                            if (error) {
                                $('.errortelefon').show();
                            } else {
                                $('.errortelefon').hide();
                            }
                        }
                    }
                });


               

        </script>
    </body>
</html>        

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of countdown
 *
 * @author Pradeep
 */

class counter{
    var $dbObject;
    var $select;
    var $where;
    var $table;
    var $campaign;
    var $value;
    var $datatype;
    var $result;
    
    function __construct($type){
        $this->table = "campaign_params";
        $this->dbObject = new db();
        $this->config = new config();
        $this->config_counter = $this->config->getConfigCounter();
    }    
    
    public function updateDatabase(){

        $this->result = $return;
    }
    
    public function updateCampaignParameter($update, $where){
        $stmt = array();
        $d = $this->dbObject;
        foreach($where as $key => $value){
            array_push($stmt, $key." LIKE '".$value."'");
        }
        $stmt = implode(' AND ', $stmt);     
        $where = $stmt;

        $query_result= $d->execute($this->table, $update, $where, "update");
        $this->result = $query_result;        
    }
    
    public function insertCampaignParameter( $insert){       
        
        $d = $this->dbObject;
        $query_result= $d->execute($this->table, $insert, " ", "insert");
        $this->result = $query_result;
        
        return $this->result;
    }
    
    public function getCampaignParameter(){
        
        $this->select = array("value", "datatype");
        $stmt = "campaign LIKE '".$this->campaign."' AND type LIKE '".$this->type ."'";
        $this->where = array($stmt);
        $d = $this->dbObject;
        
        $query_result= $d->getOne($this->select, $this->table, $this->where);
       
        if(!empty($query_result[value])){
            
            $dval = $query_result[value];
            if($query_result[datatype] === 'integer'){
            
                $return = intval($dval);
            }else{
                
                $return = intval($dval);
            }        
        }else{
            
            $return = 0;
        }    
        
        return $return;
    }
    
    public function deleteCampaignParameter(){
        
    }
    
    public function resetCampaignParameter(){
        
    }
    
    public function updateCounterValue($url, $counter) {

        if (empty($url) || ($counter == 0)) {
            # Invalid URL or Counter
            return false;
        }
        $value = array(
            "value" => $counter
        );
        $where = array(
            "campaign" => $url,
            "type" => $this->type
        );
        $response = $this->updateCampaignParameter($value, $where);
  
        return $response;
    }

    public function getCounterValue($url){

        if(empty($url)){
            
            return false;
        }
        $this->type = "AvailableItems";
        $this->campaign = $url;
        $value = $this->getCampaignParameter();
        return $value;
    }
    
   
}
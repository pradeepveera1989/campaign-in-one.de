<?php
/* _______________________________________________________________________________________________________________________________________________
 *
  MEDIALABCOM - ABMELDESEITE - 1709

 * Contact update via Maileon API
 * © copyright 2016 XQueue GmbH
 *
 * You need the Maileon PHP API Client to run this script
 * http://dev.maileon.com/api-clients/
 * _______________________________________________________________________________________________________________________________________________
 */

// Update with your version of the Maileon PHP API Client
require_once("./maileon-php-client-1.3.1/client/MaileonApiClient.php");

$config = array(
    "BASE_URI" => "https://api.maileon.com/1.0",
    "API_KEY" => "c844faa1-9b91-4cdc-a072-d5dfe4b2315d",
    "THROW_EXCEPTION" => true,
    "TIMEOUT" => 60,
    "DEBUG" => false // NEVER enable on production
);

$contactid = "";
$checksum = "";
$mailingid = "";
$mode = "";

if (isset($_GET["contactid"])) {
    $contactid = $_GET["contactid"];
} elseif (isset($_POST["contactid"])) {
    $contactid = $_POST["contactid"];
}

if (isset($_GET["checksum"])) {
    $checksum = $_GET["checksum"];
} elseif (isset($_POST["checksum"])) {
    $checksum = $_POST["checksum"];
}

if (isset($_GET["mailingid"])) {
    $mailingid = $_GET["mailingid"];
} elseif (isset($_POST["mailingid"])) {
    $mailingid = $_POST["mailingid"];
}

if (isset($_GET["mode"])) {
    $mode = $_GET["mode"];
} elseif (isset($_POST["mode"])) {
    $mode = $_POST["mode"];
}

if ($mode == "update" && !empty($contactid) && !empty($checksum)) {

    #$email = $_POST["email"];
    #$salutation = $_POST["salutation"];
    #$firstname = $_POST["firstname"];
    #$lastname = $_POST["lastname"];
    $adPressure = $_POST["ad_pressure"];

    $contactsService = new com_maileon_api_contacts_ContactsService($config);

    $contact = new com_maileon_api_contacts_Contact();
    $contact->id = $contactid;
    $contact->email = $email;
    $contact->anonymous = false;
    #$contact->standard_fields["SALUTATION"] = $salutation;
    #$contact->standard_fields["FIRSTNAME"] = $firstname;
    #$contact->standard_fields["LASTNAME"] = $lastname;
    $contact->custom_fields["Ad_Pressure"] = $adPressure;

    $update = $contactsService->updateContact($contact, $checksum);
} elseif ($mode == "unsubscribe" && !empty($contactid) && !empty($checksum)) {
    $contactsService = new com_maileon_api_contacts_ContactsService($config);
    $unsubscribe = $contactsService->unsubscribeContactById($contactid, $mailingid);
} elseif ($mode == "addReasons" && !empty($contactid) && !empty($checksum)) {
    $reasons = array();

    if (isset($_POST["reasons"]) && is_array($_POST["reasons"])) {
        foreach ($_POST["reasons"] as $reason) {
            if ($reason == "other" && !empty($_POST["reasons_other_details"])) {
                $reason .= "|" . $_POST["reasons_other_details"];
            }
            $reasons[] = $reason;
        }
    }

    $contactsService = new com_maileon_api_contacts_ContactsService($config);
    $addReasons = $contactsService->addUnsubscriptionReasonsToUnsubscribedContact($contactid, $checksum, $reasons);
} elseif (!empty($contactid) && !empty($checksum)) {
    $contactsService = new com_maileon_api_contacts_ContactsService($config);

    $queryExistingContact = $contactsService->getContact(
            $contactid, $checksum, array(
        "Ad_Pressure"
            )
    );




    if ($queryExistingContact->isSuccess()) {
        $contact = $queryExistingContact->getResult();
        #$email = $contact->email;
        #$salutation = $contact->standard_fields["SALUTATION"];
        #$firstname = $contact->standard_fields["FIRSTNAME"];
        #$lastname = $contact->standard_fields["LASTNAME"];
        $adPressure = $contact->custom_fields["Ad_Pressure"];
    }
    /*
      else {
      $error = "Contact not found";
      }
     */
} else {
    $error = "Invalid request";
}

if (isset($error)) {
    #$email = "";
    #$salutation = "";
    #$firstname = "";
    #$lastname = "";
    $adPressure = "";
}
?>
<!DOCTYPE html>
<html lang="de">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta charset="utf-8">
        <title>MediaLabCom Newsletter-Abmeldung</title>
        <link href="css/style.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <!--<style type="text/css">input[readonly]{ background-color: white !important; }</style>-->
        <script src="//code.jquery.com/jquery-1.11.3.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <style> 
            body {
                background-image: url("img/bibliothek.jpg");
                background-repeat: no-repeat, repeat;
                background-color: #cccccc;
            }
        </style>
    </head>
    <body>


        <div class="container">

            <div class="col-sm-1"></div>

            <div class="col-sm-10 col-md-10 form-container">



                <?php if (empty($mode) || $mode == "update") { ?>
                    <div class="page-header">
                        <h1>MediaLabCom Newsletter-Abmeldung</h1>
                    </div>

                    <p>Schade, dass Sie uns verlassen möchten! Können wir Sie noch überzeugen zu bleiben?</p>
                    <div class="page-header">
                        <h3>Ich möchte keine Newsletter mehr von MediaLabCom erhalten.</h3>
                    </div>
                    <form id="api-data-form" action="<?= $_SERVER["PHP_SELF"] ?>" method="post" accept-charset="utf-8" class="form-horizontal" role="form">
                        <input type="hidden" name="contactid" value="<?= $contactid ?>">
                        <input type="hidden" name="mailingid" value="<?= $mailingid ?>">
                        <input type="hidden" name="checksum" value="<?= $checksum ?>">
                        <input type="hidden" name="mode" value="unsubscribe">
                        


                        <div class="form-group">
                            <label class="control-label col-sm-3" for="unsubscribe">Abmeldung: <abbr title="mandatory field">*</abbr></label>
                            <div class="col-sm-9">
                                <input type="submit" class="btn btn-default" value="sofort abmelden!">
                            </div>
                        </div>



                    </form>
                <?php } elseif ($mode == "unsubscribe" || $mode == "addReasons") { ?>
                    <div class="page-header">
                        <h3>Bitte nehmen Sie sich einen Moment um uns Feedback zu geben:</h3>
                    </div>
                    <form id="api-data-form" action="<?= $_SERVER["PHP_SELF"] ?>" method="post" accept-charset="utf-8" class="form-horizontal" role="form">
                        <input type="hidden" name="contactid" value="<?= $contactid ?>">
                        <input type="hidden" name="checksum" value="<?= $checksum ?>">
                        <input type="hidden" name="mode" value="addReasons">

                        <div class="form-group">
                            <label class="control-label col-sm-3">Abmeldegründe:</label>
                            <div class="col-sm-9">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="reasons[]" value="nicht_relevant">Inhalte nicht relevant</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="reasons[]" value="rollenwechsel">Rollenwechsel / anderes Betätigungsfeld</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="reasons[]" value="zu_viele_newsletter">Zu viele Newsletter</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="reasons[]" value="andere" id="other">Sonstiges</label>
                                </div>

                                <script type="text/javascript">
                                    $("#other").change(function () {
                                        $("#other_details").toggle();
                                    });
                                </script>
                            </div>
                        </div>

                        <div class="form-group" id="other_details" style="display: none;">
                            <label class="control-label col-sm-3" for="reasons_other_details">Hinterlasse uns eine Nachricht:</label>
                            <div class="col-sm-9"><input type="text" placeholder="Your message" name="reasons_other_details" id="reasons_other_details" maxlength="255" class="form-control"></div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-10">
                                <input type="submit" class="btn btn-default" value="Abmeldegründe senden">
                            </div>
                        </div>
                    </form>
                <?php } ?>

                <?php if (!empty($error)) { ?>
                    <div class="alert alert-danger fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <h5><strong>Ein Fehler ist aufgetreten. Bitte versuchen Sie es erneut.</strong></h5>
                        <ul class="list-group">
                            <li class="list-group-item list-group-item-danger"><?= $error ?></li>
                        </ul>
                    </div>
                <?php } elseif (isset($update) && $update->isSuccess()) { ?>
                    <div class="alert alert-success fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <h5><strong>Ihr Versandprofil wurde erfolgreich aktualisiert. Sie erhalten ab jetzt nur noch die Sonderaktions Newsletter.</strong></h5>
                    </div>
                <?php } elseif (isset($update)) { ?>
                    <div class="alert alert-danger fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <h5><strong>Ein Fehler ist aufgetreten. Bitte versuchen Sie es erneut.</strong></h5>
                    </div>
                <?php } elseif (isset($unsubscribe) && $unsubscribe->isSuccess()) { ?>
                    <div style="background-color: #003867!important;" class="alert alert-success fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <h5 style="color:#fff!important;"><strong>Sie wurden erfolgreich abgemeldet.</strong></h5>
                    </div>
                <?php } elseif (isset($unsubscribe)) { ?>
                    <div class="alert alert-danger fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <h5><strong>Abmeldung fehlgeschlagen. Bitte versuchen Sie es erneut.</strong></h5>
                    </div>
                <?php } elseif (isset($addReasons) && $addReasons->isSuccess()) { ?>
                    <div class="alert alert-success fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <h5><strong>Vielen Dank für Ihre Feedback.</strong></h5>
                    </div>
                <?php } elseif (isset($addReasons)) { ?>
                    <div class="alert alert-danger fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <h5><strong>Saving unsubscription reasons failed - please check your data and try again.</strong></h5>
                    </div>
                <?php } ?>


            </div>
        </div>  


        <!-- Footer -->    
        <div id="footer">
            <div class="footer-bottom">
                <div class="header"></div>
                <center>
                    <p>&nbsp;</p>
                    <h4><small style="color:#FFF;">Copyright © 2018 LABcom GmbH</small></h4>
                    <p>&nbsp;</p>
                </center>
            </div>
        </div>

    </body>
</html>
<?php
require "cio.php";
?>

<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Nutzen Sie unser neues FREE EMS® PROGRAMM und trainieren sie bequem von zu Hause aus.">
        <meta name="author" content="StimaWELL">

        <title>EMS Training - Rabatt für FREE EMS eBook Leser</title>

        <link href="css/creative.css" rel="stylesheet" type="text/css">  
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">    
        <link rel="stylesheet" href="vendor/telefonvalidator-client/build/css/intlTelInput.css"/>

        <script type="text/javascript" src="vendor/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="vendor/telefonvalidator-client/build/js/utils.js"></script>
        <script type="text/javascript" src="vendor/telefonvalidator-client/build/js/intlTelInput.js"></script>
        <script type="text/javascript" src="vendor/bxslider/src/js/jquery.bxslider.js "></script>
        <script type="text/javascript" src="vendor/google/maps.js"></script>
        <script type="text/javascript" src="vendor/jquery-cookie-master/src/jquery.cookie.js"></script>
        <script type="text/javascript">
            var countdown_status = "<?= $counter_status; ?>";
            var countdown_expire = "<?= $config[GeneralSetting][CountdownExpire]; ?>";
            var countdown_expire_period = "<?= $config[GeneralSetting][MaximumCountdownperiod]; ?>";
            var current_stocks = "<?= $current_count_value; ?>";
            var countdown_end_date = "<?= $current_end_date ?>";
            var autofill_postal_code = "<?= $config[Postal_Code][Autofill]; ?>";
            var localization_postal_code = "<?= $config[Postal_Code][Localization]; ?>"
            var countdown_expire_message = "<?= $config[GeneralSetting][CountdownExpireMessage] ?>";
            var validate_telefon = "<?= $config[Telefon][Status]; ?>";
            var single_submit = "<?= $config[GeneralSetting][SingleSubmit]; ?>"
            var single_submit_text = "<?= $single_submit_text ?>"
            var cookie = "<?= $cookie; ?>";
            var webgains_cookie = "<?= $webgains_cookie ?>";
        </script>
        <script language="JavaScript" type="text/javascript" src="cio.js"></script>   
        <!-- Google Analytics -->
        <?php include_once 'clients/tracking/google/google.php'; ?>        

        <!-- Facebook Pixel -->
        <?php include_once('clients/tracking/facebook/facebook.php'); ?>      
    </head>

    <!-- Outbrain Tracking -->
    <?php $config[TrackingTools][EnableOutbrain] ? include_once('clients/tracking/outbrain/outbrain_index.php') : " "; ?>

    <body>
        <!-- Web Gains Tracking -->
        <?php $config[TrackingTools][EnableWebGains] ? include_once 'clients/tracking/webgains/webgains_index.php' : " "; ?>

        <!-- Remarketing Target360 Tracking -->
        <?php $config[TrackingTools][EnableTarget360] ? include_once 'clients/tracking/target360/target_index.php' : ' ';?>      

        <!-- DESKTOP NAV -->
        <nav class="navbar navbar-light bg-light static-top">   
            <div class="container">
                <a class="navbar-brand" style="text-transform: uppercase;" href="https://www.stimawell-ems.de/" target="_blank"><img style="width:200px;" alt="EMS-Training-zuhause" src="img/logo.svg"></a>
                <img>
                <span class="navbar-details"style="text-transform:uppercase; "><i class="fas fa-check"></i> 40 JAHRE MADE IN GERMANY&nbsp;&nbsp;<i class="fas fa-check"></i> DIREKT VOM HERSTELLER&nbsp;&nbsp;<i class="fas fa-check"></i> SERVICE: <strong>06443 4369914</strong></span>
            </div>
        </nav>

        <!-- MOBILE HEADER-->  
        <div class="mobile-nav"><center><img style="width:200px;" alt="EMS-Training-zuhause" src="img/logo.svg"></center></div>



        <div class="youtube-video-header">
            <iframe width="1920" height="1080" src="https://www.youtube.com/embed/dfb7hibyG4I?autoplay=1&mute=1&rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>    

        <!-- VIDEO HEADER -->
        <div class="video-header">
            <div class="video-overlay">
                <center>
                    <h1 style="color: #fff; font-size: 3rem; ">
                        Jetzt für FREE EMS eBook-Leser –<br>
                        EMS-Training zum Aktionspreis
                    </h1>  
                    <h2 style="color: #fff; font-size: 3rem; margin-top: 15px;">Nur <strong>59.- €</strong> pro Monat und zuhause trainieren so oft Du willst</h2>  
                    <p>&nbsp;</p>
                    <button style="margin-bottom: 10px;" type="button" class="btn btn-outline-info" id="stimawell-button-1" data-toggle="modal" data-target="#modal-video">VIDEO ANSEHEN</button>
                    <form style="display: inline;" method = "post" action="<?php echo $jetzt_kaufen; ?>"><button style="margin-bottom: 10px;" type="submit" class="btn btn-outline-info" id="stimawell-button-2">JETZT KAUFEN</button></form>
                </center>
            </div>
        </div>

        <!-- MOBILE -->
        <div class="video-header-mobile">
            <div  class="video-overlay-mobile">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <p style="color: #fff; font-size: 2rem; ">
                                Jetzt für FREE EMS eBook-Leser –<br>
                                EMS-Training zum Aktionspreis</p>  
                            <p style="color: #fff; font-size: 1.2rem; ">Nur <strong>59.- €</strong> pro Monat und zuhause trainieren so oft Du willst</p>  
                            <p>&nbsp;</p>
                            <button style="margin-bottom: 10px;" type="button" class="btn btn-outline-info"  data-toggle="modal" data-target="#modal-video">VIDEO ANSEHEN</button>
                            <form style="display: inline;" method = "post"  action="<?php echo $jetzt_kaufen; ?>"><button style="margin-bottom: 10px;" type="submit" class="btn btn-outline-info">JETZT KAUFEN</button></form>
                        </div>    
                    </div>
                </div>
            </div>
        </div>


        <!-- COUNTDOWN -->
        <section>
            <div class="nav-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 text-center">

                            <span class="countdown-nav">Aktionsende:&nbsp;&nbsp;</span><span class="countdown-nav" id="demo"></span>&nbsp;<span class="countdown-nav">-&nbsp;&nbsp;Nur noch <?php echo $current_count_value; ?> von <?php echo $config[GeneralSetting][MaxOffers]; ?> Angeboten verfügbar.&nbsp;</span>
                        </div> 
                    </div>
                </div>   
            </div>  

        </section>   
        <!-- ./COUNTDOWN -->  

        <p>&nbsp;</p>

        <section class="form-container">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-6" style="padding-top: 50px;">    

                        <h2 style="color: #000000;">FREE EMS - Trainiere bequem von zu Hause wann Du willst</h2>
                        <hr>
                        <p>
                            Für alle Leser unseres FREE EMS eBooks bieten wir absofort einen limitierten Aktionspreis von nur 59 Euro im Monat an.<br>
                        </p>

                        <ul>
                            <li>Trainiere für nur <strong>59 Euro im Monat</strong> wann und wo Du willst</li>
                            <li>Keine versteckten Kosten</li>
                            <li><strong>Alles inklusive:</strong> Stimawell-EMS-Gerät, Anzug und Bandelektroden im Wert von 8.599.- zur exklusiven Nutzung</li>
                            <li><strong>Kein Risiko</strong> - 4 Wochen Rückgaberecht</li>
                            <li>Individuelles Trainingsprogramm</li>
                            <li>Option: jeder weiterer Nutzer im Haushalt nur <strong>39.- € pro Monat</strong> inkl. Anzug</li>  
                        </ul>

                        <div class="img-ghost">
                            <img style="width: 450px;" src="img/stimawell-model_ebook.png" alt="ems-training zu Hause"/>
                        </div>

                    </div>  
                    <div class="col-lg-6 check-submit-form" id="form" style="padding-top: 50px; padding-bottom: 20px;">     
                        <form id="api-data-form" action="<?= $_SERVER['PHP_SELF'] ?>" method="post" accept-charset="utf-8" class="form-horizontal" role="form">
                            <input type="hidden" name="contactid" value="<?= $contactid ?>">
                            <input type="hidden" name="checksum" value="<?= $checksum ?>">
                            <input type="hidden" name="mailingid" value="<?= $mailingid ?>">

                            <!-- INPUT ANREDE FRAU & HERR -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="btn-group">
                                        <?php
                                        if ($_SESSION['data']['anrede'] === "Frau") {
                                            $frau = "checked";
                                        } else {
                                            $herr = "checked";
                                        }
                                        ?>										
                                        <label class="btn btn-outline-secondary" style="border: 1px solid transparent;">
                                            <input value="Frau" style="width: 15px; height: 15px;" type="radio" name="<?php echo $config[MailInOne][Mapping][SALUTATION]; ?>" autocomplete="off" required <?php echo $frau; ?>> Frau
                                        </label>
                                        <span class="input-group-btn" style="width:15px;"></span> 
                                        <label class="btn btn-outline-secondary" style="border: 1px solid transparent;">
                                            <input value="Herr" style="width: 15px; height: 15px;" type="radio" name="<?php echo $config[MailInOne][Mapping][SALUTATION]; ?>" autocomplete="off" <?php echo $herr ?>> 
                                            Herr
                                        </label>
                                    </div>  
                                </div>       
                            </div>

                            <!-- INPUT VORNAME -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">

                                        <input value="<?php echo ((!empty($standard_001) ? $standard_001 : $_SESSION['data'][$config[MailInOne][Mapping][FIRSTNAME]])); ?>" class="input-fields form-control" id="" name="<?php echo $config[MailInOne][Mapping][FIRSTNAME]; ?>" placeholder="*Vorname" type="text" required 
                                               <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>      
                                    </div>
                                </div>       
                            </div>


                            <!-- INPUT NACHNAME -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">

                                        <input  value="<?php echo ((!empty($standard_002) ? $standard_002 : $_SESSION['data'][$config[MailInOne][Mapping][LASTNAME]])); ?>" class="input-fields form-control" id="nachname" name="<?php echo $config[MailInOne][Mapping][LASTNAME]; ?>" placeholder="*Nachname" type="text" required
                                                <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                    </div>
                                </div>       
                            </div>

                            <!-- INPUT ADRESSE -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <input value="<?php echo ((!empty($standard_004) ? $standard_004 : $_SESSION['data'][$config[MailInOne][Mapping][ADDRESS]])); ?>"  class="input-fields form-control plz" id="strasse" name="<?php echo $config[MailInOne][Mapping][ADDRESS]; ?>" placeholder="*Straße & Hausnummer" type="text" required 
                                               <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                    </div>
                                </div>       
                            </div>

                            <!-- INPUT PLZ -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <input value="<?php echo ((!empty($standard_004) ? $standard_004 : $_SESSION['data'][$config[MailInOne][Mapping][ZIP]])); ?>"  class="input-fields form-control plz" id="plz" name="<?php echo $config[MailInOne][Mapping][ZIP]; ?>" placeholder="*PLZ" type="number" required 
                                               <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                    </div>
                                    <span class="p-light errorplz" style="color:<?php echo $config[ErrorHandling][ErrorMsg_Color]; ?>"><?php echo $config[ErrorHandling][ErrorMsgPostalcode]; ?></span>          
                                </div>       
                            </div>

                            <!-- INPUT ORT -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <input value="<?php echo ((!empty($standard_005) ? $standard_005 : $_SESSION['data'][$config[MailInOne][Mapping][CITY]])); ?>" class="input-fields form-control ort" id="ort" name="<?php echo $config[MailInOne][Mapping][CITY]; ?>" placeholder="*Ort" type="text" required  
                                               <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                    </div>
                                </div>       
                            </div>

                            <!-- INPUT EMAIL -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <input value="<?php echo ((!empty($email_002) ? $email_002 : $_SESSION['data'][$config[MailInOne][Mapping][EMAIL]])); ?>" class="input-fields form-control email" id="email" name="<?php echo $config[MailInOne][Mapping][EMAIL]; ?>" placeholder="*E-Mail-Adresse" type="email" required 
                                               <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                    </div>
                                    <?php if (($_SESSION['error'] === "email") && !empty($_SESSION['error_msg'])) { ?>   
                                        <span class="p-light erroremail"  style="float: left; padding-bottom:15px; color:<?php echo $config[ErrorHandling][ErrorMsg_Color]; ?>"><?php echo $_SESSION['error_msg']; ?></span>
                                    <?php } ?>  										
                                </div>       
                            </div>

                            <!-- INPUT TELEFON -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <input value="<?php echo ((!empty($custom_003) ? $custom_003 : $_SESSION['data'][$config[MailInOne][Mapping][Telefon]])); ?>" class="input-fields form-control" id="telefon"  placeholder="*Telefonnummer" type="text" required <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?> >

                                               <input value="<?php echo $_SESSION['data'][$config[MailInOne][Mapping][Telefon]] ?>" class="input-fields form-control telefon" id="tel2" name="<?php echo $config[MailInOne][Mapping][Telefon]; ?>" placeholder="*Telefonnummer" type="text" hidden >   

                                    </div>
                                    <span class="p-light errortelefon" style="color:<?php echo $config[ErrorHandling][ErrorMsg_Color]; ?> ;float:left; padding-bottom:15px"><?php echo $config[ErrorHandling][ErrorMsgTelefon]; ?></span>
                                </div>       
                            </div>

                            <label class="form-group">
                                <div class="col-sm-12">
                                    <input style="width: 20px; height: 20px;" type="checkbox" required>
                                    <span class="p-dark">&nbsp;&nbsp;Ich erkläre mich mit den <a target="_blank" href="https://www.stimawell-ems.de/agb">AGB</a>, <a target="_blank" href="https://www.stimawell-ems.de/de/mietbedingungen">Mietbedingungen </a>und <a target="_blank" href="https://www.stimawell-ems.de/datenschutz">Datenschutz </a>von Schwa-Medico Medizinische Apparate Vertriebsgesellschaft mbH einverstanden. Vertragslaufzeit beträgt 24 Monate.</span>
                                </div>    
                            </label>    

                            <!--Hidden Fields-->   
                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">URL</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][url]; ?>" value="<?php echo $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>">
                                </div>
                            </div>                                        

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Source</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_source]; ?>" value="<?php echo $_GET['utm_source'] ?>" >
                                </div>
                            </div>  

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Name</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_name]; ?>" value="<?php echo $_GET['utm_name'] ?>">
                                </div>
                            </div>      

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Term</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_term]; ?>" value="<?php echo $_GET['utm_term'] ?>">
                                </div>
                            </div>                             

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Content</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_content]; ?>" value="<?php echo $_GET['utm_content'] ?>">
                                </div>
                            </div>                              

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Medium</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_medium]; ?>" value="<?php echo $_GET['utm_medium'] ?>">
                                </div>
                            </div>    

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Campaign</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_campaign]; ?>" value="<?php echo $_GET['utm_campaign'] ?>">
                                </div>
                            </div>                                               

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">TRAFFICSOURCE</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][trafficsource]; ?>" id="ts" value="<?php echo $_GET['trafficsource'] ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Quelle</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][Quelle]; ?>" id="quelle" value="<?php echo $config[MailInOne][Constants][Quelle]; ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Typ</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" ame="<?php echo $config[MailInOne][Mapping][Typ]; ?>" id="" value="<?php echo $config[MailInOne][Constants][Typ]; ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Segment</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" ame="<?php echo $config[MailInOne][Mapping][Segment]; ?>" id="" value="<?php echo $config[MailInOne][Constants][Segment]; ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Test Mode</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="test_mode" id="" value="<?php echo $_GET['testmode']; ?>">
                                </div>
                            </div>
                            <div class="form-group"> 
                                <div class="col-sm-12">
                                    <input style="margin-top: 25px; cursor: pointer; background-color: rgb(0, 162, 219); border-color: rgb(0, 162, 219);" type="submit" class="col-sm-12 btn btn-danger btn-xl js-scroll-trigger" value="JETZT KAUFEN" name="submit" style="width: 100%;">
                                </div>        
                            </div>
                            <div class="form-group"> 
                                <div class="col-sm-12">
                                    <p>Hast Du Fragen zum Angebot? Unsere Experten stehen Dir zur Verfügung. Rufe uns an unter <strong>06443 4369914</strong>.</p>
                                </div>        
                            </div>   
                            <?php if (isset($response) && $response->isSuccess()) { ?>
                                <div class="alert alert-success fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Subscription successful</strong>
                                </div>
                            <?php } elseif (isset($warning)) { ?>
                                <div class="alert alert-warning fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong style="color: red; z-index: 1000;">Subscription failed</strong>
                                    <?= $warning['message'] ?>
                                </div>
                            <?php } elseif (isset($response)) { ?>
                                <div class="alert alert-danger fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Subscription failed</strong>
                                </div>
                            <?php } ?>
                        </form>          


                    </div>
                </div>
            </div>  
        </section>


        <section class="info-content">
            <div class="container">
                <div class="row">
                    <center>
                        <div class="col-sm-12 col-md-10">
                            <h2 style="color: #EEE">Was ist EMS-Training?</h2>
                            <hr style="border-color: #EEE;">
                            <h5 style="color: rgb(0, 162, 219);">Das innovative Training – mit minimalen Zeiteinsatz ein maximales Ergebnis erreichen</h5>
                            <br>
                            <p style="text-align: left; color: #EEE;">Dass elektrische Muskelstimulation eine hervorragende Methode in Puncto Schmerztherapie ist, die sich bereits in zahlreichen Kliniken fest etabliert hat, ist seit vielen Jahren bewiesen. Mit der Zeit wurde EMS auch als neue Ganzkörpertraining-Sportart immer bekannter. Die Muskeln werden durch kleine Stromimpulse trainiert. Mittlerweile zeigen jedoch zahlreiche Studien, dass mit EMS beste Ergebnisse erzielt werden können.</p>
                            <p style="text-align: left; color: #EEE;">Egal ob zur Stärkung der Rückenmuskulatur, zum Muskelaufbau, zur Verminderung von Cellulite, zum Anregen des Stoffwechsels, zur nachhaltigen Figurformung oder auch einfach zum Entspannen – EMS ist besonders effektiv und zeitsparend!</p>
                        </div>
                    </center>
                </div>
            </div>  
        </section>

        <section class="testimonials">
            <div class="container">
                <center>
                    <h2 style="color: #3c3b3b;">Das sagen unsere Kunden.</h2> 
                    <hr>
                    <p>Hier siehst du einige Erfahrungsberichte unserer Kunden und bei welchen Problemen ihnen FreeEMS von StimaWELL helfen konnte.</p>
                </center>
            </div>

            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="6"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="7"></li>
                </ol>
                <div class="carousel-inner">

                    <div class="carousel-item active">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links">
                                    <img class="img-fluid" src="img/testimonial_zeitmangel.png" alt="testimonial zeitmangel">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts">
                                    <h5>Du hast keine Zeit für herkömmlichen Sport mit langen Trainings?</h5>
                                    <p>Im Alltag sammeln sich viele To-Dos an. Auch Anne H., Mutter von zwei Kindern, fällt es nicht leicht, sich Zeit für die eigene Gesundheit zu nehmen. Mit der FREE EMS-Heimanwendung, hat sich das jedoch geändert. <i>„Endlich kann ich trainieren wo und wann ich will. Mit EMS finde ich endlich wieder Zeit dazu, meinen Körper in Form zu bringen und meinen Beckenboden nach der Geburt zu stärken“</i>, erzählt Anne.<br>&nbsp;</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links">
                                    <img class="img-fluid" src="img/testimonial_figur.png" alt="testimonial figur">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts">
                                    <h5>Du hast Probleme mit Deiner Figur?</h5>
                                    <p>Viele Mütter haben Schwierigkeiten, ihre hartnäckigen Fettpölsterchen aus der Schwangerschaft loszuwerden – so auch Birgitt N. Doch mittlerweile hat die Mutter von drei Kindern das EMS-Training für sich entdeckt: <i>„16 cm weniger Bauchumfang und 12 cm weniger am Po. Endlich habe ich wieder Kleidergröße S und fühle mich wohl in meinem Körper!“</i> Birgitts Erfolgsrezept? Zwei EMS-Einheiten pro Woche, aufgeteilt in ein Kraft- und ein Cardiotraining – ohne zusätzlichen Sport und ohne Ernährungsumstellung.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links">
                                    <img class="img-fluid" src="img/testimonial_ruecken.png" alt="testimonial rücken">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts">
                                    <h5>Du hast Probleme mit Deinem Rücken?</h5>
                                    <p>Rückenschmerzen sind die Volkskrankheit Nummer 1. Auch bei Claudia S., die an ihrem Arbeitsplatz lange Stehen muss, bahnten sich über die Jahre hinweg teilweise unerträgliche Rückenschmerzen an. Weder Massagen, noch Krankengymnastik oder Rückenschulen verhalfen Claudia zu einem langanhaltenden Erfolg. Erst als sie es mit einem Ganzkörper-EMS-Training versucht, verspürte sie eine erhebliche Verbesserung. <i>„Die Rückenschmerzen sind einfach verschwunden und auch seither nicht wieder aufgetaucht“</i>, erzählt Claudia.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links">
                                    <img class="img-fluid" src="img/testimonial_relax.png" alt="testimonial relax">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts">
                                    <h5>Du suchst Entspannung & Ausgleich vom Alltag?</h5>
                                    <p>Bei den meisten Trainings kommt eines oft zu kurz: die Entspannung. Doch nicht bei EMS! Für Christine F. ist es wichtig, neben den normalen EMS-Trainingseinheiten auch die Regenerationsprogramme zu nutzen. <i>„Mindestens einmal pro Woche gönne ich mir das Vergnügen der Entspannungs- und Massageprogramme – so schalte ich vom Alltag ab und vergesse jeglichen Stress“</i>, erzählt Christine. Und das Beste daran: Dafür muss sie nicht einmal die Haustür verlassen.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links">
                                    <img class="img-fluid" src="img/testimonial_cellulite.png" alt="testimonial cellulite">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts">
                                    <h5>Du hast Probleme mit Cellulite?</h5>
                                    <p><i>„Seit einigen Wochen trainiere ich nach dem speziellen EMS-Celluliteprogramm und mein Hautbild hat sich schon deutlich verbessert. Rechtzeitig zur Badesaison sind meine Oberschenkel und Hüfte endlich glatter“</i>, berichtet Dagmar K. Um Cellulite jedoch nachhaltig zu bekämpfen, reicht ein EMS-Training allein nicht aus. Denn gerade bei sogenannter „Orangenhaut“, spielt Ernährung eine wichtige Rolle. <i>„Daher habe ich mich auch an das EMS-Cellulite-Ernährungsprogramm gehalten – mit Erfolg“</i>, erzählt Dagmar.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links">
                                    <img class="img-fluid" src="img/testimonial_muskeln.png" alt="testimonial muskeln">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts">
                                    <h5>Du möchtest Muskeln aufbauen?</h5>
                                    <p>EMS-Ganzkörpertraining kann Dich genauso wie Markus T. auf Hochtouren befördern. <i>„Seit Jahren bringt mich fast ausschließlich EMS-Muskeltraining in Form. Und mit dem Ergebnis bin ich sehr zufrieden“</i>, berichtet er. Markus trainiert zweimal die Woche für jeweils 20 Minuten. Diese Zeit reicht aus, um seine Muskeln zu definieren und perfekt in Form zu bringen. <i>„Die Leistungsfähigkeit und die Qualität der Muskel-Formung sind bei diesem Training unschlagbar“</i>, erzählt Markus.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links">
                                    <img class="img-fluid" src="img/testimonial_beckenboden.png" alt="testimonial beckenboden">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts">
                                    <h5>Du suchst effektive Trainingsmethoden für den Beckenboden?</h5>
                                    <p>Grete S. lebte 40 Jahre lang mit einer Inkontinenz. Seitdem sie regelmäßig mit elektrischer Muskelstimulation trainiert, hat sich für sie einiges geändert: <i>„Ich fühle mich nicht mehr unsauber, kann meine Blase besser kontrollieren und kann endlich wieder ohne Beschwerden Treppen steigen! Das Training war das Beste, was mir je passiert ist und ich würde es jeder Frau empfehlen, die auch mit Inkontinenz zu kämpfen hat. Schade, dass es Ganzkörper-EMS nicht schon früher gab“</i>, erzählt Grete.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links">
                                    <img class="img-fluid" src="img/testimonial_stoffwechsel.png" alt="testimonial stoffwechsel">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts">
                                    <h5>Du hast Stoffwechselprobleme?</h5>
                                    <p>Als Bärbel P. ihrer Nikotinsucht den Kampf ansagte, nahm sie einiges an Gewicht zu. Jahrelang versuchte Bärbel alles, um ihre Extrakilos loszuwerden – leider ohne Erfolg. Doch mit EMS-Training fand sie endlich eine einfache Lösung für ihr Problem: <i>„Ich habe das EMS-Stoffwechselprogramm und die Stoffwechselkur gleich zweimal mit großem Erfolg durchgeführt. Und ich habe tatsächlich in nur wenigen Monaten 12 kg abgenommen – sogar ohne weiteren Sport!“</i>, erzählt Bärbel.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>


            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <center>
                            <a href="#form"><input style="margin-top: 25px; margin-bottom: 25px; cursor: pointer; background-color: rgb(0, 162, 219); border-color: rgb(0, 162, 219);" type="submit" class="col-sm-8 btn btn-danger btn-xl js-scroll-trigger" value="JETZT KAUFEN" style="width: 100%;"></a>
                        </center>   
                    </div>
                </div>
            </div>
        </section>  

        <section class="partner-container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 model-feature">       
                        <h2>Weil Fitness unkompliziert und bezahlbar sein soll.</h2>
                        <hr>
                        <p class="lead mb-0">Erlebe die neue, freie Art des EMS-Trainings. Erreiche deine Trainingsziele schneller und bequemer. Kombiniere die regelmäßige, persönliche Betreuung durch deinen EMS-Experten und dein eigenständiges EMS-Training mit dem virtuellen Trainer zu Hause oder unterwegs.</p><br>

                    </div>  
                    <div class="col-sm-6 text-center">
                        <img style="width: 350px; margin-top: 25px;" class="hidden-model" src="img/stimawell-model_1.png" alt="ems-training zu Hause"/>
                    </div>
                </div>
            </div>  
        </section>  

        <section class="partner-container" style="background-image: url('img/ems-bg.jpg');">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 text-center">
                        <img style="width: 350px; margin-top: 25px;" class="hidden-model" src="img/stimawell-model_3.png" alt="ems-training zu Hause"/>
                    </div>
                    <div  class="col-sm-6 model-feature">       
                        <h2>Vielseitige Anwendung um deine Ziele schneller zu erreichen.</h2>
                        <hr>
                        <p class="lead mb-0">Die EMS Technologie ermöglicht es dir Ziele verschiedenster Art schneller zu erreichen. Die Kombination aus persönlicher Betreuung durch deinen EMS-Experten und eigenständigem EMS-Training machen es möglich. Wo und wann du willst!</p><br>

                    </div>  
                </div>
            </div>  
        </section>  

        <section class="experte">
            <div class="container">
                <center>
                    <h2>Dein persönlicher Ansprechpartner und EMS-Experte <span style="color: #00a2db;">Dominic Heilig</span></h2>
                    <hr style="border-color: #00a2db;">
                </center>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-3" style="margin-top: 30px;">
                        <center>
                            <img src="img/ansprechpartner_dominic-heilig.png" alt="ems-experte dominic heilig" class="experte-img img-fluid">
                            <p class="experte-img-subtext" style="color: #00a2db; margin-top: 15px; margin-bottom: 0px;"><strong>DOMINIC HEILIG</strong></p>
                            <p class="experte-img-subtext"><i>StimaWELL-EMS Experte</i></p>
                        </center>
                    </div>
                    <div class="col-sm-12 col-md-9 experte-text-container">
                        <p class="experte-text">"Mein Name ist Dominic Heilig. Seit meinem 5. Lebensjahr treibe ich leidenschaftlich gerne Sport. Aus dieser Leidenschaft heraus resultiert mein Beruf.  Während meines Studiums zum Bachelor of Arts in Fitnessökonomie durchlief ich mehrere Stationen. Durch die Vielzahl an Stationen konnte ich ein umfassendes Fachwissen aufbauen, was ich gerne heute an meine Kunden und Partner weitergebe.</p>
                        <p class="experte-text">Durch meine Arbeit in zwei Microstudios habe ich täglich im Umgang mit den Kunden gelernt, welche Herausforderungen zu lösen sind, welche Ziele die Kunden haben und vor allem wie man diese Ziele am besten erreicht. Seit 2017 betreute ich für StimaWELL unsere Kunden für die Ganzkörper EMS Systeme. Meine Arbeit bringt mich mit vielen verschiedenen Kunden zusammen. Ich helfe sowohl Privatkunden als auch professionellen Anwendern wie Physiotherapeuten, Fitnessstudios, Sonnenstudios, Hebammen, Personal-Trainern, Heilpraktikern oder EMS-Studios. Mit meiner mehrjährigen Erfahrung im EMS-Training Bereich berate ich sehr gerne Existenzgründer und helfe diesen mit innovativen EMS Produkten von schwa-medico erfolgreich eine Existenz aufzubauen."</p>
                    </div>         
                </div>
            </div>  
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <center>
                            <a href="#form"><input style="margin-top: 40px; cursor: pointer; background-color: rgb(0, 162, 219); border-color: rgb(0, 162, 219);" type="submit" class="col-sm-8 btn btn-danger btn-xl js-scroll-trigger" value="JETZT KAUFEN" style="width: 100%;"></a>
                        </center>   
                    </div>
                </div>
            </div>
        </section>  



        <!-- MODAL -->
        <div class="modal fade" id="modal-video" tabindex="-1" role="dialog" aria-labelledby="modal-video-label">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="background-color: #1A1A1A;">
                    <div class="modal-header" style="border-bottom: none; padding-top: 9px;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="color: #FFF;">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="padding: 0px 0px 40px 0px;">
                        <div class="modal-video">
                            <center>
                                <iframe width="1920" height="1080" src="https://www.youtube.com/embed/18xXcSH0cY4?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow=" encrypted-media" allowfullscreen></iframe>
                                <form style="display: inline;" method = "post"  action="<?php echo $jetzt_kaufen; ?>"><button  data-dismiss="modal" style="margin-top: 15px; cursor: pointer; background-color: rgb(0, 162, 219); border-color: rgb(0, 162, 219);" type="button" class="col-sm-8 btn btn-danger">JETZT KAUFEN</button></form>
                            </center> 
                        </div>
                    </div>
                </div> 
            </div>
        </div>
        <!-- ./MODAL -->

        <section>
            <div class="clearspace"></div>
        </section>  

        <footer class="footer bg-light">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 h-100 text-center text-lg-left my-auto" style="height:auto!important">
                        <ul class="list-inline mb-2">
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/impressum" target="_blank">Impressum</a>
                            </li>
                            <li class="list-inline-item">&sdot;</li>
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/agb" target="_blank">AGB</a>
                            </li>
                            <li class="list-inline-item">&sdot;</li>
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/datenschutz" target="_blank">Datenschutz</a>
                            </li>
                            <li class="list-inline-item">&sdot;</li>
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/de/mietbedingungen" target="_blank">Mietbedingungen</a>
                            </li>
                        </ul>
                        <p style="padding-top:15px;" class="text-muted small mb-4 mb-lg-0"><strong>✔</strong> 40 JAHRE MADE IN GERMANY&nbsp;&nbsp;&nbsp;<strong>✔</strong> DIREKT VOM HERSTELLER&nbsp;&nbsp;&nbsp;<strong>✔</strong> SERVICE: 06443 4369914</p>
                    </div>
                    <div class="col-lg-4 h-100 text-center text-lg-right my-auto" style="height:auto!important">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item mr-3">
                                <a href="https://www.facebook.com/stimawellems/" target="_blank">
                                    <i class="fa fa-facebook fa-2x fa-fw"></i>
                                </a>
                            </li>
                            <li class="list-inline-item mr-3">
                                <a href="https://www.instagram.com/stimawell.ems/" target="_blank">
                                    <i class="fa fa-instagram fa-2x fa-fw"></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                </div>   
            </div>
        </footer>


        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="vendor/telefonvalidator-client/build/js/utils.js"></script>		
        <script type="text/javascript" src="vendor/telefonvalidator-client/build/js/intlTelInput.js"></script>	
        <script src="vendor/google/maps.js"></script>  
        <script type="text/javascript" src="vendor/jquery-cookie-master/src/jquery.cookie.js"></script>  


    </body>
</html>        

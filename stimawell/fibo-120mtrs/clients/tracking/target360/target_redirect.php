<?php

/*
 * Traget360 Tracking Snippet 
 * 
 * Integrated to redirect.php Page
 * 
*/
?>

<!-- oliro private network / Advertiser #48403 / Dynamic-Retargeting-Tag / Productfeed #14927 (Dummy) / Transaction-Event / generated Wed, 23 Jan 2019 10:44:21 +0100 -->
<script language="JavaScript" type="text/javascript">

    /* ALL COMMENTS IN THIS JAVASCRIPT-TAG MAY BE REMOVED */

    /*
     * Insert your items or products of productfeed #14927 (Dummy) in the parameter "tp_rtrgt_items".
     * This is a single-productfeed-tag, so you do not need to specify the productfeed-id individually for every item or product you pass.
     *   a) Without passing the product-quantity: ID,ID,...
     *   b) For better retargeting-results pass the product-quantity: ID|QUANTITY,ID|QUANTITY,...
     */

    var tp_rtrgt_items = '<?= $lead_reference ?>';

    /*
     * Optionally you can pass your predefined retargeting-segments to do 
     * both dynamic product-based and segmented retargeting.
     */

    var tp_rtrgt_segment = '';

    /* DO NOT change the javascript below */
    var tp_rtrgt_random = Math.random() * 10000000000000000;
    var tp_rtrgt_url = 'http' + (('https:' == document.location.protocol) ? 's' : '') + '://ad.ad-srv.net/retarget?a=48403&version=1&event=transaction&cat=14927';
    tp_rtrgt_url += '&segment=' + tp_rtrgt_segment;
    tp_rtrgt_url += '&items=' + tp_rtrgt_items;
    document.write('<div id="tp_rtrgt_div_' + tp_rtrgt_random + '" style="position:absolute; visibility:hidden; left:0px; top:0px; width:1px; height:1px; border:0"><iframe id="tp_rtrgt_iframe_' + tp_rtrgt_random + '" name="tp_rtrgt_iframe_' + tp_rtrgt_random + '" src="' + tp_rtrgt_url + '" scrolling="no" width="1" height="1"></iframe></div>');
</script>    
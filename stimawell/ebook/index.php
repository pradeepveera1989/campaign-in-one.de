<?php
require 'cio.php';
?>

<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Nutzen Sie unser neues FREE EMS® PROGRAMM und trainieren sie bequem von zu Hause aus.">
        <meta name="author" content="StimaWELL">

        <title>Das neue StimaWELL-EMS eBook</title>

        <link href="css/creative.css" rel="stylesheet" type="text/css">  
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">    
        <link rel="stylesheet" href="vendor/telefonvalidator-client/build/css/intlTelInput.css"/>
        <link rel="stylesheet" href="css/animate.css"/>
        
        <script type="text/javascript" src="vendor/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="vendor/telefonvalidator-client/build/js/utils.js"></script>
        <script type="text/javascript" src="vendor/telefonvalidator-client/build/js/intlTelInput.js"></script>
        <script type="text/javascript" src="vendor/google/maps.js"></script>
        <script type="text/javascript" src="vendor/jquery-cookie-master/src/jquery.cookie.js"></script>
        <script type="text/javascript">
            var countdown_status = "<?= $counter_status; ?>";
            var countdown_expire = "<?= $config[GeneralSetting][CountdownExpire]; ?>";
            var countdown_expire_period = "<?= $config[GeneralSetting][MaximumCountdownperiod]; ?>";
            var current_stocks = "<?= $current_count_value; ?>";
            var countdown_end_date = "<?= $current_end_date ?>";
            var autofill_postal_code = "<?= $config[Postal_Code][Autofill]; ?>";
            var localization_postal_code = "<?= $config[Postal_Code][Localization]; ?>"
            var countdown_expire_message = "<?= $config[GeneralSetting][CountdownExpireMessage] ?>";
            var validate_telefon = "<?= $config[Telefon][Status]; ?>";
            var single_submit = "<?= $config[GeneralSetting][SingleSubmit]; ?>"
            var single_submit_text = "<?= $single_submit_text ?>"
            var cookie = "<?= $cookie; ?>";
            var webgains_cookie = "<?= $webgains_cookie ?>";

        </script>
        <script language="JavaScript" type="text/javascript" src="cio.js"></script>          

        <!-- Google Analytics -->
        <?php include_once 'clients/tracking/google/google.php'; ?>        

        <!-- Facebook Pixel -->
        <?php include_once('clients/tracking/facebook/facebook.php'); ?>      

    </head>        

    <!-- Outbrain Tracking -->
    <?php $config[TrackingTools][EnableOutbrain] ? include_once('clients/tracking/outbrain/outbrain_index.php') : " "; ?>

    <body>
        <!-- Web Gains Tracking -->
        <?php $config[TrackingTools][EnableWebGains] ? include_once 'clients/tracking/webgains/webgains_index.php' : " "; ?>

        <!-- Remarketing Target360 Tracking -->
        <?php $config[TrackingTools][EnableTarget360] ? include_once 'clients/tracking/target360/target_index.php' : ' '; ?>  

        <!-- DESKTOP NAV -->
        <nav class="navbar navbar-light bg-light static-top">   
            <div class="container">
                <a class="navbar-brand" style="text-transform: uppercase;" href="https://www.stimawell-ems.de/" target="_blank"><img style="width:200px;" alt="EMS-Training-zuhause" src="img/logo.svg"></a>
                <span class="navbar-details"style="text-transform:uppercase; "><i class="fas fa-check"></i> 40 JAHRE ERFAHRUNG&nbsp;&nbsp;<i class="fas fa-check"></i> SERVICE-HOTLINE BEI FRAGEN: <strong>06443 4369914</strong></span>
            </div>
        </nav>

        <section>
            <div class="clearspace"></div>
        </section> 

        <section class="form-container">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-6" style="padding-top: 50px;">
                        <div>
                            <center>
                                <img class="img-fluid animated bounceInLeft titelbild" src="img/tablet_ebook_web.png" alt="ebook"/>
                            </center>
                        </div>
                    </div>  
                    <div class="col-lg-6 check-submit-form" id="form" style="padding-top: 50px; padding-bottom: 20px;">     
                        <form id="api-data-form" action="<?= $_SERVER['PHP_SELF'] ?>" method="post" accept-charset="utf-8" class="form-horizontal" role="form">
                            <input type="hidden" name="contactid" value="<?= $contactid ?>">
                            <input type="hidden" name="checksum" value="<?= $checksum ?>">
                            <input type="hidden" name="mailingid" value="<?= $mailingid ?>">

                            <!-- INPUT ANREDE FRAU & HERR -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="btn-group">
                                        <?php
                                        if ($_SESSION['data']['anrede'] === "Frau") {
                                            $frau = "checked";
                                        } else {
                                            $herr = "checked";
                                        }
                                        ?>										
                                        <label class="btn btn-outline-secondary" style="border: 1px solid transparent;">
                                            <input value="Frau" style="width: 15px; height: 15px;" type="radio" name="<?php echo $config[MailInOne][Mapping][SALUTATION]; ?>" autocomplete="off" required<?php echo $frau; ?>> Frau
                                        </label>
                                        <span class="input-group-btn" style="width:15px;"></span> 
                                        <label class="btn btn-outline-secondary" style="border: 1px solid transparent;">
                                            <input value="Herr" style="width: 15px; height: 15px;" type="radio" name="<?php echo $config[MailInOne][Mapping][SALUTATION]; ?>" autocomplete="off" <?php echo $herr ?>> 
                                            Herr
                                        </label>
                                    </div>  
                                </div>       
                            </div>

                            <!-- INPUT VORNAME -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">

                                        <input value="<?php echo ((!empty($standard_001) ? $standard_001 : $_SESSION['data']['vorname'])); ?>" class="input-fields form-control" id="" name="<?php echo $config[MailInOne][Mapping][FIRSTNAME]; ?>" placeholder="*Vorname" type="text" required 
                                               <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>      
                                    </div>
                                </div>       
                            </div>


                            <!-- INPUT NACHNAME -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">

                                        <input  value="<?php echo ((!empty($standard_002) ? $standard_002 : $_SESSION['data']['nachname'])); ?>" class="input-fields form-control" id="nachname" name="<?php echo $config[MailInOne][Mapping][LASTNAME]; ?>" placeholder="*Nachname" type="text" required
                                                <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                    </div>
                                </div>       
                            </div>


                            <!-- INPUT PLZ -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <input value="<?php echo ((!empty($standard_004) ? $standard_004 : $_SESSION['data']['plz'])); ?>"  class="input-fields form-control plz" id="plz" name="<?php echo $config[MailInOne][Mapping][ZIP]; ?>" placeholder="*PLZ" type="number" required 
                                               <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                    </div>
                                    <span class="p-light errorplz" style="color:<?php echo $config[ErrorHandling][ErrorMsg_Color]; ?>"><?php echo $config[ErrorHandling][ErrorMsgPostalcode]; ?></span>
                                </div>       
                            </div>

                            <!-- INPUT ORT -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <input value="<?php echo ((!empty($standard_005) ? $standard_005 : $_SESSION['data']['ort'])); ?>" class="input-fields form-control ort" id="ort" name="<?php echo $config[MailInOne][Mapping][CITY]; ?>" placeholder="*Ort" type="text" required  
                                               <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                    </div>
                                </div>       
                            </div>

                            <!-- INPUT EMAIL -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <input value="<?php echo ((!empty($email_002) ? $email_002 : $_SESSION['data']['email'])); ?>" class="input-fields form-control email" id="email" name="<?php echo $config[MailInOne][Mapping][EMAIL]; ?>" placeholder="*E-Mail-Adresse" type="email" required 
                                               <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                    </div>
                                <?php if (($_SESSION['error'] === "email") && !empty($_SESSION['error_msg'])) { ?>   
                                    <span class="p-light erroremail"  style="float: left; padding-bottom:15px; color:<?php echo $config[ErrorHandling][ErrorMsg_Color]; ?>"><?php echo $_SESSION['error_msg']; ?></span>
                                <?php } ?>										
                                </div>       
                            </div>

                            <label class="form-group">
                                <div class="col-sm-12">
                                    <input style="width: 20px; height: 20px;" type="checkbox" required>
                                    <span class="p-dark">&nbsp;&nbsp;Ich bin einverstanden, dass die Schwa-Medico Medizinische Apparate Vertriebsgesellschaft mbH, Gehrnstraße 4, 35630 Ehringhausen, meine eingetragenen Daten nutzt und mir E-Mails zuschickt bzw. mich anruft, um mir Angebote aus dem Bereich EMS-Training (Zubehör, Training, Fachliteratur) zukommen zu lassen. Diese Einwilligung kann ich jederzeit widerrufen, etwa durch einen Brief an die oben genannte Adresse oder durch eine E-Mail an <a href="mailto:widerruf@schwa-medico.de">widerruf@schwa-medico.de</a>. Anschließend wird jede werbliche Nutzung unterbleiben.</span>
                                </div>    
                            </label>

                            <!--Hidden Fields-->   
                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">URL</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][url]; ?>" value="<?php echo $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>">
                                </div>
                            </div>                                        

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Source</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_source]; ?>" value="<?php echo $_GET['utm_source'] ?>" >
                                </div>
                            </div>  

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Name</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_name]; ?>" value="<?php echo $_GET['utm_name'] ?>">
                                </div>
                            </div>      

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Term</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_term]; ?>" value="<?php echo $_GET['utm_term'] ?>">
                                </div>
                            </div>                             

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Content</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_content]; ?>" value="<?php echo $_GET['utm_content'] ?>">
                                </div>
                            </div>                              

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Medium</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_medium]; ?>" value="<?php echo $_GET['utm_medium'] ?>">
                                </div>
                            </div>    

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Campaign</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_campaign]; ?>" value="<?php echo $_GET['utm_campaign'] ?>">
                                </div>
                            </div>                                               

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">TRAFFICSOURCE</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][trafficsource]; ?>" id="ts" value="<?php echo $_GET['trafficsource'] ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Quelle</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][Quelle]; ?>" id="quelle" value="<?php echo $config[MailInOne][Constants][Quelle]; ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Typ</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" ame="<?php echo $config[MailInOne][Mapping][typ]; ?>" id="" value="<?php echo $config[MailInOne][Constants][typ]; ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Segment</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" ame="<?php echo $config[MailInOne][Mapping][Segment]; ?>" id="" value="<?php echo $config[MailInOne][Constants][Segment]; ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Test Mode</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="test_mode" id="" value="<?php echo $_GET['testmode']; ?>">
                                </div>
                            </div>

                            <div class="form-group"> 
                                <div class="col-sm-12">
                                    <input style="margin-top: 25px; cursor: pointer; background-color: rgb(0, 162, 219); border-color: rgb(0, 162, 219);" type="submit" class="col-sm-12 btn btn-danger btn-xl js-scroll-trigger single-submit" value="JETZT HERUNTERLADEN" name="submit" style="width: 100%;">
                                </div>        
                            </div>
                            <?php if (isset($response) && $response->isSuccess()) { ?>
                                <div class="alert alert-success fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Subscription successful</strong>
                                </div>
                            <?php } elseif (isset($warning)) { ?>
                                <div class="alert alert-warning fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong style="color: red; z-index: 1000;">Subscription failed</strong>
                                    <?= $warning['message'] ?>
                                </div>
                            <?php } elseif (isset($response)) { ?>
                                <div class="alert alert-danger fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Subscription failed</strong>
                                </div>
                            <?php } ?>
                        </form>          


                    </div>
                </div>
            </div>  
        </section>

        <section class="mehrwerte">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-4">
                        <p class="icon"><i class="fa fa-4x fa-info-circle"></i></p>
                        <p class="icon-text">Spannende Informationen rund um das Thema EMS und die Vorteile dieses innovativen Trainigs</p>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <p class="icon"><i class="fa fa-4x fa-chart-bar"></i></p>
                        <p class="icon-text">Wissenschaftliche Studien und Belege über Wirkungsweise und Wirksamkeit von EMS-Training</p>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <p class="icon"><i class="fa fa-4x fa-comments"></i></p>
                        <p class="icon-text">Antworten auf häufig gestellte Fragen in Form eines ausführlichen FAQs</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="problemfelder">
            <div class="container">
                <center>
                    <h2 style="color:#3c3b3b;">Praxisbezogene Informationen zu EMS-Training</h2> 
                    <hr style="border-color: #00a2db;">
                    <p style="margin:0px;">Detaillierte und aufschlussreiche Informationen, wie dir EMS-Training mit den folgenden Problemen helfen kann, findest du im eBook.</p>
                </center>
            </div>

            <div class="container">
                <div class="row">

                    <div class="col-md-12 col-lg-6">
                        <!-- Figur -->
                        <div class="problemfeld">
                            <div class="problem-icon">
                                <img class="problem-icon-img" src="img/claim_figur.jpg">
                            </div>
                            <div class="problem-text">
                                <h4>Figur</h4>
                                <p>Manche Körperstellen wie Taille und Hüfte sind mit herkömmlichem Training nur schwer zu erreichen. Mit Ganzkörper-EMS-Training hingegen, trainierst Du nicht nur schwer zugängliche Problemzonen, sondern fast alle Muskelgruppen gleichzeitig – für maximale Ergebnisse!</p>
                            </div>
                        </div>
                        <!-- Muskeln -->
                        <div class="problemfeld">
                            <div class="problem-icon">
                                <img class="problem-icon-img" src="img/claim_muskeln.jpg">
                            </div>
                            <div class="problem-text">
                                <h4>Muskelaufbau</h4>
                                <p>Trainierte Muskeln formen Deinen Körper und bringen Dich auf die Überholspur. Wie gut, dass elektrische Muskelstimulation nicht nur einige bestimmte Muskeln, sondern gleich alle Haupt-Muskelgruppen gleichzeitig aufbaut!</p>
                            </div>
                        </div>
                        <!-- Cellulite -->
                        <div class="problemfeld">
                            <div class="problem-icon">
                                <img class="problem-icon-img" src="img/claim_cellulite.jpg">
                            </div>
                            <div class="problem-text">
                                <h4>Cellulite</h4>
                                <p>Ist Cellulite erst einmal aufgetaucht, verschwindet sie nur schwer wieder. Doch mit der richtigen Ernährung und kleinen elektrischen Impulsen bis in tiefliegende Muskeln, kannst Du schnell eine deutliche Verbesserung Deines Hautbildes erkennen. </p>
                            </div>
                        </div>
                        <!-- Beckenboden -->
                        <div class="problemfeld">
                            <div class="problem-icon">
                                <img class="problem-icon-img" src="img/claim_beckenboden.jpg">
                            </div>
                            <div class="problem-text">
                                <h4>Beckenboden</h4>
                                <p>So ein geschwächter Beckenboden kann einem ziemlich die Laune verderben. Egal ob frisch gebackene Mütter oder Menschen mit Inkontinenz – mit EMS-Training kannst Du Deine Blasenschwäche bekämpfen und sogar Deine Sexualität verbessern!</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-6">
                        <!-- Rücken -->
                        <div class="problemfeld">
                            <div class="problem-icon">
                                <img class="problem-icon-img" src="img/claim_ruecken.jpg">
                            </div>
                            <div class="problem-text">
                                <h4>Rücken</h4>
                                <p>Wenn die Schmerzen erst einmal da sind, gehen sie in den meisten Fällen auch nicht von alleine wieder weg. Mit einer guten Rückenmuskulatur kannst Du jedoch Rückenproblemen vorbeugen. Außerdem können die elektrischen Impulse Deine Schmerzen lindern!</p>
                            </div>
                        </div>
                        <!-- Stoffwechsel -->
                        <div class="problemfeld">
                            <div class="problem-icon">
                                <img class="problem-icon-img" src="img/claim_stoffwechsel.jpg">
                            </div>
                            <div class="problem-text">
                                <h4>Stoffwechsel</h4>
                                <p>Ein besonders aktiver Stoffwechsel kann durch erhöhten Kalorienumsatz mit dem Abnehmen helfen. Auch Du kannst Deinen Stoffwechsel ankurbeln: StimaWELL®-EMS hält genau das passende Stoffwechselprogramm inklusive Stoffwechselkur für Dich bereit!</p>
                            </div>
                        </div>
                        <!-- Relax -->
                        <div class="problemfeld">
                            <div class="problem-icon">
                                <img class="problem-icon-img" src="img/claim_relax.jpg">
                            </div>
                            <div class="problem-text">
                                <h4>Entspannung</h4>
                                <p>Mit EMS kannst du nicht nur trainieren, sondern Dich mit den Massage- und Erholungsprogrammen auch wann immer Du nur willst entspannen und verwöhnen lassen. Dein StimaWELL®-EMS-Paket ist fast wie Dein eigener kleiner Masseur.</p>
                            </div>
                        </div>
                        <!-- Zeit -->
                        <div class="problemfeld">
                            <div class="problem-icon">
                                <img class="problem-icon-img" src="img/claim_zeit.jpg">
                            </div>
                            <div class="problem-text">
                                <h4>Trainingszeit</h4>
                                <p>Wenn ein Termin den nächsten jagt, ist es oft schwer Zeit zu finden, auch nur die Sporttasche zu packen. Mit StimaWELL®-EMS kannst Du Dein Training so flexibel gestalten wie Du möchtest und kleine Zeitfenster in Deinem vollen Terminkalender optimal nutzen.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <center>
                            <a href="#form"><input style="margin-top: 60px; cursor: pointer; background-color: rgb(0, 162, 219); border-color: rgb(0, 162, 219);" type="submit" class="col-sm-8 btn btn-danger btn-xl js-scroll-trigger" value="JETZT HERUNTERLADEN" style="width: 100%;"></a>
                        </center>   
                    </div>
                </div>
            </div>
        </section>

        <section class="testimonials">
            <div class="container">
                <center>
                    <h2 style="color: #3c3b3b;">Das sagen unsere Kunden</h2> 
                    <hr style="border-color:#169FDB">
                    <p>Hier siehst du einige Erfahrungsberichte unserer Kunden und bei welchen Problemen ihnen FreeEMS von StimaWELL helfen konnte.</p>
                </center>
            </div>

            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="6"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="7"></li>
                </ol>
                <div class="carousel-inner">

                    <div class="carousel-item active">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links">
                                    <img class="img-fluid" src="img/testimonial_zeitmangel.png" alt="testimonial zeitmangel">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts">
                                    <h5>Du hast keine Zeit für herkömmlichen Sport mit langen Trainings?</h5>
                                    <p>Im Alltag sammeln sich viele To-Dos an. Auch Anne H., Mutter von zwei Kindern, fällt es nicht leicht, sich Zeit für die eigene Gesundheit zu nehmen. Mit der FREE EMS-Heimanwendung, hat sich das jedoch geändert. <i>„Endlich kann ich trainieren wo und wann ich will. Mit EMS finde ich endlich wieder Zeit dazu, meinen Körper in Form zu bringen und meinen Beckenboden nach der Geburt zu stärken“</i>, erzählt Anne.<br>&nbsp;</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links">
                                    <img class="img-fluid" src="img/testimonial_figur.png" alt="testimonial figur">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts">
                                    <h5>Du hast Probleme mit Deiner Figur?</h5>
                                    <p>Viele Mütter haben Schwierigkeiten, ihre hartnäckigen Fettpölsterchen aus der Schwangerschaft loszuwerden – so auch Birgitt N. Doch mittlerweile hat die Mutter von drei Kindern das EMS-Training für sich entdeckt: <i>„16 cm weniger Bauchumfang und 12 cm weniger am Po. Endlich habe ich wieder Kleidergröße S und fühle mich wohl in meinem Körper!“</i> Birgitts Erfolgsrezept? Zwei EMS-Einheiten pro Woche, aufgeteilt in ein Kraft- und ein Cardiotraining – ohne zusätzlichen Sport und ohne Ernährungsumstellung.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links">
                                    <img class="img-fluid" src="img/testimonial_ruecken.png" alt="testimonial rücken">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts">
                                    <h5>Du hast Probleme mit Deinem Rücken?</h5>
                                    <p>Rückenschmerzen sind die Volkskrankheit Nummer 1. Auch bei Claudia S., die an ihrem Arbeitsplatz lange Stehen muss, bahnten sich über die Jahre hinweg teilweise unerträgliche Rückenschmerzen an. Weder Massagen, noch Krankengymnastik oder Rückenschulen verhalfen Claudia zu einem langanhaltenden Erfolg. Erst als sie es mit einem Ganzkörper-EMS-Training versucht, verspürte sie eine erhebliche Verbesserung. <i>„Die Rückenschmerzen sind einfach verschwunden und auch seither nicht wieder aufgetaucht“</i>, erzählt Claudia.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links">
                                    <img class="img-fluid" src="img/testimonial_relax.png" alt="testimonial relax">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts">
                                    <h5>Du suchst Entspannung & Ausgleich vom Alltag?</h5>
                                    <p>Bei den meisten Trainings kommt eines oft zu kurz: die Entspannung. Doch nicht bei EMS! Für Christine F. ist es wichtig, neben den normalen EMS-Trainingseinheiten auch die Regenerationsprogramme zu nutzen. <i>„Mindestens einmal pro Woche gönne ich mir das Vergnügen der Entspannungs- und Massageprogramme – so schalte ich vom Alltag ab und vergesse jeglichen Stress“</i>, erzählt Christine. Und das Beste daran: Dafür muss sie nicht einmal die Haustür verlassen.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links">
                                    <img class="img-fluid" src="img/testimonial_cellulite.png" alt="testimonial cellulite">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts">
                                    <h5>Du hast Probleme mit Cellulite?</h5>
                                    <p><i>„Seit einigen Wochen trainiere ich nach dem speziellen EMS-Celluliteprogramm und mein Hautbild hat sich schon deutlich verbessert. Rechtzeitig zur Badesaison sind meine Oberschenkel und Hüfte endlich glatter“</i>, berichtet Dagmar K. Um Cellulite jedoch nachhaltig zu bekämpfen, reicht ein EMS-Training allein nicht aus. Denn gerade bei sogenannter „Orangenhaut“, spielt Ernährung eine wichtige Rolle. <i>„Daher habe ich mich auch an das EMS-Cellulite-Ernährungsprogramm gehalten – mit Erfolg“</i>, erzählt Dagmar.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links">
                                    <img class="img-fluid" src="img/testimonial_muskeln.png" alt="testimonial muskeln">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts">
                                    <h5>Du möchtest Muskeln aufbauen?</h5>
                                    <p>EMS-Ganzkörpertraining kann Dich genauso wie Markus T. auf Hochtouren befördern. <i>„Seit Jahren bringt mich fast ausschließlich EMS-Muskeltraining in Form. Und mit dem Ergebnis bin ich sehr zufrieden“</i>, berichtet er. Markus trainiert zweimal die Woche für jeweils 20 Minuten. Diese Zeit reicht aus, um seine Muskeln zu definieren und perfekt in Form zu bringen. <i>„Die Leistungsfähigkeit und die Qualität der Muskel-Formung sind bei diesem Training unschlagbar“</i>, erzählt Markus.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links">
                                    <img class="img-fluid" src="img/testimonial_beckenboden.png" alt="testimonial beckenboden">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts">
                                    <h5>Du suchst effektive Trainingsmethoden für den Beckenboden?</h5>
                                    <p>Grete S. lebte 40 Jahre lang mit einer Inkontinenz. Seitdem sie regelmäßig mit elektrischer Muskelstimulation trainiert, hat sich für sie einiges geändert: <i>„Ich fühle mich nicht mehr unsauber, kann meine Blase besser kontrollieren und kann endlich wieder ohne Beschwerden Treppen steigen! Das Training war das Beste, was mir je passiert ist und ich würde es jeder Frau empfehlen, die auch mit Inkontinenz zu kämpfen hat. Schade, dass es Ganzkörper-EMS nicht schon früher gab“</i>, erzählt Grete.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links">
                                    <img class="img-fluid" src="img/testimonial_stoffwechsel.png" alt="testimonial stoffwechsel">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts">
                                    <h5>Du hast Stoffwechselprobleme?</h5>
                                    <p>Als Bärbel P. ihrer Nikotinsucht den Kampf ansagte, nahm sie einiges an Gewicht zu. Jahrelang versuchte Bärbel alles, um ihre Extrakilos loszuwerden – leider ohne Erfolg. Doch mit EMS-Training fand sie endlich eine einfache Lösung für ihr Problem: <i>„Ich habe das EMS-Stoffwechselprogramm und die Stoffwechselkur gleich zweimal mit großem Erfolg durchgeführt. Und ich habe tatsächlich in nur wenigen Monaten 12 kg abgenommen – sogar ohne weiteren Sport!“</i>, erzählt Bärbel.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>


            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <center>
                            <a href="#form"><input style="margin-top: 25px; margin-bottom: 10px; cursor: pointer; background-color: rgb(0, 162, 219); border-color: rgb(0, 162, 219);" type="submit" class="col-sm-8 btn btn-danger btn-xl js-scroll-trigger" value="JETZT HERUNTERLADEN" style="width: 100%;"></a>
                        </center>   
                    </div>
                </div>
            </div>
        </section>

        <section class="experte">
            <div class="container">
                <center>
                    <h2>Dein persönlicher Ansprechpartner und EMS-Experte <span style="color: #00a2db;">Dominic Heilig</span></h2>
                    <hr style="border-color: #00a2db;">
                </center>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-3" style="margin-top: 30px;">
                        <center>
                            <img src="img/stimawell-dominic-heilig-neu.jpg" alt="ems-experte dominic heilig" class="experte-img rounded-circle img-fluid">
                            <p class="experte-img-subtext" style="color: #00a2db; margin-top: 15px; margin-bottom: 0px;"><strong>DOMINIC HEILIG</strong></p>
                            <p class="experte-img-subtext"><i>StimaWELL-EMS Experte</i></p>
                        </center>
                    </div>
                    <div class="col-sm-12 col-md-9 experte-text-container">
                        <p class="experte-text">"Mein Name ist Dominic Heilig. Seit meinem 5. Lebensjahr treibe ich leidenschaftlich gerne Sport. Aus dieser Leidenschaft heraus resultiert mein Beruf.  Während meines Studiums zum Bachelor of Arts in Fitnessökonomie durchlief ich mehrere Stationen. Durch die Vielzahl an Stationen konnte ich ein umfassendes Fachwissen aufbauen, was ich gerne heute an meine Kunden und Partner weitergebe.</p>
                        <p class="experte-text">Durch meine Arbeit in zwei Microstudios habe ich täglich im Umgang mit den Kunden gelernt, welche Herausforderungen zu lösen sind, welche Ziele die Kunden haben und vor allem wie man diese Ziele am besten erreicht. Seit 2017 betreute ich für StimaWELL unsere Kunden für die Ganzkörper EMS Systeme. Meine Arbeit bringt mich mit vielen verschiedenen Kunden zusammen. Ich helfe sowohl Privatkunden als auch professionellen Anwendern wie Physiotherapeuten, Fitnessstudios, Sonnenstudios, Hebammen, Personal-Trainern, Heilpraktikern oder EMS-Studios. Mit meiner mehrjährigen Erfahrung im EMS-Training Bereich berate ich sehr gerne Existenzgründer und helfe diesen mit innovativen EMS Produkten von schwa-medico erfolgreich eine Existenz aufzubauen."</p>
                    </div>         
                </div>
            </div>  
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <center>
                            <a href="#form"><input style="margin-top: 40px; cursor: pointer; background-color: rgb(0, 162, 219); border-color: rgb(0, 162, 219);" type="submit" class="col-sm-8 btn btn-danger btn-xl js-scroll-trigger" value="JETZT HERUNTERLADEN" style="width: 100%;"></a>
                        </center>   
                    </div>
                </div>
            </div>
        </section>

        <section class="stimawell">
            <div class="container">
                <div class="row">
                    <center>
                        <div class="col-sm-12 col-md-8">
                            <img class="img-fluid" src="img/logo_weiss.png" style="max-width: 280px;" alt="stimawell-logo">
                            <p style="margin-top: 30px;">StimaWELL ist eine Marke der schwa-medico GmbH. Wir begleiten seit vielen Jahren die Vermarktung von EMS – erst als portable Geräte und seit 2010 als Variante der Ganzkörperstimulation. schwa-medico verkauft seit 1975 Elektrotherapiegeräte. Seit 1980 entwickeln und produzieren wir eigene Nerven- und Muskelstimulatoren. Wir blicken als Händler und Hersteller von elektrotherapeutischen Geräten auf 43 Jahre Erfahrung zurück. Wir sind täglich bemüht, zusammen mit unserer Belegschaft unsere Kunden mit qualitativ hochwertigen und preisattraktiven EMS Geräten zu versorgen. Es ist uns besonders wichtig, die Sicherheit des Anwenders zu gewährleisten. Aus diesem Grund achten wir auf strenge aber sichere Entwicklungs- und Herstellungsprozesse, um unsere EMS-Geräte als zugelassene Medizinprodukte anbieten zu können. Die vielen positiven Erfahrungen unserer Studios, Anwender, Sportler, medizinischen Fachkräften stärken täglich unser Stolz für die StimaWELL Marke.</p>
                        </div>
                    </center>
                </div>
            </div>
        </section>

        <footer class="footer bg-light">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 h-100 text-center text-lg-left my-auto" style="height:auto!important">
                        <ul class="list-inline mb-2">
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/impressum" target="_blank">Impressum</a>
                            </li>
                            <li class="list-inline-item">&sdot;</li>
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/agb" target="_blank">AGB</a>
                            </li>
                            <li class="list-inline-item">&sdot;</li>
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/datenschutz" target="_blank">Datenschutz</a>
                            </li>
                        </ul>
                        <p style="padding-top:15px;" class="text-muted small mb-4 mb-lg-0"><strong>✔</strong> 40 JAHRE ERFAHRUNG&nbsp;&nbsp;&nbsp;<strong>✔</strong> SERVICE-HOTLINE BEI FRAGEN: 06443 4369914</p>
                    </div>
                    <div class="col-lg-4 h-100 text-center text-lg-right my-auto" style="height:auto!important">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item mr-3">
                                <a href="https://www.facebook.com/stimawellems/" target="_blank">
                                    <i class="fa fa-facebook fa-2x fa-fw"></i>
                                </a>
                            </li>
                            <li class="list-inline-item mr-3">
                                <a href="https://www.instagram.com/stimawell.ems/" target="_blank">
                                    <i class="fa fa-instagram fa-2x fa-fw"></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                </div>   
            </div>
        </footer>
    </body>
</html>

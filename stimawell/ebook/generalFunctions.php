<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of generalSettings
 *
 * @author Pradeep
 */
class generalFunctions {
    //put your code here
    public $get_params;
    
#    function __construct($text){
#        $this->text = $text;
#    }

    public function checkUTMParamsInPost($post_params){
        
        if(empty($post_params)){
            return false;
        }

        if(array_key_exists("trafficsource", $post_params) && empty($post_params["trafficsource"])){

            $post_params["trafficsource"] = $_SESSION['trafficsource'];
        }          
        
        //utm_term
        if(array_key_exists("utm_term", $post_params) && empty($post_params["utm_term"])){
            $post_params["utm_term"] = $_SESSION['utm_term'];
        }  
        
        //utm_name
        if(array_key_exists("utm_name", $post_params) && empty($post_params["utm_name"])){
            $post_params["utm_name"] = $_SESSION['utm_name'];
        }
        
        //utm_medium
        if(array_key_exists("utm_content", $post_params) && empty($post_params["utm_content"])){
            $post_params["utm_content"] = $_SESSION['utm_content'];
        }
        
        //utm_medium
        if(array_key_exists("utm_medium", $post_params) && empty($post_params["utm_medium"])){
            $post_params["utm_medium"] = $_SESSION['utm_medium'];
        }

        //utm_source
        if(array_key_exists("utm_source", $post_params) && empty($post_params["utm_source"])){
            $post_params["utm_source"] = $_SESSION['utm_source'];
        }     
        
        //utm_source
        if(array_key_exists("utm_campaign", $post_params) && empty($post_params["utm_campaign"])){
            $post_params["utm_campaign"] = $_SESSION['utm_campaign'];
        }         

        return $post_params;
    }
    
    public function checkSession(){
        
        if(empty($_SESSION)){
            
            return false;
        }       
        return true;
    }
    
    public function setCookieAsEmail($email){
        
        if(empty($email)){
            return false;
        }
        $cookie = $email;
        setcookie("campaign", $cookie);   
    }    
    
    public function setCookieForWebgains($source){

        if(empty($source)){
            return false;
        }
        $cookie = $source;
        setcookie("webgains", $cookie);          
    }   
    
    public function getUTMParsamFromURL(){

        if(empty($_GET)){
            return false;
        }
        foreach($_GET as $key => $value){
      
            $_SESSION[$key] = $value;
        }

        return true;
    }
    
    public function redirectPage($url){
        
        if(empty($url)){
           return false;
        }
        
        echo '<script type="text/javascript">';
		echo "window.top.location='".$url."'";
        echo '</script>'; 
    }
    
    public function clearSession(){
        session_destroy ();
    }
}

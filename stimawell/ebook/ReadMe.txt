
CheckList for Testing:
1. Check the Tracking URL's
    a. Google Analytics
    b. Facebook Pixel
    c. Adeblo Tracking
    d. Other Activated tracking URL's

2. Check the contact in Mail-in-One
3. Check the contact in Hubspot if enabled.
4. Check the contact in cio.leads table.
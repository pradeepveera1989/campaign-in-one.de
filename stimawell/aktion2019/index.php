<?php
require 'cio.php';

# Facebook Pixel URL
$fbURL = "https://www.facebook.com/tr?id=" . $config[TrackingTools][FacebookPixel] . "&ev=PageView&noscript=1";

# Google Maps URL
$googleURL = "https://maps.googleapis.com/maps/api/js?key=" . $config[TrackingTools][GoogleAPIKey] . "&libraries=places";

#Google Analytics Key
$googleAnalyticsURL = "https://www.googletagmanager.com/gtag/js?id=" . $config[TrackingTools][GoogleAnalytics];
?>

<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Noch nie war EMS Training so günstig - trainiere für 49 Euro monatlich">
        <meta name="author" content="StimaWELL EMS">

        <title>StimaWELL | Aktion nur 49€ im Monat</title>

        <link href="css/creative.css" rel="stylesheet" type="text/css">  
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">    
        <link rel="stylesheet" href="vendor/telefonvalidator-client/build/css/intlTelInput.css"/>

        <script type="text/javascript" src="vendor/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="vendor/telefonvalidator-client/build/js/utils.js"></script>
        <script type="text/javascript" src="vendor/telefonvalidator-client/build/js/intlTelInput.js"></script>
        <script type="text/javascript" src="vendor/google/maps.js"></script>
        <script type="text/javascript" src="vendor/jquery-cookie-master/src/jquery.cookie.js"></script>
        <script type="text/javascript">

            var countdown_status = "<?= $counter_status; ?>";
            var countdown_expire = "<?= $config[GeneralSetting][CountdownExpire]; ?>";
            var countdown_expire_period = "<?= $config[GeneralSetting][MaximumCountdownperiod]; ?>";
            var current_stocks = "<?= $current_count_value; ?>";
            var countdown_end_date = "<?= $current_end_date ?>";
            var autofill_postal_code = "<?= $config[Postal_Code][Autofill]; ?>";
            var localization_postal_code = "<?= $config[Postal_Code][Localization]; ?>"
            var countdown_expire_message = "<?= $config[GeneralSetting][CountdownExpireMessage] ?>";
            var validate_telefon = "<?= $config[Telefon][Status]; ?>";
            var single_submit = "<?= $config[GeneralSetting][SingleSubmit]; ?>"
            var single_submit_text = "<?= $single_submit_text ?>"
            var cookie = "<?= $cookie; ?>";
            var webgains_cookie = "<?= $webgains_cookie ?>";
        </script>         

    </head>       
    <script language="JavaScript" type="text/javascript" src="cio.js"></script>          

    <!-- Google Analytics -->
    <?php include_once 'clients/tracking/google/google.php'; ?>        

    <!-- Facebook Pixel -->
    <?php include_once('clients/tracking/facebook/facebook.php'); ?>      

    <!-- Outbrain Tracking -->
    <?php $config[TrackingTools][EnableOutbrain] ? include_once('clients/tracking/outbrain/outbrain_index.php') : " "; ?>

    <body>
        <!-- Web Gains Tracking -->
        <?php $config[TrackingTools][EnableWebGains] ? include_once 'clients/tracking/webgains/webgains_index.php' : " "; ?>

        <!-- Remarketing Target360 Tracking -->
        <?php $config[TrackingTools][EnableTarget360] ? include_once 'clients/tracking/target360/target_index.php' : ' '; ?> 
        
        
        <!-- TRUSTED SHOPS -->
        <div id="trusted-shops">
            <a href="https://www.trustedshops.de/bewertung/info_X3622DCF7FD04D7D6428D3621CC4D24C9.html">
                <img style="width: 100%; height: 100%;" alt="trusted-shops" src="img/trusted-shops_schatten.png">
            </a>
        </div>
        
        <!-- KUNDEN STOERER DESKTOP -->
        <div id="stoerer-kunden">
            <img style="width: 100%; height: 100%;" alt="stoerer-kunden" src="img/stoerer_neu.png">
        </div>
        
        <!-- DESKTOP NAV  -->
        <nav class="navbar navbar-light bg-light static-top">   
            <div class="container">
                <a class="navbar-brand" style="text-transform: uppercase;" href="https://www.stimawell-ems.de/" target="_blank"><img style="width:200px;" alt="EMS-Training-zuhause" src="img/logo.svg"></a>
                <span class="navbar-details"style="text-transform:uppercase; "><i class="fas fa-check"></i> 40 JAHRE MADE IN GERMANY&nbsp;&nbsp;<i class="fas fa-check"></i> DIREKT VOM HERSTELLER&nbsp;&nbsp;<i class="fas fa-check"></i> SERVICE: <strong>06443 4369914</strong></span>
            </div>
        </nav>
        
        <!-- MOBILE HEADER --> 
        <div class="mobile-nav">
            <img class="nav-logo"  alt="EMS-Training-zuhause" src="img/logo.svg">
            <span style="font-size:10px; font-weight:700; color:#000; padding:5px;">ÜBER 1.500 ZUFRIEDENE KUNDEN</span>
        </div>
        
        <!-- MOBILE IMAGE HEADER --> 
        <div class="mobile-nav-img">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h5>NOCH NIE WAR<br> EMS-TRANING SO GÜNSTIG</h5>
                        <span style="font-size:13px;">Trainiere für <strong style="font-size:14px;">49 Euro</strong> anstatt <span style="text-decoration: line-through;">79 Euro</span> monatlich<br> mit StimaWELL EMS</span>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- YOUTUBE - BG VIDEO -->
        <div class="youtube-video-header">
            <iframe width="1920" height="1080" src="https://www.youtube.com/embed/dfb7hibyG4I?autoplay=1&mute=1&rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>    

        <!-- VIDEO HEADER -->
        <div class="video-header">
            <div class="video-overlay">
                <center>
                    <h1 style="color: #fff; font-size: 3rem; ">NOCH NIE WAR EMS-TRANING SO GÜNSTIG</h1>  
                    <h2 style="color: #fff; font-size: 3rem; ">Trainiere für <span style="color: #ff6a00;"><strong>49€</strong></span>* anstatt <span style="text-decoration: line-through;">79€</span> monatlich!</h2>
                </center>
            </div>
        </div>
        
        <!-- COUNTDOWN -->
        <section>
            <div class="nav-bottom">
                <div class="container">
                    <div class="row">
                        
                        <div class="col-sm-12 text-center hidden-item" style="padding:15px;"> 
                            
                            <span class="countdown-nav-desktop">Aktionsende:&nbsp;</span>
                            <span class="countdown-nav-desktop" id="demo"></span>
                            <span class="countdown-nav-desktop">- Nur noch <?php echo $current_count_value; ?> von <?php echo $config[GeneralSetting][MaxOffers]; ?> Angeboten verfügbar.&nbsp;</span>
                            
                            <a href="#form"><input style="margin: 15px; cursor: pointer;" type="submit" class="btn-grad btn" value="ANGEBOT JETZT SICHERN" style="width: 100%;"></a>
                            
                        </div>
                        
                        <div class="col-sm-12 text-center hidden-mobile" style="padding:15px;">                           
                            <span class="countdown-nav">Aktionsende:&nbsp;&nbsp;</span><span class="countdown-nav" id="demo"></span>&nbsp;<span class="countdown-nav"><br>&nbsp;&nbsp;Nur noch <?php echo $current_count_value; ?> von <?php echo $config[GeneralSetting][MaxOffers]; ?> Angeboten verfügbar.&nbsp;</span>
                            <a href="#form"><input style="margin: 15px; cursor: pointer;" type="submit" class="btn-grad btn" value="ANGEBOT JETZT SICHERN" style="width: 100%;"></a>
                        </div>
                        
                    </div>
                </div>   
            </div>  
        </section>   
        <!-- ./COUNTDOWN --> 
        
         <!-- DESKTOP BULLETS -->
        <div class="introduction" style="line-height:1.2;">
            <div class="container">
                
                <div class="row">
                    <div class="col-2 text-center">
                       <i class="fas fa-stopwatch" style="color: #00a2db; font-size: 3em; margin-bottom: 15px;"></i><br> 
                       <span style="font-size:16px; font-weight:600;">Spare Zeit – fit in nur 20 Minuten</span>
                    </div>
                    <div class="col-2 text-center">
                        <i class="fas fa-home" style="color: #00a2db; font-size: 3em; margin-bottom: 15px;"></i><br> 
                        <span style="font-size:16px; font-weight:600;">Trainiere zuhause Keine Fahrtzeiten</span><br> 
                    </div>
                    <div class="col-2 text-center">
                        <i class="fas fa-dumbbell" style="color: #00a2db; font-size: 3em; margin-bottom: 15px;"></i><br>
                        <span style="font-size:16px; font-weight:600;">Fördert den Muskelaufbau</span>
                    </div>
                    <div class="col-2 text-center">
                        <i class="fas fa-weight" style="color: #00a2db; font-size: 3em; margin-bottom: 15px;"></i><br>
                        <span style="font-size:16px; font-weight:600;">Unterstützt dich beim Abnehmen</span>
                    </div>
                    <div class="col-2 text-center">
                        <i class="fas fa-child" style="color: #00a2db; font-size: 3em; margin-bottom: 15px;"></i><br>
                        <span style="font-size:16px; font-weight:600;">Bewährt gegen Rückenschmerzen</span>
                    </div>
                    <div class="col-2 text-center">
                        <i class="fas fa-heartbeat" style="color: #00a2db; font-size: 3em; margin-bottom: 15px;"></i><br>
                        <span style="font-size:16px; font-weight:600;">Bringt deinen Stoffwechsel in Schwung</span>
                    </div>
                </div>
                
            </div>
        </div>

        <!-- MOBILE  BULLETS -->
        <div class="video-header-mobile" style="line-height:1;">
            <div class="container">
                
                <div class="row">
                    <div class="col-4 text-center">
                       <i class="fas fa-stopwatch" style="color: #00a2db; font-size: 2.2em; margin-bottom: 5px;"></i><br> 
                       <span style="font-size:9px; font-weight:600;">Spare Zeit – fit in nur 20 Minuten</span>
                    </div>
                    <div class="col-4 text-center">
                        <i class="fas fa-home" style="color: #00a2db; font-size: 2.2em; margin-bottom: 5px;"></i><br> 
                        <span style="font-size:9px; font-weight:600;">Trainiere zuhause Keine Fahrtzeiten</span><br> 
                    </div>
                    <div class="col-4 text-center">
                        <i class="fas fa-weight" style="color: #00a2db; font-size: 2.2em; margin-bottom: 5px;"></i><br>
                        <span style="font-size:9px; font-weight:600;">Unterstützt dich beim Abnehmen</span>
                    </div>
                </div>
                
            </div>
        </div>

        <section  class="form-container">
            <div class="container">
                <div class="row">
                    
                    
                    
                    <div class="col-sm-7 check-submit-form"> 
                        
                        <h2 class="mobile-text-center">Trainiere wo und wann du willst</h2>
                        <p class="mobile-text-center">Trainiere erfolgreicher mit Deinem eigenen StimaWELL® EMS-Gerät und  Stimulationsanzug bei Dir zu Hause oder auch unterwegs. Natürlich auch für mehrere Personen im Haushalt nutzbar. Das flexible und effektive FREE EMS®-Training kennt keine Öffnungszeiten, lange Anfahrtswege, belegte Geräte oder überfüllte Kursräume. So bleibt mehr Zeit für Dich und Deine Lieben.</p>
                        
                        <h2 class="mobile-text-center">StimaWELL in 70 Sekunden:</h2>
                        
                         <div class="videoWrapper" style="margin-top: 15px;">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/dfb7hibyG4I" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
                         </div>	  
                    
                     <ul class="list-group">
                         
                          <!-- BULLETS MOBILE ONLY -->
                          <li class="list-group-item"><i style="color: #00a2db;" class="fas fa-check-circle"></i>&nbsp;Sei fit mit nur 2 x 20. Min. pro Woche</li>
                          <li class="list-group-item"><i style="color: #00a2db;" class="fas fa-check-circle"></i>&nbsp;Studien belegen – EMS ist effektiv und gelenkschonend</li>
                          <li class="list-group-item"><i style="color: #00a2db;" class="fas fa-check-circle"></i>&nbsp;Geprüftes Medizinprodukt</li>
                          <li class="list-group-item"><i style="color: #00a2db;" class="fas fa-check-circle"></i>&nbsp;Keine versteckten Kosten</li>
                          <li class="list-group-item"><i style="color: #00a2db;" class="fas fa-check-circle"></i>&nbsp;Alles inklusive: Stimawell-EMS-Gerät, Anzug und Bandelektroden im Wert von 8.599€ zur exklusiven Nutzung</li>
                          <li class="list-group-item"><i style="color: #00a2db;" class="fas fa-check-circle"></i>&nbsp;Kein Risiko – 4 Wochen Rückgaberecht</li>
                          <li class="list-group-item"><i style="color: #00a2db;" class="fas fa-check-circle"></i>&nbsp;Optional: weitere Nutzer im Haushalt nur 39€ im Monat</li>
                         
                          <!-- BULLETS DESKTOP ONLY -->
                         <li class="list-group-item hidden-item"><i style="color: #00a2db;" class="fas fa-check-circle"></i>&nbsp;20 Minuten EMS Training entsprechen mehreren Stunden konventionellem Training mit Gewichten</li>
                         <li class="list-group-item hidden-item"><i style="color: #00a2db;" class="fas fa-check-circle"></i>&nbsp;Keine Anfahrtswege – keine Wartezeiten</li>
                         <li class="list-group-item hidden-item"><i style="color: #00a2db;" class="fas fa-check-circle"></i>&nbsp;EMS – seit vielen Jahren bewährt</li>
                         <li class="list-group-item hidden-item"><i style="color: #00a2db;" class="fas fa-check-circle"></i>&nbsp;Individuelles Trainingsprogramm</li>
                         <li class="list-group-item hidden-item"><i style="color: #00a2db;" class="fas fa-check-circle"></i>&nbsp;Positive Bonitätsprüfung vorausgesetzt</li>
                         
                        </ul>    
                    
                   
                        
                    </div>
                    
                    
                    
                
                    <div id="form" class="col-sm-5 check-submit-form formular"  style="padding-top: 50px; padding-bottom: 20px;">     
                        <form id="api-data-form" action="<?= $_SERVER['PHP_SELF'] ?>" method="post" accept-charset="utf-8" class="form-horizontal" role="form">
                            <input type="hidden" name="contactid" value="<?= $contactid ?>">
                            <input type="hidden" name="checksum" value="<?= $checksum ?>">
                            <input type="hidden" name="mailingid" value="<?= $mailingid ?>">
                            
                             <div class="form-group">
                             <div class="col-sm-12">
                             <h5 class="mobile-text-center" style="color:#fff;">Werde jetzt in wenigen Schritten StimaWELL-EMS Mitglied für nur 49€ im Monat.</h5>
                             <span class="mobile-text-center" style="font-size:12px; color:#fff;">Trage unten deine Daten ein und bestätige mit den Button. Im Anschluss erhälst Du von uns eine Bestätigungsmail.</span>
                             </div>
                              </div>

                             <!-- INPUT ANREDE FRAU & HERR -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label style="color:#fff; font-size:11px;">*Anrede:</label>
                                        <div style="width:100%;"class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label style="width:100%;" class="btn btn-outline-light btn-outline-light-hover">
                                                <input class="sr-only" value="Frau" type="radio" name="<?php echo $config[MailInOne][Mapping][SALUTATION]; ?>" id="option1" autocomplete="off" required> <span style="color:#fff!important;">Frau</span>
                                            </label>
                                            <label style="width:100%;" class="btn btn-outline-light btn-outline-light-hover">
                                                <input class="sr-only" value="Herr" type="radio"name="<?php echo $config[MailInOne][Mapping][SALUTATION]; ?>" id="option2" autocomplete="off" required> <span style="color:#fff!important;">Herr</span>   
                                            </label>
                                        </div>
                                    </div>       
                                </div>

                            <!-- INPUT VORNAME -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label style="color:#fff; font-size:11px;">*Vorname:&nbsp;</label>
                                    <div class="input-group">
                                        
                                        <input value="<?php echo ((!empty($standard_001) ? $standard_001 : $_SESSION['data'][$config[MailInOne][Mapping][FIRSTNAME]])); ?>" class="input-fields form-control" id="" name="<?php echo $config[MailInOne][Mapping][FIRSTNAME]; ?>" placeholder="*Vorname" type="text" required 
                                               <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>      
                                    </div>
                                </div>       
                            </div>


                            <!-- INPUT NACHNAME -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label style="color:#fff; font-size:11px;">*Nachname:&nbsp;</label>
                                    <div class="input-group">

                                        <input  value="<?php echo ((!empty($standard_002) ? $standard_002 : $_SESSION['data'][$config[MailInOne][Mapping][LASTNAME]])); ?>" class="input-fields form-control" id="nachname" name="<?php echo $config[MailInOne][Mapping][LASTNAME]; ?>" placeholder="*Nachname" type="text" required
                                                <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                    </div>
                                </div>       
                            </div>

                            <!-- INPUT ADRESSE -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label style="color:#fff; font-size:11px;">*Straße & Hausnummer:&nbsp;</label>
                                    <div class="input-group">
                                        <input value="<?php echo ((!empty($standard_004) ? $standard_004 : $_SESSION['data'][$config[MailInOne][Mapping][ADDRESS]])); ?>"  class="input-fields form-control plz" id="strasse" name="<?php echo $config[MailInOne][Mapping][ADDRESS]; ?>" placeholder="*Straße & Hausnummer" type="text" required 
                                               <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                    </div>
                                </div>       
                            </div>

                            <!-- INPUT PLZ -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label style="color:#fff; font-size:11px;">*PLZ:&nbsp;</label>
                                    <div class="input-group">
                                        <input value="<?php echo ((!empty($standard_004) ? $standard_004 : $_SESSION['data'][$config[MailInOne][Mapping][ZIP]])); ?>"  class="input-fields form-control plz" id="plz" name="<?php echo $config[MailInOne][Mapping][ZIP]; ?>" placeholder="*PLZ" type="number" required 
                                               <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                    </div>
                                    <p class="error-alert errorplz"><i class="fas fa-exclamation-circle"></i>&nbsp;&nbsp;<?php echo $config[ErrorHandling][ErrorMsgPostalcode]; ?></p>     

                                </div>       
                            </div>
                            

                            <!-- INPUT ORT -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label style="color:#fff; font-size:11px;">*Ort:&nbsp;</label>
                                    <div class="input-group">
                                        <input value="<?php echo ((!empty($standard_005) ? $standard_005 : $_SESSION['data'][$config[MailInOne][Mapping][CITY]])); ?>" class="input-fields form-control ort" id="ort" name="<?php echo $config[MailInOne][Mapping][CITY]; ?>" placeholder="*Ort" type="text" required  
                                               <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                    </div>
                                </div>       
                            </div>

                            <!-- INPUT EMAIL -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label style="color:#fff; font-size:11px;">*E-Mail Adresse:&nbsp;</label>
                                    <div class="input-group">
                                        <input value="<?php echo ((!empty($email_002) ? $email_002 : $_SESSION['data'][$config[MailInOne][Mapping][EMAIL]])); ?>" class="input-fields form-control email" id="email" name="<?php echo $config[MailInOne][Mapping][EMAIL]; ?>" placeholder="*E-Mail-Adresse" type="email" required 
                                               <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                    </div>
                                    <?php if (($_SESSION['error'] === "email") && !empty($_SESSION['error_msg'])) { ?>   
                                        <p class="error-alert erroremail" ><i class="fas fa-exclamation-circle"></i>&nbsp;&nbsp;<?php echo $_SESSION['error_msg']; ?></p>

                                    <?php } ?>  					 										
                                </div>       
                            </div>
                            
                         
                            <!-- INPUT TELEFON -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label style="color:#fff; font-size:11px;">*Telefonnummer:&nbsp;</label>
                                    <div class="input-group">
                                        <input value="<?php echo ((!empty($custom_003) ? $custom_003 : $_SESSION['data'][$config[MailInOne][Mapping][Telefon]])); ?>" class="input-fields form-control" id="telefon-mobile"  placeholder="*Telefonnummer" type="text" required <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?> >

                                        <input value="<?php echo $_SESSION['data'][$config[MailInOne][Mapping][Telefon]] ?>" class="input-fields form-control telefon" id="tel2" name="<?php echo $config[MailInOne][Mapping][Telefon]; ?>" placeholder="*Telefonnummer" type="text" hidden >   
                                    </div>
                                    <p class="error-alert errortelefon"><i class="fas fa-exclamation-circle"></i>&nbsp;&nbsp;<?php echo $config[ErrorHandling][ErrorMsgTelefon]; ?></p>
                                </div>    
                            </div>
                            
                           

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input style="width: 20px; height: 20px;" type="checkbox" required>
                                    <span class="p-light">&nbsp;&nbsp;Ich erkläre mich mit den <a style="color: #ff6a00;" target="_blank" href="https://www.stimawell-ems.de/agb">AGB</a>, <a style="color: #ff6a00;" target="_blank" href="https://www.stimawell-ems.de/de/mietbedingungen">Mietbedingungen </a>und <a style="color: #ff6a00;" target="_blank" href="https://www.stimawell-ems.de/datenschutz">Datenschutz </a>von Schwa-Medico Medizinische Apparate Vertriebsgesellschaft mbH einverstanden.<br>
                                        *Ab dem 13 Monat ist der Preis 59 Euro / Monat. Vertragslaufzeit beträgt 24 Monate.</span>
                                </div>    
                            </div>    

                            <!--Hidden Fields-->   
                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">URL</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][url]; ?>" value="<?php echo $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>">
                                </div>
                            </div>                                        

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Source</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_source]; ?>" value="<?php echo $_GET['utm_source'] ?>" >
                                </div>
                            </div>  

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Name</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_name]; ?>" value="<?php echo $_GET['utm_name'] ?>">
                                </div>
                            </div>      

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Term</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_term]; ?>" value="<?php echo $_GET['utm_term'] ?>">
                                </div>
                            </div>                             

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Content</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_content]; ?>" value="<?php echo $_GET['utm_content'] ?>">
                                </div>
                            </div>                              

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Medium</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_medium]; ?>" value="<?php echo $_GET['utm_medium'] ?>">
                                </div>
                            </div>    

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Campaign</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_campaign]; ?>" value="<?php echo $_GET['utm_campaign'] ?>">
                                </div>
                            </div>                                               

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">TRAFFICSOURCE</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][trafficsource]; ?>" id="ts" value="<?php echo $_GET['trafficsource'] ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Quelle</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][Quelle]; ?>" id="quelle" value="<?php echo $config[MailInOne][Constants][Quelle]; ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Typ</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" ame="<?php echo $config[MailInOne][Mapping][Typ]; ?>" id="" value="<?php echo $config[MailInOne][Constants][Typ]; ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Segment</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" ame="<?php echo $config[MailInOne][Mapping][Segment]; ?>" id="" value="<?php echo $config[MailInOne][Constants][Segment]; ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Test Mode</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="test_mode" id="" value="<?php echo $_GET['testmode']; ?>">
                                </div>
                            </div>                            

                            <div class="form-group"> 
                                <div class="col-sm-12">
                                    <input type="submit" class="col-sm-12 btn btn-grad  js-scroll-trigger" value="JETZT KAUFEN" name="submit" style="width: 100%;">
                                </div>        
                            </div>
                            <div class="form-group"> 
                                <div class="col-sm-12">
                                    <p class="p-light">Hast du Fragen zum Angebot? Unsere Experten stehen für dich zur Verfügung. Rufe uns an unter <strong>06443 4369914</strong>.</p>
                                </div>        
                            </div>   
                            <?php if (isset($response) && $response->isSuccess()) { ?>
                                <div class="alert alert-success fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                   	<strong>Subscription successful</strong>
                               	</div>
                            <?php } elseif (isset($warning)) { ?>
                                <div class="alert alert-warning fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong style="color: red; z-index: 1000;">Subscription failed</strong>
                                    <?= $warning['message'] ?>
                                </div>
                            <?php } elseif (isset($response)) { ?>
                                <div class="alert alert-danger fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Subscription failed</strong>
                                </div>
                            <?php } ?>
                        </form>          


                    </div>
                </div>
            </div>  
        </section>

        <section class="testimonials">
            <div class="container">
                <center>
                    <h2 style="color: #3c3b3b;">Das sagen unsere Kunden.</h2> 
                    <hr>
                    <p>Über 1.500 zufriedene Kunden trainieren mit StimaWELL EMS</p>
                </center>
            </div>

            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="6"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="7"></li>
                </ol>
                <div class="carousel-inner">

                    <div class="carousel-item active">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links">
                                    <img class="img-fluid" src="img/testimonial_zeitmangel.png" alt="testimonial zeitmangel">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts">
                                    <h5>Anne H., 28, spart viel Zeit und schafft trotz 2 Kindern zu trainieren</h5>
                                    <p><i>„Als junge Mutter von zwei Kindern ist es oft nicht leicht, Zeit für sich zu finden. Doch endlich kann ich mit EMS trainieren wo und wann ich will! Die Bedienung des Gerätes ist wirklich einfach und sicher und mittlerweile kann ich mir das EMS-Training gar nicht mehr aus meinem Leben wegdenken!“</i></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links">
                                    <img class="img-fluid" src="img/testimonial_figur.png" alt="testimonial figur">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts">
                                    <h5>Jenny P., 40, ist als dreifache Mutter wieder zufrieden mit Ihrer Figur</h5>
                                    <p><i>„Nach der Geburt von drei Kindern ist es mir unheimlich schwer gefallen, meine damalige Figur wieder zu erreichen. Als ich von EMS-Training gehört habe, dachte ich, ein Versuch ist es wert. Mittlerweile habe ich 16 cm Bauchumfang verloren und auch mein Po ist 12 cm schmaler geworden. Endlich habe ich wieder Kleidergröße S!“</i></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links">
                                    <img class="img-fluid" src="img/testimonial_ruecken.png" alt="testimonial rücken">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts">
                                    <h5>Claudia S., 38, konnte ihre Rückenschmerzen loswerden</h5>
                                    <p><i>„In meinem Beruf muss ich fast den ganzen Tag im Stehen arbeiten. Irgendwann bahnten sich teilweise unerträgliche Rückenschmerzen an und nichts half. Erst mit EMS-Training spürte ich nicht nur eine deutliche Verbesserung – meine Rückenschmerzen sind einfach verschwunden!“</i></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links">
                                    <img class="img-fluid" src="img/testimonial_relax.png" alt="testimonial relax">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts">
                                    <h5>Christine F., 44, entspannt gerne mit EMS</h5>
                                    <p><i>„Für mich sind die EMS-Trainingsprogramme genauso wichtig wie die Regenerationsprogramme. Ich versuche mir meine Zeit bestmöglich einzuteilen, damit ich mir die Entspannungs- und Massageeinheiten mindestens einmal pro Woche gönnen kann. So schalte ich vom Alltag ab und vergesse jeglichen Stress. Das Beste daran ist, dass ich das ganz bequem und stressfrei von meinem Wohlfühlumfeld machen kann und nicht einmal die Haustür verlassen muss.“</i></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links">
                                    <img class="img-fluid" src="img/testimonial_cellulite.png" alt="testimonial cellulite">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts">
                                    <h5>Dagmar K., 26, bekämpft erfolgreich Cellulite</h5>
                                    <p><i>„Seit einigen Wochen trainiere ich mit dem speziellen EMS-Celluliteprogramm und habe zusätzlich meine Ernährung umgestellt. Mein Hautbild hat sich schon deutlich verbessert und rechtzeitig zur Badesaison sind meine Oberschenkel und Hüfte sichtbar glatter!“</i></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links">
                                    <img class="img-fluid" src="img/testimonial_muskeln.png" alt="testimonial muskeln">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts">
                                    <h5>Markus T., 27, fördert den Muskelaufbau</h5>
                                    <p><i>„Ich war schon immer ein sehr sportlicher Typ. Doch ich wollte noch nie meine Muskeln übermäßig aufpumpen, daher ist ein Ganzkörper-EMS-Training genau richtig für mich. Zwei Einheiten in der Woche für jeweils 20 Minuten reichen aus, um meine Muskeln klar zu definieren und für mich perfekt in Form zu bringen. Ich bin mit dem Ergebnis sehr zufrieden und kann wirklich sagen, dass die Leistungsfähigkeit von EMS-Training einfach unschlagbar ist.“</i></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links">
                                    <img class="img-fluid" src="img/testimonial_beckenboden.png" alt="testimonial beckenboden">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts">
                                    <h5>Grete S., 72, hat endlich keine Beckenbodenprobleme mehr</h5>
                                    <p><i>„Ich habe 40 Jahre lang mit einer Beckenboden-Problematik und daraus resultierender Inkontinenz gelebt. Seit ich mit EMS trainiere, hat sich für mich einiges geändert. Ich fühle mich endlich nicht mehr unsauber, kann meine Blase besser kontrollieren und vor allem endlich wieder ohne Beschwerden jede Treppe hochgehen! Das Training war das Beste, was mir je passiert ist und ich würde es jeder Frau empfehlen, die auch mit Inkontinenz zu kämpfen hat. Schade, dass es das EMS-Ganzkörpertraining nicht schon früher gab.“</i></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="container">
                            <div class="row" style="padding-top: 50px; padding-bottom: 70px;">
                                <div class="col-sm-12 col-md-3 text-center slider-bild-links">
                                    <img class="img-fluid" src="img/testimonial_stoffwechsel.png" alt="testimonial stoffwechsel">
                                </div>
                                <div class="col-sm-12 col-md-9 slider-text-rechts">
                                    <h5>Bärbel P., 46, bringt ihren Stoffwechsel in Gang</h5>
                                    <p><i>„Als ich damals mit dem Rauchen aufgehört habe, habe ich sehr viel zugenommen. Jetzt habe ich dank dem EMS-Stoffwechselprogramm und der dazugehörigen Stoffwechselkur in nur wenigen Monaten ganze 12 kg verloren!“</i></p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>
        </section>

        
        <section class="info-content hidden-item">
        <div class="container">
            <div class="row">
                <center>
                    <div class="col-sm-12 col-md-10">
                        <h2 style="color: #EEE">Was ist EMS-Training?</h2>
                        <hr style="border-color: #EEE;">
                        <h5 style="color: #ff6a00;">Das innovative Training – mit minimalen Zeiteinsatz ein maximales Ergebnis erreichen</h5>
                        <br>
                        <p style="text-align: left; color: #EEE;">Dass elektrische Muskelstimulation eine hervorragende Methode in Puncto Schmerztherapie ist, die sich bereits in zahlreichen Kliniken fest etabliert hat, ist seit vielen Jahren bewiesen. Mit der Zeit wurde EMS auch als neue Ganzkörpertraining-Sportart immer bekannter. Die Muskeln werden durch kleine Stromimpulse trainiert. Mittlerweile zeigen jedoch zahlreiche Studien, dass mit EMS beste Ergebnisse erzielt werden können.</p>
                        <p style="text-align: left; color: #EEE;">Egal ob zur Stärkung der Rückenmuskulatur, zum Muskelaufbau, zur Verminderung von Cellulite, zum Anregen des Stoffwechsels, zur nachhaltigen Figurformung oder auch einfach zum Entspannen – EMS ist besonders effektiv und zeitsparend!</p>
                    </div>
                </center>
            </div>
        </div>  
    </section>

     <section class="partner-container hidden-item">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 model-feature">       
                    <h2>Weil Fitness unkompliziert und bezahlbar sein soll.</h2>
                    <hr>
                    <p>Erlebe die neue, freie Art des EMS-Trainings. Erreiche deine Trainingsziele schneller und bequemer. Kombiniere die regelmäßige, persönliche Betreuung durch deinen EMS-Experten und dein eigenständiges EMS-Training mit dem virtuellen Trainer zu Hause oder unterwegs.</p><br>
                </div>  
                <div class="col-sm-6 model-feature">       
                    <h2>Vielseitige Anwendung um deine Ziele schneller zu erreichen.</h2>
                    <hr>
                    <p>Die EMS Technologie ermöglicht es dir Ziele verschiedenster Art schneller zu erreichen. Die Kombination aus persönlicher Betreuung durch deinen EMS-Experten und eigenständigem EMS-Training machen es möglich. Wo und wann du willst!</p><br>

                </div>  
            </div>
        </div>  
    </section>  

    <section class="experte hidden-item">
        <div class="container">
            <center>
                <h2>Dein persönlicher Ansprechpartner und EMS-Experte <span style="color: #00a2db;">Dominic Heilig</span></h2>
                <hr>
            </center>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-3" style="margin-top: 30px;">
                    <center>
                        <img src="img/ansprechpartner_dominic-heilig.png" alt="ems-experte dominic heilig" class="experte-img img-fluid">
                        <p class="experte-img-subtext" style="color: #00a2db; margin-top: 15px; margin-bottom: 0px;"><strong>DOMINIC HEILIG</strong></p>
                        <p class="experte-img-subtext"><i>StimaWELL-EMS Experte</i></p>
                    </center>
                </div>
                <div class="col-sm-12 col-md-9 experte-text-container">
                    <p class="experte-text">"Mein Name ist Dominic Heilig. Seit meinem 5. Lebensjahr treibe ich leidenschaftlich gerne Sport. Aus dieser Leidenschaft heraus resultiert mein Beruf.  Während meines Studiums zum Bachelor of Arts in Fitnessökonomie durchlief ich mehrere Stationen. Durch die Vielzahl an Stationen konnte ich ein umfassendes Fachwissen aufbauen, was ich gerne heute an meine Kunden und Partner weitergebe.</p>
                    <p class="experte-text">Durch meine Arbeit in zwei Microstudios habe ich täglich im Umgang mit den Kunden gelernt, welche Herausforderungen zu lösen sind, welche Ziele die Kunden haben und vor allem wie man diese Ziele am besten erreicht. Seit 2017 betreute ich für StimaWELL unsere Kunden für die Ganzkörper EMS Systeme. Meine Arbeit bringt mich mit vielen verschiedenen Kunden zusammen. Ich helfe sowohl Privatkunden als auch professionellen Anwendern wie Physiotherapeuten, Fitnessstudios, Sonnenstudios, Hebammen, Personal-Trainern, Heilpraktikern oder EMS-Studios. Mit meiner mehrjährigen Erfahrung im EMS-Training Bereich berate ich sehr gerne Existenzgründer und helfe diesen mit innovativen EMS Produkten von schwa-medico erfolgreich eine Existenz aufzubauen."</p>
                </div>         
            </div>
        </div>  
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <center>
                        <a href="#form"><input style="margin: 15px; cursor: pointer;" type="submit" class="btn-grad btn" value="JETZT EMS-TRAINING SICHERN FÜR NUR 49€ IM MONAT" style="width: 100%;"></a>
                    </center>   
                </div>
            </div>
        </div>
    </section>     
        
        
        
        
        
        
        
        
<!-- MOBILE ACCORDION -->        
      
<div class="accordion hidden-mobile" id="accordionExample">
     
  <div class="card" style="background-color:#fff!important;">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <button style="float:right;" class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseTwo">
          Was ist EMS-Training?&nbsp;&nbsp;&nbsp;<i class="fas fa-angle-right"></i>
        </button>
      </h5>
    </div>
    <div id="collapseOne" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
      <div class="card-body">
          
        <h5 style="color: rgb(0, 162, 219);">Das innovative Training – mit minimalen Zeiteinsatz ein maximales Ergebnis erreichen</h5>
        <br>
        <p style="text-align: left; color: #000;">Dass elektrische Muskelstimulation eine hervorragende Methode in Puncto Schmerztherapie ist, die sich bereits in zahlreichen Kliniken fest etabliert hat, ist seit vielen Jahren bewiesen. Mit der Zeit wurde EMS auch als neue Ganzkörpertraining-Sportart immer bekannter. Die Muskeln werden durch kleine Stromimpulse trainiert. Mittlerweile zeigen jedoch zahlreiche Studien, dass mit EMS beste Ergebnisse erzielt werden können.</p>
        <p style="text-align: left; color: #000;">Egal ob zur Stärkung der Rückenmuskulatur, zum Muskelaufbau, zur Verminderung von Cellulite, zum Anregen des Stoffwechsels, zur nachhaltigen Figurformung oder auch einfach zum Entspannen – EMS ist besonders effektiv und zeitsparend!</p>
          
      </div>
    </div>
  </div>
    

  <div class="card" style="background-color:#fff!important;">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <button style="float:right;" class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Warum StimaWELL-EMS?&nbsp;&nbsp;&nbsp;<i class="fas fa-angle-right"></i>
        </button>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
      <div class="card-body">
        <h5 style="color: rgb(0, 162, 219);">Einfaches Training Zuhause</h5>
        <br>
        <p style="text-align: left; color: #000;">Es trainieren schon mehrere hundert Kunden über Jahre erfolgreich mit EMS Zuhause. Mit dem Einsatz der Videoanleitungen ist das Training kinderleicht und es lässt sich per Fernbedienung individuell regulieren oder auch stoppen.</p>
       
        <h5 style="color: rgb(0, 162, 219);">Weil Fitness unkompliziert und bezahlbar sein soll.</h5>
        <br>
        <p style="text-align: left; color: #000;">Erlebe die neue, freie Art des EMS-Trainings. Erreiche deine Trainingsziele schneller und bequemer. Kombiniere die regelmäßige, persönliche Betreuung durch deinen EMS-Experten und dein eigenständiges EMS-Training mit dem virtuellen Trainer zu Hause oder unterwegs.</p>
        
        <h5 style="color: rgb(0, 162, 219);">Vielseitige Anwendung um deine Ziele schneller zu erreichen.</h5>
        <br>
        <p style="text-align: left; color: #000;">Die EMS Technologie ermöglicht es dir Ziele verschiedenster Art schneller zu erreichen. Die Kombination aus persönlicher Betreuung durch deinen EMS-Experten und eigenständigem EMS-Training machen es möglich. Wo und wann du willst!</p>
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button style="float:right;" class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Was ist das FREE EMS-Programm?&nbsp;&nbsp;&nbsp;<i class="fas fa-angle-right"></i>
        </button>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
        <h5 style="color: rgb(0, 162, 219);">DAS FREE EMS-PROGRAMM FÜR ZUHAUSE BRINGT DICH SCHNELLER ZU DEINEN ZIELEN</h5>
        <br>
        <p style="text-align: left; color: #000;">DIE WIRKSAMKEIT VON EMS-TRAINING WURDE IN VIELEN STUDIEN NACHGEWIESEN. WÄHLE AUS EINER VIELZAHL VON STIMAWELL® EMS-PROGRAMMEN DIE PASSENDEN AUS UND BEGLEITET DICH AUF DEM WEG ZU DEINEN ZIELEN. TRAINIERE MIT EMS GANZ FLEXIBEL ZUHAUSE!</p>
      </div>
    </div>
  </div>
</div>
        

        <section>
            <div class="clearspace"></div>
        </section>  

        <footer class="footer bg-light">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 h-100 text-center text-lg-left my-auto" style="height:auto!important">
                        <ul class="list-inline mb-2">
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/impressum" target="_blank">Impressum</a>
                            </li>
                            <li class="list-inline-item">&sdot;</li>
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/agb" target="_blank">AGB</a>
                            </li>
                            <li class="list-inline-item">&sdot;</li>
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/datenschutz" target="_blank">Datenschutz</a>
                            </li>
                            <li class="list-inline-item">&sdot;</li>
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/de/mietbedingungen" target="_blank">Mietbedingungen</a>
                            </li>
                        </ul>
                        <p style="padding-top:15px;" class="text-muted small mb-4 mb-lg-0"><strong>✔</strong> 40 JAHRE MADE IN GERMANY&nbsp;&nbsp;&nbsp;<strong>✔</strong> DIREKT VOM HERSTELLER&nbsp;&nbsp;&nbsp;<strong>✔</strong> SERVICE: 06443 4369914</p>
                    </div>
                    <div class="col-lg-4 h-100 text-center text-lg-right my-auto" style="height:auto!important">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item mr-3">
                                <a href="https://www.facebook.com/stimawellems/" target="_blank">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li class="list-inline-item mr-3">
                                <a href="https://www.instagram.com/stimawell.ems/" target="_blank">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                </div>   
            </div>
        </footer>
    </body>
</html>        

<?php
require 'cio.php';

# Facebook Pixel URL
$fbURL = "https://www.facebook.com/tr?id=" . $config[TrackingTools][FacebookPixel] . "&ev=PageView&noscript=1";

# Google Maps URL
$googleURL = "https://maps.googleapis.com/maps/api/js?key=" . $config[TrackingTools][GoogleAPIKey] . "&libraries=places";

#Google Analytics Key
$googleAnalyticsURL = "https://www.googletagmanager.com/gtag/js?id=" . $config[TrackingTools][GoogleAnalytics];
?>

<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Nutzen Sie unser neues FREE EMS® PROGRAMM und trainieren sie bequem von zu Hause aus.">
        <meta name="author" content="StimaWELL">

        <title>StimaWELL® 120MTRS | FIBO Messepreis</title> 

        <link href="css/creative.css" rel="stylesheet" type="text/css">  
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">    
        <link rel="stylesheet" href="vendor/telefonvalidator-client/build/css/intlTelInput.css"/>

        <script type="text/javascript" src="vendor/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="vendor/telefonvalidator-client/build/js/utils.js"></script>
        <script type="text/javascript" src="vendor/telefonvalidator-client/build/js/intlTelInput.js"></script>
        <script type="text/javascript" src="vendor/google/maps.js"></script>
        <script type="text/javascript" src="vendor/jquery-cookie-master/src/jquery.cookie.js"></script>
        <script type="text/javascript">

            var countdown_status = "<?= $counter_status; ?>";
            var countdown_expire = "<?= $config[GeneralSetting][CountdownExpire]; ?>";
            var countdown_expire_period = "<?= $config[GeneralSetting][MaximumCountdownperiod]; ?>";
            var current_stocks = "<?= $current_count_value; ?>";
            var countdown_end_date = "<?= $current_end_date ?>";
            var autofill_postal_code = "<?= $config[Postal_Code][Autofill]; ?>";
            var localization_postal_code = "<?= $config[Postal_Code][Localization]; ?>"
            var countdown_expire_message = "<?= $config[GeneralSetting][CountdownExpireMessage] ?>";
            var validate_telefon = "<?= $config[Telefon][Status]; ?>";
            var single_submit = "<?= $config[GeneralSetting][SingleSubmit]; ?>"
            var single_submit_text = "<?= $single_submit_text ?>"
            var cookie = "<?= $cookie; ?>";
            var webgains_cookie = "<?= $webgains_cookie ?>";
        </script>         

    </head>       
    <script language="JavaScript" type="text/javascript" src="cio.js"></script>          

    <!-- Google Analytics -->
    <?php include_once 'clients/tracking/google/google.php'; ?>        

    <!-- Facebook Pixel -->
    <?php include_once('clients/tracking/facebook/facebook.php'); ?>      

    <!-- Outbrain Tracking -->
    <?php $config[TrackingTools][EnableOutbrain] ? include_once('clients/tracking/outbrain/outbrain_index.php') : " "; ?>

    <body>
        <!-- Web Gains Tracking -->
        <?php $config[TrackingTools][EnableWebGains] ? include_once 'clients/tracking/webgains/webgains_index.php' : " "; ?>

        <!-- Remarketing Target360 Tracking -->
        <?php $config[TrackingTools][EnableTarget360] ? include_once 'clients/tracking/target360/target_index.php' : ' '; ?>   


        <!-- DESKTOP NAV -->
        <nav class="navbar navbar-light bg-light static-top">   
            <div class="container">
                <a class="navbar-brand" style="text-transform: uppercase;" href="https://www.stimawell-ems.de/" target="_blank"><img style="height:46px;" alt="EMS-Training-zuhause" src="img/logo-120mtrs.png"><img style="height:40px; vertical-align: middle; margin-left: 20px;" alt="FIBO2019" src="img/fibo-logo.png"></a>
                <span class="navbar-details"style="text-transform:uppercase; text-align: right;"><i class="fas fa-check"></i> 40 JAHRE MADE IN GERMANY&nbsp;&nbsp;<i class="fas fa-check"></i> SERVICE: <strong>06443 8333-500</strong></span>
            </div>
        </nav>

        <!-- MOBILE HEADER-->  
        <div class="mobile-nav"><center><img style="width:200px;" alt="EMS-Training-zuhause" src="img/logo.svg"></center></div>

        <!-- TRUSTED SHOPS -->
        <div id="trusted-shops">
            <a href="https://www.trustedshops.de/bewertung/info_X3622DCF7FD04D7D6428D3621CC4D24C9.html">
                <img style="width: 100%; height: 100%;" alt="trusted-shops" src="img/trusted-shops_schatten.png">
            </a>
        </div>

    
        <!-- VIDEO HEADER -->
        <div class="video-header">
            <div class="video-overlay">
                <center>
                    <h1 style="color: #fff; font-size: 3rem; ">StimaWELL 120MTRS® zum <span style="color: #ff6a00;">Messepreis!</span></h1>  
                    <h2 style="color: #fff; font-size: 3rem; ">Sichere Dir jetzt die 120MTRS Rückenliege zum Messepreis von <span style="color: #ff6a00;">129€ monatlich!</span></h2>
                    <p>&nbsp;</p>
                    <form style="display: inline;" method = "post" action="<?php echo $jetzt_kaufen; ?>"><button style="margin-bottom: 10px;" type="submit" class="btn btn-outline-info" id="stimawell-button-2">JETZT INFORMATIONEN ANFORDERN</button></form>
                </center>
            </div>
        </div>


        <!-- MOBILE -->
        <div class="video-header-mobile">
            <div  class="video-overlay-mobile">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <p style="color: #fff; font-size: 2rem; ">StimaWELL 120MTRS® zum <span style="color: #ff6a00;">Messepreis!</span></p>  
                            <p style="color: #fff; font-size: 1.2rem; ">Sichere Dir jetzt die 120MTRS Rückenliege zum Messepreis von <span style="color: #ff6a00;">129€ monatlich!</span></p>  
                            <p>&nbsp;</p>
                            <form style="display: inline;" method="post"  action="<?php echo $jetzt_kaufen; ?>"><button style="margin-bottom: 10px;" type="submit" class="btn btn-outline-info">JETZT INFORMATIONEN ANFORDERN</button></form>
                        </div>    
                    </div>
                </div>
            </div>
        </div>

        <!-- COUNTDOWN -->
        <section>
            <div class="nav-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <span class="countdown-nav">Aktionsende:&nbsp;&nbsp;</span><span class="countdown-nav" id="demo"></span>&nbsp;<span class="countdown-nav">-&nbsp;&nbsp;Nur noch <?php echo $current_count_value; ?> von <?php echo $config[GeneralSetting][MaxOffers]; ?> Angeboten verfügbar.&nbsp;</span>
                        </div> 
                    </div>
                </div>   
            </div>  

        </section>   
        <!-- ./COUNTDOWN -->      
            
        <p>&nbsp;</p>
        
        <section class="form-container">
            <div class="container">
                <div class="row">
                      <div class="col-md-12 col-lg-6 title-column" style="padding-top:50px;">
                        <h1 style="color: #00a2db; font-weight: bold; margin-left: 20px; margin-bottom: 0px; font-size: 3rem; line-height: 0.9;">STIMAWELL 120MTRS</h1>
                        <h2 style="margin-left: 20px; margin-bottom: 20px; margin-top: 6px; line-height: 1.2; font-size: 1.5rem !important;">JETZT ZUM MESSEPREIS SICHERN</h2>
                          
                        <p style="margin-left: 20px;">Das medizinisch zugelassene StimaWELL® 120MTRS System ist ein einzigartiges, patentiertes therapeutisches Instrument zur Behandlung von Rückenbeschwerden. Diese sind in vielen Physiopraxen ein wichtiges Thema: Schließlich leiden nahezu 4 von 5 Erwachsenen in Deutschland mindestens einmal im Leben unter akuten oder chronischen Rückenschmerzen.</p>
                      
						
                        <div>
                            <center>
                                <img class="img-fluid titelbild" src="img/120mtrs_fibo-preis.png" alt="fibo-preis" style="margin-top: 10px; margin-bottom: 10px;"/>
                            </center>
                        </div>
                    </div>  
                    <div class="col-lg-6 check-submit-form" id="form" style="padding-top: 50px; padding-bottom: 20px;">     
                        
                        
                       <form id="api-data-form" action="<?= $_SERVER['PHP_SELF'] ?>" method="post" accept-charset="utf-8" class="form-horizontal" role="form">
                            <input type="hidden" name="contactid" value="<?= $contactid ?>">
                            <input type="hidden" name="checksum" value="<?= $checksum ?>">
                            <input type="hidden" name="mailingid" value="<?= $mailingid ?>">
                           

                                <!-- INPUT ANREDE FRAU & HERR -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <p>Bitte trage deine Daten ein:</p>
                                        <div style="width:100%;"class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label style="width:100%;" class="btn btn-outline-light">
                                                <input class="sr-only" value="Frau" type="radio" name="<?php echo $config[MailInOne][Mapping][SALUTATION]; ?>" id="option1" autocomplete="off" required> <span style="color:#8b8b8b!important;">Frau</span>
                                            </label>
                                            <label style="width:100%;" class="btn btn-outline-light">
                                                <input class="sr-only" value="Herr" type="radio"name="<?php echo $config[MailInOne][Mapping][SALUTATION]; ?>" id="option2" autocomplete="off" required> <span style="color:#8b8b8b!important;">Herr</span>   
                                            </label>
                                        </div>
                                    </div>       
                                </div>


                            <!-- INPUT VORNAME -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">

                                        <input value="<?php echo ((!empty($standard_001) ? $standard_001 : $_SESSION['data'][$config[MailInOne][Mapping][FIRSTNAME]])); ?>" class="input-fields form-control" id="" name="<?php echo $config[MailInOne][Mapping][FIRSTNAME]; ?>" placeholder="*Vorname" type="text" required 
                                               <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>      
                                    </div>
                                </div>       
                            </div>


                            <!-- INPUT NACHNAME -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">

                                        <input  value="<?php echo ((!empty($standard_002) ? $standard_002 : $_SESSION['data'][$config[MailInOne][Mapping][LASTNAME]])); ?>" class="input-fields form-control" id="nachname" name="<?php echo $config[MailInOne][Mapping][LASTNAME]; ?>" placeholder="*Nachname" type="text" required
                                                <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                    </div>
                                </div>       
                            </div>
						   
						   <!-- INPUT FIRMA -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">

                                        <input  value="<?php echo ((!empty($auto_fill['firma']) ? $auto_fill['firma'] : $_SESSION['data'][$config[MailInOne][Mapping][Firma]])); ?>" class="input-fields form-control" id="firma" name="<?php echo $config[MailInOne][Mapping][Firma]; ?>" placeholder="*Firma" type="text" required
                                                <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                    </div>
                                </div>       
                            </div>

                            <!-- INPUT ADRESSE 
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <input value="<?php echo ((!empty($standard_004) ? $standard_004 : $_SESSION['data'][$config[MailInOne][Mapping][ADDRESS]])); ?>"  class="input-fields form-control plz" id="strasse" name="<?php echo $config[MailInOne][Mapping][ADDRESS]; ?>" placeholder="*Straße & Hausnummer" type="text" required 
                                               <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                    </div>
                                </div>       
                            </div>-->

                            <!-- INPUT PLZ -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <input value="<?php echo ((!empty($standard_004) ? $standard_004 : $_SESSION['data'][$config[MailInOne][Mapping][ZIP]])); ?>"  class="input-fields form-control plz" id="plz" name="<?php echo $config[MailInOne][Mapping][ZIP]; ?>" placeholder="*PLZ" type="number" required 
                                               <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                    </div>
                                    <span class="p-light errorplz" style="color:<?php echo $config[ErrorHandling][ErrorMsg_Color]; ?>"><?php echo $config[ErrorHandling][ErrorMsgPostalcode]; ?></span>     
     
                                </div>       
                            </div>

                            <!-- INPUT ORT
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <input value="<?php echo ((!empty($standard_005) ? $standard_005 : $_SESSION['data'][$config[MailInOne][Mapping][CITY]])); ?>" class="input-fields form-control ort" id="ort" name="<?php echo $config[MailInOne][Mapping][CITY]; ?>" placeholder="*Ort" type="text" required  
                                               <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                    </div>
                                </div>       
                            </div> -->

                            <!-- INPUT EMAIL -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <input value="<?php echo ((!empty($email_002) ? $email_002 : $_SESSION['data'][$config[MailInOne][Mapping][EMAIL]])); ?>" class="input-fields form-control email" id="email" name="<?php echo $config[MailInOne][Mapping][EMAIL]; ?>" placeholder="*E-Mail-Adresse" type="email" required 
                                               <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                    </div>
									<?php if (($_SESSION['error'] === "email") && !empty($_SESSION['error_msg'])) { ?>   
																		<span class="p-light erroremail"  style="float: left; padding-bottom:15px; color:<?php echo $config[ErrorHandling][ErrorMsg_Color]; ?>"><?php echo $_SESSION['error_msg']; ?></span>
									<?php } ?>  					 										
                                </div>       
                            </div>

                            <!-- INPUT TELEFON -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <input value="<?php echo ((!empty($custom_003) ? $custom_003 : $_SESSION['data'][$config[MailInOne][Mapping][Telefon]])); ?>" class="input-fields form-control" id="telefon-mobile"  placeholder="*Telefonnummer" type="text" required <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?> >

                                               <input value="<?php echo $_SESSION['data'][$config[MailInOne][Mapping][Telefon]] ?>" class="input-fields form-control telefon" id="tel2" name="<?php echo $config[MailInOne][Mapping][Telefon]; ?>" placeholder="*Telefonnummer" type="text" hidden >   

                                    </div>
                                    <span class="p-light errortelefon" style="color:<?php echo $config[ErrorHandling][ErrorMsg_Color]; ?> ;float:left; padding-bottom:15px"><?php echo $config[ErrorHandling][ErrorMsgTelefon]; ?></span>
                                </div>       
                            </div>

                            <label class="form-group">
                                <div class="col-sm-12">
                                    <input style="width: 20px; height: 20px;" type="checkbox" required>
                                    <span style="color:;" class="p-dark">&nbsp;&nbsp;Ich bin einverstanden, dass die Schwa-Medico Medizinische Apparate Vertriebsgesellschaft mbH, Gehrnstraße 4, 35630 Ehringhausen, meine eingetragenen Daten nutzt und mir E-Mails zuschickt bzw. mich anruft, um mir Angebote aus dem Bereich EMS-Training (Zubehör, Training, Fachliteratur) zukommen zu lassen. Diese Einwilligung kann ich jederzeit widerrufen, etwa durch einen Brief an die oben genannte Adresse oder durch eine E-Mail an <a href="mailto:widerruf@schwa-medico.de" target="_blank" >widerruf@schwa-medico.de</a>. Anschließend wird jede werbliche Nutzung unterbleiben.</span>
                                </div>    
                            </label>    

                            <!--Hidden Fields-->   
                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">URL</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][url]; ?>" value="<?php echo $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>">
                                </div>
                            </div>                                        

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Source</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_source]; ?>" value="<?php echo $_GET['utm_source'] ?>" >
                                </div>
                            </div>  

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Name</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_name]; ?>" value="<?php echo $_GET['utm_name'] ?>">
                                </div>
                            </div>      

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Term</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_term]; ?>" value="<?php echo $_GET['utm_term'] ?>">
                                </div>
                            </div>                             

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Content</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_content]; ?>" value="<?php echo $_GET['utm_content'] ?>">
                                </div>
                            </div>                              

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Medium</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_medium]; ?>" value="<?php echo $_GET['utm_medium'] ?>">
                                </div>
                            </div>    

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Campaign</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_campaign]; ?>" value="<?php echo $_GET['utm_campaign'] ?>">
                                </div>
                            </div>                                               

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">TRAFFICSOURCE</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][trafficsource]; ?>" id="ts" value="<?php echo $_GET['trafficsource'] ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Quelle</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][Quelle]; ?>" id="quelle" value="<?php echo $config[MailInOne][Constants][Quelle]; ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Typ</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" ame="<?php echo $config[MailInOne][Mapping][Typ]; ?>" id="" value="<?php echo $config[MailInOne][Constants][Typ]; ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Segment</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" ame="<?php echo $config[MailInOne][Mapping][Segment]; ?>" id="" value="<?php echo $config[MailInOne][Constants][Segment]; ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Test Mode</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="test_mode" id="" value="<?php echo $_GET['testmode']; ?>">
                                </div>
                            </div>                            

                            <div class="form-group"> 
                                <div class="col-sm-12">
                                    <input style="margin-top: 25px; cursor: pointer; background-color: rgb(0, 162, 219); border-color: rgb(0, 162, 219);" type="submit" class="col-sm-12 btn btn-danger btn-xl js-scroll-trigger" value="JETZT INFORMATIONEN ANFORDERN" name="submit" style="width: 100%;">
                                </div>        
                            </div>
                            <div class="form-group"> 
                                <div class="col-sm-12">
                                    <p>Hast Du Fragen zum Messepreis? Unsere Experten stehen Dir zur Verfügung. Rufe uns an unter <strong>06443 8333-500</strong></p>
                                </div>        
                            </div>   
							<?php if (isset($response) && $response->isSuccess()) { ?>
                            <div class="alert alert-success fade in">
                            	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                               	<strong>Subscription successful</strong>
                           	</div>
							<?php } elseif (isset($warning)) { ?>
                            <div class="alert alert-warning fade in">
                            	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong style="color: red; z-index: 1000;">Subscription failed</strong>
    						<?= $warning['message'] ?>
                            </div>
							<?php } elseif (isset($response)) { ?>
                            <div class="alert alert-danger fade in">
                            	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Subscription failed</strong>
                            </div>
							<?php } ?>
                        </form>         


                    </div>
                </div>
            </div>  
        </section>
        
        <section class="fibo-messe">
            <div class="container">
                <div class="row">
					<div class="col-sm-12 col-lg-2"></div>
                    <div class="col-sm-12 col-lg-8 fibo-text">       
                        <h2 style="color: #FFF;">StimaWELL® auf der <span style="color: #ff6a00;">FIBO 2019</span></h2>
                        <hr style="border-color: #FFF;">
                        <p class="lead mb-0" style="color: #FFF;">Komm und besuche uns auf der FIBO 2019 in Köln. Unsere Kundenberater nehmen sich gerne Zeit für Dich auf der FIBO 2019 am <span style="color: #ff6a00;">Stand B20 in Halle 5.2</span>.</p><br>
                    </div>  
					<div class="col-sm-12 col-lg-2"></div>
                </div>
            </div>  
        </section> 
		
		 <section class="unser-angebot">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <div class="info-box2" style="background-color: #00a2db;">
                            <h4 class="info-title2">DER STIMAWELL® 120MTRS MESSEPREIS</h4>
                            <p>Das StimaWELL® 120MTRS Messe-Preismodell setzt sich aus den folgenden Punkten zusammen:</p>
                            <ul style="color: #FFF; padding-bottom: 20px; margin-top: 10px;">
                                <li>
                                    <strong>129,-€</strong> Servicegebühr / Monat, bei 24-monatiger Laufzeit
                                </li>
                                <li>
                                    Der empfohlene Endkunden-VK-Preis beträgt 99€ / Monat.
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="info-box2">
                            <h4 class="info-title2" style="color: #000;">ZUSATZINFORMATIONEN</h4>
                            <p style="color: #000;"></p>
                            <ul style="color: #000; padding-bottom: 20px; margin-top: 10px;">
                                <li>Defekte Geräte werden garantiert binnen 48 Stunden ersetzt.</li>
                                <li>Das StimaWELL® MTRS Messepreis-Modell ist bereits ab dem dritten Kunden profitabel.</li>
                            </ul> 
                        </div>
                   	</div>
                    <div class"col-sm-12 col-md-12 col-lg-12">
                         <img src="img/tabelle_120mtrs-fibo.jpg" style="width: 100%; margin: 25px 15px 10px 15px;">
                        <p style="padding-left: 15px;">(Dargestellte Beträge ergeben sich bei 129,-€ Servicegebühr im Monat und 24-monatiger Laufzeit, sowie einem Endkunden-VK-Preis von 99 Euro brutto)</p>
                    </div>
                </div>
            </div> 
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <center>
                            <a href="#form"><input style="margin-top: 25px; margin-bottom: 25px; cursor: pointer; background-color: rgb(0, 162, 219); border-color: rgb(0, 162, 219);" type="submit" class="col-sm-8 btn btn-danger btn-xl js-scroll-trigger" value="JETZT INFORMATIONEN ANFORDERN" style="width: 100%;"></a>
                        </center>   
                    </div>
                </div>
            </div>
        </section>
        
        <section class="produktinformation-1">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-1"></div>
                    <div class="col-sm-12 col-md-5">
                        <div class="info-box">
                            <h4 class="info-title">DAS STIMAWELL® 120MTRS SYSTEM</h4>
                            <p>Das medizinisch zugelassene StimaWELL® 120MTRS System ist ein einzigartiges, patentiertes therapeutisches Instrument zur Behandlung von Rückenbeschwerden. Diese sind in vielen Physiopraxen ein wichtiges Thema: Schließlich leiden nahezu 4 von 5 Erwachsenen in Deutschland mindestens einmal im Leben unter akuten oder chronischen Rückenschmerzen.</p>
							<p>Das StimaWELL® 120MTRS System kombiniert 4 Therapiebausteine miteinander, die sich seit langem in der täglichen Praxis bewährt haben:</p>
							<ul style="color: #FFF; padding-bottom: 20px;">
								<li>Schmerztherapie mittels TENS-Impulsen</li>
								<li>Muskeltherapie mittels EMS</li>
								<li>Dynamische Tiefenmassagen</li> 
								<li>Wärmetherapie</li>
							</ul>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-5 info-img">
                        <img class="img-fluid" src="img/praxisbild.jpg" alt="relax ems praxisbild"/>
                    </div>
                    <div class="col-sm-12 col-md-1"></div>
                </div>
                <div class="row" style="margin-top: 14px;">
                    <div class="col-sm-12 col-md-1"></div>
                    <div class="col-sm-12 col-md-5 info-img">
						<img class="img-fluid" id="mobile-img" src="img/relax-ems-model.jpg" alt="relax ems model"/>
                    </div>
                    <div class="col-sm-12 col-md-5">
                        <div class="info-box">
                            <p id="mobile-text" style="padding-top: 15px;">Die sanften Mittelfrequenz-Impulse des StimaWELL® 120MTRS Systems werden über eine ergonomische 12-Kanal-Stimulationsmatte an den Rücken abgegeben, die dich bis auf 40 °C erwärmen können. Damit kann wahlweise der komplette Rücken oder einzelne Teilbereiche gezielt und angepasst behandelt werden. Die angenehmen Dynamischen Tiefenwellen des StimaWELL® 120MTRS Systems werden von Patienten als Klopfmassage, Knetmassage oder mehr wahrgenommen – so verknüpfst du angenehme Entspannung mit intensiver Muskelstimulation.</p>
							<p style="padding-bottom: 15px;">Das StimaWELL® 120MTRS System lässt sich einfach und individuell anpassen. Über die Einstellung der Mattengröße sowie die „Kalibrierung“ der einzelnen Kanäle stimmt sich das System auf den Rücken und das Empfinden jedes einzelnen Patienten ab.</p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-1"></div>
                </div>
                <div class="row" style="margin-top: 14px;">
                    <div class="col-sm-12 col-md-1"></div>
                    <div class="col-sm-12 col-md-5">
                        <div class="info-box">
                            <h4 class="info-title">ANAMNESEGESTÜTZTE PROGRAMMAUSWAHL VEREINFACHT DIE RÜCKENTHERAPIE</h4>
                            <p>Im Modus „Automatische Programmauswahl nach Indikationen“ gibst du die anamnestischen Daten wie Verlaufsform, Lokalisation und Stärke der Schmerzen ein. Das StimaWELL® 120MTRS System beginnt automatisch einen Therapiezyklus mit dem passenden Programm. Bei jeder weiteren Sitzung deines Patienten wird das Programm automatisch an den Therapieverlauf angepasst. Natürlich können erfahrene Anwender auch jederzeit im Modus „Manuelle Programmauswahl“ spezifische Therapieprogramme aus dem vielfältigen Programmangebot auswählen oder eigene Programme erstellen.</p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-5 info-img">
                        <img class="img-fluid" id="mobile-img" src="img/geraet_komplett_info.jpg" alt="relax ems komplett"/>
                    </div>
                    <div class="col-sm-12 col-md-1"></div>
                </div>
				<div class="row" style="margin-top: 14px;">
                    <div class="col-sm-12 col-md-1"></div>
                    <div class="col-sm-12 col-md-5 info-img">
                        <img class="img-fluid" id="mobile-img" src="img/geraet_bg.jpg" alt="relax ems komplett"/>
                    </div>
					<div class="col-sm-12 col-md-5">
                        <div class="info-box">
                            <h4 class="info-title">MONITORING DANK DATENBANKEN & HISTORIE</h4>
                            <p>Im Modus „Automatische Programmauswahl nach Indikationen“ ist eine detaillierte Auswertung der Therapiezyklen möglich. Das erlaubt dir beispielsweise, eine Veränderung der Schmerzen deines Patienten über einen längeren Zeitraum nachzuverfolgen.</p>
							<p style="padding-bottom: 15px;">Auch eine Auswertung der gesamten Patientendatenbank ist möglich. Filtere nach definierten Kriterien und behalten so den Überblick über die Effizienz der Therapie bei unterschiedlichen Patientenkollektiven.</p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-1"></div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <center>
                            <a href="#form"><input style="margin-top: 25px; cursor: pointer; background-color: rgb(0, 162, 219); border-color: rgb(0, 162, 219);" type="submit" class="col-sm-8 btn btn-danger btn-xl js-scroll-trigger" value="JETZT INFORMATIONEN ANFORDERN" style="width: 100%;"></a>
                        </center>   
                    </div>
                </div>
            </div>
        </section>
		
		<section class="text-center" style="padding-top: 4rem; padding-bottom: 4rem; background-color: #EEEEEE;"> 
            <div class="container">
                <h2 style="color: #000000;">StimaWELL® 120MTRS - Funktionsweise und Wirkung</h2>
                <hr style="border-color: #00a2db;">
                <div class="row">
                    <div class="col-sm-12 col-md-2">
					</div>
					<div class="col-sm-12 col-md-8">
						<center>
							<div class="videoWrapper" style="margin-top: 15px;">
								<iframe width="560" height="315" src="https://www.youtube.com/embed/lZ4U8dq1Gq0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
							</div>
						</center>
                    </div>
					<div class="col-sm-12 col-md-2">
				</div>
                </div>
            </div>
        </section>
		
        <section class="text-center testimonials testimonials-b2b"> 
            <div class="container">
                <h2 style="color: #000000;">Das sagen unsere Partner über StimaWELL</h2>
                <hr style="border-color: #00a2db;">
                <p>Das können unsere Partner über die Zusammenarbeit mit StimaWELL berichten.</p>
                <p>&nbsp;</p>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                            <img class="img-fluid rounded-circle mb-3" src="img/testimonial-partner_b.jpg" alt="formwerk hermsdorf">
                            <h5>Birgit F., Body World Velden</h5>
                            <p class="font-weight-light mb-0"><i>"Die Body World/Velden betreibt das StimaWell Shop in Shop Konzept sehr erfolgreich. Mit wenig Aufwand bietet das Konzept eine zusätzliche Einnahmequelle. Unser Angebot wird abgerundet, da wir das StimaWell EMS System und die Relax Rückenmatte betreiben."</i></p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                            <img class="img-fluid rounded-circle mb-3" src="img/partner-testimonials_neogym.jpg" alt="neogym">
                            <h5>NEOGYM</h5>
                            <p class="font-weight-light mb-0"><i>"Mit unserer Studiokette arbeiten wir seit über einem Jahr mit StimaWELL zusammen und sind sehr zufrieden mit dieser Entscheidung. Zuvor haben wir diverse andere Hersteller ausprobiert und wissen, wie wichtig es ist, einen verlässlichen Partner an seiner Seite zu haben. Das gesamte Paket stimmt hier einfach - danke dafür!"</i></p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                            <img class="img-fluid rounded-circle mb-3" src="img/testimonial-partner_a.jpg" alt="rueckenwerkstatt raubling">
                            <h5>Rückenwerkstatt Raubling</h5>
                            <p class="font-weight-light mb-0"><i>"Die EMS-Rückenmassage-Liege mit dynamischen EMS-Tiefenwellen und Wärme für einen entspannten und gekräftigten Rücken erfeut sich größter Beliebtheit bei unseren Kunden. Viele Kunden genießen es 1x pro Woche, nachdem sie die Liege innerhalb weniger Anwendungen von langjährigen Rückenschmerzen befreit hatte."</i></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="experte">
            <div class="container">
                <center>
                    <h2>Dein persönlicher Ansprechpartner und EMS-Experte <span style="color: #00a2db;">Dominic Heilig</span></h2>
                    <hr style="border-color: #00a2db;">
                </center>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-3" style="margin-top: 30px;">
                        <center>
                            <img src="img/ansprechpartner_dominic-heilig.png" alt="ems-experte dominic heilig" class="experte-img img-fluid">
                            <p class="experte-img-subtext" style="color: #00a2db; margin-top: 15px; margin-bottom: 0px;"><strong>DOMINIC HEILIG</strong></p>
                            <p class="experte-img-subtext"><i>StimaWELL-EMS Experte</i></p>
                        </center>
                    </div>
                    <div class="col-sm-12 col-md-9 experte-text-container">
                        <p class="experte-text">"Mein Name ist Dominic Heilig. Seit meinem 5. Lebensjahr treibe ich leidenschaftlich gerne Sport. Aus dieser Leidenschaft heraus resultiert mein Beruf.  Während meines Studiums zum Bachelor of Arts in Fitnessökonomie durchlief ich mehrere Stationen. Durch die Vielzahl an Stationen konnte ich ein umfassendes Fachwissen aufbauen, was ich gerne heute an meine Kunden und Partner weitergebe.</p>
                        <p class="experte-text">Durch meine Arbeit in zwei Microstudios habe ich täglich im Umgang mit den Kunden gelernt, welche Herausforderungen zu lösen sind, welche Ziele die Kunden haben und vor allem wie man diese Ziele am besten erreicht. Seit 2017 betreute ich für StimaWELL unsere Kunden für die Ganzkörper EMS Systeme. Meine Arbeit bringt mich mit vielen verschiedenen Kunden zusammen. Ich helfe sowohl Privatkunden als auch professionellen Anwendern wie Physiotherapeuten, Fitnessstudios, Sonnenstudios, Hebammen, Personal-Trainern, Heilpraktikern oder EMS-Studios. Mit meiner mehrjährigen Erfahrung im EMS-Training Bereich berate ich sehr gerne Existenzgründer und helfe diesen mit innovativen EMS Produkten von schwa-medico erfolgreich eine Existenz aufzubauen."</p>
                    </div>         
                </div>
            </div>  
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <center>
                            <a href="#form"><input style="margin-top: 40px; cursor: pointer; background-color: rgb(0, 162, 219); border-color: rgb(0, 162, 219);" type="submit" class="col-sm-8 btn btn-danger btn-xl js-scroll-trigger" value="JETZT INFORMATIONEN ANFORDERN" style="width: 100%;"></a>
                        </center>   
                    </div>
                </div>
            </div>
        </section>  



        <!-- MODAL -->
        <div class="modal fade" id="modal-video" tabindex="-1" role="dialog" aria-labelledby="modal-video-label">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="background-color: #1A1A1A;">
                    <div class="modal-header" style="border-bottom: none; padding-top: 9px;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="color: #FFF;">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="padding: 0px 0px 40px 0px;">
                        <div class="modal-video">
                            <center>
                                <iframe width="1920" height="1080" src="https://www.youtube.com/embed/18xXcSH0cY4?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow=" encrypted-media" allowfullscreen></iframe>
                                <form style="display: inline;" method = "post"  action="<?php echo $jetzt_kaufen; ?>"><button  data-dismiss="modal" style="margin-top: 15px; cursor: pointer; background-color: rgb(0, 162, 219); border-color: rgb(0, 162, 219);" type="button" class="col-sm-8 btn btn-danger">JETZT INFORMATIONEN ANFORDERN</button></form>
                            </center> 
                        </div>
                    </div>
                </div> 
            </div>
        </div>
        <!-- ./MODAL -->

        <section>
            <div class="clearspace"></div>
        </section>  

        <footer class="footer bg-light">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 h-100 text-center text-lg-left my-auto" style="height:auto!important">
                        <ul class="list-inline mb-2">
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/impressum" target="_blank">Impressum</a>
                            </li>
                            <li class="list-inline-item">&sdot;</li>
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/agb" target="_blank">AGB</a>
                            </li>
                            <li class="list-inline-item">&sdot;</li>
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/datenschutz" target="_blank">Datenschutz</a>
                            </li>
                        </ul>
                        <p style="padding-top:15px;" class="text-muted small mb-4 mb-lg-0"><strong>✔</strong> 40 JAHRE MADE IN GERMANY&nbsp;&nbsp;&nbsp;<strong>✔</strong> DIREKT VOM HERSTELLER&nbsp;&nbsp;&nbsp;<strong>✔</strong> SERVICE: 06443 8333-500</p>
                    </div>
                    <div class="col-lg-4 h-100 text-center text-lg-right my-auto" style="height:auto!important">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item mr-3">
                                <a href="https://www.facebook.com/stimawellems/" target="_blank">
                                    <i class="fa fa-facebook fa-2x fa-fw"></i>
                                </a>
                            </li>
                            <li class="list-inline-item mr-3">
                                <a href="https://www.instagram.com/stimawell.ems/" target="_blank">
                                    <i class="fa fa-instagram fa-2x fa-fw"></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                </div>   
            </div>
        </footer>
    </body>
</html>        

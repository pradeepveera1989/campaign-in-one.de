<?php
// Get the config data from config.ini       

require 'config.php';

// Get the config data from config.ini       
$conf_obj = new config('config.ini');
$config = $conf_obj->getAllConfig();

$currentUrl = parse_url($_SERVER['REQUEST_URI']);

// Keep URL Parameters
$redirect_path = $config[GeneralSetting][KeepURLParameter] ? $config[GeneralSetting][Redirect][RedirectPath] . $currentUrl["query"] : $config[GeneralSetting][Redirect][RedirectPath];
// Redirect Time
$redirect_time = $config[GeneralSetting][Redirect][RedirectTime];

// Webgains
$webgains = $_GET['trafficsource'] === "webgains" ? "true" : "false";
$lead_reference = empty($_GET['lead_reference']) ? " " : $_GET['lead_reference'];

?>
<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">  
<?php if ($config[GeneralSetting][Redirect][RedirectStatus]) { ?>  
            <meta http-equiv="refresh" content="<?php echo $redirect_time; ?>;url=<?php echo $redirect_path; ?>" />  
        <?php } ?>
        <title>EMS Training zu Hause</title>
        <!-- implementation bootstrap -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- implementation fontawesome icons -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- implementation simpleline icons -->
        <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
        <!-- implementation googlefonts -->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
        <!-- implementation Animated Header -->
        <!-- implementation custom css -->
        <link href="css/creative.css" rel="stylesheet">
        <!-- implementation animate css -->
        <link href="css/animate.css" rel="stylesheet">

        
        <!-- Google Analytics -->
        <?php include_once 'clients/tracking/google/google.php'; ?>        

        <!-- Facebook Pixel -->
        <?php include_once('clients/tracking/facebook/facebook.php'); ?>          
        
    </head>


    <!-- Outbrain Tracking -->
    <?php $config[TrackingTools][EnableOutbrain] ? include_once'clients/tracking/outbrain/outbrain_redirect.php' : ' '; ?>

    <body>
        <!--Adeblo Tracking-->
        <?php $config[TrackingTools][EnableAdeblo] ? include_once'clients/tracking/adeblo/adeblo.php' : ' ' ?>
        
        <!-- Remarketing Target360 Tracking -->
        <?php $config[TrackingTools][EnableTarget360] ? include_once 'clients/tracking/target360/target_redirect.php' : ' '; ?>

        <!-- DESKTOP NAV -->
        <nav class="navbar navbar-light bg-light static-top">   
            <div class="container">
                <a class="navbar-brand" style="text-transform: uppercase;" href="https://www.stimawell-ems.de/" target="_blank"><img style="width:200px;" alt="EMS-Training-zuhause" src="img/logo.svg"></a>
                <span style="text-transform:uppercase; "><i class="fas fa-check"></i> 40 JAHRE MADE IN GERMANY&nbsp;&nbsp;<i class="fas fa-check"></i> DIREKT VOM HERSTELLER&nbsp;&nbsp;<i class="fas fa-check"></i> SERVICE: <strong>+49 6443 83330</strong></span>
            </div>
        </nav>

        <!-- MOBILE HEADER-->  
        <div class="mobile-nav"><center><img style="width:200px;" alt="EMS-Training-zuhause" src="img/logo.svg"></center></div>

        <!-- FORM -->
        <section class="form-container" style="padding-top: 250px; padding-bottom: 250px; height:100%;" id="about">
            <div class="container">  
                <div class="row">
                    <div class="col-sm-12">
                        <center>

                            <h1>Bitte überprüfe dein E-Mail Postfach.</h1>
                            <hr>
                            <p>Wir haben dir eine E-Mail mit einem Bestätigungslink an deine genannte E-Mail-Adresse gesendet. Solltest du in den nächsten 15 Minuten keine Mail erhalten, schaue bitte auch in dein Spam oder Junk Ordner.</p> 
                        </center>
                    </div>
                </div>
            </div>  

        </section>  
        <div class="clearspace"></div>     
        <!-- Footer -->
        <footer class="footer bg-light">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 h-100 text-center text-lg-left my-auto" style="height:auto!important">
                        <ul class="list-inline mb-2">
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/impressum" target="_blank">Impressum</a>
                            </li>
                            <li class="list-inline-item">&sdot;</li>
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/agb" target="_blank">AGB</a>
                            </li>
                            <li class="list-inline-item">&sdot;</li>
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/datenschutz" target="_blank">Datenschutz</a>
                            </li>
                            <li class="list-inline-item">&sdot;</li>
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/de/mietbedingungen" target="_blank">Mietbedingungen</a>
                            </li>
                        </ul>
                        <p style="padding-top:15px;" class="text-muted small mb-4 mb-lg-0"><strong>✔</strong> 40 JAHRE MADE IN GERMANY&nbsp;&nbsp;&nbsp;<strong>✔</strong> DIREKT VOM HERSTELLER&nbsp;&nbsp;&nbsp;<strong>✔</strong> SERVICE: 06443 4369914</p>
                    </div>
                    <div class="col-lg-4 h-100 text-center text-lg-right my-auto" style="height:auto!important">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item mr-3">
                                <a href="https://www.facebook.com/stimawellems/" target="_blank">
                                    <i class="fa fa-facebook fa-2x fa-fw"></i>
                                </a>
                            </li>
                            <li class="list-inline-item mr-3">
                                <a href="https://www.instagram.com/stimawell.ems/" target="_blank">
                                    <i class="fa fa-instagram fa-2x fa-fw"></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                </div>   
            </div>
        </footer>


        <!-- </Webgains Tracking Code> -->    
        <?php $config[TrackingTools][EnableWebGains] ? include_once 'clients/tracking/webgains/webgains_redirect.php' : ' '; ?> 

        <!-- Bootstrap core JavaScript -->
        <script src="vendor/jquery/jquery.min.js">
        </script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js">
        </script>
        <script src="vendor/header-animation/demo-1.js">
        </script>  
        <script src="vendor/header-animation/TweenLite.min.js">
        </script>
        <script src="vendor/header-animation/EasePack.min.js">
        </script>
        <script src="vendor/header-animation/rAF.js">
        </script>
        <script src="vendor/header-animation/demo-1.js">
        </script>  

    </body>

</html>

<?php
// Get the config data from config.ini       
$config = parse_ini_file("config.ini", true);
$currentUrl = parse_url($_SERVER['REQUEST_URI']);
$redirect_path = $config[GeneralSetting][Redirect][RedirectPath];

if ($config[GeneralSetting][KeepURLParameter]) {
    $redirect_path = $redirect_path . $currentUrl["query"];
}
$redirect_time = $config[GeneralSetting][Redirect][RedirectTime];

// Webgains
$webgains = false;
if ($_GET['trafficsource'] === "webgains") {
    $lead_reference = $_GET['lead_reference'];
    $webgains = true;
}
?>
<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">  
<?php if ($config[GeneralSetting][Redirect][RedirectStatus]) { ?>  
            <meta http-equiv="refresh" content="<?php echo $redirect_time; ?>;url=<?php echo $redirect_path; ?>" />  
        <?php } ?>
        <title>EMS Training zu Hause</title>
        <!-- implementation bootstrap -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- implementation fontawesome icons -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- implementation simpleline icons -->
        <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
        <!-- implementation googlefonts -->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
        <!-- implementation Animated Header -->
        <!-- implementation custom css -->
        <link href="css/creative.css" rel="stylesheet">
        <!-- implementation animate css -->
        <link href="css/animate.css" rel="stylesheet">
    </head>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-16152616-31');
    </script>
    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s)
        {
            if (f.fbq)
                return;
            n = f.fbq = function () {
                n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq)
                f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
                'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '2069635113314986');
        fbq('track', 'PageView');
        fbq('track', 'Purchase');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=2069635113314986&ev=PageView&noscript=1"
                   /></noscript>
    <!-- End Facebook Pixel Code -->
    <body>
        <?php
        $src = "https://adeblo.com/tracker/loader?rid=5&ord=" . $_GET['lead_reference'] . "&rev=1450";
        ?>
        <img src="<?php echo $src ?>" width="3" height="3">
        <script type="text/javascript" src="vendor/jquery-cookie-master/src/jquery.cookie.js"></script>  
        <script type="text/javascript">

        $(document).ready(function () {
            console.log($.cookie('campaign'));
        });

        </script>

        <!-- Conversion Pixel - Stimawell-TY-Dez18 - DO NOT MODIFY -->
        <img src="https://secure.adnxs.com/px?id=1059710&seg=16011161&t=2" width="1" height="1" />
        <!-- End of Conversion Pixel -->


        <!-- DESKTOP NAV -->
        <nav class="navbar navbar-light bg-light static-top">   
            <div class="container">
                <a class="navbar-brand" style="text-transform: uppercase;" href="https://www.stimawell-ems.de/" target="_blank"><img style="width:200px;" alt="EMS-Training-zuhause" src="img/logo.svg"></a>
                <span style="text-transform:uppercase; "><i class="fas fa-check"></i> 40 JAHRE MADE IN GERMANY&nbsp;&nbsp;<i class="fas fa-check"></i> DIREKT VOM HERSTELLER&nbsp;&nbsp;<i class="fas fa-check"></i> SERVICE: <strong>+49 6443 83330</strong></span>
            </div>
        </nav>

        <!-- MOBILE HEADER-->  
        <div class="mobile-nav"><center><img style="width:200px;" alt="EMS-Training-zuhause" src="img/logo.svg"></center></div>

        <!-- FORM -->
        <section class="form-container" style="padding-top: 250px; padding-bottom: 250px; height:100%;" id="about">
            <div class="container">  
                <div class="row">
                    <div class="col-sm-12">
                        <center>

                            <h1>Bitte überprüfe dein E-Mail Postfach.</h1>
                            <hr>
                            <p>Wir haben dir eine E-Mail mit einem Bestätigungslink an deine genannte E-Mail-Adresse gesendet. Solltest du in den nächsten 15 Minuten keine Mail erhalten, schaue bitte auch in dein Spam oder Junk Ordner.</p> 
                        </center>
                    </div>
                </div>
            </div>  

        </section>  
        <div class="clearspace"></div>     
        <!-- Footer -->
        <footer class="footer bg-light">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 h-100 text-center text-lg-left my-auto" style="height:auto!important">
                        <ul class="list-inline mb-2">
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/impressum" target="_blank">Impressum</a>
                            </li>
                            <li class="list-inline-item">&sdot;</li>
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/agb" target="_blank">AGB</a>
                            </li>
                            <li class="list-inline-item">&sdot;</li>
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/datenschutz" target="_blank">Datenschutz</a>
                            </li>
                            <li class="list-inline-item">&sdot;</li>
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/de/mietbedingungen" target="_blank">Mietbedingungen</a>
                            </li>
                        </ul>
                        <p style="padding-top:15px;" class="text-muted small mb-4 mb-lg-0"><strong>✔</strong> 40 JAHRE MADE IN GERMANY&nbsp;&nbsp;&nbsp;<strong>✔</strong> DIREKT VOM HERSTELLER&nbsp;&nbsp;&nbsp;<strong>✔</strong> SERVICE: 06443 4369914</p>
                    </div>
                    <div class="col-lg-4 h-100 text-center text-lg-right my-auto" style="height:auto!important">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item mr-3">
                                <a href="https://www.facebook.com/stimawellems/" target="_blank">
                                    <i class="fa fa-facebook fa-2x fa-fw"></i>
                                </a>
                            </li>
                            <li class="list-inline-item mr-3">
                                <a href="https://www.instagram.com/stimawell.ems/" target="_blank">
                                    <i class="fa fa-instagram fa-2x fa-fw"></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                </div>   
            </div>
        </footer>


        <!--Web Tracking-->
        <script>
            (function (w, e, b, g, a, i, n, s) {
                w['ITCLKOBJ'] = a;w[a] = w[a] || function () {
                    (w[a].q = w[a].q || []).push(arguments)
                }, w[a].l = 1 * new Date();
                i = e.createElement(b), n = e.getElementsByTagName(b)[0];i.async = 1;
                i.src = g;
                n.parentNode.insertBefore(i, n)
            })(window, document, 'script', 'https://analytics.webgains.io/clk.min.js', 'ITCLKQ');
            ITCLKQ('set', 'internal.cookie', true);
            ITCLKQ('click');
        </script>  

        <!-- Read the Webgains cookie -->
        <script type="text/javascript ">

            // Function to read the cookie
            function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
            }
            return null;
            }  

            // Check the status of the source 
            var status = "<?= $webgains ?>";
            var lead_ref = "";

            // Check if the status is true and Webgains cookie is set
            if (readCookie('webgains') && status) { 
            lead_ref = "<?= $lead_reference ?>";
            }        


        </script>

        <!-- <Webgains Tracking Code> -->
        <!-- <Variablendefinitionen> -->
        <script language="javascript" type="text/javascript">

            var wgOrderReference = lead_ref;
            var wgOrderValue = "0";
            var wgEventID = 1037645;
            var wgComment = "";
            var wgLang = "de_DE";
            var wgsLang = "javascript-client";
            var wgVersion = "1.2";
            var wgProgramID = 269785;
            var wgSubDomain = "track";
            var wgCheckSum = "";
            var wgItems = "";
            var wgVoucherCode = "";
            var wgCustomerID = "";
            var wgCurrency = "EUR";

            console.log(wgOrderReference);

        </script>
        <!-- </Variablendefinitionen> -->


        <!-- <Webgains Tracking Code NG> -->
        <script language="javascript" type="text/javascript">
            (function (w, e, b, g, a, i, n, s) {
                w['ITCVROBJ'] = a;
                w[a] = w[a] || function () {
                    (w[a].q = w[a].q || []).push(arguments)
                }, w[a].l = 1 * new Date();
                i = e.createElement(b),
                        n = e.getElementsByTagName(b)[0];
                i.async = 1;
                i.src = g;
                n.parentNode.insertBefore(i, n)
            })(window, document, 'script', 'https://analytics.webgains.io/cvr.min.js', 'ITCVRQ');
            ITCVRQ('set', 'trk.programId', wgProgramID);
            ITCVRQ('set', 'cvr', {
                value: wgOrderValue,
                currency: wgCurrency,
                language: wgLang,
                eventId: wgEventID,
                orderReference: wgOrderReference,
                comment: wgComment,
                multiple: '',
                checksum: '',
                items: wgItems,
                customerId: wgCustomerID,
                voucherId: wgVoucherCode
            });
            ITCVRQ('conversion');
        </script>
        <!-- </Webgains Tracking Code NG> -->
        <!-- </Webgains Tracking Code> --> 


        <!-- Bootstrap core JavaScript -->
        <script src="vendor/jquery/jquery.min.js">
        </script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js">
        </script>
        <script src="vendor/header-animation/demo-1.js">
        </script>  
        <script src="vendor/header-animation/TweenLite.min.js">
        </script>
        <script src="vendor/header-animation/EasePack.min.js">
        </script>
        <script src="vendor/header-animation/rAF.js">
        </script>
        <script src="vendor/header-animation/demo-1.js">
        </script>  

    </body>

</html>

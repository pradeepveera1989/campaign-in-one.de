<?php
// Get the config data from config.ini       
$config = parse_ini_file("config.ini", true);
$currentUrl = parse_url($_SERVER['REQUEST_URI']);
$redirect_path = $config[GeneralSetting][Redirect][RedirectPath];

if ($config[GeneralSetting][KeepURLParameter]) {
    $redirect_path = $redirect_path . $currentUrl["query"];
}
$redirect_time = $config[GeneralSetting][Redirect][RedirectTime];

// Webgains
$webgains = false;
if ($_GET['trafficsource'] === "webgains") {
    $webgains = true;
}
$lead_reference = $_GET['lead_reference'];
?>
<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">  
        <?php if ($config[GeneralSetting][Redirect][RedirectStatus]) { ?>  
            <meta http-equiv="refresh" content="<?php echo $redirect_time; ?>;url=<?php echo $redirect_path; ?>" />  
        <?php } ?>
        <title>EMS Training zu Hause</title>
        <!-- implementation bootstrap -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- implementation fontawesome icons -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- implementation simpleline icons -->
        <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
        <!-- implementation googlefonts -->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
        <!-- implementation Animated Header -->
        <!-- implementation custom css -->
        <link href="css/creative.css" rel="stylesheet">
        <!-- implementation animate css -->
        <link href="css/animate.css" rel="stylesheet">
    </head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-16152616-31"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-16152616-31');
    </script>
    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s)
        {
            if (f.fbq)
                return;
            n = f.fbq = function () {
                n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq)
                f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
                'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '2069635113314986');
        fbq('track', 'PageView');
        fbq('track', 'Purchase');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=2069635113314986&ev=PageView&noscript=1"
                   /></noscript>
    <!-- End Facebook Pixel Code -->

    <!-- Outbrain Tracking -->
    <?php if ($config[TrackingTools][EnableOutbrain]) { ?>
        <script data-obct type="text/javascript">
            /** DO NOT MODIFY THIS CODE**/
            !function (_window, _document) {
                var OB_ADV_ID = '007035eac06ecc119b917903a5dc025178';
                if (_window.obApi) {
                    var toArray = function (object) {
                        return Object.prototype.toString.call(object) === '[object Array]' ? object : [object];
                    };
                    _window.obApi.marketerId = toArray(_window.obApi.marketerId).concat(toArray(OB_ADV_ID));
                    return;
                }
                var api = _window.obApi = function () {
                    api.dispatch ? api.dispatch.apply(api, arguments) : api.queue.push(arguments);
                };
                api.version = '1.1';
                api.loaded = true;
                api.marketerId = OB_ADV_ID;
                api.queue = [];
                var tag = _document.createElement('script');
                tag.async = true;
                tag.src = '//amplify.outbrain.com/cp/obtp.js';
                tag.type = 'text/javascript';
                var script = _document.getElementsByTagName('script')[0];
                script.parentNode.insertBefore(tag, script);
            }(window, document);
            obApi('track', 'PAGE_VIEW');
obApi('track', '30sec');
        </script>    
    <?php } ?>    
    <!-- End of Outbrain Tracking -->
    <body>

        <!-- Remarketing Target360 Tracking -->
        <?php if ($config[TrackingTools][EnableTarget360]) { ?>        
            <!-- oliro private network / Advertiser #48403 / Dynamic-Retargeting-Tag / Productfeed #14927 (Dummy) / Transaction-Event / generated Wed, 23 Jan 2019 10:44:21 +0100 -->
            <script language="JavaScript" type="text/javascript">

                /* ALL COMMENTS IN THIS JAVASCRIPT-TAG MAY BE REMOVED */

                /*
                 * Insert your items or products of productfeed #14927 (Dummy) in the parameter "tp_rtrgt_items".
                 * This is a single-productfeed-tag, so you do not need to specify the productfeed-id individually for every item or product you pass.
                 *   a) Without passing the product-quantity: ID,ID,...
                 *   b) For better retargeting-results pass the product-quantity: ID|QUANTITY,ID|QUANTITY,...
                 */

                var tp_rtrgt_items = '<?= $lead_reference ?>';

                /*
                 * Optionally you can pass your predefined retargeting-segments to do 
                 * both dynamic product-based and segmented retargeting.
                 */

                var tp_rtrgt_segment = '';

                /* DO NOT change the javascript below */
                var tp_rtrgt_random = Math.random() * 10000000000000000;
                var tp_rtrgt_url = 'http' + (('https:' == document.location.protocol) ? 's' : '') + '://ad.ad-srv.net/retarget?a=48403&version=1&event=transaction&cat=14927';
                tp_rtrgt_url += '&segment=' + tp_rtrgt_segment;
                tp_rtrgt_url += '&items=' + tp_rtrgt_items;
                document.write('<div id="tp_rtrgt_div_' + tp_rtrgt_random + '" style="position:absolute; visibility:hidden; left:0px; top:0px; width:1px; height:1px; border:0"><iframe id="tp_rtrgt_iframe_' + tp_rtrgt_random + '" name="tp_rtrgt_iframe_' + tp_rtrgt_random + '" src="' + tp_rtrgt_url + '" scrolling="no" width="1" height="1"></iframe></div>');
            </script>    
        <?php } ?>    

        <!--Adeblo Tracking-->
        <?php
        if ($config[TrackingTools][EnableAdeblo]) {
            $src = "https://adeblo.com/tracker/loader?rid=5&ord=" . $_GET['lead_reference'] . "&rev=1450";
            ?>
            <img src="<?php echo $src ?>" width="3" height="3">
        <?php } ?>  
        <!--End of Adeblo Tracking-->      

        <script type="text/javascript" src="vendor/jquery-cookie-master/src/jquery.cookie.js"></script>  


      <!-- DESKTOP NAV -->
        <nav class="navbar navbar-light bg-light static-top">   
            <div class="container">
                <a class="navbar-brand" style="text-transform: uppercase;" href="https://www.stimawell-ems.de/" target="_blank"><img style="height:46px;" alt="EMS-Training-zuhause" src="img/logo-120mtrs.png"><img style="height:40px; vertical-align: middle; margin-left: 20px;" alt="FIBO2019" src="img/fibo-logo.png"></a>
                <span class="navbar-details"style="text-transform:uppercase; text-align: right;"><i class="fas fa-check"></i> 40 JAHRE MADE IN GERMANY&nbsp;&nbsp;<i class="fas fa-check"></i> SERVICE: <strong>06443 8333-500</strong></span>
            </div>
        </nav>

        <!-- MOBILE HEADER-->  
        <div class="mobile-nav"><center><img style="width:200px;" alt="EMS-Training-zuhause" src="img/logo.svg"></center></div>


        <!-- FORM -->
        <section class="form-container" style="background-image: url('img/fibo-frau.jpg'); padding-top: 250px; padding-bottom: 250px; height:100%;" id="about">
            <div class="container">  
                <div class="row">
                    <div class="col-sm-12">
                        <center>

                            <h1 style="color:#fff;">Bitte überprüfe dein E-Mail Postfach.</h1>
                            <hr>
                            <p style="color:#fff;">Wir haben Dir eine E-Mail mit einem Bestätigungslink an deine genannte E-Mail-Adresse gesendet. Solltest Du in den nächsten 15 Minuten keine Mail erhalten, schaue bitte auch in dein Spam oder Junk Ordner.</p> 
                        </center>
                    </div>
                </div>
            </div>  

        </section>  
        <div class="clearspace"></div>     
        <!-- Footer -->
        <footer class="footer bg-light">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 h-100 text-center text-lg-left my-auto" style="height:auto!important">
                        <ul class="list-inline mb-2">
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/impressum" target="_blank">Impressum</a>
                            </li>
                            <li class="list-inline-item">&sdot;</li>
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/agb" target="_blank">AGB</a>
                            </li>
                            <li class="list-inline-item">&sdot;</li>
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/datenschutz" target="_blank">Datenschutz</a>
                            </li>
        
                            
                        </ul>
                        <p style="padding-top:15px;" class="text-muted small mb-4 mb-lg-0"><strong>✔</strong> 40 JAHRE MADE IN GERMANY&nbsp;&nbsp;&nbsp;<strong>✔</strong> DIREKT VOM HERSTELLER&nbsp;&nbsp;&nbsp;<strong>✔</strong> SERVICE: 06443 4369914</p>
                    </div>
                    <div class="col-lg-4 h-100 text-center text-lg-right my-auto" style="height:auto!important">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item mr-3">
                                <a href="https://www.facebook.com/stimawellems/" target="_blank">
                                    <i class="fa fa-facebook fa-2x fa-fw"></i>
                                </a>
                            </li>
                            <li class="list-inline-item mr-3">
                                <a href="https://www.instagram.com/stimawell.ems/" target="_blank">
                                    <i class="fa fa-instagram fa-2x fa-fw"></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                </div>   
            </div>
        </footer>

        <!--Check for WebGains tracking -->
        <?php if ($config[TrackingTools][EnableWebGains]) { ?>   
            <script>
                    (function (w, e, b, g, a, i, n, s) {
                        w['ITCLKOBJ'] = a;
                        w[a] = w[a] || function () {
                            (w[a].q = w[a].q || []).push(arguments)
                        }, w[a].l = 1 * new Date();
                        i = e.createElement(b), n = e.getElementsByTagName(b)[0];
                        i.async = 1;
                        i.src = g;
                        n.parentNode.insertBefore(i, n)
                    })(window, document, 'script', 'https://analytics.webgains.io/clk.min.js', 'ITCLKQ');
                    ITCLKQ('set', 'internal.cookie', true);
                    ITCLKQ('click');
            </script>  

            <!-- Read the Webgains cookie -->
            <script type="text/javascript ">

                // Function to read the cookie
                function readCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for(var i=0;i < ca.length;i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1,c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
                }
                return null;
                }  

                // Check the status of the source 
                var status = "<?= $webgains ?>";
                var lead_ref = "";

                // Check if the status is true and Webgains cookie is set
                if (readCookie('webgains') && status) { 
                lead_ref = "<?= $lead_reference ?>";
                }        


            </script>

            <!-- <Webgains Tracking Code> -->
            <!-- <Variablendefinitionen> -->
            <script language="javascript" type="text/javascript">

                var wgOrderReference = lead_ref;
                var wgOrderValue = "0";
                var wgEventID = 1037645;
                var wgComment = "";
                var wgLang = "de_DE";
                var wgsLang = "javascript-client";
                var wgVersion = "1.2";
                var wgProgramID = 269785;
                var wgSubDomain = "track";
                var wgCheckSum = "";
                var wgItems = "";
                var wgVoucherCode = "";
                var wgCustomerID = "";
                var wgCurrency = "EUR";

                console.log(wgOrderReference);

            </script>
            <!-- </Variablendefinitionen> -->


            <!-- <Webgains Tracking Code NG> -->
            <script language="javascript" type="text/javascript">
                (function (w, e, b, g, a, i, n, s) {
                    w['ITCVROBJ'] = a;
                    w[a] = w[a] || function () {
                        (w[a].q = w[a].q || []).push(arguments)
                    }, w[a].l = 1 * new Date();
                    i = e.createElement(b),
                            n = e.getElementsByTagName(b)[0];
                    i.async = 1;
                    i.src = g;
                    n.parentNode.insertBefore(i, n)
                })(window, document, 'script', 'https://analytics.webgains.io/cvr.min.js', 'ITCVRQ');
                ITCVRQ('set', 'trk.programId', wgProgramID);
                ITCVRQ('set', 'cvr', {
                    value: wgOrderValue,
                    currency: wgCurrency,
                    language: wgLang,
                    eventId: wgEventID,
                    orderReference: wgOrderReference,
                    comment: wgComment,
                    multiple: '',
                    checksum: '',
                    items: wgItems,
                    customerId: wgCustomerID,
                    voucherId: wgVoucherCode
                });
                ITCVRQ('conversion');
            </script>
        <?php } ?>      
        <!-- </Webgains Tracking Code NG> -->
        <!-- </Webgains Tracking Code> --> 


        <!-- Bootstrap core JavaScript -->
        <script src="vendor/jquery/jquery.min.js">
        </script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js">
        </script>
        <script src="vendor/header-animation/demo-1.js">
        </script>  
        <script src="vendor/header-animation/TweenLite.min.js">
        </script>
        <script src="vendor/header-animation/EasePack.min.js">
        </script>
        <script src="vendor/header-animation/rAF.js">
        </script>
        <script src="vendor/header-animation/demo-1.js">
        </script>  

    </body>

</html>

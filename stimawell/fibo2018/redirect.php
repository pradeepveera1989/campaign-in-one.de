
<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>stimaWELL - FIBO Gewinnspiel 2018</title>
    <!-- implementation bootstrap -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- implementation fontawesome icons -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- implementation simpleline icons -->
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <!-- implementation googlefonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <!-- implementation Animated Header -->
    <!-- implementation custom css -->
    <link href="css/creative.css" rel="stylesheet">
    <!-- implementation animate css -->
    <link href="css/animate.css" rel="stylesheet">
  </head>
    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '2069635113314986');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=2069635113314986&ev=PageView&noscript=1"
    /></noscript>
<!-- End Facebook Pixel Code -->
  <body>

     <!-- Navigation -->
    <nav class="navbar navbar-light bg-light static-top">
      <div class="container">
          <a class="navbar-brand" style="text-transform: uppercase;" href="https://www.stimawell-ems.de/"><img style="width:200px;" src="img/logo.svg"></a>
      </div>
    </nav>


    <!-- FORM -->
    <section class="form-container" style="padding-top: 250px; padding-bottom: 250px; height:100%;" id="about">
    <div class="container">  
    <div class="row">
    <div class="col-sm-12">
        <center>
            
            <h1 style="color:#ffffff; text-transform: uppercase;">Bitte überprüfe dein E-Mail Postfach.</h1>
            <hr>
            <p style="color: #fff;">Wir haben dir eine E-Mail mit einem Bestätigungslink an deine genannte E-Mail-Adresse gesendet. Solltest du in den nächsten 15 Minuten keine Mail erhalten, schaue bitte auch in dein Spam oder Junk Ordner.</p> 
        </center>
    </div>
    </div>
    </div>  

</section>  
 <div class="clearspace"></div>     
 <!-- Footer -->
    <footer class="footer bg-light">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
            <ul class="list-inline mb-2">
              <li class="list-inline-item">
                <a href="https://www.stimawell-ems.de/impressum" target="_blank">Impressum</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="https://www.stimawell-ems.de/agb" target="_blank">AGB</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="https://www.stimawell-ems.de/datenschutz" target="_blank">Datenschutz</a>
              </li>
                <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                 <a href="https://stimawell.campaign-in-one.de/fibo/teilnahmebedingungen.php" target="_blank">Teilnahmebedingungen</a>
              </li>
            </ul>
            <p class="text-muted small mb-4 mb-lg-0">&copy; stimaWELL-EMS FIBO Gewinnspiel 2018</p>
          </div>
          <div class="col-lg-6 h-100 text-center text-lg-right my-auto">
            <ul class="list-inline mb-0">
              <li class="list-inline-item mr-3">
                <a href="https://www.facebook.com/stimawellems/" target="_blank">
                  <i class="fa fa-facebook fa-2x fa-fw"></i>
                </a>
              </li>
              <li class="list-inline-item mr-3">
                <a href="https://www.instagram.com/stimawell.ems/" target="_blank">
                  <i class="fa fa-instagram fa-2x fa-fw"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>   
      </div>
    </footer>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js">
    </script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js">
    </script>
    <script src="vendor/header-animation/demo-1.js">
    </script>  
    <script src="vendor/header-animation/TweenLite.min.js">
    </script>
    <script src="vendor/header-animation/EasePack.min.js">
    </script>
    <script src="vendor/header-animation/rAF.js">
    </script>
    <script src="vendor/header-animation/demo-1.js">
    </script>  

  </body>

</html>

<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>stimaWELL - FIBO Gewinnspiel 2018</title>
    <!-- implementation bootstrap -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- implementation fontawesome icons -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- implementation simpleline icons -->
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <!-- implementation googlefonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">   
    <!-- implementation custom css -->
    <link href="css/creative.css" rel="stylesheet">
    <!-- implementation animate css -->
    <link href="css/animate.css" rel="stylesheet">
  </head>
    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '2069635113314986');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=2069635113314986&ev=PageView&noscript=1"
    /></noscript>
<!-- End Facebook Pixel Code -->
  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-light bg-light static-top">
      <div class="container">
          <a class="navbar-brand" style="text-transform: uppercase;" href="#"><img style="width:200px;" src="img/logo.svg"></a>
      </div>
    </nav>

    <!-- FORM -->
    <section class="form-container" style="padding-top: 100px; padding-bottom: 100px; height:100%;" id="about">
    <div class="container">  
    <div class="row">
    <div class="col-sm-12">
       
            
            <h1 style="color:#ffffff; text-transform: uppercase;">Teilnahmebedingungen</h1>
          
            <p>&nbsp;</p>
           
            <p style="color: #fff;"><strong>1.</strong> Veranstalterin des Gewinnspiels ist die Schwa-Medico Medizinische Apparate Vertriebsgesellschaft mbH, Gehrnstraße 4, 35630 Ehringhausen (im Folgenden: „Schwa-Medico“).</p> 
        
            <p>&nbsp;</p>
        
            <p style="color: #fff;"><strong>2.</strong>An dem Gewinnspiel nimmt ein Nutzer teil, indem er sich bis zum angegebenen Teilnahmeschluss über die Gewinnspielseite registriert und dabei seine Teilnahmedaten (Name, E-Mail-Adresse, Telefonnummer) angibt und am FIBO Gewinnspiel teilgenommen hat und eine Einwilligung zur E-Mail-/Telefonwerbung und zur werblichen Nutzer seiner Daten (im Folgen-den: Werbeeinwilligung) erklärt. Alternativ ist auch eine Teilnahme ohne eine Werbeeinwilligung möglich, wenn ein Nutzer innerhalb der Teilnahmefrist eine E-Mail an die E-Mail-Adresse <a href="mailto:widerruf@stimawell-ems.de">widerruf@stimawell-ems.de</a> unter Angabe seines Namens und seiner Telefonnummer und des Hauptpreises des Gewinnspiels sendet. Die Form der Teilnahme (also Online-Registrierung mit Werbeeinwilligung oder E-Mail-Registrierung ohne Werbeeinwilligung) hat auf die Gewinnchancen keinen Einfluss.</p> 
        
            <p>&nbsp;</p>
        
            <p style="color: #fff;"><strong>3.</strong> Jeder Teilnehmer kann nur einmal an einem Gewinnspiel teilnehmen. Mitarbeitern von Schwa-Medico und ihren unmittelbaren Angehörigen (Eltern, Kinder, Ehepartner, Geschwister) ist eine Teilnahme untersagt. Zudem ist eine Teilnahme unter Angabe einer falschen E-Mail-Adresse oder Telefonnummer unzulässig. </p>
        
            <p>&nbsp;</p>
        
            <p style="color: #fff;"><strong>4.</strong> Der Gewinner wird per Verlosung unter allen zugelassenen Teilnehmern (vgl. Ziff. 3) ermittelt. Die Gewinnermittlung findet innerhalb von zwei Wochen nach Teilnahmeschluss statt. </p>
        
            <p>&nbsp;</p>
        
            <p style="color: #fff;"><strong>5.</strong> Der oder die Gewinner werden per E-Mail oder per Telefon über den Gewinn informiert. In diesem Zusammenhang werden Sie aufgefordert, ihre Adresse zu benennen. Wird der Nutzer bei einem Telefonanruf nicht erreicht, wird er – sofern eine Mailbox aktiviert ist – zu einem Rückruf innerhalb von zwei Wochen unter einer konkret benannten Telefonnummer aufgefordert. Hat er keine aktivierte Mailbox, wird es noch drei weitere Anrufversuche geben. Erhält die Veranstalterin bei keinem der Anrufe eine nähere Information und wird auch auf eine Rückrufbitte per Mailbox nicht innerhalb der Zwei-Wochen-Frist reagiert, ist die telefonische Gewinnmitteilung gescheitert. Bei einer Gewinnmitteilung per E-Mail wird der Gewinner aufgefordert, innerhalb von zwei Wochen seine Postadresse zu benennen. Reagiert der Gewin-ner darauf nicht, gilt auch die Gewinnmitteilung per E-Mail als gescheitert. Wenn sowohl die Gewinnmitteilung per Telefon als auch die Gewinnmitteilung per E-Mail gescheitert ist, verfällt der Gewinnanspruch. In diesem Fall kommt es zu einer Wiederholung der Gewinnermittlung.</p>
        
            <p>&nbsp;</p>
        
            <p style="color: #fff;"><strong>6.</strong> Stellt sich nach der Gewinnermittlung heraus, dass ein Nutzer unter Verstoß gegen Ziff. 3 teilgenommen hat, kann die Gewinnermittlung wiederholt haben. </p>
        
            <p>&nbsp;</p>
        
            <p style="color: #fff;"><strong>7.</strong> Sofern in den Gewinnspielinformationen nichts Anderes mitgeteilt wird, wird ein Gewinn auf Kosten der Schwa-Medico an den Gewinnspielteilnehmer verschickt. </p>
        
            <p>&nbsp;</p>
        
            <p style="color: #fff;"><strong>8.</strong> Es gilt deutsches Recht.</p>
        
            
        
    </div>
    </div>
    </div>  

</section>  
      
 <!-- Footer -->
    <footer class="footer bg-light">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
            <ul class="list-inline mb-2">
              <li class="list-inline-item">
                <a href="https://www.stimawell-ems.de/impressum" target="_blank">Impressum</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="https://www.stimawell-ems.de/agb" target="_blank">AGB</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="https://www.stimawell-ems.de/datenschutz" target="_blank">Datenschutz</a>
              </li>
                <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="https://stimawell.campaign-in-one.de/fibo/teilnahmebedingungen.php" target="_blank">Teilnahmebedingungen</a>
              </li>
            </ul>
            <p class="text-muted small mb-4 mb-lg-0">&copy; stimaWELL-EMS FIBO Gewinnspiel 2018</p>
          </div>
          <div class="col-lg-6 h-100 text-center text-lg-right my-auto">
            <ul class="list-inline mb-0">
              <li class="list-inline-item mr-3">
                <a href="https://www.facebook.com/stimawellems/" target="_blank">
                  <i class="fa fa-facebook fa-2x fa-fw"></i>
                </a>
              </li>
              <li class="list-inline-item mr-3">
                <a href="https://www.instagram.com/stimawell.ems/" target="_blank">
                  <i class="fa fa-instagram fa-2x fa-fw"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      
    <script src="vendor/header-animation/demo-1.js">
    </script>  
    <script src="vendor/header-animation/TweenLite.min.js">
    </script>
    <script src="vendor/header-animation/EasePack.min.js">
    </script>
    <script src="vendor/header-animation/rAF.js">
    </script>
    <script src="vendor/header-animation/demo-1.js">
    </script>  

  </body>

</html>
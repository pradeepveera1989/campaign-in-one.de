<?php
/* ______________________________________________________________________________________________________________________________________________
 *
  MAIL IN ONE - STIMAWELL - GEWINNSPIEL FORMULAR - FIBO2018 - 1803
 * _______________________________________________________________________________________________________________________________________________
 */

//vorname  
$standard_01 = $_GET["1"];
//nachname
$standard_02 = $_GET["2"];
//email
$email_02 = $_GET["3"];
//plz
$standard_04 = $_GET["4"];
//ort
$standard_05 = $_GET["5"];
//trafficsource
$ts = $_GET["trafficsource"];

$standard_01 = str_replace(' ', '+', $standard_01);
$standard_001 = base64_decode($standard_01);

$standard_02 = str_replace(' ', '+', $standard_02);
$standard_002 = base64_decode($standard_02);

$email_02 = str_replace(' ', '+', $email_02);
$email_002 = base64_decode($email_02);

$standard_04 = str_replace(' ', '+', $standard_04);
$standard_004 = base64_decode($standard_04);

$standard_05 = str_replace(' ', '+', $standard_05);
$standard_005 = base64_decode($standard_05);


// Update with your version of the Maileon PHP API Client
require_once('./maileon-php-client-1.3.1/client/MaileonApiClient.php');

$config = array(
    'BASE_URI' => 'https://api.maileon.com/1.0',
    'API_KEY' => '8ebc2b16-379a-4c62-80da-8b15eca17a68',
    'THROW_EXCEPTION' => true,
    'TIMEOUT' => 60,
    'DEBUG' => 'false' // NEVER enable on production
);


$contactid = '';
$checksum = '';
$mailingid = "";

if (isset($_GET['contactid'])) {
    $contactid = $_GET['contactid'];
} elseif (isset($_POST['contactid'])) {
    $contactid = $_POST['contactid'];
}

if (isset($_GET['checksum'])) {
    $checksum = $_GET['checksum'];
} elseif (isset($_POST['checksum'])) {
    $checksum = $_POST['checksum'];
}

if (isset($_GET['mailingid'])) {
    $checksum = $_GET['mailingid'];
} elseif (isset($_POST['mailngid'])) {
    $checksum = $_POST['mailingid'];
}


if (isset($_POST['email'])) {
    $email = $_POST['email'];

    $standard_0 = $_POST["anrede"];
    $standard_1 = $_POST["vorname"];
    $standard_2 = $_POST["nachname"];
    $standard_3 = $_POST["plz"];
    $standard_4 = $_POST["ort"];

    $custom_0 = $_POST["quelle"];
    $custom_1 = $_POST["typ"];
    $custom_2 = $_POST["segment"];
    $custom_3 = $_POST["telefon"];

    $ip = $_SERVER['REMOTE_ADDR'];
    $url = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

    $trafficsource = $_POST["ts"];

    $contactsService = new com_maileon_api_contacts_ContactsService($config);
    $contactsService->setDebug(false);

    function updateContact($email, $config, $standard_0,$standard_1,$standard_2,$standard_3,$standard_4,$custom_0,$custom_1,$custom_2,$custom_3,$ip,$url,$trafficsource) {

        $debug = FALSE;
        $contactsService = new com_maileon_api_contacts_ContactsService($config);
        $contactsService->setDebug($debug);
        $newContact = new com_maileon_api_contacts_Contact();
        $newContact->email = $email;

        $newContact->standard_fields["SALUTATION"] = $standard_0;
        $newContact->standard_fields["FIRSTNAME"] = $standard_1;
        $newContact->standard_fields["LASTNAME"] = $standard_2;
        $newContact->standard_fields["ZIP"] = $standard_3;
        $newContact->standard_fields["CITY"] = $standard_4;
        $newContact->custom_fields["Quelle"] = $custom_0;
        $newContact->custom_fields["Typ"] = $custom_1;
        $newContact->custom_fields["Segment"] = $custom_2;
        $newContact->custom_fields["Telefon"] = $custom_3;
        $newContact->custom_fields["ip_adresse"] = $ip;
        $newContact->custom_fields["url"] = $url;
        $newContact->custom_fields["trafficsource"] = $trafficsource;
        $newContact->custom_fields["FIBO2018"] = TRUE;

        $response = $contactsService->createContact($newContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, "", "", false);
        return $response;
    }

    $getContact = $contactsService->getContactByEmail($email);
    if ($getContact->isSuccess() && $getContact->getResult()->permission != com_maileon_api_contacts_Permission::$NONE) {


        $response = updateContact($email, $config, $standard_0,$standard_1,$standard_2,$standard_3,$standard_4,$custom_0,$custom_1,$custom_2,$custom_3,$ip,$url,$trafficsource);

        header('Location: https://stimawell.campaign-in-one.de/fibo/erfolg.php');
    } else {
        $newContact = new com_maileon_api_contacts_Contact();
        $newContact->email = $email;
        $newContact->anonymous = false;
        $newContact->permission = com_maileon_api_contacts_Permission::$NONE;
        $newContact->standard_fields["SALUTATION"] = $standard_0;
        $newContact->standard_fields["FIRSTNAME"] = $standard_1;
        $newContact->standard_fields["LASTNAME"] = $standard_2;
        $newContact->standard_fields["ZIP"] = $standard_3;
        $newContact->standard_fields["CITY"] = $standard_4;
        $newContact->custom_fields["Quelle"] = $custom_0;
        $newContact->custom_fields["Typ"] = $custom_1;
        $newContact->custom_fields["Segment"] = $custom_2;
        $newContact->custom_fields["Telefon"] = $custom_3;
        $newContact->custom_fields["ip_adresse"] = $ip;
        $newContact->custom_fields["url"] = $url;
        $newContact->custom_fields["trafficsource"] = $trafficsource;
        $newContact->custom_fields["FIBO2018"] = TRUE;

        $response = $contactsService->createContact($newContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, '', '', true, true, 'SxT5hIsw');

        #$transactionsService = new com_maileon_api_transactions_TransactionsService($config);
        #$transactionsService->setDebug(FALSE);
        #$transaction = new com_maileon_api_transactions_Transaction();
        #$transaction->contact = new com_maileon_api_transactions_ContactReference();
        #$transaction->type = 1;   
        #$transaction->contact->email = $email;
        #$transactions = array($transaction);
        #$transaction->content["FIBO2018"] = $fibo2018;
        #$transactionsService->createTransactions($transactions, true, false);

        header('Location: https://stimawell.campaign-in-one.de/fibo/redirect.php');
    }
} else {
    $email = "";
    $standard_0 = "";
    $standard_1 = "";
    $standard_2 = "";
    $standard_3 = "";
    $standard_4 = "";
    $custom_0 = "";
    $custom_1 = "";
    $custom_2 = "";
    $custom_3 = "";
    $ip = "";
    $url = "";
    $trafficsource = "";
    $fibo2018 = "";
}
?>

<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>stimaWELL® - FIBO Gewinnspiel 2018</title>
        <!-- implementation bootstrap -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- implementation fontawesome icons -->
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- implementation simpleline icons -->
        <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
        <!-- implementation googlefonts -->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
        <!-- implementation Animated Header -->
        <!-- implementation custom css -->
        <link href="css/creative.css" rel="stylesheet">
        <!-- implementation animate css -->
        <link href="css/animate.css" rel="stylesheet">
    </head>

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfPExLIePbHmy_rtDbHD406iuQ46n-SMA&libraries=places"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-16152616-30"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-16152616-30');
    </script>



    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s)
        {
            if (f.fbq)
                return;
            n = f.fbq = function () {
                n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq)
                f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
                'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '2069635113314986');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=2069635113314986&ev=PageView&noscript=1"
                   /></noscript>
    <!-- End Facebook Pixel Code -->
    <body>


        <!-- Navigation -->
        <nav class="navbar navbar-light bg-light static-top">
            <div class="container">
                <a class="navbar-brand" style="text-transform: uppercase;" href="https://www.stimawell-ems.de/" target="_blank"><img style="width:200px;" src="img/logo.svg">&nbsp;&nbsp;&nbsp;&nbsp;<img class="logo-img" style="width:200px;" src="img/FIBO-logo.png"><span style="color: #00a1db;"></span></a>
            </div>
        </nav>

        <!-- FORM -->
        <section class="form-container" style="padding-top: 15px; height:100%;" id="about">
            <div class="container">
                <div class="row">

                    <!-- FORM IMG MOBILE -->    
                    <center>
                        <div class="col-sm-9 form-img-mobile">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">  
                                        <h2 style="color: #fff;">JETZT MITMACHEN!</h2>
                                        <hr>
                                        <h4 style="color: #fff;">Wir verlosen täglich 1 Jahr EMS-Training für Zuhause inklusive EMS Miet-Gerät, Anzug und Bandelektroden im Wert von 1900.- EUR.</h4> 
                                        <p style="margin-top: 30px; padding: 15px; background-color: #00a1db; color: #fff;">* GEWINNER MÜSSEN PERSÖNLICH, TÄGLICH UM 17 UHR AUF UNSEREN <strong>STAND B20 HALLE 5.2</strong> SEIN.</p>
                                        <p>&nbsp;</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </center>
                    <!-- ./FORM IMG MOBILE -->

                    <!-- FORM IMG -->
                    <div class="col-sm-6 form-img">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">  
                                    <h2 style="color: #fff;">JETZT MITMACHEN!</h2>
                                    <hr>
                                    <h4 style="color: #fff;">Wir verlosen täglich 1 Jahr kostenlos EMS-Training für Zuhause mit dem StimaWELL EMS Miet-Gerät inklusive Anzug und Bandelektroden im Wert von 1900.- EUR.</h4> 
                                    <p style="margin-top: 30px; padding: 15px; background-color: #00a1db; color: #fff;">* GEWINNER MÜSSEN PERSÖNLICH, TÄGLICH UM 17 UHR AUF UNSEREN <strong>STAND B20 HALLE 5.2</strong> SEIN.</p> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ./FORM IMG -->

                    <!-- FORM MOBILE -->
                    <div class="col-sm-12 gewinn-form-mobile">
                        <form id="api-data-form" action="<?= $_SERVER['PHP_SELF'] ?>" method="post" accept-charset="utf-8" class="form-horizontal" role="form">
                            <input type="hidden" name="contactid" value="<?= $contactid ?>">
                            <input type="hidden" name="checksum" value="<?= $checksum ?>">
                            <input type="hidden" name="mailingid" value="<?= $mailingid ?>">
                            <label class="form-group">
                                <div class="col-sm-12">
                                    <span style="font-size: 1em; color:#fff;">Bitte erfasse deine Teilnehmerdaten:</span>
                                </div>    
                            </label>

                            <!-- INPUT ANREDE FRAU & HERR -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="btn-group">
                                        <label class="btn btn-outline-secondary" style="border: 1px solid transparent;">
                                            <input value="Frau" style="width: 15px; height: 15px;" type="radio" name="anrede" autocomplete="off" required><span style="color: #fff!important"> Frau</span>
                                        </label>
                                        <span class="input-group-btn" style="width:15px;"></span> 
                                        <label class="btn btn-outline-secondary" style="border: 1px solid transparent;">
                                            <input value="Herr" style="width: 15px; height: 15px;" type="radio" name="anrede" autocomplete="off"><span style="color: #fff!important"> Herr</span>
                                        </label>
                                    </div>  
                                </div>       
                            </div>

                            <!-- INPUT VORNAME -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <input value="" class="input-fields form-control" id="name" name="vorname" placeholder="*Vorname" type="text" required> 
                                    </div>
                                </div>       
                            </div>

                            <!-- INPUT NACHNAME -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <input value="" class="input-fields form-control" id="name" name="nachname" placeholder="*Nachname" type="text" required>
                                    </div>
                                </div>       
                            </div>

                            <!-- INPUT EMAIL ADRESSE -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input style="text-align:left;!important" type="email" name="email" id="email" maxlength="255" placeholder="*Deine E-Mail Adresse" value="" class="form-control" required>
                                </div>
                            </div>


                            <!-- PLZ & ORT -->
                            <div id="#hiddenDiv" class="form-group ">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <input value="" class="input-fields form-control" id="name" name="plz" placeholder="*PLZ" type="text" required> 
                                        <span class="input-group-btn" style="width:15px;"></span> 
                                        <input value="" class="input-fields form-control" id="name" name="ort" placeholder="*Ort" type="text" required>
                                    </div>
                                </div>       
                            </div>

                            <!-- INPUT TELEFONNUMMER -->
                            <div id="#hiddenDiv" class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <input value="" class="input-fields form-control" id="" name="telefon" placeholder="*Telefonnummer" type="text" required>
                                    </div>
                                </div>
                            </div>

                            <label class="form-group">
                                <div class="col-sm-12">
                                    <input style="width: 20px; height: 20px;" type="checkbox" required>
                                    <span class="p-light">&nbsp;&nbsp;Ich bin einverstanden, dass die Schwa-Medico Medizinische Apparate Vertriebsgesellschaft mbH, Gehrnstraße 4, 35630 Ehringhausen, meine bei der Gewinnspielregistrierung eingetragenen Daten nutzt und mir E-Mails zuschickt bzw. mich anruft, um mir Angebote aus dem Bereich EMS-Training (Zubehör, Training, Fachliteratur) zukommen zu lassen. Diese Einwilligung kann ich jederzeit widerrufen, etwa durch einen Brief an die oben genannte Adresse oder durch eine E-Mail an <a href="mailto:widerruf@stimawell-ems.de">widerruf@stimawell-ems.de.</a> Anschließend wird jede werbliche Nutzung unterbleiben.</span><br>

                                </div>    
                            </label>

                            <!-- TRAFFIC QUELLE MANUEL -->

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">TRAFFICSOURCE</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  value="<?php echo $ts ?>" placeholder="" type="text" name="ts" id="ts" value="">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Quelle</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="quelle" id="quelle" value="FIBO2018">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Typ</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="typ" id="" value="Lead">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Segment</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="segment" id="" value="B2C">
                                </div>
                            </div>

                            <div class="form-group"> 
                                <div class="col-sm-12">
                                    <input style="margin-top: 25px; cursor: pointer;" type="submit" class="col-sm-12 btn btn-danger btn-xl js-scroll-trigger" value="Jetzt abschicken und gewinnen!" name="submit" style="width: 100%;">
                                </div>        
                            </div>
                            <p>&nbsp;</p>
<?php if (isset($response) && $response->isSuccess()) { ?>
                                <div class="alert alert-success fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Subscription successful</strong>
                                </div>
                            <?php } elseif (isset($warning)) { ?>
                                <div class="alert alert-warning fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong style="color: red; z-index: 1000;">Subscription failed</strong>
    <?= $warning['message'] ?>
                                </div>
                            <?php } elseif (isset($response)) { ?>
                                <div class="alert alert-danger fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Subscription failed</strong>
                                </div>
                            <?php } ?>
                        </form>
                    </div>
                    <!-- ./FORM MOBILE -->

                    <!-- FORM DESKTOP -->
                    <div class="col-sm-6 gewinn-form">
                        <form id="api-data-form" action="<?= $_SERVER['PHP_SELF'] ?>" method="post" accept-charset="utf-8" class="form-horizontal" role="form">
                            <input type="hidden" name="contactid" value="<?= $contactid ?>">
                            <input type="hidden" name="checksum" value="<?= $checksum ?>">
                            <input type="hidden" name="mailingid" value="<?= $mailingid ?>">

                            <!-- INPUT ANREDE FRAU & HERR -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="btn-group">
                                        <label class="btn btn-outline-secondary" style="border: 1px solid transparent;">
                                            <input value="Frau" style="width: 15px; height: 15px;" type="radio" name="anrede" autocomplete="off" required> Frau
                                        </label>
                                        <span class="input-group-btn" style="width:15px;"></span> 
                                        <label class="btn btn-outline-secondary" style="border: 1px solid transparent;">
                                            <input value="Herr" style="width: 15px; height: 15px;" type="radio" name="anrede" autocomplete="off"> Herr
                                        </label>
                                    </div>  
                                </div>       
                            </div>

                            <!-- INPUT VORNAME & NACHNAME -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <input value="<?= $standard_001 ?>" class="input-fields form-control" id="" name="vorname" placeholder="*Vorname" type="text" required onblur="if (this.value == '') {
                                                                            this.value = '<?= $standard_001 ?>';}"
                                               onfocus="if (this.value == '<?= $standard_001 ?>') {
             this.value = '';}"> 
                                        <span class="input-group-btn" style="width:15px;"></span> 
                                        <input value="<?= $standard_002 ?>" class="input-fields form-control" id="nachname" name="nachname" placeholder="*Nachname" type="text" required onblur="if (this.value == '') {
                                            this.value = '<?= $standard_002 ?>';}"
                                               onfocus="if (this.value == '<?= $standard_002 ?>') {
             this.value = '';}">
                                    </div>
                                </div>       
                            </div>

                            <!-- INPUT PLZ & ORT -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <input value="<?= $standard_004 ?>" class="input-fields form-control" id="plz" name="plz" placeholder="*PLZ" type="text" required onblur="if (this.value == '') {
                                                                             this.value = '<?= $standard_004 ?>';}"
                                               onfocus="if (this.value == '<?= $standard_004 ?>') {
             this.value = '';
         }">

                                        <span class="input-group-btn" style="width:15px;"></span> 
                                        <input value="<?= $standard_005 ?>" class="input-fields form-control" id="ort" name="ort" placeholder="*Ort" type="text" required onblur="if (this.value == '') {
                                            this.value = '<?= $standard_005 ?>';
                                        }"
                                               onfocus="if (this.value == '<?= $standard_005 ?>') {
             this.value = '';
         }">
                                    </div>
                                </div>       
                            </div>

                            <!-- INPUT EMAIL & TELEFON -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <input value="<?= $email_002 ?>" class="input-fields form-control" id="email" name="email" placeholder="*E-Mail-Adresse" type="email" required onblur="if (this.value == '') {
                                                                            this.value = '<?= $email_002 ?>';}"
                                               onfocus="if (this.value == '<?= $email_002 ?>') {
             this.value = '';
         }"> 
                                        <span class="input-group-btn" style="width:15px;"></span> 
                                        <input value="" class="input-fields form-control" id="telefon" name="telefon" placeholder="*Telefonnummer" type="text" required>
                                    </div>
                                </div>       
                            </div>

                            <label class="form-group">
                                <div class="col-sm-12">
                                    <input style="width: 20px; height: 20px;" type="checkbox" required>
                                    <span class="p-light">&nbsp;&nbsp;Ich bin einverstanden, dass die Schwa-Medico Medizinische Apparate Vertriebsgesellschaft mbH, Gehrnstraße 4, 35630 Ehringhausen, meine bei der Gewinnspielregistrierung eingetragenen Daten nutzt und mir E-Mails zuschickt bzw. mich anruft, um mir Angebote aus dem Bereich EMS-Training (Zubehör, Training, Fachliteratur) zukommen zu lassen. Diese Einwilligung* kann ich jederzeit widerrufen, etwa durch einen Brief an die oben genannte Adresse oder durch eine E-Mail an <a href="mailto:widerruf@schwa-medico.de">widerruf@schwa-medico.de.</a> Anschließend wird jede werbliche Nutzung unterbleiben.</span><br>

                                </div>    
                            </label>  


                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">TRAFFICSOURCE</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  value="<?= $ts ?>" placeholder="" type="text" name="ts" id="ts" value="">
                                </div>
                            </div>


                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Quelle</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="quelle" id="quelle" value="FIBO2018">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Typ</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="typ" id="" value="Lead">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Segment</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="segment" id="" value="B2C">
                                </div>
                            </div>


                            <div class="form-group"> 
                                <div class="col-sm-12">
                                    <input style="margin-top: 25px; cursor: pointer;" type="submit" class="col-sm-12 btn btn-danger btn-xl js-scroll-trigger" value="Jetzt abschicken und gewinnen!" name="submit" style="width: 100%;">
                                </div>        
                            </div>
<?php if (isset($response) && $response->isSuccess()) { ?>
                                <div class="alert alert-success fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Subscription successful</strong>
                                </div>
                            <?php } elseif (isset($warning)) { ?>
                                <div class="alert alert-warning fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong style="color: red; z-index: 1000;">Subscription failed</strong>
    <?= $warning['message'] ?>
                                </div>
                            <?php } elseif (isset($response)) { ?>
                                <div class="alert alert-danger fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Subscription failed</strong>
                                </div>
                            <?php } ?>
                        </form>
                    </div>
                    <!-- ./FORM DESKTOP -->


                </div>
            </div>  

        </section>

        <!--      
        <section class="features-icons text-center">
              <div class="container">
                <div class="row">
                  <div class="col-lg-12">  
                      <h3 style="color: #fff;">1 Jahr kostenloses EMS-Training für Zuhause mit dem StimaWELL® EMS Miet-Gerät inklusive Anzug und Bandelektroden im Wert von 1900.- EUR.</h3> 
                  </div>
                </div>
              </div>
        </section>  
        -->      

        <section class="showcase">
            <div class="container">
                <div class="row no-gutters">

                    <div class="col-lg-6 order-lg-2 text-white showcase-img"><img style="border: ;z-index:-2; margin-top: 50px; margin-bottom:; margin-left: px;" class="img-fluid" src="img/EMS-Produkt.png"></div>

                    <div class="col-lg-6 order-lg-1 my-auto showcase-text">
                        <h2>DAS STIMAWELL® EMS MIET-GERÄT</h2>
                        <p class="lead mb-0">Das Original StimaWELL® EMS ist genau das richtige für Dich! Das überlegene Mittelfrequenz-Trainingsgerät zur elektrische Muskelstimulation bietet Dir eine sanfte aber intensive Methode Dich schnell und dauerhaft in Form zu bringen. Als führender Hersteller von Geräten für elektrische Therapien haben wir unsere gesamte Erfahrung von mehr als 40 Jahren in die Entwicklung einfließen lassen. Geprüftes Medizinprodukt. Made in Germany.</p><br>
                        <a href="#about" class="btn btn-text btn-danger">JETZT GEWINNEN!</a>
                    </div>

                </div>
            </div>
        </section>  

        <section class="features-icons text-center">
            <div class="container">
                <div class="row">
                    <div class="col-12">  
                        <h2 style="color: #fff;">Gewinnspielzeitraum: 12.04.2018 bis 15.04.2018 - 18:00 Uhr.</h2> 
                    </div>
                </div>
            </div>
        </section>  

        <!--
      <section class="showcase">
        <div class="container">  
          <div class="row no-gutters">
            <div class="col-lg-6 text-white showcase-img" style="margin-top:50px; margin-bottom: 50px; background-image: url('img/produkte-anzug-elektroden.jpg');"></div>
            <div class="col-lg-6 my-auto showcase-text">
              <h2>EMS ANZUG UND BANDELEKTRODEN</h2>
              <p class="lead mb-0">Mit den 12 Elektrodenpaaren des Hightech-Stimulationsanzugs erreicht man nahezu alle Muskelgruppen.</p><br>
              <li>Indivuelle Fließrichtung des Stroms für unterschiedliche Rekrutierung von Muskelfasern</li>
              <li>Perfekte Passform durch Größen von XXS bis 5XL</li>
              <li>Unterschiedlich lange Arm- und Beinelektroden</li>
              <li>Silberbeschichtet für beste Leitfähigkeit</li>
              <li>Waschmaschinentauglich</li><br>
              <a href="#about" class="btn btn-text btn-danger">JETZT GEWINNEN!</a>     
            </div>
          </div>
        </div>
      </section>-->


        <!-- Call to Action --><!-- 
        <section class="call-to-action text-white text-center">
        <!--<div class="overlay"></div>--><!-- 
        <div class="container">
          <div class="row">
            <div class="col-xl-12 mx-auto">
              <h2>DAS STIMAWELL-EMS FIBO GEWINNSPIEL 2018<br> JETZT TEILNEHMEN UND GEWINNEN!</h2>
            </div>
              <p>&nbsp;</p>
            <div class="col-md-12">
              <a href="#about" class="btn btn-text btn-danger">TEILNEHMEN & GEWINNEN</a>
            </div>
            </div>
          </div>
      </section>-->

        <!-- Footer -->
        <footer class="footer bg-light">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
                        <ul class="list-inline mb-2">
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/impressum" target="_blank">Impressum</a>
                            </li>
                            <li class="list-inline-item">&sdot;</li>
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/agb" target="_blank">AGB</a>
                            </li>
                            <li class="list-inline-item">&sdot;</li>
                            <li class="list-inline-item">
                                <a href="https://www.stimawell-ems.de/datenschutz" target="_blank">Datenschutz</a>
                            </li>
                            <li class="list-inline-item">&sdot;</li>
                            <li class="list-inline-item">
                                <a href="https://stimawell.campaign-in-one.de/fibo/teilnahmebedingungen.php" target="_blank">Teilnahmebedingungen</a>
                            </li>
                        </ul>
                        <p class="text-muted small mb-4 mb-lg-0">&copy; stimaWELL-EMS FIBO Gewinnspiel 2018</p>
                    </div>
                    <div class="col-lg-6 h-100 text-center text-lg-right my-auto">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item mr-3">
                                <a href="https://www.facebook.com/stimawellems/" target="_blank">
                                    <i class="fa fa-facebook fa-2x fa-fw"></i>
                                </a>
                            </li>
                            <li class="list-inline-item mr-3">
                                <a href="https://www.instagram.com/stimawell.ems/" target="_blank">
                                    <i class="fa fa-instagram fa-2x fa-fw"></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="ds">
                        <div class="col-sm-12">
                            <div class="container">
                                <div class="row">
                                    <center><span style="font-size: 12px;">* Die Online-Registrierung ist nur bei Abgabe einer Werbeeinwilligung möglich. Sie können aber auch ohne Werbeeinwilligung teilnehmen, wenn Sie uns innerhalb der Teilnahmefrist eine E-Mail an <a href="mailto:widerruf@stimawell-ems.de">widerruf@stimawell-ems.de</a> senden und in der E-Mail Ihren Namen, Ihre Telefonnummer und den Hauptpreis des Gewinnspiels angeben. Ihre Gewinnchancen werden durch die Art der Teilnahme nicht beeinflusst.</span></center>  
                                </div>
                            </div>
                        </div>
                    </div>

                </div>   
            </div>
        </footer>


        <!-- Bootstrap core JavaScript -->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <script src="vendor/header-animation/demo-1.js">
        </script>  
        <script src="vendor/header-animation/TweenLite.min.js">
        </script>
        <script src="vendor/header-animation/EasePack.min.js">
        </script>
        <script src="vendor/header-animation/rAF.js">
        </script>
        <script src="vendor/header-animation/demo-1.js">
        </script>  

    </body>

</html>

<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>stimaWELL - Umfrage Gewinnspiel</title>
    <!-- implementation bootstrap -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- implementation fontawesome icons -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- implementation simpleline icons -->
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <!-- implementation googlefonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <!-- implementation Animated Header -->
   
    <!-- implementation custom css -->
    <link href="css/creative.css" rel="stylesheet">
    <!-- implementation animate css -->
    <link href="css/animate.css" rel="stylesheet">
  </head>
  <body>
    

      <!-- Navigation -->
    <nav class="navbar navbar-light bg-light static-top">
      <div class="container">
          <a class="navbar-brand" style="text-transform: uppercase;" href="#"><img style="width:200px;" src="img/logo.svg">&nbsp;&nbsp;&nbsp;&nbsp;<span style="color: #00a1db;"><strong>Fitnessumfrage 2018</strong></span></a>
      </div>
    </nav>


    <!-- FORM -->
    <section class="form-container" style="padding-top: 250px; padding-bottom: 250px; height:100%;" id="about">
    <div class="container">  
    <div class="row">
    <div class="col-sm-12">
        <center>
            
            <h1 style="color:#ffffff; text-transform: uppercase;">Ein Fehler ist aufgetreten.</h1>
            <hr>
            <p style="color: #fff;">Es tut uns Leid, aber Sie haben schon an der Umfrage Gewinnspiel teilgenommen!</p> 
        </center>
    </div>
    </div>
    </div>  

</section>  
      
 <!-- Footer -->
    <footer class="footer bg-light">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
            <ul class="list-inline mb-2">
              <li class="list-inline-item">
                <a href="https://www.stimawell-ems.de/impressum" target="_blank">Impressum</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="https://www.stimawell-ems.de/agb" target="_blank">AGB</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="https://www.stimawell-ems.de/datenschutz" target="_blank">Datenschutz</a>
              </li>
                <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="#">Teilnahmebedingungen</a>
              </li>
            </ul>
            <p class="text-muted small mb-4 mb-lg-0">&copy; stimaWELL Umfrage Gewinnspiel 2018.</p>
          </div>
          <div class="col-lg-6 h-100 text-center text-lg-right my-auto">
            <ul class="list-inline mb-0">
              <li class="list-inline-item mr-3">
                <a href="https://www.facebook.com/stimawellems/" target="_blank">
                  <i class="fa fa-facebook fa-2x fa-fw"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      
    <script src="vendor/header-animation/demo-1.js">
    </script>  
    <script src="vendor/header-animation/TweenLite.min.js">
    </script>
    <script src="vendor/header-animation/EasePack.min.js">
    </script>
    <script src="vendor/header-animation/rAF.js">
    </script>
    <script src="vendor/header-animation/demo-1.js">
    </script>  

  </body>

</html>
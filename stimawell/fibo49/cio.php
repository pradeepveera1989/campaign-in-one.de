<?php

require "vendor/autoload.php";

session_start();

// Get the Config file path
$config_path = "config.ini";
$conf = new config($config_path);
$config = $conf->getAllConfig();

spl_autoload_register('Autoload::generalFunctionLoader');
$func = new generalFunctions();
$func->getUTMParsamFromURL($_GET);
$single_submit_text = $config[GeneralSetting][SingleSubmitErrorTxt];
$jetzt_kaufen = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '#form';

// Set traffic source cookie for Webgains
$webgains_cookie = $_GET['trafficsource'];

// Page counter
$counter_status = $conf->getConfigCounterStatus();
if ($counter_status) {

    $counter_params = $conf->getConfigCounter();
    $counter = new counter();
    $url = "https://" . $_SERVER[HTTP_HOST] . "/" . explode('/', $_SERVER['REQUEST_URI'])[1];
    $counter_params['url'] = $url;
    $current_count_value = $counter->getCounterValue($url);
    $current_end_date = $counter->getCounterEndDate($url);

    if (is_bool($current_count_value) && $current_count_value == false) {

        // URL is missing ,Insert new URL        
        $status = $counter->insertCounterValue($counter_params);
        $status = $counter->insertCounterEndDate($counter_params);
    } else if ($current_count_value == 2 && $counter_params['auto_refill']) {

        // refill the counter after check the date.
        $counter_expire_date = strtotime($counter_params['expire']);
        $current_date = strtotime(date("Y-m-d"));
        if ($counter_expire_date > $current_date) {
            $new_counter_value = $counter_params['max_offers'] - $counter_params['sold_offers'];
            $counter->updateCounterValue($url, $new_counter_value);
        }
    } else if (((strtotime($current_end_date) - strtotime(date('Y-m-d H:i:s'))) <= 9500) && $counter_params['auto_refill']) {

        // Update the latest Expire date for counter
        $counter->updateCounterEndDate($counter_params);
        $current_end_date = $counter->getCounterEndDate($url);
    }
}

//Split Test
if ($conf->getConfigSplitTestStatus()) {

    spl_autoload_register('Autoload::splitTestLoader');
    $currentURL = getcwd();
    $spTest = new splitTestClient($config[Splittest][Name], $config[Splittest][NumberOfVariants]);
    $redirect_url = $spTest->getRedirectURL($currentURL);
    $func->redirectPage($redirect_url);   
}

// Post Request
if (isset($_POST[$config[MailInOne][Mapping][EMAIL]])) {

    $params = array();

    // Email Validation
    $validate = new emailValidatorApiClient($_POST[$config[MailInOne][Mapping][EMAIL]]);
    $validate->curlRequest();
    $resp = $validate->curlResponce();

    if (!$resp) {
        // invalid email address
        // Copying the data to session and intiating a sesssion variable error.
        $_SESSION['data'] = $_POST;
        $_SESSION['error'] = "email";
        $_SESSION['error_msg'] = $config[ErrorHandling][ErrorMsgEmail];
        $url = $_POST[$config[MailInOne][Mapping][url]] . '#form';
        $func->redirectPage($url);
    } else {
        // clearing session data
        $_SESSION['error'] = "";
        $_SESSION['data'] = "";
        $_SESSION['error_msg'] = "";

        // Map the POST params to Maileon Params
        $maileon_params = $config[MailInOne][Mapping];

        foreach ($maileon_params as $mp) {
            $params[$mp] = $_POST[$mp];
        }

        //Check for Test mode
        if ($_POST["test_mode"]) {
            $test_mode = $_POST["test_mode"];
        }

        // Check for UTM Params and load from SESSION 
        if ($func->checkSession()) {
            $params = $func->checkUTMParamsInPOST($params);
        }

        // Get the Date, IP, URL 
        $params[$config[MailInOne][Mapping][Datum]] = date('d.m.Y \\u\\m h:i:s a', time());
        $params[$config[MailInOne][Mapping][ip_adresse]] = $_SERVER['REMOTE_ADDR'];
        $params["url_params"] = parse_url($params[$config[MailInOne][Mapping][url]])["query"];

        spl_autoload_register('Autoload::leadsLoader');
        $lead = new leads();
        $result = $lead->checklead($params);

        if (!empty($result["created"]) && date('Y-m-d', strtotime($result["created"])) == date("Y-m-d") && !$test_mode) {
            
            $_SESSION['data'] = $_POST;
            $_SESSION['error'] = "email";
            $_SESSION['error_msg'] = $config[ErrorHandling][ErrorMsgDuplicate];
            $url = $_POST[$config[MailInOne][Mapping][url]] . '#form';
            $func->redirectPage($url);
        } else {

            // User Device Type
            if (!empty($_SERVER['HTTP_USER_AGENT'])) {
                $device = new WhichBrowser\Parser($_SERVER['HTTP_USER_AGENT']);
                if (!empty($device)) {

                    $params[$config[MailInOne][Mapping][DeviceType]] = $device->device->type;

                    if ($device->isType('mobile', 'tablet') && empty($device->browser->name)) {

                        if ($device->os->name === "Android") {

                            $browser = $device->browser->name ? $device->browser->name : "Chrome";
                        } else if ($device->os->name === "iOS") {

                            $browser = $device->browser->name ? $device->browser->name : "Safari";
                        } else {

                            $browser = $device->browser->name ? $device->browser->name : "Chrome";
                        }
                    }
                    $params[$config[MailInOne][Mapping][DeviceBrowser]] = $device->browser->name;
                    $params[$config[MailInOne][Mapping][DeviceBrowserVersion]] = $device->browser->version->value;
                    $params[$config[MailInOne][Mapping][DeviceOs]] = $device->os->name;
                    $params[$config[MailInOne][Mapping][DeviceOsVersion]] = $device->os->version->value;
                }
            }

            // set Campaign cookie
            if(is_bool($config[Splittest][StikyVersionOnCookie]) && ($config[Splittest][StikyVersionOnCookie] == "True")) {
                $func->setCookieAsSplitVersion($params[$config[MailInOne][Mapping][url]]);
            }

            // Set Cookie
            if (!$test_mode) {
                $func->setCookieAsEmail($params[$config[MailInOne][Mapping][EMAIL]]);
            }

            // Keep URL Parameters
            $success_page = $config[GeneralSetting][SuccessPage];

            if ($conf->getConfigKeepURLParms() && !empty($params["url_params"])) {
                $success_page .= $params["url_params"];
            }

            // Update the Counter Value 
            // Counter does not modify in Test mode
            if ($counter_status && !$test_mode) {
                $new_counter_value = $current_count_value - 1;
                $resp = $counter->updateCounterValue($url, $new_counter_value);
            }

            // Assignment Manager
            if ($conf->getConfigAssignmentManagerStatus()) {

                $assignmentmanager = new assignmentManager();
                $vertriebsmitarbeiter = $assignmentmanager->getAssignmentManager(
                        $params[$config[MailInOne][Mapping][ZIP]], $config[AssignmentManager][Vertriebsmitarbeiter]
                );
                $vertriebsmitarbeiterid = $assignmentmanager->getAssignmentManagerId();
                $params[$config[MailInOne][Mapping][Vertriebsmitarbeiter]] = $vertriebsmitarbeiter;
                $params[$config[MailInOne][Mapping][Vertriebsmitarbeiterid]] = $vertriebsmitarbeiterid;
            }

            // Update LeadView
            if ($conf->getConfigUpdateLeadView()) {
                // Returns the lead reference after creating the Lead
                spl_autoload_register('Autoload::leadsLoader');
                $lead = new leads();
                $params[$config[MailInOne][Mapping][LeadReference]] = $lead->createLead($params);

                if ($params[$config[MailInOne][Mapping][LeadReference]] != 0) {
                    $lead_reference = '&lead_reference=' . $params[$config[MailInOne][Mapping][LeadReference]];
                    $success_page .= trim($lead_reference);
                }
            }

            //Hubspot Configuration Sync
            if ($conf->getConfigHubspotStatus()) {

                spl_autoload_register('Autoload::hubspotLoader');
                $hubspot = new hubspotApiClient();
                $params[$config[MailInOne][Mapping][HubspotContactID]] = $hubspot->createContact($params);
                $params[$config[MailInOne][Mapping][HubspotDealID]] = $hubspot->createDeal($params);
                $hubspot->associateDealWithContact($params);
            }

            // Forward Email
            if ($conf->getConfigForwardEmail()) {

                $mail = new forwardMail();
                $s = $mail->forwardEmail($params);
            }

            // Maileon Configuration Sync
            if ($conf->getConfigMailInOneStatus()) {

                spl_autoload_register('Autoload::maileonLoader');
                $maileon = new maileonApiClient();
                 $params[$config[MailInOne][Mapping][url]] = (strlen($params[$config[MailInOne][Mapping][url]]) > 250) ? substr($params[$config[MailInOne][Mapping][url]],  0 , 250) : $params[$config[MailInOne][Mapping][url]];
                $response = $maileon->createEvent($params);

                // Redirect the Page
                $func->redirectPage($success_page);
            }
        }
    }
}
?>


<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of countdown
 *
 * @author Pradeep
 */

class counter{
    var $dbObject;
    var $select;
    var $where;
    var $table;
    var $campaign;
    var $value;
    var $datatype;
    var $result;
    
    function __construct(){
        
        $this->table = "campaign_params";
        $this->dbObject = new db();
    }    

    /* Function Name : updateDatabase
     *
     * Parameters : 
     *
     * Returns : 
     * 
     */    
    public function updateDatabase(){

        $this->result = $return;
    }
    
    
    /* 
     * Function Name : updateCampaignParameter
     *
     * Parameters : $update : Update parameters
     *              $where  : Where Parameters
     *
     * Returns : status of the query.
     * 
     */        
    public function updateCampaignParameter($update, $where){
        $stmt = array();
        $d = $this->dbObject;
        foreach($where as $key => $value){
            array_push($stmt, $key." LIKE '".$value."'");
        }
        $stmt = implode(' AND ', $stmt);     

        $where = $stmt;
        $query_result= $d->execute($this->table, $update, $where, "update");
        $this->result = $query_result;        
    }

    
    /* 
     * Function Name : insertCampaignParameter
     *
     * Parameters : $insert : Insert Parameters
     *
     * Returns : status of the query.
     * 
     */         
    public function insertCampaignParameter( $insert){       
        
        $d = $this->dbObject;
        $query_result= $d->execute($this->table, $insert, " ", "insert");
        $this->result = $query_result;
       
        return $this->result;
    }


    /* 
     * Function Name : getCampaignParameter
     *
     * Parameters : 
     *
     * Returns : Returns the value and datatype for given URL.
     * 
     */     
    public function getCampaignParameter(){
        
        $return = "";
        $this->select = array("value", "datatype");
        #$this->campaign = "https://stimawell.campaign-in-one.de/fibo";
        $stmt = "campaign LIKE '".$this->campaign."' AND type LIKE '".$this->type ."'";
        $this->where = array($stmt);
        $d = $this->dbObject;
        $query_result= $d->getOne($this->select, $this->table, $this->where);

        if(array_key_exists($this->select[0], $query_result)){
            
            // Row exits in the table
            // send the value 
            if($query_result[datatype] === 'Integer'){
               
                $return = intval($query_result[value]);
            }else if($query_result[datatype] === 'Date'){
               
                $return = $query_result[value];
            }
        } else {
            
            // Row doesnot exits in the table
            // Insert a new row
            $return = false;
        }
       
        return $return;
    }
    
    public function deleteCampaignParameter(){
        
    }
    
    public function resetCampaignParameter(){
        
    }

    /* 
     * Function Name : updateCounterValue
     *
     * Parameters : URL : URL of the campaign.  
     *              $counter : Counter value to be updated
     *
     * Returns : status of the query.
     * 
     */        
    public function updateCounterValue($url, $counter) {
        if (empty($url) || ($counter < 0 )) {
            # Invalid URL or Counter
            return false;
        }
        $value = array(
            "value" => $counter
        );
        $where = array(
            "campaign" => $url,
            "type" => "AvailableItems"
        );
        $response = $this->updateCampaignParameter($value, $where);
        return $response;
    }
    
    
    /* 
     * Function Name : updateCounterEndDate
     *
     * Parameters : $camp_params : Campaign Parameters 
     *
     * Returns : Update counter expire date.
     * 
     */     
    public function updateCounterEndDate($camp_params){
        
        if(empty($camp_params)){
            
            return false;
        }

        $final_expire_date = date("Y-m-d H:i:s", strtotime($camp_params['expire']));
        // Latest Expire date by adding the max count down period to current date
        $latest_expire_date = date('Y-m-d H:i:s', strtotime(date("Y-m-d H:i:s"). ' + '.$camp_params['max_countdown_period'].' days'));
        // Consider the latest date amoung them.
        $expire_date = $final_expire_date > $latest_expire_date ? $latest_expire_date : $final_expire_date;        

        
        $value = [
            "value" => '"'.$expire_date.'"'
        ];
        
        $where = array(
            "campaign" => $camp_params['url'],
            "type" => "CounterEndDate"
        );        

        $response = $this->updateCampaignParameter($value, $where);
        return $response;        
    }
    
    /* 
     * Function Name : getCounterValue
     *
     * Parameters : $url : URL of the Campaign 
     *
     * Returns : Avilable number of the counter.
     * 
     */ 
    public function getCounterValue($url){
        
        if(empty($url)){
            
            return false;
        }
        $this->type = "AvailableItems";
        $this->campaign = $url;
        $value = $this->getCampaignParameter();
        return $value;
    }


    /* 
     * Function Name : getCounterEndDate
     *
     * Parameters : $url : URL of the Campaign 
     *
     * Returns : Date for the Campaign.
     * 
     */      
    public function getCounterEndDate($url){
        
        if(empty($url)){
            
            return false;
        }        
        $this->type = "CounterEndDate";
        $this->campaign = $url;
        $value = $this->getCampaignParameter();
        return $value;
    }
    
    /* 
     * Function Name : insertCounterValue
     *
     * Parameters : $camp_params : Counter parameters for the campaign
     *
     * Returns : status of the query.
     * 
     */          
    public function insertCounterValue($camp_params){
        
        if(empty($camp_params)){
            
            return false;
        }
        $this->type = "AvailableItems";
        $this->campaign = $camp_params['url'];
        $value = [
            "campaign"  => $this->campaign,
            "type"      => $this->type,
            "value"     => $camp_params['max_offers'] - $camp_params['sold_offers'],
            "datatype"  => "Integer"
        ];
       
        $return = $this->insertCampaignParameter($value);
        return $return;
    }
    
    /* 
     * Function Name : insertCounterEndDate
     *
     * Parameters : $camp_params : Counter parameters for the campaign
     *
     * Returns : status of the query.
     * 
     */       
    public function insertCounterEndDate($camp_params){
        
        if(empty($camp_params)){
            return false;
        }
        $this->type = "CounterEndDate";
        $this->campaign = $camp_params['url'];
        // Final Expire date
        $final_expire_date = date("Y-m-d H:i:s", strtotime($camp_params['expire']));
        // Latest Expire date by adding the max count down period to current date
        $latest_expire_date = date('Y-m-d H:i:s', strtotime(date("Y-m-d H:i:s"). ' + '.$camp_params['max_countdown_period'].' days'));
        // Consider the latest date amoung them.
        $expire_date = $final_expire_date > $latest_expire_date ? $latest_expire_date : $final_expire_date;

        $value = [
            "campaign"  => $this->campaign,
            "type"      => $this->type,
            "value"     => $expire_date,
            "datatype"  => "Date"
        ];        
        
        $return  = $this->insertCampaignParameter($value);
    }
    
   
}

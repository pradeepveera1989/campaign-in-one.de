<?php
/**
* Copyright 2013 HubSpot, Inc.
*
* Licensed under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.
* You may obtain a copy of the License at
*
*       http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
* either express or implied. See the License for the specific
* language governing permissions and limitations under the
* License.
*/
require_once('class.baseclient.php');

class HubSpot_Company extends HubSpot_BaseClient{
	//Client for HubSpot Contacts API

	    //Define required client variables
	protected $API_PATH = 'companies';
	protected $API_VERSION = 'v2';


    /**
    * Create a Contact
    *
    * @param params: array of properties and property values for new contact, email is required
    *
    * @return Response body with JSON object 
    * for created Contact from HTTP POST request
    *
    * @throws HubSpot_Exception
    **/
    public function create_company($params){
    	$endpoint = 'companies';
    	$properties = array();
    	foreach ($params as $key => $value) {
    		array_push($properties, array("name"=>$key,"value"=>$value));
    	}
    	$properties = json_encode(array("properties"=>$properties));
    	try{
    		return json_decode($this->execute_JSON_post_request($this->get_request_url($endpoint,null),$properties));
    	} catch (HubSpot_Exception $e) {
    		throw new HubSpot_Exception('Unable to create contact: ' . $e);
    	}
    }

    /**
    * Update a company
    *
    * @param params: array of properties and property values for company
    *
    * @return Response body from HTTP POST request
    *
    * @throws HubSpot_Exception
    **/
    public function update_company($vid, $params){
    	$endpoint = 'companies/'.$vid;
    	$properties = array();
    	foreach ($params as $key => $value) {
    		array_push($properties, array("name"=>$key,"value"=>$value));
    	}
    	$properties = json_encode(array("properties"=>$properties));
    	try{
			return json_decode($this->execute_JSON_post_request($this->get_request_url($endpoint,null),$properties));
    	} catch (HubSpot_Exception $e) {
    		throw new HubSpot_Exception('Unable to update contact: ' . $e);
    	}
    }

    /**
	* Get the company by domain
	*
	* @param vid: domain name of the company
	*
	* @return Response body from HTTP POST request
	*
	* @throws HubSpot_Exception
    *
    *  https://api.hubapi.com/companies/v2/domains/testcompany.de/companies?hapikey=d32204c1-536a-4140-b212-840c4ca8b1ad    
    *
    **/    
    public function get_company_by_domain($domain, $params){
        $endpoint ="domains/".$domain."/companies";
    	$params = json_encode($params);
        try{
    		return json_decode($this->execute_JSON_post_request($this->get_request_url($endpoint,null),$params));
    	}
    	catch (HubSpot_Exception $e) {
    		throw new HubSpot_Exception('Unable to get company: ' . $e);
    	}
    }
    

    /**
	* Associate Contact to Company
	*
	* @param contactId: Unique ID for the contact,
     *       companyId: Unique ID for the company.
	*
	* @return Response body from HTTP POST request
	*
	* @throws HubSpot_Exception
    * 
    * @URL https://api.hubapi.com/companies/v2/companies/$companyId/contacts/$contactId?hapikey=demo'
    **/
    public function assosiate_company_to_contact($contactId, $companyId){
    	$endpoint = 'companies/'.$companyId.'/contacts/'.$contactId;
    	try{
			return json_decode($this->execute_put_request($this->get_request_url($endpoint,null),null));
    	} catch (HubSpot_Exception $e) {
    		throw new HubSpot_Exception('Unable to update contact: ' . $e);
    	}
    }

}

?>
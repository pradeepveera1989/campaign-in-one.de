<?php
require 'config.php';

// Get the config data from config.ini       
$conf_obj = new config('config.ini');
$config = $conf_obj->getAllConfig();

$currentUrl = parse_url($_SERVER['REQUEST_URI']);

// Keep URL Parameters
$redirect_path = $config[GeneralSetting][KeepURLParameter] ? $config[GeneralSetting][Redirect][RedirectPath] . $currentUrl["query"] : $config[GeneralSetting][Redirect][RedirectPath];
// Redirect Time
$redirect_time = $config[GeneralSetting][Redirect][RedirectTime];

// Webgains
$webgains = $_GET['trafficsource'] === "webgains" ? "true" : "false";
$lead_reference = empty($_GET['lead_reference']) ? " " : $_GET['lead_reference'];
?>
<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">  
        <?php if ($config[GeneralSetting][Redirect][RedirectStatus]) { ?>  
            <meta http-equiv="refresh" content="<?php echo $redirect_time; ?>;url=<?php echo $redirect_path; ?>" />  
        <?php } ?>
        <title>Auto-Scholz AHG GmbH &amp; Co. KG | Volkswagen Zentrum Bamberg</title>
        <!-- implementation bootstrap -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- implementation fontawesome icons -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- implementation simpleline icons -->
        <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
        <!-- implementation googlefonts -->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
        <!-- implementation Animated Header -->
        <!-- implementation custom css -->
        <link href="css/creative.css" rel="stylesheet">
        <!-- implementation animate css -->
        <link href="css/animate.css" rel="stylesheet">

        <!-- Google Analytics -->
        <?php include_once 'clients/tracking/google/google.php'; ?>        

        <!-- Facebook Pixel -->
        <?php include_once('clients/tracking/facebook/facebook.php'); ?>       
    </head>

    <!-- Outbrain Tracking -->
    <?php $config[TrackingTools][EnableOutbrain] ? include_once'clients/tracking/outbrain/outbrain_redirect.php' : ' '; ?>

    <!--Adeblo Tracking-->
    <?php $config[TrackingTools][EnableAdeblo] ? include_once'clients/tracking/adeblo/adeblo.php' : ' ' ?>

    <!-- Remarketing Target360 Tracking -->
    <?php $config[TrackingTools][EnableTarget360] ? include_once 'clients/tracking/target360/target_redirect.php' : ' ' ?>;
    <body>
        <script type="text/javascript" src="vendor/jquery-cookie-master/src/jquery.cookie.js"></script>  
        <div class="container">


            <!-- LOGO HEADER -->
            <div class="grampp-header">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="grampp-logo">
                            <a href="https://www.volkswagen-zentrum-bamberg.de/de.html" target="_blank"><img style="width:220px; margin: 10px 0px;" src="img/logo-vw-bamberg.png" /></a>
                        </div>
                        <div style="float:right;" class="grampp-logo">
                            <a href="https://www.volkswagen-zentrum-bamberg.de/de.html" target="_blank"><img style="" src="img/logo-vw.jpg" /></a>
                        </div>

                        <!--
                        <div class="auto-logos">
                            <a href="https://www.mercedes-benz-grampp.de/content/deutschland/retail-plz9/autohaus-grampp/de/desktop/home.html"><img src="img/mercedes-benz.png" /></a>
                            <a href="https://www.grampp-lohr.audi/de.html" target="_blank"><img src="img/audi-logo-weiss.png" /></a>
                            <a href="https://www.volkswagen-grampp.de/p_35297.html" target="_blank"><img src="img/vw-logo.png" /></a>
                            <a href="https://www.clever-driver-lohr.de/" target="_blank"><img src="img/clever-driver-logo.png" /></a>
                        </div>
                        -->

                    </div>
                </div>
            </div>
            <!-- /.LOGO HEADER -->

            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>

            <h1>Bitte überprüfen Sie Ihr E-Mail Postfach.</h1>
            <hr>
            <p>Wir haben Ihnen eine E-Mail mit einem Bestätigungslink an Ihre genannte E-Mail-Adresse gesendet. Sollten Sie in den nächsten 15 Minuten keine Mail erhalten, schauen Sie bitte auch in Ihren Spam oder Junk Ordner.</p> 

            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>


        </div>
        <section class="grampp-footer">     
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <small>* Ergänzende Information zu Verbrauchsangaben: Alle angegebenen Werte wurden nach dem vorgeschriebenen Messverfahren (§ 2 Nr. 5, 6, 6 a Pkw-EnVKV in der jeweils geltenden Fassung) ermittelt. Die Angaben beziehen sich nicht auf ein einzelnes Fahrzeug und sind nicht Bestandteil des Angebots, sondern dienen allein Vergleichszwecken zwischen den verschiedenen Fahrzeugtypen. Weitere aktuelle Informationen zu den einzelnen Fahrzeugen erhalten Sie bei Ihrem Händler. Ermittlung des Verbrauchs auf Grundlage der Serienausstattung. Sonderausstattungen können Verbrauch und Fahrleistungen beeinflussen. Weitere Informationen zum offiziellen Kraftstoffverbrauch und den offiziellen spezifischen CO2-Emissionen neuer Personenkraftwagen können dem „Leitfaden über den Kraftstoffverbrauch, CO2-Emissionen und den Stromverbrauch neuer Personenkraftwagen“ entnommen werden, der an allen Verkaufsstellen und bei Deutschen Automobil Treuhand GmbH (www.dat.de) unentgeltlich erhältlich ist.</small><br><br>

                        <br><br>
                        <center>  
                            <p>©2019 Auto Scholz AHG GmbH & Co. KG - Alle Rechte vorbehalten.&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.volkswagen-zentrum-bamberg.de/de/rechtliches/datenschutz.html" target="_blank">Datenschutz</a> | <a href="https://www.volkswagen-zentrum-bamberg.de/de/rechtliches/impressum.html" target="_blank">Impressum</a></p> 
                        </center>  

                    </div>
                </div>
            </div>
        </section>

        <?php $config[TrackingTools][EnableWebGains] ? include_once 'clients/tracking/webgains/webgains_redirect.php' : ' '; ?>   
        <!-- </Webgains Tracking Code NG> -->
        <!-- </Webgains Tracking Code> --> 


        <!-- Bootstrap core JavaScript -->
        <script src="vendor/jquery/jquery.min.js">
        </script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js">
        </script>
        <script src="vendor/header-animation/demo-1.js">
        </script>  
        <script src="vendor/header-animation/TweenLite.min.js">
        </script>
        <script src="vendor/header-animation/EasePack.min.js">
        </script>
        <script src="vendor/header-animation/rAF.js">
        </script>
        <script src="vendor/header-animation/demo-1.js">
        </script>  

    </body>

</html>
